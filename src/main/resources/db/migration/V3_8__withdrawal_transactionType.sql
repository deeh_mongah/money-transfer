set foreign_key_checks = 0;
DROP TABLE IF EXISTS `transaction_types`;
CREATE TABLE `transaction_types` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `short_code` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

INSERT INTO `transaction_types` (`id`, `code`, `short_code`, `name`) VALUES ('1', 'withdrawal', 'W', 'Withdrawal');
INSERT INTO `transaction_types` (`id`, `code`, `short_code`, `name`) VALUES ('2', 'deposit', 'D', 'Deposit');
INSERT INTO `transaction_types` (`id`, `code`, `short_code`, `name`) VALUES ('3', 'push-float', 'BI', 'Push Float');
INSERT INTO `transaction_types` (`id`, `code`, `short_code`, `name`) VALUES ('4', 'pull-float', 'MS', 'Pull Float');
INSERT INTO `transaction_types` (`id`, `code`, `short_code`, `name`) VALUES ('5', 'transfer', 'T', 'Transfer');
set foreign_key_checks = 1;