ALTER TABLE `transactions`
ADD COLUMN `receiver_contact_no` VARCHAR(45) NULL ,
ADD COLUMN `sender_telephone_no` VARCHAR(45) NULL ,
ADD COLUMN `agent_present` VARCHAR(45) NULL ,
ADD COLUMN `mpesa_transaction_no` VARCHAR(45) NULL ,
ADD COLUMN `mpesa_no` VARCHAR(45) NULL ;