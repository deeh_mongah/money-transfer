set foreign_key_checks = 0;
DROP TABLE IF EXISTS `fees`;
CREATE TABLE `fees` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `MIN_AMOUNT` decimal(18,2) DEFAULT NULL,
  `MAX_AMOUNT` decimal(18,2) DEFAULT NULL,
  `AGENT_AMOUNT` decimal(18,2) DEFAULT NULL,
  `BINARY_AMOUNT` decimal(18,2) DEFAULT NULL,
  `FLEX_AMOUNT` decimal(18,2) DEFAULT NULL,
  `PAYMENT_MODE` varchar(100) DEFAULT NULL,
  `FEE_GROUP_NO` int(11) DEFAULT NULL,
  `FEE_ORIGIN_NO` int(11) DEFAULT NULL,
  `TRANSACTION_TYPE_NO` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `transactional_amount` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FEE_ORIGIN_NO` (`FEE_ORIGIN_NO`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `TRANSACTION_TYPE_NO` (`TRANSACTION_TYPE_NO`),
  KEY `FEE_GROUP_NO` (`FEE_GROUP_NO`),
  CONSTRAINT `fees_fk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`),
  CONSTRAINT `fees_fk_2` FOREIGN KEY (`TRANSACTION_TYPE_NO`) REFERENCES `transaction_types` (`ID`),
  CONSTRAINT `fees_fk_3` FOREIGN KEY (`FEE_ORIGIN_NO`) REFERENCES `fee_origin` (`ID`),
  CONSTRAINT `fees_fk_4` FOREIGN KEY (`FEE_GROUP_NO`) REFERENCES `fee_groups` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
set foreign_key_checks = 1;