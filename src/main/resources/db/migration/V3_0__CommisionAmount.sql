CREATE TABLE `commission_transaction` (
  `id` INT NOT NULL,
  `transaction_no` INT NULL,
  `agent_no` VARCHAR(45) NULL,
  `agent_amount` DECIMAL(16,2) NULL,
  `flex_amount` DECIMAL(16,2) NULL,
  `binary_amount` DECIMAL(16,2) NULL,
  `tariff_amount` DECIMAL(16,2) NULL,

  PRIMARY KEY (`id`));