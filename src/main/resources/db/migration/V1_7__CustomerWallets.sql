DROP TABLE IF EXISTS `customer_wallets`;
CREATE TABLE `customer_wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_no` int(11) NOT NULL,
  `wallet_no` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_no` (`customer_no`),
  KEY `wallet_no` (`wallet_no`),
  CONSTRAINT `customer_wallets_fk_1` FOREIGN KEY (`customer_no`) REFERENCES `agents` (`ID`),
  CONSTRAINT `customer_wallets_fk_2` FOREIGN KEY (`wallet_no`) REFERENCES `wallets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;