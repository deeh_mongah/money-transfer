ALTER TABLE `commission_transaction`
ADD COLUMN `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP;