-- MySQL dump 10.13  Distrib 8.0.17, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: flex_demo_agency
-- ------------------------------------------------------
-- Server version	5.7.27

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `agent_accounts`
--

DROP TABLE IF EXISTS `agent_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agent_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_no` varchar(45) DEFAULT NULL,
  `balance` decimal(18,2) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `business_id` varchar(200) DEFAULT NULL,
  `commission_account` varchar(45) DEFAULT NULL,
  `branch_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agent_accounts`
--

LOCK TABLES `agent_accounts` WRITE;
/*!40000 ALTER TABLE `agent_accounts` DISABLE KEYS */;
INSERT INTO `agent_accounts` VALUES (1,'1234567',10000.00,'Wakala A','wakalaa@gmail.com','711158196','234452','556632',1),(2,'1234568',10000.00,'Wakala B','wakalab@gmail.com','711158196','123421','112245',2),(3,'1234569',1234567.00,'Wakala C','wakalac@gmail.com','711158196','124521','667724',3);
/*!40000 ALTER TABLE `agent_accounts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `agents`
--

DROP TABLE IF EXISTS `agents`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `agents` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `business_id` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `float_account` varchar(200) DEFAULT NULL,
  `commission_account` varchar(200) DEFAULT NULL,
  `bank_no` int(11) DEFAULT NULL,
  `flag` varchar(20) DEFAULT '0',
  `edit_data` longtext,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `reason_code_no` int(11) DEFAULT NULL,
  `reason_description` varchar(200) DEFAULT NULL,
  `branches_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agents_fk_1` (`bank_no`),
  CONSTRAINT `agents_fk_1` FOREIGN KEY (`bank_no`) REFERENCES `banks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `agents`
--

LOCK TABLES `agents` WRITE;
/*!40000 ALTER TABLE `agents` DISABLE KEYS */;
/*!40000 ALTER TABLE `agents` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_roles`
--

DROP TABLE IF EXISTS `app_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `APP_CODE` varchar(100) DEFAULT NULL,
  `CREATED_AT` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_AT` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `APP_FUNCTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_roles`
--

LOCK TABLES `app_roles` WRITE;
/*!40000 ALTER TABLE `app_roles` DISABLE KEYS */;
INSERT INTO `app_roles` VALUES (1,'User Groups','ROLE_USER_GROUP','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(2,'Users','ROLE_USERS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(3,'Branches','ROLE_BRANCHES','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(4,'Outlets','ROLE_OUTLETS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(5,'Fee Groups','ROLE_FEE_GROUPS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(6,'Fees','ROLE_FEES','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(7,'Devices','ROLE_DEVICES','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(8,'Officers','ROLE_OFFICERS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(9,'Device Allocation','ROLE_DEVICE_ALLOCATION','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(10,'Maker-Checker','ROLE_MAKER_CHECKER','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(11,'Reason Codes','ROLE_REASON_CODES','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(12,'System Settings','ROLE_SETTINGS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(13,'Agents','ROLE_AGENTS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(14,'SACCOs','ROLE_BANKS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin'),(15,'Audit Logs','ROLE_AUDIT_LOGS','2019-11-06 11:17:43','2019-11-06 11:17:43','superadmin');
/*!40000 ALTER TABLE `app_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `app_settings`
--

DROP TABLE IF EXISTS `app_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `app_settings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(150) DEFAULT NULL,
  `CODE` varchar(150) DEFAULT NULL,
  `FLAG` varchar(100) DEFAULT NULL,
  `VALUE` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `app_settings`
--

LOCK TABLES `app_settings` WRITE;
/*!40000 ALTER TABLE `app_settings` DISABLE KEYS */;
INSERT INTO `app_settings` VALUES (1,'Login Trials','LOGIN_TRIALS','settings','5','INT of allowed login attempts'),(2,'Lock out period','LOCK_OUT_PERIOD','settings','30','Period in minutes in which a locked account will be inaccessible'),(3,'Reset code expiry period','RESET_EXPIRY','settings','2','Period in hours in which a reset password token is valid'),(4,'Password expiry window','PASSWORD_EXPIRY_WINDOW','settings','90','Password expiry window in days');
/*!40000 ALTER TABLE `app_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `audit_trail`
--

DROP TABLE IF EXISTS `audit_trail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `audit_trail` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ACTIVITY` longtext,
  `STATUS` varchar(45) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `OLD_VALUES` longtext,
  `NEW_VALUES` longtext,
  `LOG_TYPE` varchar(100) DEFAULT NULL,
  `USER_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audit_trail`
--

LOCK TABLES `audit_trail` WRITE;
/*!40000 ALTER TABLE `audit_trail` DISABLE KEYS */;
/*!40000 ALTER TABLE `audit_trail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_user_groups`
--

DROP TABLE IF EXISTS `bank_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bank_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_no` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bank_no` (`bank_no`),
  CONSTRAINT `bank_user_groups_fk_1` FOREIGN KEY (`id`) REFERENCES `user_groups` (`ID`),
  CONSTRAINT `bank_user_groups_fk_2` FOREIGN KEY (`bank_no`) REFERENCES `banks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_user_groups`
--

LOCK TABLES `bank_user_groups` WRITE;
/*!40000 ALTER TABLE `bank_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank_users`
--

DROP TABLE IF EXISTS `bank_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bank_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_no` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bank_no` (`bank_no`),
  CONSTRAINT `bank_users_fk_1` FOREIGN KEY (`id`) REFERENCES `users` (`ID`),
  CONSTRAINT `bank_users_fk_2` FOREIGN KEY (`bank_no`) REFERENCES `banks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank_users`
--

LOCK TABLES `bank_users` WRITE;
/*!40000 ALTER TABLE `bank_users` DISABLE KEYS */;
/*!40000 ALTER TABLE `bank_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banks`
--

DROP TABLE IF EXISTS `banks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `bank_code` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `flag` varchar(20) DEFAULT '0',
  `edit_data` longtext,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `deleted_on` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `reason_code_no` int(11) DEFAULT NULL,
  `reason_description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banks`
--

LOCK TABLES `banks` WRITE;
/*!40000 ALTER TABLE `banks` DISABLE KEYS */;
/*!40000 ALTER TABLE `banks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bill_requests`
--

DROP TABLE IF EXISTS `bill_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bill_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bill_type` varchar(100) NOT NULL,
  `json_request` longtext,
  `json_response` longtext,
  `member_no` varchar(100) DEFAULT NULL,
  `stk_reference_no` varchar(100) DEFAULT NULL,
  `flag` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bill_requests`
--

LOCK TABLES `bill_requests` WRITE;
/*!40000 ALTER TABLE `bill_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `bill_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `branches` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `BRANCH_CODE` varchar(200) DEFAULT NULL,
  `LOCATION` varchar(200) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `bank_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `branches_fk_2` (`bank_no`),
  CONSTRAINT `branches_fk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`),
  CONSTRAINT `branches_fk_2` FOREIGN KEY (`bank_no`) REFERENCES `banks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `branches`
--

LOCK TABLES `branches` WRITE;
/*!40000 ALTER TABLE `branches` DISABLE KEYS */;
/*!40000 ALTER TABLE `branches` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `channels`
--

DROP TABLE IF EXISTS `channels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `channels` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `channels`
--

LOCK TABLES `channels` WRITE;
/*!40000 ALTER TABLE `channels` DISABLE KEYS */;
INSERT INTO `channels` VALUES (1,'Agency Banking','agency-banking'),(2,'Mobile Banking','mobile-banking');
/*!40000 ALTER TABLE `channels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cheque_book_applications`
--

DROP TABLE IF EXISTS `cheque_book_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cheque_book_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cheque_book_type_no` int(11) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `collection_branch` varchar(100) DEFAULT NULL,
  `flag` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `cheque_book_type_no` (`cheque_book_type_no`),
  CONSTRAINT `cheque_book_ibfk_1` FOREIGN KEY (`cheque_book_type_no`) REFERENCES `cheque_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cheque_book_applications`
--

LOCK TABLES `cheque_book_applications` WRITE;
/*!40000 ALTER TABLE `cheque_book_applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `cheque_book_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cheque_type`
--

DROP TABLE IF EXISTS `cheque_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cheque_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `number_of_leafs` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` varchar(150) DEFAULT NULL,
  `UPDATED_BY` varchar(150) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `REASON_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cheque_type`
--

LOCK TABLES `cheque_type` WRITE;
/*!40000 ALTER TABLE `cheque_type` DISABLE KEYS */;
/*!40000 ALTER TABLE `cheque_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_groups`
--

DROP TABLE IF EXISTS `fee_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `bank_no` int(11) DEFAULT NULL,
  `outlet_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `fee_groups_fk_2` (`bank_no`),
  CONSTRAINT `fee_groups_fk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`),
  CONSTRAINT `fee_groups_fk_2` FOREIGN KEY (`bank_no`) REFERENCES `banks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_groups`
--

LOCK TABLES `fee_groups` WRITE;
/*!40000 ALTER TABLE `fee_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `fee_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fee_origin`
--

DROP TABLE IF EXISTS `fee_origin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fee_origin` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `CODE` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fee_origin`
--

LOCK TABLES `fee_origin` WRITE;
/*!40000 ALTER TABLE `fee_origin` DISABLE KEYS */;
INSERT INTO `fee_origin` VALUES (1,'INSTITUTE','institute','2019-11-06 11:17:42','2019-11-06 11:17:42'),(2,'Customer','customer','2019-11-06 11:17:42','2019-11-06 11:17:42');
/*!40000 ALTER TABLE `fee_origin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fees`
--

DROP TABLE IF EXISTS `fees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `fees` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `MIN_AMOUNT` decimal(18,2) DEFAULT NULL,
  `MAX_AMOUNT` decimal(18,2) DEFAULT NULL,
  `OUTLET_AMOUNT` decimal(18,2) DEFAULT NULL,
  `SACCO_AMOUNT` decimal(18,2) DEFAULT NULL,
  `OUTLET_RATIO` decimal(18,2) DEFAULT NULL,
  `SACCO_RATIO` decimal(18,2) DEFAULT NULL,
  `PAYMENT_MODE` varchar(100) DEFAULT NULL,
  `FEE_GROUP_NO` int(11) DEFAULT NULL,
  `FEE_ORIGIN_NO` int(11) DEFAULT NULL,
  `TRANSACTION_TYPE_NO` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `channel_no` int(11) DEFAULT NULL,
  `binary_amount` decimal(18,2) DEFAULT NULL,
  `binary_ratio` decimal(18,2) DEFAULT NULL,
  `transactiona_mount` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FEE_ORIGIN_NO` (`FEE_ORIGIN_NO`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `TRANSACTION_TYPE_NO` (`TRANSACTION_TYPE_NO`),
  KEY `FEE_GROUP_NO` (`FEE_GROUP_NO`),
  KEY `fees_fk_5` (`channel_no`),
  CONSTRAINT `fees_fk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`),
  CONSTRAINT `fees_fk_2` FOREIGN KEY (`TRANSACTION_TYPE_NO`) REFERENCES `transaction_types` (`ID`),
  CONSTRAINT `fees_fk_3` FOREIGN KEY (`FEE_ORIGIN_NO`) REFERENCES `fee_origin` (`ID`),
  CONSTRAINT `fees_fk_4` FOREIGN KEY (`FEE_GROUP_NO`) REFERENCES `fee_groups` (`ID`),
  CONSTRAINT `fees_fk_5` FOREIGN KEY (`channel_no`) REFERENCES `channels` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fees`
--

LOCK TABLES `fees` WRITE;
/*!40000 ALTER TABLE `fees` DISABLE KEYS */;
/*!40000 ALTER TABLE `fees` ENABLE KEYS */;
UNLOCK TABLES;



--
-- Table structure for table `group_permissions`
--

DROP TABLE IF EXISTS `group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `group_permissions` (
  `PERMISSION_ID` int(11) NOT NULL,
  `USER_GROUP_ID` int(11) NOT NULL,
  PRIMARY KEY (`PERMISSION_ID`,`USER_GROUP_ID`),
  KEY `group_permissions_ibfk_2` (`USER_GROUP_ID`),
  CONSTRAINT `group_permissions_ibfk_1` FOREIGN KEY (`PERMISSION_ID`) REFERENCES `permissions` (`ID`),
  CONSTRAINT `group_permissions_ibfk_2` FOREIGN KEY (`USER_GROUP_ID`) REFERENCES `user_groups` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `group_permissions`
--

LOCK TABLES `group_permissions` WRITE;
/*!40000 ALTER TABLE `group_permissions` DISABLE KEYS */;
INSERT INTO `group_permissions` VALUES (1,1),(2,1),(3,1),(4,1),(5,1),(6,1),(7,1),(8,1),(9,1),(10,1),(11,1),(12,1),(13,1),(14,1),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(21,1),(22,1),(23,1),(24,1),(25,1),(26,1),(27,1),(28,1),(29,1),(30,1),(31,1),(32,1),(33,1),(34,1),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(42,1),(43,1),(44,1),(45,1),(46,1),(47,1),(48,1),(49,1),(50,1),(51,1),(52,1),(53,1),(54,1),(55,1),(56,1),(57,1),(58,1),(59,1),(60,1),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(67,1),(68,1),(69,1),(70,1),(71,1),(72,1),(73,1),(74,1),(75,1),(76,1),(77,1),(78,1),(79,1),(80,1),(81,1),(82,1),(83,1),(84,1),(85,1),(86,1),(87,1),(88,1),(89,1),(90,1),(91,1),(92,1),(93,1),(94,1),(95,1),(96,1),(97,1),(98,1),(99,1),(100,1),(101,1),(102,1),(103,1),(104,1),(105,1),(106,1),(107,1),(108,1),(109,1),(110,1),(111,1),(112,1),(113,1),(114,1),(115,1),(116,1),(117,1),(118,1),(119,1),(120,1),(121,1),(122,1),(123,1),(124,1),(125,1),(126,1),(127,1),(128,1),(129,1),(130,1),(131,1),(132,1),(133,1),(134,1),(135,1),(136,1),(137,1);
/*!40000 ALTER TABLE `group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `journal_entries`
--

DROP TABLE IF EXISTS `journal_entries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `journal_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry_type` int(11) NOT NULL,
  `wallet_no` int(11) NOT NULL,
  `transaction_no` int(11) NOT NULL,
  `running_balance` decimal(18,2) DEFAULT '0.00',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `entry_type` (`entry_type`),
  KEY `wallet_no` (`wallet_no`),
  KEY `transaction_no` (`transaction_no`),
  CONSTRAINT `journal_entries_fk_1` FOREIGN KEY (`entry_type`) REFERENCES `journal_entry_types` (`id`),
  CONSTRAINT `journal_entries_fk_2` FOREIGN KEY (`wallet_no`) REFERENCES `wallets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=129 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `journal_entries`
--

LOCK TABLES `journal_entries` WRITE;
/*!40000 ALTER TABLE `journal_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `journal_entries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `journal_entry_types`
--

DROP TABLE IF EXISTS `journal_entry_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `journal_entry_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `journal_entry_types`
--

LOCK TABLES `journal_entry_types` WRITE;
/*!40000 ALTER TABLE `journal_entry_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `journal_entry_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lanster_session_key`
--

DROP TABLE IF EXISTS `lanster_session_key`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `lanster_session_key` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `session_key` text,
  `updated_on` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lanster_session_key`
--

LOCK TABLES `lanster_session_key` WRITE;
/*!40000 ALTER TABLE `lanster_session_key` DISABLE KEYS */;
/*!40000 ALTER TABLE `lanster_session_key` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_applications`
--

DROP TABLE IF EXISTS `loan_applications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loan_type_no` int(11) DEFAULT NULL,
  `repayment_period` int(11) NOT NULL,
  `amount` decimal(18,2) DEFAULT '0.00',
  `channel_no` int(11) DEFAULT NULL,
  `first_name` varchar(100) DEFAULT NULL,
  `middle_name` varchar(100) DEFAULT NULL,
  `surname` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `nrc` varchar(100) DEFAULT NULL,
  `customer_ref` varchar(100) DEFAULT NULL,
  `mobile` varchar(20) DEFAULT NULL,
  `flag` varchar(50) DEFAULT 'Pending',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `transaction_code` varchar(100) DEFAULT NULL,
  `customer_id` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_applications`
--

LOCK TABLES `loan_applications` WRITE;
/*!40000 ALTER TABLE `loan_applications` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_applications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_types`
--

DROP TABLE IF EXISTS `loan_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loan_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_types`
--

LOCK TABLES `loan_types` WRITE;
/*!40000 ALTER TABLE `loan_types` DISABLE KEYS */;
INSERT INTO `loan_types` VALUES (1,'SALARY LOAN','salary-loan','2019-04-04 16:09:48','2019-04-04 16:09:48'),(2,'FRIENDLY LOAN','friendly-loan','2019-04-04 16:09:48','2019-04-04 16:09:48'),(3,'BUILDING LOAN','building-loan','2019-04-04 16:09:48','2019-04-04 16:09:48'),(4,'PSMD','psmd-loan','2019-04-04 16:09:48','2019-04-04 16:09:48');
/*!40000 ALTER TABLE `loan_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loans`
--

DROP TABLE IF EXISTS `loans`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loans` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `max_tenure` decimal(18,2) DEFAULT '0.00',
  `max_amount` decimal(18,2) DEFAULT '0.00',
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` varchar(150) DEFAULT NULL,
  `UPDATED_BY` varchar(150) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `EDIT_DATA` longtext,
  `REASON_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `REASON_NO` (`REASON_NO`),
  CONSTRAINT `loans_ibfk_1` FOREIGN KEY (`REASON_NO`) REFERENCES `reason_codes` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loans`
--

LOCK TABLES `loans` WRITE;
/*!40000 ALTER TABLE `loans` DISABLE KEYS */;
/*!40000 ALTER TABLE `loans` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `maker_checker`
--

DROP TABLE IF EXISTS `maker_checker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `maker_checker` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `module` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `enabled` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `maker_checker`
--

LOCK TABLES `maker_checker` WRITE;
/*!40000 ALTER TABLE `maker_checker` DISABLE KEYS */;
INSERT INTO `maker_checker` VALUES (1,'Users','Users',_binary ''),(2,'User Groups','UserGroups',_binary ''),(3,'Agents','Agents',_binary ''),(4,'Fee Groups','FeeGroups',_binary ''),(5,'Fees','Fees',_binary ''),(6,'Outlets','Outlets',_binary ''),(7,'Banks','Banks',_binary ''),(8,'Branches','Branches',_binary ''),(9,'Devices','Terminals',_binary ''),(10,'Terminal Officers','TerminalOfficers',_binary '');
/*!40000 ALTER TABLE `maker_checker` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `merchant_transactions`
--

DROP TABLE IF EXISTS `merchant_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `merchant_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminal_no` int(11) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `amount` decimal(16,2) DEFAULT NULL,
  `transaction_reference` varchar(100) DEFAULT NULL,
  `transaction_type` int(11) DEFAULT NULL,
  `flag` varchar(45) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `officer_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `merchant_transactions`
--

LOCK TABLES `merchant_transactions` WRITE;
/*!40000 ALTER TABLE `merchant_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `merchant_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mpesa_raw_requests`
--

DROP TABLE IF EXISTS `mpesa_raw_requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mpesa_raw_requests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference_number` varchar(100) DEFAULT NULL,
  `start_time` date DEFAULT NULL,
  `end_time` date DEFAULT NULL,
  `initial_response` varchar(100) DEFAULT NULL,
  `final_response` varchar(100) DEFAULT NULL,
  `transaction_category` varchar(100) DEFAULT NULL,
  `client_request` longtext,
  `processing_flag` varchar(100) DEFAULT '0',
  `user_no` int(11) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  `transaction_no` varchar(100) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpesa_raw_requests`
--

LOCK TABLES `mpesa_raw_requests` WRITE;
/*!40000 ALTER TABLE `mpesa_raw_requests` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpesa_raw_requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mpesa_transactions`
--

DROP TABLE IF EXISTS `mpesa_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mpesa_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `transaction_type` varchar(400) DEFAULT NULL,
  `transaction_id` varchar(200) DEFAULT NULL,
  `mpesa_reference` varchar(200) DEFAULT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `client_no` int(11) DEFAULT NULL,
  `account_number` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT '0.00',
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mpesa_transactions`
--

LOCK TABLES `mpesa_transactions` WRITE;
/*!40000 ALTER TABLE `mpesa_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `mpesa_transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client_no` int(11) DEFAULT NULL,
  `message` longtext,
  `read` bit(1) NOT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `officer_types`
--

DROP TABLE IF EXISTS `officer_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `officer_types` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `CODE` varchar(100) DEFAULT NULL,
  `PROPERTY` varchar(20) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `officer_types`
--

LOCK TABLES `officer_types` WRITE;
/*!40000 ALTER TABLE `officer_types` DISABLE KEYS */;
INSERT INTO `officer_types` VALUES (1,'Supervisor','supervisor',NULL,'2019-11-06 11:17:42','2019-11-06 11:17:42'),(2,'Normal Officer','normal-officer',NULL,'2019-11-06 11:17:42','2019-11-06 11:17:42');
/*!40000 ALTER TABLE `officer_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outlets`
--

DROP TABLE IF EXISTS `outlets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `outlets` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(200) DEFAULT NULL,
  `LOCATION` varchar(200) DEFAULT NULL,
  `MOBILE_NO` varchar(20) DEFAULT NULL,
  `BRANCH_NO` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `float_account` varchar(100) DEFAULT NULL,
  `commission_account` varchar(100) DEFAULT NULL,
  `agent_no` int(11) DEFAULT NULL,
  `amount` decimal(16,2) DEFAULT NULL,
  `fee_group_no` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `BRANCH_NO` (`BRANCH_NO`),
  KEY `outlets_fk_3` (`agent_no`),
  CONSTRAINT `outlets_fk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`),
  CONSTRAINT `outlets_fk_2` FOREIGN KEY (`BRANCH_NO`) REFERENCES `branches` (`ID`),
  CONSTRAINT `outlets_fk_3` FOREIGN KEY (`agent_no`) REFERENCES `agents` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outlets`
--

LOCK TABLES `outlets` WRITE;
/*!40000 ALTER TABLE `outlets` DISABLE KEYS */;
/*!40000 ALTER TABLE `outlets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `outlets_wallets`
--

DROP TABLE IF EXISTS `outlets_wallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `outlets_wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `outlet_no` int(11) NOT NULL,
  `wallet_no` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `outlet_no` (`outlet_no`),
  KEY `wallet_no` (`wallet_no`),
  CONSTRAINT `outlets_wallets_fk_1` FOREIGN KEY (`outlet_no`) REFERENCES `outlets` (`ID`),
  CONSTRAINT `outlets_wallets_fk_2` FOREIGN KEY (`wallet_no`) REFERENCES `wallets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `outlets_wallets`
--

LOCK TABLES `outlets_wallets` WRITE;
/*!40000 ALTER TABLE `outlets_wallets` DISABLE KEYS */;
/*!40000 ALTER TABLE `outlets_wallets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `permissions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `APP_CODE` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` varchar(150) DEFAULT 'System',
  `UPDATED_BY` varchar(150) DEFAULT 'System',
  `ROLE_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `ROLE_NO` (`ROLE_NO`),
  CONSTRAINT `permissions_ibfk_1` FOREIGN KEY (`ROLE_NO`) REFERENCES `app_roles` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'View User Groups','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(2,'View Active User Groups','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(3,'View New User Groups','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(4,'View Edited User Groups','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(5,'View Deactivated User Groups','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(6,'View Inactive User Groups','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(7,'Create User Groups','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(8,'Edit User Groups','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(9,'Approve New User Groups','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(10,'Decline New User Groups','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(11,'Approve Edited User Groups','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(12,'Decline Edited User Groups','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(13,'Approve Deactivated User Groups','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(14,'Decline Deactivated User Groups','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(15,'Delete User Groups','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(16,'Activate User Groups','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',1),(17,'View Users','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(18,'View Active Users','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(19,'View New Users','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(20,'View Edited Users','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(21,'View Deactivated Users','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(22,'View Inactive Users','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(23,'Create Users','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(24,'Edit Users','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(25,'Approve New Users','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(26,'Decline New Users','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(27,'Approve Edited Users','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(28,'Decline Edited Users','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(29,'Approve Deactivated Users','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(30,'Decline Deactivated Users','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(31,'Delete Users','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(32,'Activate Users','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',2),(33,'View Branches','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(34,'View Active Branches','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(35,'View New Branches','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(36,'View Edited Branches','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(37,'View Deactivated Branches','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(38,'View Inactive Branches','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(39,'Create Branches','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(40,'Edit Branches','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(41,'Approve New Branches','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(42,'Decline New Branches','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(43,'Approve Edited Branches','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(44,'Decline Edited Branches','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(45,'Approve Deactivated Branches','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(46,'Decline Deactivated Branches','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(47,'Delete Branches','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(48,'Activate Branches','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',3),(49,'View Outlets','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(50,'View Active Outlets','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(51,'View New Outlets','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(52,'View Edited Outlets','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(53,'View Deactivated Outlets','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(54,'View Inactive Outlets','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(55,'Create Outlets','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(56,'Edit Outlets','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(57,'Approve New Outlets','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(58,'Decline New Outlets','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(59,'Approve Edited Outlets','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(60,'Decline Edited Outlets','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(61,'Approve Deactivated Outlets','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(62,'Decline Deactivated Outlets','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(63,'Delete Outlets','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(64,'Activate Outlets','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',4),(65,'View Fee Groups','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(66,'View Active Fee Groups','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(67,'View New Fee Groups','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(68,'View Edited Fee Groups','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(69,'View Deactivated Fee Groups','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(70,'View Inactive Fee Groups','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(71,'Create Fee Groups','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(72,'Edit Fee Groups','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(73,'Approve New Fee Groups','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(74,'Decline New Fee Groups','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(75,'Approve Edited Fee Groups','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(76,'Decline Edited Fee Groups','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(77,'Approve Deactivated Fee Groups','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(78,'Decline Deactivated Fee Groups','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(79,'Delete Fee Groups','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(80,'Activate Fee Groups','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',5),(81,'View Fees','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(82,'View Active Fees','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(83,'View New Fees','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(84,'View Edited Fees','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(85,'View Deactivated Fees','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(86,'View Inactive Fees','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(87,'Create Fees','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(88,'Edit Fees','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(89,'Approve New Fees','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(90,'Decline New Fees','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(91,'Approve Edited Fees','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(92,'Decline Edited Fees','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(93,'Approve Deactivated Fees','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(94,'Decline Deactivated Fees','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(95,'Delete Fees','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(96,'Activate Fees','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',6),(97,'View Devices','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(98,'View Active Devices','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(99,'View New Devices','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(100,'View Edited Devices','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(101,'View Deactivated Devices','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(102,'View Inactive Devices','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(103,'Create Devices','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(104,'Edit Devices','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(105,'Approve New Devices','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(106,'Decline New Devices','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(107,'Approve Edited Devices','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(108,'Decline Edited Devices','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(109,'Approve Deactivated Devices','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(110,'Decline Deactivated Devices','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(111,'Delete Devices','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(112,'Activate Devices','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',7),(113,'View Officers','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(114,'View Active Officers','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(115,'View New Officers','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(116,'View Edited Officers','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(117,'View Deactivated Officers','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(118,'View Inactive Officers','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(119,'Create Officers','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(120,'Edit Officers','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(121,'Approve New Officers','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(122,'Decline New Officers','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(123,'Approve Edited Officers','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(124,'Decline Edited Officers','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(125,'Approve Deactivated Officers','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(126,'Decline Deactivated Officers','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(127,'Delete Officers','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(128,'Activate Officers','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',8),(129,'View Device Allocations','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',9),(130,'Allocate Devices','allocate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',9),(131,'De-allocate Devices','de-allocate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',9),(132,'View Maker-Checker Configurations','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',10),(133,'Edit Maker-Checker Configuration','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',10),(134,'View Reason Codes','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',11),(135,'Create Reason Codes','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',11),(136,'Edit Reason Codes','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',11),(137,'Delete Reason Codes','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',11),(138,'View System Settings','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',12),(139,'Edit System Settings','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',12),(140,'View Agents','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(141,'View Active Agents','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(142,'View New Agents','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(143,'View Edited Agents','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(144,'View Deactivated Agents','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(145,'View Inactive Agents','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(146,'Create Agents','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(147,'Edit Agents','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(148,'Approve New Agents','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(149,'Decline New Agents','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(150,'Approve Edited Agents','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(151,'Decline Edited Agents','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(152,'Approve Deactivated Agents','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(153,'Decline Deactivated Agents','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(154,'Delete Agents','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(155,'Activate Agents','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',13),(156,'View SACCOs','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(157,'View Active SACCOs','view-active','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(158,'View New SACCOs','view-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(159,'View Edited SACCOs','view-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(160,'View Deactivated SACCOs','view-deactivated','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(161,'View Inactive SACCOs','view-inactive','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(162,'Create SACCOs','new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(163,'Edit SACCOs','edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(164,'Approve New SACCOs','approve-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(165,'Decline New SACCOs','decline-new','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(166,'Approve Edited SACCOs','approve-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(167,'Decline Edited SACCOs','decline-edit','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(168,'Approve Deactivated SACCOs','approve-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(170,'Decline Deactivated SACCOs','decline-deactivation','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(171,'Delete SACCOs','delete','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(172,'Activate SACCOs','activate','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',14),(173,'View Audit Logs','default','2019-11-06 11:17:43','2019-11-06 11:17:43','System','System',15);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reason_codes`
--

DROP TABLE IF EXISTS `reason_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reason_codes` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(200) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '1',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reason_codes`
--

LOCK TABLES `reason_codes` WRITE;
/*!40000 ALTER TABLE `reason_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `reason_codes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `standing_order_frequency`
--

DROP TABLE IF EXISTS `standing_order_frequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `standing_order_frequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `CREATED_BY` varchar(150) DEFAULT NULL,
  `UPDATED_BY` varchar(150) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `REASON_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `standing_order_frequency`
--

LOCK TABLES `standing_order_frequency` WRITE;
/*!40000 ALTER TABLE `standing_order_frequency` DISABLE KEYS */;
/*!40000 ALTER TABLE `standing_order_frequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terminal_allocation`
--

DROP TABLE IF EXISTS `terminal_allocation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `terminal_allocation` (
  `id` int(11) NOT NULL,
  `outlet_no` int(11) NOT NULL,
  `created_on` timestamp NULL DEFAULT NULL,
  `updated_on` timestamp NULL DEFAULT NULL,
  `deleted_on` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `flag` varchar(45) DEFAULT NULL,
  `allocate` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`,`outlet_no`),
  KEY `terminal_allocation_ibfk_2` (`outlet_no`),
  CONSTRAINT `terminal_allocation_ibfk_1` FOREIGN KEY (`id`) REFERENCES `terminals` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminal_allocation`
--

LOCK TABLES `terminal_allocation` WRITE;
/*!40000 ALTER TABLE `terminal_allocation` DISABLE KEYS */;
/*!40000 ALTER TABLE `terminal_allocation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terminal_officers`
--

DROP TABLE IF EXISTS `terminal_officers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `terminal_officers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `LAST_NAME` varchar(100) DEFAULT NULL,
  `PASSPORT` varchar(100) DEFAULT NULL,
  `PIN` varchar(50) DEFAULT NULL,
  `TERMINAL_NO` int(11) DEFAULT NULL,
  `OFFICER_TYPE_NO` int(11) DEFAULT NULL,
  `USER_CODE` varchar(100) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  PRIMARY KEY (`ID`),
  KEY `TERMINAL_NO` (`TERMINAL_NO`),
  KEY `OFFICER_TYPE_NO` (`OFFICER_TYPE_NO`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  CONSTRAINT `terminal_officers_fk_1` FOREIGN KEY (`TERMINAL_NO`) REFERENCES `terminals` (`ID`),
  CONSTRAINT `terminal_officers_fk_2` FOREIGN KEY (`OFFICER_TYPE_NO`) REFERENCES `officer_types` (`ID`),
  CONSTRAINT `terminal_officers_fk_3` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminal_officers`
--

LOCK TABLES `terminal_officers` WRITE;
/*!40000 ALTER TABLE `terminal_officers` DISABLE KEYS */;
/*!40000 ALTER TABLE `terminal_officers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terminal_wallets`
--

DROP TABLE IF EXISTS `terminal_wallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `terminal_wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `terminal_no` int(11) NOT NULL,
  `wallet_no` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `terminal_no` (`terminal_no`),
  KEY `wallet_no` (`wallet_no`),
  CONSTRAINT `terminals_wallets_fk_1` FOREIGN KEY (`terminal_no`) REFERENCES `terminals` (`ID`),
  CONSTRAINT `terminals_wallets_fk_2` FOREIGN KEY (`wallet_no`) REFERENCES `wallets` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminal_wallets`
--

LOCK TABLES `terminal_wallets` WRITE;
/*!40000 ALTER TABLE `terminal_wallets` DISABLE KEYS */;
/*!40000 ALTER TABLE `terminal_wallets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `terminals`
--

DROP TABLE IF EXISTS `terminals`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `terminals` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SERIAL_NO` varchar(100) DEFAULT NULL,
  `VERSION_NO` varchar(100) DEFAULT NULL,
  `DAILY_DEPOSIT_LIMIT` varchar(100) DEFAULT NULL,
  `DAILY_TRANS_LIMIT` varchar(100) DEFAULT NULL,
  `DAILY_WITHDRAW_LIMIT` varchar(100) DEFAULT NULL,
  `ASSIGNED` bit(1) DEFAULT b'0',
  `ASSIGNED_DATE` timestamp NULL DEFAULT NULL,
  `OUTLET_NO` int(11) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `location` varchar(200) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `fee_group_no` int(11) DEFAULT NULL,
  `bank_no` int(11) DEFAULT NULL,
  `have_wallets` varchar(45) DEFAULT NULL,
  `amount` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `OUTLET_NO` (`OUTLET_NO`),
  KEY `terminals_fk_3` (`bank_no`),
  CONSTRAINT `terminals_fk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`),
  CONSTRAINT `terminals_fk_2` FOREIGN KEY (`OUTLET_NO`) REFERENCES `outlets` (`ID`),
  CONSTRAINT `terminals_fk_3` FOREIGN KEY (`bank_no`) REFERENCES `banks` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `terminals`
--

LOCK TABLES `terminals` WRITE;
/*!40000 ALTER TABLE `terminals` DISABLE KEYS */;
/*!40000 ALTER TABLE `terminals` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_types`
--

DROP TABLE IF EXISTS `transaction_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transaction_types` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `code` varchar(100) DEFAULT NULL,
  `short_code` varchar(20) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_types`
--

LOCK TABLES `transaction_types` WRITE;
/*!40000 ALTER TABLE `transaction_types` DISABLE KEYS */;
INSERT INTO `transaction_types` VALUES (1,'deposit','D','Deposit'),(2,'withdrawal','W','Withdrawal'),(3,'balance-inquiry','BI','Balance Inquiry'),(4,'mini-statement','MS','Mini-Statement'),(5,'internal-transfer','IFT','Internal Funds Transfer'),(6,'bill-payment','UBP','Utility Bill Payments'),(7,'account-opening','AC','Account Opening'),(8,'merchant-purchase','MP','Merchant Purchase'),(9,'cash-back','CB','Merchnat Cash Back'),(10,'merchant-reversal','MR','Merchant Reversal'),(11,'merchant-pre-auth','MPA','Pre Auth');
/*!40000 ALTER TABLE `transaction_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `transactions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `receipt_no` varchar(100) DEFAULT NULL,
  `amount` decimal(18,2) DEFAULT '0.00',
  `terminal_no` int(11) DEFAULT NULL,
  `transaction_type_no` int(11) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `lat` varchar(100) DEFAULT NULL,
  `lng` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `flag` varchar(100) DEFAULT 'Success',
  `agent_fee` decimal(18,2) DEFAULT '0.00',
  `binary_fee` decimal(18,2) DEFAULT '0.00',
  `bank_fee` decimal(18,2) DEFAULT '0.00',
  `total_fee` decimal(18,2) DEFAULT '0.00',
  `officer_id` varchar(100) DEFAULT NULL,
  `account_no` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `terminal_no` (`terminal_no`),
  KEY `transaction_type_no` (`transaction_type_no`),
  CONSTRAINT `transactions_fk_1` FOREIGN KEY (`terminal_no`) REFERENCES `terminals` (`ID`),
  CONSTRAINT `transactions_fk_2` FOREIGN KEY (`transaction_type_no`) REFERENCES `transaction_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transactions`
--

LOCK TABLES `transactions` WRITE;
/*!40000 ALTER TABLE `transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `transactions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_attempts`
--

DROP TABLE IF EXISTS `user_attempts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_attempts` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `EMAIL` varchar(150) DEFAULT NULL,
  `ATTEMPTS` int(11) DEFAULT NULL,
  `LAST_MODIFIED` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `IP` varchar(100) DEFAULT NULL,
  `USER_AGENT` varchar(100) DEFAULT NULL,
  `COUNTRY` varchar(100) DEFAULT NULL,
  `REGION` varchar(100) DEFAULT NULL,
  `CITY` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_attempts`
--

LOCK TABLES `user_attempts` WRITE;
/*!40000 ALTER TABLE `user_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_attempts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_groups`
--

DROP TABLE IF EXISTS `user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_groups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(100) DEFAULT NULL,
  `DESCRIPTION` varchar(250) DEFAULT NULL,
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `FLAG` varchar(20) DEFAULT '0',
  `BASE_TYPE` varchar(100) DEFAULT NULL,
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `SYSTEM_DEFINED` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  CONSTRAINT `user_groups_ibfk_1` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_groups`
--

LOCK TABLES `user_groups` WRITE;
/*!40000 ALTER TABLE `user_groups` DISABLE KEYS */;
INSERT INTO `user_groups` VALUES (1,'Superadmin','Superadmin','2016-12-08 14:39:51','2016-12-08 14:39:53',NULL,NULL,NULL,'1','superadmin',NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_types`
--

DROP TABLE IF EXISTS `user_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_types`
--

LOCK TABLES `user_types` WRITE;
/*!40000 ALTER TABLE `user_types` DISABLE KEYS */;
INSERT INTO `user_types` VALUES (1,'System Admin','system-admin'),(2,'Mobile Customer','mobile-customer'),(3,'Bank Admin','bank-admin'),(4,'Agent','agent-admin');
/*!40000 ALTER TABLE `user_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `FIRST_NAME` varchar(100) DEFAULT NULL,
  `MIDDLE_NAME` varchar(100) DEFAULT NULL,
  `SURNAME` varchar(100) DEFAULT NULL,
  `EMAIL` varchar(250) DEFAULT NULL,
  `PHONE` varchar(20) DEFAULT NULL,
  `PASSPORT` varchar(100) DEFAULT NULL,
  `PASSWORD` varchar(100) DEFAULT NULL,
  `PHOTO_URL` longtext,
  `PHOTO_KEY` varchar(200) DEFAULT NULL,
  `EXPIRY` timestamp NULL DEFAULT NULL,
  `ENABLED` tinyint(4) DEFAULT '1',
  `NONLOCKED` tinyint(4) DEFAULT '0',
  `CREATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `UPDATED_ON` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `DELETED_ON` timestamp NULL DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `MOBILE_TOKEN` varchar(200) DEFAULT NULL,
  `EMAIL_TOKEN` varchar(200) DEFAULT NULL,
  `FCM_TOKEN` varchar(600) DEFAULT NULL,
  `RESET_KEY` varchar(200) DEFAULT NULL,
  `MOBILE_VERIFIED` tinyint(4) DEFAULT '0',
  `EMAIL_VERIFIED` tinyint(4) DEFAULT '0',
  `FLAG` varchar(20) DEFAULT '0',
  `USER_GROUP_NO` int(11) DEFAULT NULL,
  `EDIT_DATA` longtext,
  `REASON_CODE_NO` int(11) DEFAULT NULL,
  `REASON_DESCRIPTION` varchar(200) DEFAULT NULL,
  `LAST_TIME_PASSWORD_UPDATED` timestamp NULL DEFAULT NULL,
  `PASSWORD_NEVER_EXPIRES` tinyint(4) DEFAULT '0',
  `RESET_REQ_DATE` timestamp NULL DEFAULT NULL,
  `ACTIVATION_KEY` varchar(200) DEFAULT NULL,
  `user_type_no` int(11) DEFAULT NULL,
  `member_no` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `USER_GROUP_NO` (`USER_GROUP_NO`),
  KEY `REASON_CODE_NO` (`REASON_CODE_NO`),
  KEY `users_fk_4` (`user_type_no`),
  CONSTRAINT `users_fk_4` FOREIGN KEY (`user_type_no`) REFERENCES `user_types` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`USER_GROUP_NO`) REFERENCES `user_groups` (`ID`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`REASON_CODE_NO`) REFERENCES `reason_codes` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Anthony',NULL,'Mwawughanga','admin@live.com','+254720727490',NULL,'$2a$10$hPGlXyKIf652JeUmWeVECuHKh8O9YADm4TsX1MUflDSIJhY2nlR8O',NULL,NULL,NULL,1,0,'2016-12-08 14:40:56','2016-12-08 14:40:58',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,1,'1',1,NULL,NULL,NULL,NULL,1,NULL,NULL,1,NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallets`
--

DROP TABLE IF EXISTS `wallets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wallets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` decimal(16,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallets`
--

LOCK TABLES `wallets` WRITE;
/*!40000 ALTER TABLE `wallets` DISABLE KEYS */;
/*!40000 ALTER TABLE `wallets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallets_transactions`
--

DROP TABLE IF EXISTS `wallets_transactions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `wallets_transactions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amount` varchar(45) DEFAULT NULL,
  `wallet_no` int(11) DEFAULT NULL,
  `entry_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallets_transactions`
--

LOCK TABLES `wallets_transactions` WRITE;
/*!40000 ALTER TABLE `wallets_transactions` DISABLE KEYS */;
/*!40000 ALTER TABLE `wallets_transactions` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-06 14:19:55
