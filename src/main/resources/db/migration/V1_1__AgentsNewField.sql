ALTER TABLE `agents`
ADD COLUMN `location` VARCHAR(200) NULL AFTER `branches_no`,
ADD COLUMN `agent-code` VARCHAR(200) NULL AFTER `location`;
