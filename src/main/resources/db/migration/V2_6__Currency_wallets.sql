CREATE TABLE `currency_type_wallet` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`));


ALTER TABLE `wallets`
ADD COLUMN `currency_no` INT NOT NULL AFTER `amount`;


INSERT INTO `currency_type_wallet` (`name`) VALUES ('USD');
INSERT INTO `currency_type_wallet` (`name`) VALUES ('SSP');

