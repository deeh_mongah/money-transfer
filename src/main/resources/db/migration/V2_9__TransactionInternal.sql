CREATE TABLE `transaction_internal` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `transaction_no` INT NULL,
  `receivers_name` VARCHAR(145) NULL,
  `receivers_phone_number` VARCHAR(145) NULL,
  PRIMARY KEY (`id`));
