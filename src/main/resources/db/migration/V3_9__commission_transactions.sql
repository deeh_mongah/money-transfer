DROP TABLE  IF EXISTS commission_transaction;
CREATE TABLE commission_transaction (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `transaction_no` INT NULL,
  `agent_no` VARCHAR(45) NULL,
  `agent_amount` DECIMAL(16,2) NULL,
  `flex_amount` DECIMAL(16,2) NULL,
  `binary_amount` DECIMAL(16,2) NULL,
  `tariff_amount` DECIMAL(16,2) NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;