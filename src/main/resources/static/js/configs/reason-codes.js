(function($, window){
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){
        var ui = $(".edit-view"), panel = $(".content-body"), oTable;
        function fnEdit( o ){
            $('input[name="id"]', ui).val( o.id );
            $('input[name="name"]', ui).val( o.name );
            $('textarea[name="description"]', ui).val( o.description );
            $('input[name="action"]', ui).val("edit");
            ui.modal('show');
        }

        function actions( o ){
            var actions = "";
            actions = '<button  type="button" class="btn btn-success btn-sm edit" data-index="' + o + '">Edit</button >';
            actions += '&nbsp;&nbsp;<button  type="button" class="btn btn-danger btn-sm delete" data-index="' + o + '">Delete</button >';
            return actions;
        }

        oTable = utils.dataTable({
            table : $(".table"),
            fnRowCallback: function(nRow, aData){
                $(nRow).children('td:eq(-1)').html( actions( aData[2]) );

                $(".edit", nRow).click( function(){
                    var index = $(this).data("index");
                    utils.http
                        .jsonRequest( panel, {'action':'fetch-record', 'index':index })
                        .done( function( o ){
                            if("00" === o.status ){
                                fnEdit( o );
                            }
                            else utils.alert.error( o.message );
                        });
                });

                $(".delete", nRow).click( function(){
                    var index = $(this).data("index");
                    swal({
                        title:'',
                        text: $('.lang-pack > ._ajax_question_delete', panel).html(),
                        type: (typeof type === 'undefined') ? 'info':type,
                        showCancelButton: true,
                        closeOnConfirm: false,
                        showLoaderOnConfirm: true
                    }, function( isConfirm){
                        if( isConfirm )
                            setTimeout( function(){
                                utils.http
                                    .jsonRequest( panel, {'action':'delete', 'index':index })
                                    .done( function( o ){
                                        if("00" === o.status ){
                                            utils.alert.success(o.message );
                                            oTable.fnDraw();
                                        }
                                        else utils.alert.error( o.message );
                                    });
                            }, 200);
                    });
                });
            }
        });

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                oTable.fnDraw();
            });
        });

        $('form', ui).formValidation({
            framework: 'bootstrap',
            fields: {
                name: {validators: {notEmpty: {message: 'Name is required'}}},
                description: {validators: {notEmpty: {message: 'Description is required'}}}
            }
        }).on('success.form.fv', function (e) {
            e.preventDefault();
            ui.hide();
            utils.http.jsonRequest(panel, $(this).serializeArray())
                .done( function( o ){
                    if( o.status === "00"){
                        utils.alert.success( o.message );
                        oTable.fnDraw();
                        ui.modal("hide");
                    }
                    else{
                        utils.alert.error(o.message, function(){
                            ui.show();
                        });
                    }
                });
        });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            $("form", ui).data('formValidation').resetForm();
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });

    });
})(jQuery, window);