(function (document, window, $) {
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(document).ready(function ($) {

        var ui = $(".edit-view"), oTable,
            fnEdit = function (o) {
                $('[name="id"]').val( o.id );
                $('[name="module"]').val( o.module );
                $('[name="enabled"]', ui).val( o.enabled );
                $('[name="status"]', ui).prop('checked', o.enabled);
                $('[name="action"]', ui).val( "edit" );
                ui.modal("show");
            },
            actions = function( o ){
                var actions = "";
                actions = '<button  type="button" class="btn btn-success btn-xs edit" data-index="' + o + '">Edit</button >';
                return actions;
            };

        oTable = utils.dataTable({
            table : $(".table"),
            fnRowCallback: function(nRow, aData){
                //Process data
                var k = 0;
                $(aData).each(function (i) {
                    k++;
                    if (aData.length === k)
                        $(nRow).children('td:eq(-1)').html( actions(aData[i]));
                    else
                        aData[k] === true ? $(nRow).find('td:eq(' + k + ')').text('Yes') : $(nRow).find('td:eq(' + k + ')').text('No');
                });

                //Handle table events
                $(".edit", nRow).click( function( e ){
                    let index = $(this).data("index");
                    utils.http
                        .jsonRequest(undefined, {'action':'fetch-record', 'index':index })
                        .done( function( o ){
                            if("00" === o.status ) fnEdit( o );
                            else utils.alert.error( o.message );
                        });
                });
            }
        });

        function reloadTables(){
            oTable.fnDraw();
        };
        //Refresh call within helpers
        utils.makerchecker.options.refreshFn = reloadTables;

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                reloadTables();
            });
        });

        $("form", ui).submit( function(e){
            e.preventDefault();
            $('[name="enabled"]', ui).val( $('[name="status"]', ui).is(":checked") );
            utils.submitForm($(this).serializeArray(), ui);
        });

    });

})(document, window, jQuery);





