{
    const startOfWeek = moment().startOf('isoWeek');
    const endOfWeek = moment().endOf('isoWeek');

    let days = [];
    let day = startOfWeek;

    while (day <= endOfWeek) {
        days.push(day.toDate());
        day = day.clone().add(1, 'd');
    }

    Morris.Line({
            element: "weekly-activity-chart",
            data: [
                { day: Date.parse( days[0] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0 },
                { day: Date.parse( days[1] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0  },
                { day: Date.parse( days[2] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0  },
                { day: Date.parse( days[3] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0  },
                { day: Date.parse( days[4] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0  },
                { day: Date.parse( days[5] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0  },
                { day: Date.parse( days[6] ), Deposits: 0, Withdrawals: 0, 'Balance Inquiry': 0,  'Mini-Statements': 0  }
            ],
            xkey: "day",
            xLabels: ["day"],
            ykeys: ["Deposits", "Withdrawals", "Balance Inquiry", "Mini-Statements"],
            labels: ["Deposits", "Withdrawals", "Balance Inquiry", "Mini-Statements"],
            resize: !0,
            smooth: !0,
            pointSize: 3,
            pointStrokeColors: ["#FF7588", "#16D39A", "#FFA87D", "#0D47A1"],
            gridLineColor: "#e3e3e3",
            behaveLikeLine: !0,
            numLines: 6,
            gridtextSize: 14,
            lineWidth: 3,
            hideHover: "auto",
            lineColors: ["#FF7588", "#16D39A", "#FFA87D", "#0D47A1"],
            xLabelFormat: function (t) {
                return ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"][t.getDay()]
            }
        }
    );
}