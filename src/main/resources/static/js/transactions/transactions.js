{
    let fnStatusLabels = status =>{
        let label;
        switch( status ){
            case "Success":
                label = "primary";
                break;
            case "Failed":
                label = "danger";
                break;
        }
        return "<span class='badge badge-"+label+"'> " + status +" </span>";
    };

    let statusIndex = 4;
    $(window).on('tableDrawing', e => {
        let row = $(e.tableRow);
        let data = e.aData;
        let status = data[ statusIndex ];
        $( row ).children('td:eq('+ statusIndex +')').html( fnStatusLabels( status ) );
    });


}