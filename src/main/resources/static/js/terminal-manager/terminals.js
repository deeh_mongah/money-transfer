{
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    let ui = $('.edit-view');
    let fnEdit = function (o) {
        $('input[name="serialNo"]', ui).val(o.serialNo);
        $('input[name="action"]', ui).val('edit');
        $('input[name="id"]', ui).val(o.id);
        ui.modal('show');
    };
    utils.makerchecker.initTables( fnEdit );

    //When to refresh table info
    $('[data-action="refresh"]').click( function(){
        setTimeout(function () {
            utils.makerchecker.refreshMkTables();
        });
    });

    //Form validation
    $('form', ui)
        .formValidation({
            framework: 'bootstrap',
            fields:{
                serialNo: { validators:{ notEmpty :{ message: 'Serial No is required'}}}
            }
        })
        .on('success.fv.form', function(e){
            e.preventDefault();
            utils.submitForm( $(this).serializeArray(), ui);
        });

    //House keep
    ui.on('hidden.bs.modal', function(){
        utils.houseKeep( ui );
        //always have this default action
        $('input[name="action"]', ui).val('new');
    });


    //Handle file uploads
    let upload = $('.upload-view');
    $(document).on("change", ".custom-file [type=file]", function() {
        let $this = $(this);
        let value = "";

        $.each($this[0].files, function(i, file) {
            value += file.name + ", ";
        });

        value = value.substring(0, value.length - 2);
        console.log(value);
        $(".custom-file-label").text( value );
    });

    $("form", upload)
        .formValidation().on("success.fv.form", function(e){
        e.preventDefault();
        let $this = $(this);

        //Handle csv file
        let data;
        $('[name="file"]', upload).parse({
            config: {
                delimiter: "",
                newline: "",
                quoteChar: '"',
                header: true,
                skipEmptyLines: true,
                complete : fnComplete
            }
        });

        //Submit formData
        function fnComplete( results ){

            if (results && results.errors ) {
                if (results.errors) {
                    console.log("No of errors: ",  results.errors.length);
                    console.log("First error: ",  results.errors[0]);
                }
                if (results.data && results.data.length > 0)
                    console.log("No of parsed rows:", results.data.length);
            }

            data = results.data;

            //Update hidden input
            $("[name='data']", upload).val( JSON.stringify( data ) );
            utils.submitForm( $this.serializeArray(), upload );
        }
    });
}