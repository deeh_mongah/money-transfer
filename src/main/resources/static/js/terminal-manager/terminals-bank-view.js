{

    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });


    let $allocateModal = $('.allocate-view');
    let fnActions = (recordId, allocationState )=>{
        let action = '';

        if( 'Not Allocated' === allocationState ){
            action = `<button  type='button' class='btn btn-success btn-sm allocate' data-index='${recordId}'>Allocate</button >`;
        }
        else{
            action = `<button  type='button' class='btn btn-danger btn-sm de-allocate' data-index='${recordId}'>De-Allocate</button >`;
        }
        return action;
    };

    let $table = $('.table');
    let cols = $table.find('thead > tr > th').length;
    let oTable = utils.dataTable({
        table: $table,
        fnRowCallback: function(nRow, aData){
            let recordId = aData[ cols-1 ];
            let allocationState = aData[2];
            let serialNo = aData[0];
            $(nRow).children('td:eq(-1)').html( fnActions( recordId, allocationState));

            $('.allocate', nRow).click( function(){
                $('.serial_no', $allocateModal).html( serialNo );
                $('[name="terminalId"]', $allocateModal).val( recordId );

               // Load modal
                $allocateModal.modal('show');
            });

            $(".de-allocate", nRow).click( (e)=>{
                let rowIndex = $(e.target).data("index");
                utils.alert.confirm("Are you sure you want to de-allocate this device?", "info", function(){

                    setTimeout( () => {
                        utils.http.jsonRequest( $('.card-body'), { action: 'de-allocate', 'index': rowIndex } )
                            .done( ( o ) => {
                                if("00" === o.status ){
                                    utils.alert.success( o.message, ()=>{
                                        oTable.fnDraw();
                                    });
                                }
                                else utils.alert.error( o.message );
                            });
                    }, 200);

                });
            });
        }
    });

    let reload = ()=>{
        oTable.fnDraw();
    };

    //When to refresh table info
    $('[data-action="refresh"]').click( function(){
        setTimeout( ()=> reload() );
    });

    // Handle allocation form
    $('form', $allocateModal).formValidation({
        framework: 'bootstrap',
        fields:{
            outletNo: {
                validators:{
                    notEmpty:{
                        message: 'Outlet is required'
                    }
                }
            }
        }
    }).on('success.form.fv', function(e){
        e.preventDefault();

        // Submit form via ajax
        let data = $(this).serializeArray();
        console.log('data:', data);
        $allocateModal.hide();
        utils.http.jsonRequest($('.card-body'), data)
            .done( function( o ){
                if( o.status === "00"){
                    utils.alert.success( o.message );
                    oTable.fnDraw();
                    $allocateModal.modal("hide");
                }
                else{
                    utils.alert.error(o.message, function(){
                        $allocateModal.show();
                    });
                }
            });

    });

    //House keep
    $allocateModal.on('hidden.bs.modal', function(){
        utils.houseKeep( $allocateModal );
        $("form", $allocateModal).data('formValidation').resetForm();
        //always have this default action
        $('input[name="action"]', $allocateModal).val('allocate');
    });

}