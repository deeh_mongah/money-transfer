(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){

        let panel = $(".card-bodys");

        let floatBalance ;
        let floatBalanceV1 ;
        let currency ;
        let currencyV1 ;

        function fnParentActions( aData ){

             floatBalance = document.getElementById("floatBalance");
             floatBalanceV1 = document.getElementById("floatBalanceV1");
             currency = document.getElementById("currency");
             currencyV1 = document.getElementById("currencyV1");

            // floatBalance.value = aData[6];
            // floatBalanceV1.value = aData[6];
            // currency.value = aData[2];
            // currencyV1.value = aData[2];

            // alert(aData[5])



            var actions = '<div class="btn-group"><button type="button" class="btn btn-primary btn-square btn-sm">Options</button>';
            actions += '<button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>';
            actions += '<div class="dropdown-menu">';

            actions += '<a href="javascript:void(0)" class="dropdown-item push" data-index="' + aData[5] + '" ><i class="icon-checkmark4"></i> Push Float </a>';
            actions += '<a href="javascript:void(0)" class="dropdown-item pull" data-index="' + aData[5]+ '" ><i class="icon-cross2"></i> Pull Float </a>';


            actions += '</div></div>';
            return actions;
        }

        var parent = $(".parent-card"),
            child = $(".child-card"),
            childTable,
            pushUi = $('.push-view'),
            pullUi = $('.pull-view');
        var oTable = utils.dataTable({
            table : $(".parent-table"),
            "fnRowCallback": function (nRow, aData) {

                $(nRow).children('td:eq(0)').html('<a href="#" data-id="' + aData[4] + '">' + aData[0] + '</a>');
                // $(nRow).children('td:eq(-1)').html( fnParentActions( aData ) );


                $('a', nRow).click(function (evt) {




                    evt.preventDefault();
                    parent.hide();
                    child.toggleClass("d-none");

                    var index = $(this).data("id");
                    $('.card-title', child).html( $(this).html() );

                    childTable = utils.dataTable({
                        table : $(".child-table"),
                        "fnRowCallback": function (nRow, aData) {
                            $(nRow).children('td:eq(-1)').html( fnParentActions( aData ) );



                            //Handle events
                            $('.push', nRow).click( function(){

                                let index = $(this).data("index");

                                $.get("/currency/"+index).done( res =>{



                                    floatBalance.value = res[1];
                                    floatBalanceV1.value = res[1];
                                    currency.value = res[0];
                                    currencyV1.value = res[0];

                                    console.log("res"+res)

                                });

                                utils.http
                                    .jsonRequest(panel, {'action': 'fetch-record', 'index': index})
                                    .done(function (o) {
                                        if ("00" === o.status) {

                                            fnPush(o, "push-float");
                                        }
                                        else utils.alert.error(o.message);
                                    });
                            });

                            $('.pull', nRow).click(function () {

                                let index = $(this).data("index");


                                $.get("/currency/"+index).done( res =>{



                                    floatBalance.value = res[1];
                                    floatBalanceV1.value = res[1];
                                    currency.value = res[0];
                                    currencyV1.value = res[0];

                                    console.log("res"+res)

                                });

                                utils.http
                                    .jsonRequest(panel, {'action': 'fetch-record', 'index': index})
                                    .done(function (o) {
                                        if ("00" === o.status) {

                                            fnPull(o, "pull-float");
                                        }
                                        else utils.alert.error(o.message);
                                    });
                            });
                        },

                        "fnServerParams": function(aoData) {
                            aoData.push({name:'data', value:index});
                            return aoData;
                        }
                    });

                });



                //When updating outlet terminals


            }
        });

        let fnPush = function (o, action) {

            $('input[name="id"]', pushUi).val(o.id);
            $('input[name="action"]', pushUi).val(action);


            pushUi.modal("show");
        };

        let fnPull= function (o, action) {

            $('input[name="id"]', pullUi).val(o.id);
            $('input[name="action"]', pullUi).val(action);


            pullUi.modal("show");
        };






        function fnReloadTables(){
            // childTable.fnDraw();
        }

        $('form', pullUi)
            .formValidation({
                framework: 'bootstrap',
                excluded: [':disabled'],
                fields: {
                    amount: {validators: {notEmpty: 'Enter Amount'}},
                }
            }).on('success.form.fv', function (e) {
            e.preventDefault();
            utils.submitForm($(this).serializeArray(), pullUi);
        });

        $('form', pushUi)
            .formValidation({
                framework: 'bootstrap',
                excluded: [':disabled'],
                fields: {
                    amount: {validators: {notEmpty: 'Enter Amount'}},
                }
            }).on('success.form.fv', function (e) {
            e.preventDefault();
            utils.submitForm($(this).serializeArray(), pushUi);
        });

        pushUi.on('hidden.bs.modal', function () {
            utils.houseKeep(pushUi);
            fnReloadTables();
            $('input[name="floatBalance"]', pushUi).val('0');
            $("form", pushUi).data('formValidation').resetForm();
        });

        pullUi.on('hidden.bs.modal', function () {
            utils.houseKeep(pullUi);
            fnReloadTables();
            $('input[name="floatBalance"]', pullUi).val('0');
            $("form", pullUi).data('formValidation').resetForm();
        });

        $('.btn-info', child).click(function () {
                    if (!parent.is(":visible")) {
                             parent.show();
                            child.toggleClass("d-none");
                               childTable.fnDestroy();
                           }
                   });

    });

})(jQuery, window);