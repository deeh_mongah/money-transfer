(function ($, window) {
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function () {

        utils.makerchecker.fnModule = "Users";

        // var ui = $('.edit-view')

        function fnReloadTables(){
            activeTable.fnDraw();
        }

        utils.makerchecker.options.refreshFn = fnReloadTables;

        // When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                fnReloadTables();
            });
        });

        let asn = $('.assign-view'),
            fnAssign = function (ac) {

                $('input[name="action"]', asn).val('assign');
                $('input[name="id"]', asn).val(ac.id);

                ui.modal('show');
            };


        utils.makerchecker.initTables();


        let fnActions = function (o,ident, status) {

            var actions = '<div class="btn-group"><button type="button" class="btn btn-primary btn-square btn-sm">Options</button>';
            actions += '<button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>';
            actions += '<div class="dropdown-menu">';


            if(status === "0") {
                actions += '<a href="javascript:void(0)" class="dropdown-item assign" data-index="' + o + '" data-name="' + ident + '"><i class="icon-checkmark4"></i> Assign </a>';

            }else if (status === "1") {
                actions += '<a href="javascript:void(0)" class="dropdown-item deallocate" data-index="' + o + '" data-name="' + ident + '"><i class="icon-cross2"></i> Deallocated </a>';
            }

            actions += '</div></div>';
            return actions;
        };



           let  deallocate = function (ident, index, status, type, maker) {
            swal({
                title: '',
                text: "Are sure you want to Deallocate The record",
                type: (typeof type === 'undefined') ? 'info' : type,
                showCancelButton: true,
                closeOnConfirm: false,
                showLoaderOnConfirm: true

            }, function (isConfirm) {

                if (isConfirm)
                    setTimeout(function () {
                        $.ajax({
                            type: 'post',
                            data: {'action': status, 'index': index},
                            dataType: 'json',
                            success: function (o) {
                                if (o.status === '00') {
                                    utils.alert.success(o.message);
                                    utils.makerchecker.refreshTables();
                                }
                                else utils.alert.error(o.message);
                            },
                            statusCode: {
                                404: utils.http._404, 500: utils.http._500, 403: utils.http._403
                            }
                        });
                    }, 200);
            });
        };

        var cols = $(".table-new-terminals").find('thead > tr > th').length;

        let statusIndex = cols ;

        let activeTable = utils.dataTable({
            table: $(".table-new-terminals"),
            fnRowCallback: function (nRow, aData) {
                let status = aData[statusIndex];
                $(nRow).children('td:eq(' + (cols - 1) + ')').html(fnActions(aData[cols - 1], aData[0], status));

                //Handle events
                $('.assign', nRow).click( function(){

                    utils.fnEdit( $(this).data('index'));
                });
                // $('.assign', nRow).click(function () {
                //     let index = $(this).data('index');
                //     $('input[name="terminalNo"]', asn).val(index);
                //     asn.modal('show').end();
                // });
                $('.deallocate', nRow).click(function () {
                    deallocate($(this).data('name'), $(this).data('index'), "deallocate", "warning");
                });


            },

            fnServerParams: function (aoData) {
                aoData.push({'name': 'fetch-table', 'value': '1'});
            }
        });



        $('form', asn)
            .formValidation({
                framework: 'bootstrap',
                excluded: [':disabled'],
                fields: {
                    parkNo: {validators: {notEmpty: 'Select a Park'}},
                    gateNo: {validators: {notEmpty: 'Select a Gate'}}
                }
            }).on('success.form.fv', function (e) {
            e.preventDefault();
            utils.submitForm($(this).serializeArray(), asn);
        });

        asn.on('hidden.bs.modal', function () {
            utils.houseKeep(asn);
            fnReloadTables();
            $("form", asn).data('formValidation').resetForm();
            $('input[name="action"]', asn ).val('new');
            // $('input[name="driverNo"]', asn).val('');
        });


       


    });

})(jQuery, window);


