(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){
        var ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="firstName"]', ui).val( o.firstName );
                $('input[name="lastName"]', ui).val( o.lastName );
                $('input[name="passport"]', ui).val( o.passport );

                $('input[name="action"]', ui).val('edit' );
                $('input[name="id"]', ui).val( o.id );
                ui.modal('show');
            };
        utils.makerchecker.initTables( fnEdit );

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                utils.makerchecker.refreshMkTables();
            });
        });

        //Form validation
        $('form', ui)
            .formValidation({
                framework: 'bootstrap',
                fields:{
                    firstName: { validators:{ notEmpty :{ message: 'First name is required'}}},
                    lastName: { validators:{ notEmpty :{ message: 'Last name is required'}}},
                    passport: { validators:{ notEmpty :{ message: 'Passport No is required'}}}
                }
            })
            .on('success.fv.form', function(e){
                e.preventDefault();
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });

    });

})(jQuery, window);