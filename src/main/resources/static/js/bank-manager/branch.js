(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){
        var ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="name"]', ui).val( o.name );
                $('input[name="branchCode"]', ui).val( o.branchCode );
                $('input[name="location"]', ui).val( o.location );
                $('input[name="action"]', ui).val('edit' );
                $('input[name="id"]', ui).val( o.id );
                ui.modal('show');
            };
        utils.makerchecker.initTables( fnEdit );

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                utils.makerchecker.refreshMkTables();
            });
        });

        //Form validation
        $('form', ui)
            .formValidation({
                framework: 'bootstrap',
                fields:{
                    name: { validators:{ notEmpty :{ message: 'Name is required'}}},
                    branchCode :{ validators:{ mobile:{ message: 'Code is required '}}},
                    location :{ validators:{ notEmpty:{ message: 'Location is required '}}}
                }
            })
            .on('success.fv.form', function(e){
                e.preventDefault();
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });

    });

})(jQuery, window);