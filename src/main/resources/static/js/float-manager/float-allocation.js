(function ($, window) {
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function () {

        let panel = $(".card-body");

        utils.makerchecker.fnModule = "Users";

        // var ui = $('.edit-view')

        function fnReloadTables(){
            activeTable.fnDraw();
        }

        utils.makerchecker.options.refreshFn = fnReloadTables;

        // When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                fnReloadTables();
            });
        });

        let pushUi = $('.push-view');
        let pullUi = $('.pull-view');

        let fnPush = function (o, action) {

            $('input[name="id"]', pushUi).val(o.id);
            $('input[name="action"]', pushUi).val(action);


            pushUi.modal("show");
        };

        let fnPull= function (o, action) {

            $('input[name="id"]', pullUi).val(o.id);
            $('input[name="action"]', pullUi).val(action);


            pullUi.modal("show");
        };



        utils.makerchecker.initTables();


        let fnActions = function (o,ident, status) {

            let floatBalance = document.getElementById("floatBalance");
            let floatBalanceV1 = document.getElementById("floatBalanceV1");

            floatBalance.value = status;
            floatBalanceV1.value = status;

            var actions = '<div class="btn-group"><button type="button" class="btn btn-primary btn-square btn-sm">Options</button>';
            actions += '<button type="button" class="btn btn-danger dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>';
            actions += '<div class="dropdown-menu">';

            actions += '<a href="javascript:void(0)" class="dropdown-item push" data-index="' + o + '" data-name="' + ident + '"><i class="icon-checkmark4"></i> Push Float </a>';
            actions += '<a href="javascript:void(0)" class="dropdown-item pull" data-index="' + o + '" data-name="' + ident + '"><i class="icon-cross2"></i> Pull Float </a>';


            actions += '</div></div>';
            return actions;
        };





        var cols = $(".table-new-terminals").find('thead > tr > th').length;

        let statusIndex = cols ;

        let activeTable = utils.dataTable({
            table: $(".table-new-terminals"),
            fnRowCallback: function (nRow, aData) {
                let status = aData[statusIndex];
                $(nRow).children('td:eq(' + (cols - 1) + ')').html(fnActions(aData[cols - 1], aData[0], aData[6]));

                //Handle events
                $('.push', nRow).click( function(){

                    let index = $(this).data("index");

                    utils.http
                        .jsonRequest(panel, {'action': 'fetch-record', 'index': index})
                        .done(function (o) {
                            if ("00" === o.status) {

                                fnPush(o, "push-float");
                            }
                            else utils.alert.error(o.message);
                        });
                });

                $('.pull', nRow).click(function () {

                    let index = $(this).data("index");

                    utils.http
                        .jsonRequest(panel, {'action': 'fetch-record', 'index': index})
                        .done(function (o) {
                            if ("00" === o.status) {

                                fnPull(o, "pull-float");
                            }
                            else utils.alert.error(o.message);
                        });
                });


            },

            fnServerParams: function (aoData) {
                aoData.push({'name': 'fetch-table', 'value': '1'});
            }
        });



        $('form', pullUi)
            .formValidation({
                framework: 'bootstrap',
                excluded: [':disabled'],
                fields: {
                    amount: {validators: {notEmpty: 'Enter Amount'}},
                }
            }).on('success.form.fv', function (e) {
            e.preventDefault();
            utils.submitForm($(this).serializeArray(), pullUi);
        });

        $('form', pushUi)
            .formValidation({
                framework: 'bootstrap',
                excluded: [':disabled'],
                fields: {
                    amount: {validators: {notEmpty: 'Enter Amount'}},
                }
            }).on('success.form.fv', function (e) {
            e.preventDefault();
            utils.submitForm($(this).serializeArray(), pushUi);
        });

        pushUi.on('hidden.bs.modal', function () {
            utils.houseKeep(pushUi);
            fnReloadTables();
            $('input[name="floatBalance"]', pushUi).val('0');
            $("form", pushUi).data('formValidation').resetForm();
        });

        pullUi.on('hidden.bs.modal', function () {
            utils.houseKeep(pullUi);
            fnReloadTables();
            $('input[name="floatBalance"]', pullUi).val('0');
            $("form", pullUi).data('formValidation').resetForm();
        });


       


    });

})(jQuery, window);


