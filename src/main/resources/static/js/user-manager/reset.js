( function(){
    "use strict";

    FormValidation.Validator.password = utils.passwordValidator;
    $("form").formValidation({
        framework: "bootstrap",
        fields:{
            password: {
                validators: {
                    notEmpty: {message: "Password is required"},
                    password: { message: "Invalid password"}
                }
            },
            confPassword: {
                verbose: false,
                validators: {
                    identical: {
                        field: 'password',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    });

})();