( function(){
    "use strict";

    $("form").formValidation({
        framework: "bootstrap",
        fields:{
            email: {
                verbose: false,
                validators: {
                    notEmpty: {message: "Email is required"},
                    emailAddress: {message: 'The email address is invalid'},
                    stringLength: {max: 512, message: 'Cannot exceed 512 characters'}
                }
            }
        }
    });

})();