(function($, window){
    'use strict';
    var Site = window.Site,
        token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){

        var ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="name"]', ui).val(o.name );
                $('input[name="id"]', ui).val(o.id);
                $('input[name="description"]', ui).val(o.description);

                initPermissions( o );

                $('input[name="action"]', ui).val("edit");
                ui.modal("show");
            };

        let initPermissions = function( o ){
            $("#permissionsEl_leftAll").trigger("click");

            if( typeof o !== "undefined"){
                console.log( o.permissions );
                $.each(o.permissions, ( k, v) => {
                    $("#permissionsEl").find("option[value="+ v +"]").prop("selected", true);
                    $("#permissionsEl_rightSelected").trigger("click");

                });
            }

        };

        utils.makerchecker.initTables( fnEdit, 'User Groups' );
        var refreshTables = function () {
            utils.makerchecker.refreshMkTables();
        };

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                refreshTables();
            });
        });

        //When submitting the form
        $('form', ui)
            .find("#permissionsEl").multiselect({
                keepRenderingSort: true
            }).end()
            .formValidation({
                framework: 'bootstrap',
                excluded: ':disabled',
                fields: {
                    name: {validators: {notEmpty: {message: 'Name is required'}}},
                    description: {validators: {notEmpty: {message: 'Description is required'}}},
                    roles: {
                        validators: {
                            callback: {
                                message: 'Please select at least one permission',
                                callback: function (value, validator, $field) {
                                    var options = validator.getFieldElements('roles').val();
                                    // var options = $("form", ui).find('[name="roles"]').val();
                                    return (options !== null);
                                }
                            }
                        }
                    },
                    permission: {
                        validators: {
                            blank:{},
                            callback: {
                                message: 'Please select at least one permission',
                                callback: function (value, validator, $field) {
                                    var options = validator.getFieldElements('permission').val();
                                    // var options = $("form", ui).find('[name="roles"]').val();
                                    return (options !== null);
                                }
                            }
                        }
                    }
                }
            }).on('success.form.fv', function (e) {
                e.preventDefault();
                //Update the permissions accordingly
                var selected = [];
                $(".permissions > option", ui).each( function(){
                    if( !$(this).hasClass("hide") && $(this).is(':selected') ) selected.push( $(this).val() );
                });

                $('select[name="roles"]', ui).selectpicker('val', selected).selectpicker('refresh');
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $(".permissions").val(null);
            $("#permissionsEl_leftAll").trigger("click");
            $('input[name="action"]', ui).val('new');
        });

    });

})(jQuery, window);

