( function(){
    "use strict";

    FormValidation.Validator.password = utils.passwordValidator;
    $("form").formValidation({
        framework: "bootstrap",
        fields:{
            newPassword: {
                validators: {
                    notEmpty: {message: "Password is required"},
                    password: { message: "Invalid password"}
                }
            },
            confPassword: {
                verbose: false,
                validators: {
                    identical: {
                        field: 'newPassword',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    });

})();