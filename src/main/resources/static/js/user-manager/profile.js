(function($, window){
    "use strict";

    var passwordView = $(".password-view"),
        profileView = $(".profile-view");

    $(".profile").click( function(){
        console.log("profile");
        profileView.toggleClass("d-none", false);
        passwordView.toggleClass("d-none", true);
    });

    $(".password").click( function(){
        console.log("password");
        profileView.toggleClass("d-none", true);
        passwordView.toggleClass("d-none", false);
    });


})(jQuery, window);