{
    let dtTable = utils.dataTable({
        table: $('.table')
    });

    //When to refresh table info
    $('[data-action="refresh"]').click( function(){
        setTimeout(function () {
            dtTable.fnDraw();
        });
    });
}