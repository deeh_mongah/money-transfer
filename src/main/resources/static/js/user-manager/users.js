(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){

        var country;
        //Init custom validators
        // FormValidation.Validator.mobile = utils.mobileValidator;
        let ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="firstName"]', ui).val( o.firstName );
                $('input[name="middleName"]', ui).val( o.middleName );
                $('input[name="surname"]', ui).val( o.surname );
                $('input[name="email"]', ui).val( o.email );
                $('select[name="phoneCode"]', ui).val( o.phoneCode );

                var rawNumber = o.phone;
                var formattedNumber = rawNumber.substring(4, 13);

                $('input[name="phone"]', ui).val( formattedNumber );
                $('select[name="userGroupNo"]', ui).val( o.userGroupNo );
                $('input[name="action"]', ui).val('edit' );
                $('input[name="id"]', ui).val( o.id );
                $('.user-type', ui).hide();
                ui.modal('show');
            };
        utils.makerchecker.initTables( fnEdit );

        //Extra table
        var lockedTable = utils.dataTable({
            table: $('.table-locked'),
            "fnServerParams": function( aoData ){ aoData.push({ "name":"fetch-table", "value":"5"});},
            "fnRowCallback": function( nRow, aData){
                $(nRow).children('td:eq(-1)')
                    .html('<button class="btn btn-danger btn-xs unlock" data-index="'+aData[4]+'" >Unlock</button>');
                $('.unlock', nRow).click( function(){
                    utils.makerchecker.flagRecords("undefined", $(this).data('index'), "unlock");
                });
            }
        });

        function reloadTables(){
            utils.makerchecker.refreshMkTables();
            lockedTable.fnDraw();
        }
        //Refresh call within helpers
        utils.makerchecker.options.refreshFn = reloadTables;

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                reloadTables();
            });
        });



        //Form validation
        $('form', ui)

            .find('[name="userTypeNo"]').change( function(e){
            let element = $(this).find("option:selected").data("type");

                if( "bank-admin" === element ){
                    $(".bank-view", ui).toggleClass("d-none", false );
                }else if( "agent-admin" === element ){
                    $(".agent-view", ui).toggleClass("d-none", false );
                }
                else{
                    $(".bank-view", ui).toggleClass("d-none", true );
                }
            }).end()
            .formValidation({
                framework: 'bootstrap',
                fields:{
                    firstName: { validators:{ notEmpty :{ message: 'First name is required'}}},
                    surname:{ validators:{ notEmpty :{ message: 'Last name is required'}}},
                    userGroupNo:{ validators: {notEmpty:{ message: 'Usergroup is required'}}},
                    phone:{ validators:{ notEmpty:{ message: 'Invalid mobile number'}}},
                    userTypeNo: { validators: { notEmpty: { message: "User type is required."}}},
                    email:{
                        verbose: false,
                        validators:{
                            notEmpty:{ message:"Email is required"},
                            emailAddress: { message: 'The email address is not valid'},
                            stringLength: { max: 512,message: 'Cannot exceed 512 characters'}
                        }
                    }

                }
            })
            .on('success.fv.form', function(e){
                e.preventDefault();
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $('.user-type', ui).show();
            $('input[name="action"]', ui).val('new');
        });

    });

})(jQuery, window);