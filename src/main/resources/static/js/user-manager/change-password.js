{
    FormValidation.Validator.password = utils.passwordValidator;
    $('form').formValidation({
        framework: "bootstrap",
        fields:{
            oldPassword: {
                validators: {
                    notEmpty: {message: "Password is required"},
                    password: { message: "Invalid password"}
                }
            },
            newPassword: {
                validators: {
                    notEmpty: {message: "Password is required"},
                    password: { message: "Invalid password"}
                }
            },
            confPassword: {
                verbose: false,
                validators: {
                    identical: {
                        field: 'newPassword',
                        message: 'The password and its confirm are not the same'
                    }
                }
            }
        }
    });
}