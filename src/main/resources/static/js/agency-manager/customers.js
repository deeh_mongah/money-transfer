(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){
        //Init custom validators
        // FormValidation.Validator.mobile = utils.mobileValidator;
        var ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="firstName"]', ui).val( o.firstName );
                $('input[name="surname"]', ui).val( o.surname );


                $('input[name="email"]', ui).val( o.email );
                $('select[name="phoneCode"]', ui).val( o.phoneCode );

                var rawNumber = o.phone;
                var formattedNumber = rawNumber.substring(4, 13);

                $('input[name="phone"]', ui).val( formattedNumber );
                $('input[name="passport"]', ui).val( o.passport );
                $('input[name="floatAccount"]', ui).val( o.floatAccount );
                $('input[name="businessId"]', ui).val( o.businessId );
                $('input[name="commissionAccount"]', ui).val( o.commissionAccount );
                $('input[name="location"]', ui).val( o.location );
                $('select[name="branchesNo"]', ui).val( o.branchesNo );
                $('input[name="action"]', ui).val('edit' );
                $('input[name="id"]', ui).val( o.id );
                ui.modal('show');
            };
        utils.makerchecker.initTables( fnEdit );

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                utils.makerchecker.refreshMkTables();
            });
        });

        //Form validation
        $('form', ui)
            .formValidation({
                framework: 'bootstrap',
                fields:{
                    name: { validators:{ notEmpty :{ message: 'Name is required'}}},
                    phone:{ validators:{ notEmpty:{ message: 'Invalid mobile number'}}},
                    location :{ validators:{ notEmpty:{ message: 'Location is required '}}},
                    email:{
                        verbose: false,
                        validators:{
                            notEmpty:{ message:"Email is required"},
                            emailAddress: { message: 'The email address is not valid'},
                            stringLength: { max: 512,message: 'Cannot exceed 512 characters'}
                        }
                    }
                }
            })
            .on('success.fv.form', function(e){
                e.preventDefault();
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });





    });

})(jQuery, window);