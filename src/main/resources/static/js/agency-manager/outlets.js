(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){
        //Init custom validators
        FormValidation.Validator.mobile = utils.mobileValidator;
        var ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="name"]', ui).val( o.name );
                $('input[name="email"]', ui).val( o.email );
                $('input[name="mobileNo"]', ui).val( o.mobileNo );
                $('input[name="floatAccount"]', ui).val( o.floatAccount );
                $('input[name="commissionAccount"]', ui).val( o.commissionAccount );
                $('input[name="location"]', ui).val( o.location );
                $('select[name="feeGroupNo"]', ui).val( o.feeGroupNo );
                $('select[name="agentNo"]', ui).val( o.agentNo );
                $('input[name="action"]', ui).val('edit' );
                $('input[name="id"]', ui).val( o.id );
                ui.modal('show');

                fnFetchAccounts( o.agentNo);
            };
        utils.makerchecker.initTables( fnEdit );

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                utils.makerchecker.refreshMkTables();
            });
        });

        //Form validation
        $('form', ui)
            .formValidation({
                framework: 'bootstrap',
                fields:{
                    name: { validators:{ notEmpty :{ message: 'Name is required'}}},
                    mobileNo:{ validators:{ mobile:{ message: 'Invalid mobile number'}}},
                    location :{ validators:{ notEmpty:{ message: 'Location is required '}}},
                    commissionAccount :{ validators:{ notEmpty:{ message: 'Commission account is required '}}},
                    floatAccount :{ validators:{ notEmpty:{ message: 'Float account is required '}}},
                    agentNo :{ validators:{ notEmpty:{ message: 'Agent is required '}}},
                    email:{
                        verbose: false,
                        validators:{
                            notEmpty:{ message:"Email is required"},
                            emailAddress: { message: 'The email address is not valid'},
                            stringLength: { max: 512,message: 'Cannot exceed 512 characters'}
                        }
                    }
                }
            })
            .on('success.fv.form', function(e){
                e.preventDefault();
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });

        let fnFetchAccounts = function( agentNo){
            let url = "/agent/commission/";

            let finalUrl = url.concat(agentNo);




            $.get(finalUrl).done( res =>{



                let floatAccount = document.getElementById("floatAccount");
                let commissionsAccount = document.getElementById("commissionsAccount");
                let branchName = document.getElementById("branchName");

                floatAccount.value = res[0];
                commissionsAccount.value = res[1];
                branchName.value = res[2];



            });
        };

        $("form", ui).find("[name='agentNo']").change( function(){
            let agentNo = $(this).val();

            //Update the list of groups
            fnFetchAccounts( agentNo );
        });

    });

})(jQuery, window);