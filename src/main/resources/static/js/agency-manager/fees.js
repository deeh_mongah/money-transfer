(function($, window){
    //Global ajax setup
    'use strict';
    var token = $("meta[name='_csrf']").attr("content"),
        header = $("meta[name='_csrf_header']").attr("content");
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
        xhr.setRequestHeader("Accept", "application/json");
    });

    $(function(){
        //Init custom validators
        FormValidation.Validator.mobile = utils.mobileValidator;
        var ui = $('.edit-view'),
            fnEdit = function( o ){
                $('input[name="name"]', ui).val( o.name );
                $('input[name="minAmount"]', ui).val( o.minAmount);
                $('input[name="maxAmount"]', ui).val( o.maxAmount );
                $('select[name="feeGroupNo"]', ui).val( o.feeGroupNo );
                $('select[name="feeOriginNo"]', ui).val( o.feeOriginNo );
                $('select[name="paymentMode"]', ui).val( o.paymentMode ).trigger("change");
                $('select[name="transactionTypeNo"]', ui).val( o.transactionTypeNo );
                $('select[name="channelNo"]', ui).val( o.channelNo );

                $('input[name="outletAmount"]', ui).val( o.outletAmount );
                $('input[name="saccoAmount"]', ui).val( o.saccoAmount );
                $('input[name="binaryAmount"]', ui).val( o.binaryAmount );
                $('input[name="saccoRatio"]', ui).val( o.saccoRatio );
                $('input[name="outletRatio"]', ui).val( o.outletRatio );
                $('input[name="binaryRatio"]', ui).val( o.binaryRatio );

                $('input[name="name"]', ui).val( o.name );
                $('input[name="action"]', ui).val('edit' );
                $('input[name="id"]', ui).val( o.id );
                ui.modal('show');
            };
        utils.makerchecker.initTables( fnEdit );

        //When to refresh table info
        $('[data-action="refresh"]').click( function(){
            setTimeout(function () {
                utils.makerchecker.refreshMkTables();
            });
        });

        //Form validation
        $('form', ui)
            .find('[name="paymentMode"]').change( function(){
                var paymentMode = this.value;
                if( "1" === paymentMode ){
                    $(".fixed-mode").toggleClass("d-none", false);
                    $(".percentage-mode").toggleClass("d-none", true);
                }
                else if( "2" === paymentMode ){
                    $(".fixed-mode").toggleClass("d-none", true);
                    $(".percentage-mode").toggleClass("d-none", false);
                }
                else{
                    $(".fixed-mode").toggleClass("d-none", true);
                    $(".percentage-mode").toggleClass("d-none", true);
                }

            }).end()
            .formValidation({
                framework: 'bootstrap',
                fields:{
                    name: { validators:{ notEmpty :{ message: 'Name is required'}}},
                    transactionTypeNo: { validators:{ notEmpty :{ message: 'Transaction type is required'}}},
                    minAmount: { validators:{ notEmpty :{ message: 'Minimum amount is required'}}},
                    maxAmount: { validators:{ notEmpty :{ message: 'Maximum amount is required'}}},
                    feeGroupNo: { validators:{ notEmpty :{ message: 'Fee group is required'}}},
                    feeOriginNo: { validators:{ notEmpty :{ message: 'Fee origin is required'}}},
                    channelNo: { validators:{ notEmpty :{ message: 'Channel is required'}}},
                    paymentMode: { validators:{ notEmpty :{ message: 'Payment mode is required'}}},
                    outletAmount: { validators:{ notEmpty :{ message: 'Outlet amount is required'}}},
                    saccoAmount: { validators:{ notEmpty :{ message: 'SACCO amount is required'}}},
                    binaryAmount: { validators:{ notEmpty :{ message: 'Binary amount is required'}}},
                    outletRatio: { validators:{ notEmpty :{ message: 'Outlet ratio is required'}}},
                    saccoRatio: { validators:{ notEmpty :{ message: 'SACCO ratio is required'}}},
                    binaryRatio: { validators:{ notEmpty :{ message: 'Binary ratio is required'}}},
                }
            })
            .on('success.fv.form', function(e){
                e.preventDefault();
                utils.submitForm( $(this).serializeArray(), ui);
            });

        //House keep
        ui.on('hidden.bs.modal', function(){
            utils.houseKeep( ui );
            //always have this default action
            $('input[name="action"]', ui).val('new');
        });

    });

})(jQuery, window);