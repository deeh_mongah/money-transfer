{
    let fnStatusLabels = status =>{
        let label;
        switch( status ){
            case "Success":
                label = "primary";
                break;
            case "Failed":
                label = "danger";
                break;
        }
        return "<span class='badge badge-"+label+"'> " + status +" </span>";
    };

    let statusIndex = 4;
    let oTable = utils.dataTable({
        table : $(".table")
    });

    //When to refresh table info
    $('[data-action="refresh"]').click( function(){
        setTimeout(function () {
            oTable.fnDraw();
        });
    });


}