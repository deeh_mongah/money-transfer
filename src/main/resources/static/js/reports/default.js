$(() => {
    let $$ = $(document);

    const parent = $(".parent-panel");
    const child = $(".child-panel");
    let childTable;
    const parentTable = $('.parent-table');

    //Custom Events
    // Event fired before
    let drawStart = table => {
        let e = $.Event('tableDrawStart');
        e.table = table;
        $(window).trigger(e);
    };

    let drawing = (row, aData) => {
        let e = $.Event('tableDrawing');
        e.tableRow = row;
        e.aData = aData;
        e.data = aData;
        $(window).trigger( e );
    };

    // Fired when last row is drawn
    let drawEnd = table => {
        let e = $.Event('tableDrawEnd');
        e.table = table;
        $(window).trigger( e );
    };


    // handle tables
    let columnCount = $('thead th', parentTable).length;
    let rowCounter = 0;

    let oTable = utils.dataTable({
        table: parentTable,

        fnRowCallback: (nRow, aData) => {
            drawing(nRow, aData);
            if (rowCounter === 0)
                drawStart( parentTable );

            if (child.length > 0) {

                let rowId = aData[ columnCount ] ;
                let rowTitle = aData[ 0 ];

                $(nRow).children('td:eq(0)').html('<a href="#" class="childTrigger" data-id="' + rowId + '">' + rowTitle + '</a>');
                $('a', nRow).first().on('click', e => {
                    e.preventDefault();
                    parent.hide();
                    child.toggleClass("d-none");

                    $('.card-title', child).html( rowTitle );

                    childTable = utils.dataTable({
                        table: $(".child-table"),
                        fnServerParams: ( aoData ) => {
                            aoData.push(
                                {name: 'data', value: rowId},
                                {name: 'status', value: $('.status > .title-btn', child).data('index') },
                                {name: 'startDate', value: $('.date-range span', child).data('start-date')},
                                {name: 'endDate', value: $('.date-range span', child).data('end-date')}
                            );
                        }
                    });
                });

            }
            rowCounter++;
        },
        fnServerParams: function (aoData) {
            aoData.push(
                {name: 'status', value: $('.status > .title-btn', parent).data('index') },
                {name: 'startDate', value: $('.date-range span', parent).data('start-date')},
                {name: 'endDate', value: $('.date-range span', parent).data('end-date')}
            );
            return aoData;
        },
    });


    $$.on('click', '.btn-info', () => {
        if (!parent.is(":visible")) {
            parent.show();
            child.toggleClass("d-none");
            childTable.fnDestroy();
            childTable = undefined;
        }
    });

    $('.download-report a').each(function () {
        $(this).attr('href', window.location.pathname + "/" + $(this).data('action'));
    });

    $('.btn-success', child).click(function () {
        if (child.is(":visible")) {
            childTable.fnDraw();
        }
    });

    utils.initReportDateRange(child, function () {
        childTable.fnDraw();
    });

    $('.btn-success', parent).click(function () {
        if (parent.is(":visible")) {
            oTable.fnDraw();
        }
    });

    //When updating drop-down filters of parent panel
    $(".filter a", parent).click(function (e) {
        e.preventDefault();
        e = $(this).parents()[1];
        let css = $(this).data("filter");
        $('.title-btn', e).html($(this).html());
        $('.title-btn', e).data('index', $(this).data('index'));

        console.log("css ->", css);

        $("." + css + ' > .title-btn', child).html($(this).html());
        $("." + css + ' > .title-btn', child).data('index', $(this).data('index'));
        oTable.fnDraw();
    });

    //When updating drop-down filters of child panel
    $(".filter a", child).click(function (e) {
        e.preventDefault();
        e = $(this).parents()[1];
        $('.title-btn', e).html($(this).html());
        $('.title-btn', e).data('index', $(this).data('index'));
        childTable.fnDraw();
    });

    utils.initReportDateRange('', function (start, end) {
        try {
            let daterange = $('.date-range');

            daterange.data('daterangepicker').setStartDate(start);
            daterange.data('daterangepicker').setEndDate(end);

            $('.date-range span')
                .html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY'))
                .data('start-date', start.format('YYYY-MM-DD'))
                .data('end-date', end.format('YYYY-MM-DD'));
            if (typeof childTable === 'undefined') {
                oTable.fnDraw();
            }
            else {
                childTable.fnDraw();
            }
        }
        catch (e) {
            console.error(e);
        }

    });

    // utils.initReportDateRange(parent, function(){ oTable.fnDraw(); });
});