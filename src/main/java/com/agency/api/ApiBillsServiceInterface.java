package com.agency.api;

import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.AirtimePurchaseRequest;
import com.agency.api.vm.NairobiWaterRequest;

public interface ApiBillsServiceInterface {

    /**
     * Airtime purchase
     *
     * @param request
     * @return ResponseModel
     */
    public ResponseModel airrtimePurchase(AirtimePurchaseRequest request) throws Exception;

    /**
     * Nairobi Water Purchase
     *
     * @param request
     * @return ResponseModel
     */
    public ResponseModel nairobiWater(NairobiWaterRequest request) throws Exception;
}
