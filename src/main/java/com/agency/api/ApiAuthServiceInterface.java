package com.agency.api;

import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;

import java.io.IOException;

public interface ApiAuthServiceInterface {

    /**
     * Authenticate user
     *
     * @param request
     * @return ResponseModel
     */
    public ResponseModel login(LoginRequest request) throws Exception;


    public ResponseModel customerLogin(CustomerLoginRequest request) throws Exception;

    public ResponseModel changePassword(SetPinRequest request);


    public ResponseModel pinReset(SetPinRequest request);

    public AuthServiceResponseVm test(AuthServiceRequestVm request);

    public ResponseModel otpValidateRequest(OptValidateRequest request);


    public ResponseModel agentOtpValidateRequest(OptValidateRequest request);


    public ResponseModel createUser(CreateCustomerRequest request);

    public ResponseModel phoneValidate(String phone);

    public ResponseModel agentPasswordReset(String phone);

    public ResponseModel passwordReset(String phone);

    /**
     * Fetch details with passport
     *
     * @param passport
     * @return ResponseModel
     */
    public IPRSResponse fetchDetails(String passport) throws Exception;
}
