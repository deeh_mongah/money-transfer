package com.agency.api.services;

import com.agency.api.ApiBillsServiceInterface;
import com.agency.api.http.MfmHttpService;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.AirtimePurchaseRequest;
import com.agency.api.vm.NairobiWaterRequest;
import com.agency.web.terminals.entities.Terminals;
import com.agency.web.terminals.repository.TerminalRepository;
import com.agency.web.transactions.entities.TransactionTypes;
import com.agency.web.transactions.entities.Transactions;
import com.agency.web.transactions.repository.TransactionTypesRepository;
import com.agency.web.transactions.repository.TransactionsRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class ApiBillsService implements ApiBillsServiceInterface {

    @Autowired private TerminalRepository terminalRepository;
    @Autowired private TransactionsRepository transactionsRepository;
    @Autowired private TransactionTypesRepository transactionTypesRepository;

    @Autowired private Environment env;
    @Autowired private ObjectMapper objectMapper;
    @Autowired private MfmHttpService mfmHttpService;

    /**
     * Airtime purchase
     *
     * @param request
     * @return ResponseModel
     */
    @Override
    public ResponseModel airrtimePurchase(AirtimePurchaseRequest request) throws Exception{

//        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );
//        if( !oTerminal.isPresent() ){
//            return new ResponseModel(
//                    "01",
//                    "Unknown terminal"
//            );
//        }
//
//        Terminals terminal = oTerminal.get();
//        Long terminalNo = terminal.getId();
//
//        if( !terminal.getFlag().equals( "1") ){
//            return new ResponseModel(
//                    "01",
//                    "Terminal is inactive"
//            );
//        }

        //Handle bill payment
        String mfmUri = env.getProperty("mfm.uris.airtime");
        Map<String, Object> map = new HashMap<>();
        map.put("amount", request.getAmount() );
//        map.put("phone", request.getPhone() );
        map.put("telco", request.getServiceProvider() );
        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                mfmUri,
                map
        );

        JsonNode jsonNode = objectMapper.readTree( responseEntity.getBody() );
        String status = "";
        String message = jsonNode.get("message").asText();

        //When all went well
        if( responseEntity.getStatusCode().is2xxSuccessful() ){

//            //Populate transaction
//            Long transactionTypeNo = null ;
//            Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.TRANSFER );
//            if( olTransactionType.isPresent() ){
//                transactionTypeNo = olTransactionType.get().getId();
//            }
//
//            Transactions transaction = new Transactions();
//            transaction
//                    .setTerminalNo( terminalNo )
//                    .setReceiptNo( null )
//                    .setAccountNo( "N/A" )
//                    .setTransactionTypeNo( transactionTypeNo )
//                    .setOfficerId( request.getUserId() )
//                    .setAmount( request.getAmount() );
//            transactionsRepository.save( transaction );
//
//            String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');
//
//            //Update transaction
//            transaction.setReceiptNo( receiptNo );
//            transactionsRepository.save( transaction );

            map.clear();
            map.put("receiptNo", "ytrsd");
            map.put("charges", BigDecimal.ZERO );

            return new ResponseModel(
                    "00",
                    "Airtime purchased successfully",
                    map
            );

        }
        else{
            return new ResponseModel(
                    "01",
                    "Sorry! You request could't be completed at the moment; try again later"
            );
        }
    }

    /**
     * Nairobi Water Purchase
     *
     * @param request
     * @return ResponseModel
     */
    @Override
    public ResponseModel nairobiWater(NairobiWaterRequest request) throws Exception{
        Map<String, Object> map = new HashMap<>();

        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );
        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        String accountNumber = request.getAccountNumber();
        String phone = request.getPhone();
        BigDecimal amount = request.getAmount();

//        //1. Initiate transaction
//        String uri = env.getProperty("mfm.uris.nairobi-water.initiate");
//        Map<String, Object> map = new HashMap<>();
//        map.put("amount",  amount );
//        map.put("phone_number",phone );
//        map.put("account_number", accountNumber );
//        map.put("names", request.getNames() );
//
//        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
//                uri,
//                map
//        );
//
//        JsonNode jsonNode = objectMapper.readTree( responseEntity.getBody() );
//        String status = jsonNode.get("status").asText();
//        String message = jsonNode.get("message").asText();
//
//        //When to proceed with payment
//        if( "00".equals( status ) ){
//
//            uri = env.getProperty("mfm.uris.nairobi-water.complete");
//            String transactionId = jsonNode.get("data").get("TransactionID").asText();
//            map.put("transaction_id", transactionId);
//            responseEntity = mfmHttpService.postRequest(
//                    uri,
//                    map
//            );
//        }
//        else{
//            return new ResponseModel(
//                    "01",
//                    "Sorry! You request couldn't be completed at the moment; try again later"
//            );
//        }

        //When all went well
//        if( responseEntity.getStatusCode().is2xxSuccessful() ){

            //Populate transaction
            Long transactionTypeNo = null ;
            Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
            if( olTransactionType.isPresent() ){
                transactionTypeNo = olTransactionType.get().getId();
            }

            Transactions transaction = new Transactions();
            transaction
                    .setTerminalNo( terminalNo )
                    .setReceiptNo( null )
                    .setAccountNo( "N/A" )
                    .setTransactionTypeNo( transactionTypeNo )
                    .setOfficerId( request.getUserId() )
                    .setAmount( request.getAmount() );
            transactionsRepository.save( transaction );

            String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

            //Update transaction
            transaction.setReceiptNo( receiptNo );
            transactionsRepository.save( transaction );

            map.clear();
            map.put("receiptNo", receiptNo);
            map.put("charges", BigDecimal.ZERO );

//            return new ResponseModel(
//                    "01",
//                    "Sorry! You request could't be completed at the moment; try again later"
//            );
//        }
//        else{
            return new ResponseModel(
                    "00",
                    "Transaction Successful"
            );
//        }
    }
}
