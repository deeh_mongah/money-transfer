package com.agency.api.services;

import java.security.SecureRandom;

public class HelperUtil {

    /**
     * Generate OTP
     *
     * @param len length of token
     * @return random numeric code
     */
    public static String generatePhoneToken(int len) {
        SecureRandom sr = new SecureRandom();
        String result = (sr.nextInt(9) + 1) + "";
        for (int i = 0; i < len - 2; i++) result += sr.nextInt(10);
        result += (sr.nextInt(9) + 1);
        return result;
    }
}
