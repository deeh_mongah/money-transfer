package com.agency.api.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Service
public class MpesaApiService {

    @Value("${mpesa.url}")
    private String baseUrl;



    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private ObjectMapper objectMapper;

    private HttpHeaders createHeaders() {
        return new HttpHeaders() {
            {
                set("Content-Type", MediaType.APPLICATION_JSON.toString());
                set("Accept", "application/json");
            }
        };
    }

    /**
     *
     * @param uri
     * @param request
     * @return ResponseEntity<String>
     * @throws Exception
     */
    public ResponseEntity<String> postRequest(String uri, Object request ) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents = fromHttpUrl( baseUrl ).path( uri ).build().encode();
        HttpHeaders httpHeaders = createHeaders();
        String jsonRequest = objectMapper.writeValueAsString(request);

        HttpEntity<?> httpEntity = new HttpEntity<>( jsonRequest, httpHeaders );
        return restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );
    }


}
