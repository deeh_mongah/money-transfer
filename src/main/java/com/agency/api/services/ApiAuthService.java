package com.agency.api.services;

import com.agency.api.ApiAuthServiceInterface;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;
import com.agency.core.sms.SmsOptions;
import com.agency.core.sms.SmsService;
import com.agency.core.template.AppConstants;
import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.entities.Customers;
import com.agency.web.agency.entities.Fees;
import com.agency.web.agency.entities.Outlets;
import com.agency.web.agency.repository.AgentsRepository;
import com.agency.web.agency.repository.CustomersRepository;
import com.agency.web.agency.repository.FeeRepository;
import com.agency.web.terminals.entities.OfficerTypes;
import com.agency.web.terminals.entities.TerminalOfficers;
import com.agency.web.terminals.entities.Terminals;
import com.agency.web.terminals.repository.TerminalOfficersRepository;
import com.agency.web.terminals.repository.TerminalRepository;
import com.agency.web.usermanager.entities.Users;
import com.agency.web.wallets.entity.CustomersWallets;
import com.agency.web.wallets.entity.OutletWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.CustomerWalletsRepository;
import com.agency.web.wallets.repository.OutletWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.*;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Service
@Transactional
public class ApiAuthService implements ApiAuthServiceInterface {

    @Autowired private Environment env;
    @Autowired private TerminalOfficersRepository terminalOfficersRepository;
    @Autowired private CustomersRepository customersRepository;
    @Autowired private AgentsRepository agentsRepository;
    @Autowired private OutletWalletsRepository outletWalletsRepository;
    @Autowired private WalletsRepository walletsRepository;
    @Autowired private CustomerWalletsRepository customerWalletsRepository;
    @Autowired private FeeRepository feeRepository;
    @Autowired private RestTemplate restTemplate;
    @Autowired private FlexApiService flexApiService;


    @Autowired private SmsService smsService;

    public static String PASSWORD = "17d0609065b48c3164e49bf17ed54efe";
    public static String SYSTEMCODE = "3";
    public static String TIMESTAMP = "20190221200814";
    public static String USERCODE = "290";

    /**
     * Authenticate user
     *
     * @param request
     * @return ResponseModel
     */
    public ResponseModel login(LoginRequest request) throws Exception {




        Optional<Agents> oAgent = agentsRepository.findByAgentCodeAndFlag(request.getUserId(), AppConstants.STATUS_ACTIVERECORD);

        if( !oAgent.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown officer"
            );
        }

        //Check the credentials
        Agents agents = oAgent.get();
        boolean isCorrectPin = agents.getPin().endsWith( request.getPin() );

        if( !isCorrectPin ){
            return new ResponseModel(
                    "01",
                    "Invalid credentials"
            );
        }

        List<OutletWallets> outletWalletsList = outletWalletsRepository.findByOutletNo( agents.getId() );

//        OutletWallets outletWallets = outletWalletsList.get(0);


        List<AgentBalanceResponse> agentBalanceResponses = new ArrayList<>() ;

        for(OutletWallets k: outletWalletsList){

            AgentBalanceResponse resp= new AgentBalanceResponse();
            resp.setCurrency( k.getWalletsLink().getCurrencyTypeWalletsLink().getName());
            resp.setAgentBalance( k.getWalletsLink().getAmount());

            agentBalanceResponses.add( resp );

        }


        String uri = env.getProperty("flex.uris.forexRate");

        ObjectMapper mapper = new ObjectMapper();

        FlexForexRateApiCommonRequestVm requestVm = new FlexForexRateApiCommonRequestVm();

        requestVm.setPassword(PASSWORD);
        requestVm.setSysCode(SYSTEMCODE);
        requestVm.setTimestamp(TIMESTAMP);
        requestVm.setUserCode(USERCODE);
        requestVm.setCurrPair("USD - KES");
        requestVm.setCurrPair("SSP - KES");


        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, requestVm);

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        System.err.println("<><><>jsonNode<><><>"+ jsonNode);

        ForexRateResponseVm [] forexRateResponseVm = mapper.readValue(jsonNode.toString(), ForexRateResponseVm[].class);


        int rate = 0;
        for (ForexRateResponseVm node : forexRateResponseVm){

            System.err.println("<><><>node.getRate()<><><>"+ node.getRate());

            rate = node.getRate();
        }


        Map<String, Object> map = new HashMap<>();
        map.put("name", agents.getName() );
        map.put("phone", agents.getPhone() );
//        map.put("floatBalance", outletWallets.getWalletsLink().getAmount() );
        map.put("floatBalance", agentBalanceResponses );
        map.put("USD - KES", rate);
        map.put("SSP - KES", rate);

        return new ResponseModel(
                "00",
                "Login successful",
                map
        );
    }

    @Override
    public ResponseModel customerLogin(CustomerLoginRequest request) throws Exception {

        Optional<Customers> oCustomers = customersRepository.findByPhone(request.getPhoneNumber());

        if (!oCustomers.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown customer"
            );
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        Customers customers = oCustomers.get();

        if (!encoder.matches(request.getPassword(), customers.getPassword())){

            return new ResponseModel(
                    "01",
                    "Incorrect Password"

            );
        }


        List<Fees> feesList = feeRepository.findByFlag(AppConstants.STATUS_ACTIVERECORD);

        List<FeeRequestVm> feeRequestVmList = new ArrayList<>();

        for (Fees node: feesList){

            FeeRequestVm feeRequestVm = new FeeRequestVm();

            feeRequestVm.setName(node.getName());
            feeRequestVm.setId(node.getId());
            feeRequestVm.setMaxAmount(node.getMaxAmount());
            feeRequestVm.setMinAmount(node.getMinAmount());
            feeRequestVm.setTransactionType(node.getTransactionTypeLink().getCode());
            feeRequestVm.setTransactingAmount(String.valueOf(node.getTransactionAmount()));
            feeRequestVmList.add(feeRequestVm);

        }

        customers.setFirstTimeLogin("false");

        customersRepository.save(customers);

        CustomersWallets customersWallets = customerWalletsRepository.findByCustomerNo( customers.getId()).get();


        String uri = env.getProperty("flex.uris.forexRate");

        ObjectMapper mapper = new ObjectMapper();

        FlexForexRateApiCommonRequestVm requestVm = new FlexForexRateApiCommonRequestVm();

        requestVm.setPassword(PASSWORD);
        requestVm.setSysCode(SYSTEMCODE);
        requestVm.setTimestamp(TIMESTAMP);
        requestVm.setUserCode(USERCODE);
        requestVm.setCurrPair("USD - KES");


        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, requestVm);

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        System.err.println("<><><>jsonNode<><><>"+ jsonNode);


        ForexRateResponseVm [] forexRateResponseVm = mapper.readValue(jsonNode.toString(), ForexRateResponseVm[].class);


        int rate = 0;
        for (ForexRateResponseVm node : forexRateResponseVm){

            System.err.println("<><><>node.getRate()<><><>"+ node.getRate());

            rate = node.getRate();
        }

        Map<String, Object> map = new HashMap<>();

        map.put("accountBalance", customersWallets.getWalletsLink().getAmount());
        map.put("firstName", customersWallets.getCustomersLink().getFirstName());
        map.put("surName",customersWallets.getCustomersLink().getSurname() );
        map.put("passport",customersWallets.getCustomersLink().getPassport() );
        map.put("fees", feeRequestVmList );
        map.put("USD - KES", rate);

        return new ResponseModel(
                "00",
                "Login successful",
                map

        );
    }

    @Override
    public ResponseModel pinReset(SetPinRequest request) {
        Optional<Agents> oAgent = agentsRepository.findByPhone(request.getPhoneNumber());

        if (!oAgent.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown Agent"
            );
        }

        Agents agent = oAgent.get();
        agent.setPin( request.getPassword() );

        agentsRepository.save( agent );

        return new ResponseModel(
                "00",
                "Pin reset successful"

        );
    }

    @Override
    public ResponseModel changePassword(SetPinRequest request) {
        Optional<Customers> oCustomers = customersRepository.findByPhone(request.getPhoneNumber());

        if (!oCustomers.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown customer"
            );
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        Customers customers = oCustomers.get();
        customers.setPassword(encoder.encode(request.getPassword()));

        customersRepository.save(customers);

        return new ResponseModel(
                "00",
                "Password Changed successful"

        );
    }

    @Override
    public AuthServiceResponseVm test(AuthServiceRequestVm request) {

        AuthServiceResponseVm authServiceResponseVm = new AuthServiceResponseVm();

        authServiceResponseVm.setStatus("000");
        authServiceResponseVm.setDescription("Request successful");

        AuthServiceResponseVm.DataBean dataBean = new AuthServiceResponseVm.DataBean();
        dataBean.setPin_setup_url("http://localhost:8081/api/v3/user/pin/setup/2cb4340c1abe98bfb81bc20aad37e03fa684b675eac3d29392902033aa56e50e43a53e11c8a7c8527765f29d314f380c349d6cb5a9a57d539293b9c8cfff99ed");
        dataBean.setSession_identifier("2cb4340c1abe98bfb81bc20aad37e03fa684b675eac3d29392902033aa56e50e43a53e11c8a7c8527765f29d314f380c349d6cb5a9a57d539293b9c8cfff99ed");


        authServiceResponseVm.setData(dataBean);


        return authServiceResponseVm;
    }

    @Override
    public ResponseModel otpValidateRequest(OptValidateRequest request) {

        Optional<Customers> oCustomers = customersRepository.findByPhone(request.getPhoneNumber());

        if (!oCustomers.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown customer"
            );
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        Customers customers = oCustomers.get();

        if (!encoder.matches(request.getOpt(), customers.getPassword())){

            return new ResponseModel(
                    "01",
                    "Incorrect Otp"

            );
        }


        return new ResponseModel(
                "00",
                "Otp successful"

        );
    }


    @Override
    public ResponseModel agentOtpValidateRequest(OptValidateRequest request) {

        Optional<Agents> oAgent = agentsRepository.findByPhone(request.getPhoneNumber());

        if (!oAgent.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown agent"
            );
        }

        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

        Agents agent = oAgent.get();

        if (!request.getOpt().equals(agent.getPin())){

            return new ResponseModel(
                    "01",
                    "Incorrect Otp"

            );
        }

        return new ResponseModel(
                "00",
                "Otp successful"

        );
    }

    @Override
    public ResponseModel createUser(CreateCustomerRequest request) {

        Optional<Customers> oCustomers = customersRepository.findByPhone(request.getMobileNo());

        if (oCustomers.isPresent()){
            return new ResponseModel(
                    "01",
                    "Please Enter Unique Number"
            );
        }

        Customers customers = new Customers();

        customers.setFirstName(request.getFirstName());
        customers.setSurname(request.getSurName());
        customers.setEmail(request.getEmail());
        customers.setPhone(request.getMobileNo());
        customers.setFlag(AppConstants.STATUS_ACTIVERECORD);
        customers.setPhone(request.getMobileNo());
        customers.setCreatedOn( new Date( System.currentTimeMillis() ));
        customers.setUpdatedOn( new Date( System.currentTimeMillis() ));

        customersRepository.save(customers);

        Wallets wallets = new Wallets();

        wallets.setAmount(BigDecimal.ZERO);

        walletsRepository.save(wallets);

        CustomersWallets customersWallets = new CustomersWallets();
        customersWallets.setCustomerNo(customers.getId());
        customersWallets.setWalletNo(wallets.getId());


        customerWalletsRepository.save(customersWallets);



        String message = "Dear "+request.getFirstName()+" you have been created successfully, please log in using flex mobile app to setup your account thank you" ;

        boolean send = smsService.sendSMS(new SmsOptions()
                .setMessage(message)
                .setMobileNo(request.getMobileNo())
        );



        return new ResponseModel(
                "00",
                "User Created successful"
        );
    }

    @Override
    public ResponseModel phoneValidate(String phone) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        Map<String, Object> map = new HashMap<>();

        Optional<Customers> oCustomer = customersRepository.findByPhone(phone);

        if (oCustomer.isPresent()){

            Customers customers = oCustomer.get();

            if ("true".equals(customers.getFirstTimeLogin())){

                String authToken = HelperUtil.generatePhoneToken( 4 );
                String message = "Please enter this otp : " + authToken ;

                boolean send = smsService.sendSMS(new SmsOptions()
                        .setMessage(message)
                        .setMobileNo(customers.getPhone())
                );

                customers.setPassword(passwordEncoder.encode( authToken));

                customersRepository.save(customers);

                map.put("firstTimeLogIn", "true");

                return new ResponseModel(
                        "00",
                        "Transaction successful",
                        map

                );

            }else {
                map.put("firstTimeLogIn", "false");

                return new ResponseModel(
                        "00",
                        "Transaction successful",
                        map

                );
            }
        }

        return new ResponseModel(
                "01",
                "Unknown user",
                map

        );
    }

    @Override
    public ResponseModel passwordReset(String phone) {

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        Optional<Customers> oCustomer = customersRepository.findByPhone(phone);

        if (oCustomer.isPresent()) {

            Customers customers = oCustomer.get();
            String authToken = HelperUtil.generatePhoneToken(4);
            String message = "Please enter this otp : " + authToken;

            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo(customers.getPhone())
            );

            customers.setPassword(passwordEncoder.encode(authToken));
            customers.setFirstTimeLogin("true");

            customersRepository.save(customers);
        }

            return new ResponseModel(
                    "00",
                    "Transaction successful"

            );
    }



    @Override
    public ResponseModel agentPasswordReset(String phone) {


        Optional<Agents> oAgent = agentsRepository.findByPhone(phone);

        if (oAgent.isPresent()) {

            Agents customers = oAgent.get();
            String authToken = HelperUtil.generatePhoneToken(4);
            String message = "Please enter this otp : " + authToken;

            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo(customers.getPhone())
            );

            customers.setPin( authToken );

            agentsRepository.save(customers);
        }

        return new ResponseModel(
                "00",
                "Transaction successful"

        );
    }

    /**
     * Fetch details with passport
     *
     * @param passport
     * @return ResponseModel
     */
    @Override
    public IPRSResponse fetchDetails(String passport) throws Exception{
        //IPRS Call
        Map<String, String> request = new HashMap<>();
        ObjectMapper mapper = new ObjectMapper();
        request.put("passportNo", passport);


        UriComponents uriComponents = fromHttpUrl( env.getProperty("iprs.endpoint") )
               .build().encode();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType( MediaType.APPLICATION_JSON );

        HttpEntity<?> httpEntity = new HttpEntity<>(mapper.writerWithDefaultPrettyPrinter().writeValueAsString( request ), httpHeaders);

        //Hit IPRS for data
        ResponseEntity<String> rawResponse = restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );
        return mapper.readValue( rawResponse.getBody(), IPRSResponse.class );
    }
}
