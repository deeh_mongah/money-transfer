package com.agency.api.services;

import com.agency.api.FlexServiceInterface;
import com.agency.api.http.MfmHttpService;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;
import com.agency.core.sms.SmsOptions;
import com.agency.core.sms.SmsService;
import com.agency.core.template.AppConstants;
import com.agency.core.template.PhoneNumberFormatter;
import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.entities.Customers;
import com.agency.web.agency.entities.Fees;
import com.agency.web.agency.repository.AgentsRepository;
import com.agency.web.agency.repository.CustomersRepository;
import com.agency.web.agency.repository.FeeRepository;
import com.agency.web.transactions.entities.CommmisionTransaction;
import com.agency.web.transactions.entities.TransactionInternal;
import com.agency.web.transactions.entities.TransactionTypes;
import com.agency.web.transactions.entities.Transactions;
import com.agency.web.transactions.repository.CommissionTransactionRepository;
import com.agency.web.transactions.repository.TransactionInternalRepository;
import com.agency.web.transactions.repository.TransactionTypesRepository;
import com.agency.web.transactions.repository.TransactionsRepository;
import com.agency.web.wallets.entity.*;
import com.agency.web.wallets.repository.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by ben on 07/11/2019.
 */
@Transactional
@Service
public class FlexService implements FlexServiceInterface {



    @Autowired private FlexApiService  flexApiService;
    @Autowired private MpesaApiService  mpesaApiService;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private Environment env;
    @Autowired private CustomersRepository customersRepository;
    @Autowired private AgentsRepository agentsRepository;
    @Autowired private CustomerWalletsRepository customerWalletsRepository;
    @Autowired private OutletWalletsRepository outletWalletsRepository;
    @Autowired private WalletsRepository walletsRepository;
    @Autowired private TransactionsRepository transactionsRepository;

    @Autowired private TransactionInternalRepository transactionInternalRepository;
    @Autowired private FeeRepository feeRepository;
    @Autowired private SmsService smsService;
    @Autowired private MfmHttpService mfmHttpService;
    @Autowired private CommissionTransactionRepository commissionTransactionRepository;
    @Autowired private CurrencyTypeWalletsRepository currencyTypeWallets;
    @Autowired private TransactionTypesRepository transactionTypesRepository;
    @Autowired private FlexWalletRepository flexWalletRepository;

    public static String PASSWORD = "17d0609065b48c3164e49bf17ed54efe";
    public static String SYSTEMCODE = "3";
    public static String TIMESTAMP = "20190221200814";
    public static String USERCODE = "290";

    @Override
    public ResponseModel getCountries() throws Exception {

        String uri = env.getProperty("flex.uris.countries");

        ObjectMapper mapper = new ObjectMapper();




        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, flexApiCommonRequestVm());

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        return new ResponseModel(
                "00",
                "Transaction Successful",
                jsonNode

        );
    }

    @Override
    public ResponseModel getCurrency() throws Exception {

        String uri = env.getProperty("flex.uris.currecy");

        ObjectMapper mapper = new ObjectMapper();

        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, flexApiCommonRequestVm());

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        return new ResponseModel(
                "00",
                "Transaction Successful",
                jsonNode

        );
    }



    @Override
    public ResponseModel getAgents() throws Exception {

        String uri = env.getProperty("flex.uris.agencies");

        ObjectMapper mapper = new ObjectMapper();




        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, flexApiCommonRequestVm());

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        return new ResponseModel(
                "00",
                "Transaction Successful",
                jsonNode

        );
    }

    @Override
    public ResponseModel getForexRate(String currPair) throws Exception {

        String uri = env.getProperty("flex.uris.forexRate");

        ObjectMapper mapper = new ObjectMapper();

        FlexForexRateApiCommonRequestVm requestVm = new FlexForexRateApiCommonRequestVm();

        requestVm.setPassword(PASSWORD);
        requestVm.setSysCode(SYSTEMCODE);
        requestVm.setTimestamp(TIMESTAMP);
        requestVm.setUserCode(USERCODE);
        requestVm.setCurrPair(currPair);


        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, requestVm);

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        System.err.println("<><><>jsonNode<><><>"+ jsonNode.toString());

        ForexRateResponseVm [] forexRateResponseVm = mapper.readValue(jsonNode.toString(), ForexRateResponseVm[].class);



        for (ForexRateResponseVm node : forexRateResponseVm){

            System.err.println("<><><>node.getRate()<><><>"+ node.getRate());
        }

        return new ResponseModel(
                "00",
                "Transaction Successful",
                jsonNode

        );
    }

    @Override
    public ResponseModel transactionHistory(String phoneNumber) throws Exception {


        Optional<Customers> oCustomer = customersRepository.findByPhone(phoneNumber);

        if (!oCustomer.isPresent()) {
            return new ResponseModel(
                    "01",
                    "Unknown Customer"

            );
        }

    Customers customers = oCustomer.get();

        String names  = customers.getFirstName()+ " "+ customers.getSurname();

        //Mini-statement data
        List<Transactions> transactions = transactionsRepository.findAllByNames( names );
        List<Object> data = new ArrayList<>();
        for( Transactions record : transactions ){
            Map<String, Object> node = new HashMap<>();
            Date createdOn  = record.getCreatedOn();


            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");

            String formattedDate =  sdf.format(createdOn);


            node.put("receiptNo", record.getReceiptNo() );
            node.put("amount", record.getAmount() );
            node.put("transactionType", record.getTransactionTypeLink().getShortCode() );
            node.put("dateTime",  formattedDate);
            node.put("status",  record.getFlag() );
            data.add( node );
        }

        Map<String, Object> map = new HashMap<>();
        map.put("transactions", data );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }

    @Override
    public ResponseModel newTransaction(FlexApiMobileRequest request) throws Exception {

        System.err.println("request.getAgentNumber()" + request.getAgentNumber());

        Optional<Customers> oCustomer = customersRepository.findByPhone(request.getSenderTelephoneNo());
        if (request.getAgentNumber() == null || request.getAgentNumber().isEmpty()) {

            if (!oCustomer.isPresent()) {
                return new ResponseModel(
                        "01",
                        "Unknown Customer"

                );
            }

            Customers customers = oCustomer.get();

            Optional<CustomersWallets> oCustomersWallets = customerWalletsRepository.findByCustomerNo(customers.getId());

            if (oCustomersWallets.isPresent()){

                CustomersWallets customersWallets = oCustomersWallets.get();

                Wallets wallets = customersWallets.getWalletsLink();

                BigDecimal balanceAmount = wallets.getAmount();

                int response;

                response = balanceAmount.compareTo(new BigDecimal(request.getPayingAmount()));

                if (response == 1){
                    return new ResponseModel(
                            "01",
                            "Insufficient Amount"

                    );
                }

                wallets.setAmount(balanceAmount.subtract(new BigDecimal(request.getPayoutAmount())));

                walletsRepository.save(wallets);
            }
        } else {

            Optional<Agents> oAgent = agentsRepository.findByPhone(request.getAgentNumber());

            if (!oAgent.isPresent()) {
                return new ResponseModel(
                        "01",
                        "Unknown Agent"

                );
            }

        }

        ObjectMapper mapper = new ObjectMapper();

        String uri = env.getProperty("flex.uris.newTransaction");

        FlexApiNewTransactionRequestVm requestVm = new FlexApiNewTransactionRequestVm();

        requestVm.setPayoutAmount(request.getPayoutAmount());
        requestVm.setSenderNationality("KEN");
        requestVm.setSenderDocType(request.getSenderDocType());
        requestVm.setReceiverSurname(request.getReceiverSurname());
        requestVm.setSenderOtherNames(request.getSenderOtherNames());
        requestVm.setPayingAmount(request.getPayingAmount());
        requestVm.setReceiverContactNo(request.getReceiverContactNo());
        requestVm.setPurposeforFunds(request.getPurposeforFunds());
        requestVm.setPayoutCountry("KEN");
        requestVm.setCurrencyPair(request.getCurrencyPair());
        requestVm.setSenderDocNumber(request.getSenderDocNumber());
        requestVm.setTransactionReference(request.getTransactionReference());
        requestVm.setOriginCountry("KEN");
        requestVm.setTransferType("inPerson");
        requestVm.setCurrencyRate(100);
        requestVm.setSenderSurname(request.getSenderSurname());
        requestVm.setReceiverOthernames(request.getReceiverOthernames());
        requestVm.setPayoutAgent("DXB");
        requestVm.setSysCode(SYSTEMCODE);
        requestVm.setPassword(PASSWORD);
        requestVm.setTariff("0.00");
        requestVm.setTimestamp(TIMESTAMP);
        requestVm.setUserCode(USERCODE);


        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, requestVm);

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        String errorMsg = jsonNode.get("errorMsg").asText();

        if (!errorMsg.equals("Success")){
            return new ResponseModel(
                    "01",
                    "Transaction Failed",
                    errorMsg

            );
        }

        String mpesa = env.getProperty("mpesa.uris.disbursement");

        B2CrequestVm b2CrequestVm = new B2CrequestVm();

        b2CrequestVm.setAmount(new BigDecimal(request.getPayoutAmount()));
        b2CrequestVm.setDescription("test");
        b2CrequestVm.setOccasion("test");
        b2CrequestVm.setResultUrl("https://flex-agency-demo.binary.co.ke/api/v1/loans/callback");
//        b2CrequestVm.setResultUrl("http://7359f06a.ngrok.io/api/v1/payment/callback");
        b2CrequestVm.setSource("718762");
        b2CrequestVm.setRecipientPhoneNo(request.getReceiverContactNo().substring(1));
        b2CrequestVm.setTransactionType("BusinessPayment");

        ResponseEntity<String> mpesaReponse = mpesaApiService.postRequest(mpesa, b2CrequestVm);

        JsonNode jsonMpesa = objectMapper.readTree( mpesaReponse.getBody() );

        String transactionId = jsonMpesa.get("transactionId").asText();



        TransactionTypes transactionTypes= transactionTypesRepository.findByName( TransactionTypes.WITHDRAWAL ).get();

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction.setSenderTelephoneNo(request.getSenderTelephoneNo());
        transaction.setReceiverContactNo(request.getReceiverContactNo());
        transaction
                .setMpesaTransactionNo(transactionId)
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypes.getId() )
                .setAmount( new BigDecimal(request.getPayoutAmount()) );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        if (request.getAgentNumber() == null || request.getAgentNumber().isEmpty()) {
            transaction.setAgentNames("N/A");
//            Optional<Customers> oCustomer = customersRepository.findByPhone(request.getSenderTelephoneNo());

            transaction.setAgentPresent("0");
            if(!oCustomer.isPresent()){
                transaction.setNames(request.getSenderTelephoneNo());

            }else{
                Customers customers = oCustomer.get();

                transaction.setNames(customers.getFirstName()+ " "+ customers.getSurname());


            }
        }else {
            Agents oAgent = agentsRepository.findByPhone(request.getAgentNumber()).get();
            transaction.setAgentNames(oAgent.getName());
            transaction.setNames(request.getSenderSurname() + request.getSenderOtherNames());
        }

        transactionsRepository.save( transaction );

        String customerName;

        Optional<Customers> optionalCustomers = customersRepository.findByPhone(request.getSenderTelephoneNo());

        if (optionalCustomers.isPresent()){
            Customers customers =optionalCustomers.get();

            customerName = customers.getFirstName() + " "+customers.getSurname();
        }else {
            customerName = request.getSenderSurname() +" "+ request.getSenderOtherNames();
        }


        String message = customerName +
                        " just sent "+request.getPayoutAmount()+" Ksh to your mpesa wallets using Flex Money Transfer. " +
                "Funds should reflect-in Your M-pesa account shortly!..." ;
        boolean send = smsService.sendSMS(new SmsOptions()
                .setMessage(message)
                .setMobileNo(request.getReceiverContactNo())
        );
        return new ResponseModel(
                "00",
                "Transaction Successful",
                jsonNode

        );
    }

    @Override
    public ResponseModel tarrif(TarrrifRateRequestVm request) throws Exception {
        String uri = env.getProperty("flex.uris.tarrif");

        ObjectMapper mapper = new ObjectMapper();

        FlexTarrifsRequestFlexVm requestVm = new FlexTarrifsRequestFlexVm();

        requestVm.setPassword(PASSWORD);
        requestVm.setSysCode(SYSTEMCODE);
        requestVm.setTimestamp(TIMESTAMP);
        requestVm.setUserCode(USERCODE);
        requestVm.setCurrPair(request.getCurrencyPair());
        requestVm.setAmount(request.getAmount());


        ResponseEntity<String> responseEntity = flexApiService.postRequest(uri, requestVm);

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        return new ResponseModel(
                "00",
                "Transaction Successful",
                jsonNode

        );
    }



    @Override
    public ResponseModel depositTransaction(DepositAgentRequestVm agentRequestVm) throws Exception {

        Optional<Agents> oAgents = agentsRepository.findByPhone(agentRequestVm.getAgentPhoneNumber());

        if (!oAgents.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown Agent "

            );
        }

        Optional<Customers> oCustomers = customersRepository.findByPhone(agentRequestVm.getUserPhoneNumber());

        if (!oCustomers.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown Customer "

            );
        }

        Customers customers = oCustomers.get();
        Agents agents = oAgents.get();

//        OutletWallets outletWallets = outletWalletsRepository.findByOutletNo(agents.getId()).get();
        List<OutletWallets> outletWalletsList = outletWalletsRepository.findByOutletNo(agents.getId());
//        OutletWallets outletWallets = outletWalletsList.get(0);

        CustomersWallets oCustomersWallets = customerWalletsRepository.findByCustomerNo(customers.getId()).get();


        Optional<CurrencyTypeWallets> currType = currencyTypeWallets.findByName( agentRequestVm.getCurrencyType() );

        BigDecimal agentBalance = BigDecimal.valueOf(0.00);


        for(OutletWallets outletWallets: outletWalletsList) {

                if (outletWallets.getWalletsLink().getCurrencyTypeWalletsLink().getName().equals(currType.get().getName())) {


                agentBalance =outletWallets.getWalletsLink().getAmount().subtract( agentRequestVm.getAmount());

                Wallets agentWallets = outletWallets.getWalletsLink();

                agentWallets.setAmount(agentBalance);
                walletsRepository.save(agentWallets);

            }
        }


//        BigDecimal agentBalance =outletWallets.getWalletsLink().getAmount().subtract(agentRequestVm.getAmount());

        BigDecimal custmerBalance = oCustomersWallets.getWalletsLink().getAmount().add(agentRequestVm.getAmount());

        Wallets customerWalets = oCustomersWallets.getWalletsLink();
        customerWalets.setAmount(custmerBalance);
        walletsRepository.save(customerWalets);



        String message = "You have received "+ agentRequestVm.getCurrencyType() +" "+agentRequestVm.getAmount()+" " +
                "from agent "+ agents.getAgentCode() +". New account balance is "+ custmerBalance +". ";
        boolean send = smsService.sendSMS(new SmsOptions()
                .setMessage(message)
                .setMobileNo( agentRequestVm.getUserPhoneNumber()  )
        );


        Map<String, Object> map = new HashMap<>();
        map.put("agentBalance", agentBalance);
        map.put("customerBalance", custmerBalance);


        TransactionTypes transactionTypes= transactionTypesRepository.findByName( TransactionTypes.DEPOSIT ).get();

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setCurrencyType( agentRequestVm.getCurrencyType() )
                .setTransactionTypeNo( transactionTypes.getId() )
                .setAmount( agentRequestVm.getAmount() );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames( agents.getName());
        transaction.setNames(oCustomersWallets.getCustomersLink().getFirstName()+ " "+ oCustomersWallets.getCustomersLink().getSurname());
        transactionsRepository.save( transaction );

        CommmisionTransaction commmisionTransaction = new CommmisionTransaction();


//        Optional<Fees> optionalFees = feeRepository.findByTransactionTypeNoAndFlag(transactionTypes.getId(), AppConstants.STATUS_ACTIVERECORD);
        Optional<Fees> optionalFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(transactionTypes.getId(), agentRequestVm.getAmount() );

        Fees fees = optionalFees.get();

        BigDecimal agentAmount = fees.getAgentAmount();
        BigDecimal flexAmount = fees.getFlexAmount();
        BigDecimal binaryAmount = fees.getBinaryAmount();

        commmisionTransaction.setAgentAmount(agentAmount);
        commmisionTransaction.setAgentNo(agents.getId());
        commmisionTransaction.setTariffAmount(agentRequestVm.getAmount());
        commmisionTransaction.setTransactionNo(transaction.getId());
        commmisionTransaction.setFlexAmount(flexAmount);
        commmisionTransaction.setBinaryAmount(binaryAmount);
        commmisionTransaction.setFeeNo( fees.getId() );

        commissionTransactionRepository.save(commmisionTransaction);


        return new ResponseModel(
                "00",
                "Transaction Successful",
                map

        );
    }


    @Override
    public ResponseModel withdrawTransaction( WithdrawalRequest request) throws Exception {

        Optional<Agents> oAgents = agentsRepository.findByAgentCode( request.getAgentNo() );

        if (!oAgents.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown Agent "

            );
        }

        Optional<Customers> oCustomers = customersRepository.findByPhone( request.getPhoneNo() );

        if (!oCustomers.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown Customer "

            );
        }

        Customers customers = oCustomers.get();
        Agents agents = oAgents.get();

//        OutletWallets outletWallets = outletWalletsRepository.findByOutletNo(agents.getId()).get();
        List<OutletWallets> outletWalletsList = outletWalletsRepository.findByOutletNo(agents.getId());


//        OutletWallets outletWallets = outletWalletsList.get(0);

        CustomersWallets oCustomersWallets = customerWalletsRepository.findByCustomerNo(customers.getId()).get();

//        int amountCompare = request.getAmount().compareTo( oCustomersWallets.getWalletsLink().getAmount() );
//
//        if (amountCompare !=-1 ){
//
//            return new ResponseModel(
//                    "01",
//                    "Amount Too Big than float available"
//
//            );
//        }



//        OutletWallets outletWallets= new OutletWallets();

     Optional<CurrencyTypeWallets> currType = currencyTypeWallets.findByName( request.getCurrencyType() );

        BigDecimal agentBalance = BigDecimal.valueOf(0.00);


        for(OutletWallets outletWallets: outletWalletsList) {


            if (outletWallets.getWalletsLink().getCurrencyTypeWalletsLink().getName().equals(currType.get().getName())) {


                agentBalance =outletWallets.getWalletsLink().getAmount().add( request.getAmount());

                Wallets agentWallets = outletWallets.getWalletsLink();

                agentWallets.setAmount(agentBalance);
                walletsRepository.save(agentWallets);

            }
        }



        BigDecimal custmerBalance = oCustomersWallets.getWalletsLink().getAmount().subtract( request.getAmount());

        Wallets customerWalets = oCustomersWallets.getWalletsLink();
        customerWalets.setAmount(custmerBalance);
        walletsRepository.save(customerWalets);


        String message = "You have successfully withdrawn "+ request.getCurrencyType() +" "+request.getAmount()+" " +
                "from agent "+ request.getAgentNo() +". New account balance is "+ custmerBalance +". ";
        boolean send = smsService.sendSMS(new SmsOptions()
                .setMessage(message)
                .setMobileNo( request.getPhoneNo()  )
        );


        Map<String, Object> map = new HashMap<>();
        map.put("agentBalance", agentBalance);
        map.put("customerBalance", custmerBalance);


        TransactionTypes transactionTypes= transactionTypesRepository.findByName( TransactionTypes.WITHDRAWAL ).get();

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setCurrencyType( request.getCurrencyType() )
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypes.getId() )
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames(oAgents.get().getName() );
        transaction.setNames(oCustomersWallets.getCustomersLink().getFirstName()+ " "+ oCustomersWallets.getCustomersLink().getSurname());
        transactionsRepository.save( transaction );


        CommmisionTransaction commmisionTransaction = new CommmisionTransaction();


//        Optional<Fees> optionalFees = feeRepository.findByTransactionTypeNoAndFlag(transactionTypes.getId(), AppConstants.STATUS_ACTIVERECORD);
        Optional<Fees> optionalFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(transactionTypes.getId(), request.getAmount() );

        Fees fees = optionalFees.get();

        BigDecimal agentAmount = fees.getAgentAmount();
        BigDecimal flexAmount = fees.getFlexAmount();
        BigDecimal binaryAmount = fees.getBinaryAmount();


        commmisionTransaction.setAgentAmount(agentAmount);
        commmisionTransaction.setAgentNo(agents.getId());
        commmisionTransaction.setTariffAmount(request.getAmount());
        commmisionTransaction.setTransactionNo(transaction.getId());
        commmisionTransaction.setFlexAmount(flexAmount);
        commmisionTransaction.setBinaryAmount(binaryAmount);
        commmisionTransaction.setFeeNo( fees.getId() );

        commissionTransactionRepository.save(commmisionTransaction);


        return new ResponseModel(
                "00",
                "Transaction Successful",
                map

        );
    }

    @Override
    public ResponseModel C2CDeposit( C2CRequest c2CRequest ) throws Exception {

        Optional<Customers> oCustomer = customersRepository.findByPhone(c2CRequest.getSenderPhoneNo());


        Optional<Customers> oReceiver = customersRepository.findByPhone(c2CRequest.getReceiverPhoneNo());

        if (!oCustomer.isPresent() || !oReceiver.isPresent()) {
            return new ResponseModel(
                    "01",
                    "Unknown Customer"

            );
        } else {

            Customers customers = oCustomer.get();

            CustomersWallets oSenderWallet = customerWalletsRepository.findByCustomerNo(customers.getId()).get();

//            int amountCompare = oSenderWallet.getWalletsLink().getAmount().compareTo(c2CRequest.getAmount());
//
//
//            if (amountCompare !=-1) {
//
//                return new ResponseModel(
//                        "01",
//                        "Amount Too Big than float available"
//
//                );
//            }

            BigDecimal senderBalance = oSenderWallet.getWalletsLink().getAmount().subtract(c2CRequest.getAmount());

//                Wallets customerWallets = oSenderWallet.getWalletsLink();

            Wallets customerWallets = walletsRepository.findById(oSenderWallet.getWalletNo()).get();


            customerWallets.setAmount(senderBalance);

            walletsRepository.save(customerWallets);


            String message = "You have successfully transferred " + c2CRequest.getCurrencyType() + " " + c2CRequest.getAmount() + " " +
                    "to " + c2CRequest.getReceiverPhoneNo() + ". New account balance is " + senderBalance + ". ";
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo(c2CRequest.getSenderPhoneNo())
            );

            Customers receiver = oReceiver.get();

            CustomersWallets oReceiverWallet = customerWalletsRepository.findByCustomerNo(receiver.getId()).get();

            BigDecimal receiverBalance = oReceiverWallet.getWalletsLink().getAmount().add(c2CRequest.getAmount());

//                Wallets receiverWalets = oReceiverWallet.getWalletsLink();

            Wallets receiverWallets = walletsRepository.findById(oReceiverWallet.getWalletNo()).get();

            receiverWallets.setAmount(receiverBalance);
            walletsRepository.save(receiverWallets);


            String receiverMessage = "You have received " + c2CRequest.getCurrencyType() + " " + c2CRequest.getAmount() + " " +
                    "from " + c2CRequest.getSenderPhoneNo() + ". New account balance is " + receiverBalance + ". ";
            smsService.sendSMS(new SmsOptions()
                    .setMessage(receiverMessage)
                    .setMobileNo(c2CRequest.getReceiverPhoneNo())
            );


            TransactionTypes transactionTypes = transactionTypesRepository.findByName(TransactionTypes.TRANSFER).get();

            //Populate transaction
            Transactions transaction = new Transactions();
            transaction
                    .setCurrencyType(c2CRequest.getCurrencyType())
                    .setReceiptNo(null)
                    .setTransactionTypeNo(transactionTypes.getId())
                    .setAmount(c2CRequest.getAmount());
            transactionsRepository.save(transaction);

            String receiptNo = String.format("%1$" + 10 + "s", transaction.getId()).replace(' ', '0');

            //Update transaction
            transaction.setReceiptNo(receiptNo);
            transaction.setSenderTelephoneNo(c2CRequest.getSenderPhoneNo());
            transaction.setNames(oSenderWallet.getCustomersLink().getFirstName() + " " + oSenderWallet.getCustomersLink().getSurname());
            transaction.setReceiverContactNo(c2CRequest.getReceiverPhoneNo());
            transactionsRepository.save(transaction);


            CommmisionTransaction commmisionTransaction = new CommmisionTransaction();


//        Optional<Fees> optionalFees = feeRepository.findByTransactionTypeNoAndFlag(transactionTypes.getId(), AppConstants.STATUS_ACTIVERECORD);
            Optional<Fees> optionalFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(transactionTypes.getId(), c2CRequest.getAmount());

            Fees fees = optionalFees.get();

            BigDecimal agentAmount = fees.getAgentAmount();
            BigDecimal flexAmount = fees.getFlexAmount();
            BigDecimal binaryAmount = fees.getBinaryAmount();


            commmisionTransaction.setAgentAmount(agentAmount);
            commmisionTransaction.setTariffAmount(c2CRequest.getAmount());
            commmisionTransaction.setTransactionNo(transaction.getId());
            commmisionTransaction.setFlexAmount(flexAmount);
            commmisionTransaction.setBinaryAmount(binaryAmount);
            commmisionTransaction.setFeeNo(fees.getId());

            commissionTransactionRepository.save(commmisionTransaction);
        }

        return new ResponseModel(
                "00",
                "Transaction Successful"
        );
    }


    @Override
    public ResponseModel agentBalance(AgentBalanceRequest balance) throws Exception {

        Optional<Agents> oAgents = agentsRepository.findByPhone( balance.getPhoneNo());

        if (!oAgents.isPresent()){
            return new ResponseModel(
                    "01",
                    "Unknown Agent "

            );
        }

        Agents agent = oAgents.get();

        List<OutletWallets> outletWalletsList = outletWalletsRepository.findByOutletNo(agent.getId());
//        OutletWallets outletWallets = outletWalletsList.get(0);
        List<AgentBalanceResponse> agentBalanceResponses = new ArrayList<>() ;

        for(OutletWallets k: outletWalletsList){

            AgentBalanceResponse resp= new AgentBalanceResponse();
            resp.setCurrency( k.getWalletsLink().getCurrencyTypeWalletsLink().getName());
            resp.setAgentBalance( k.getWalletsLink().getAmount());

            agentBalanceResponses.add( resp );

        }


        return new ResponseModel(
                "00",
                "Agent balance is ",
                agentBalanceResponses

        );
    }


    @Override
    public Map<String, Object> kplcPrepaidHandler(KplcPrepaidRequest request) throws Exception {

        ObjectMapper objectMapper = new ObjectMapper();

        Map<String, Object> map = new HashMap<>();

        Optional<Customers> oCustomers = customersRepository.findByPhone(request.getLoggedInPhoneNo());


        if (!oCustomers.isPresent()){

            map.put("status", "01");
            map.put("message", "User Not Found");

            return map;

        }

        Customers customers = oCustomers.get();


        String uri = env.getProperty("mfm.uris.kplc-prepaid.purchase");
        Map<String, Object> mfmRequest = new HashMap<>();
        mfmRequest.put("amount", request.getAmount());
        mfmRequest.put("customer", request.getCustomerName());
        mfmRequest.put("phone", request.getCustomerPhoneNo());
        mfmRequest.put("meter_no", request.getMeterNo());
        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                uri,
                mfmRequest
        );

        //Parse Response
        JsonNode jsonNode = objectMapper.readTree(responseEntity.getBody());
        String mfmMessage = jsonNode.get("message").asText();
        map.put("status", jsonNode.get("status").asText());
        map.put("message", mfmMessage);
        map.put("data", jsonNode.get("data"));


//        TransactionTypes transactionTypes = transactionTypesRepository.findByName(TransactionTypes.TRANSFER).get();
        Long transactionTypeNo = Long.valueOf(5);

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        System.err.println("receiptNo"+transaction.getId());

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        System.err.println("receiptNo"+receiptNo);

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames("N/A");
        transaction.setNames(customers.getFirstName()+ " "+ customers.getSurname());
        transactionsRepository.save( transaction );

        Map<String, Object> response = new HashMap<>();
        response.put("status", "00");
        response.put("message", "successful");

        return response;
    }

    @Override
    public Map<String, Object> foreCastTransaction(FetchAmountRequestVm request) throws Exception {

        Map<String, Object> response = new HashMap<>();
        response.put("amount", 100);
        response.put("status", "00");
        response.put("message", "successful");

        return response;
    }

    @Override
    public Map<String, Object> sentInternalTransaction(SentInternalAmountRequestVm request) throws Exception {
        Map<String, Object> map = new HashMap<>();

        Random random = new Random();

        //Generate numbers between 0 and 100
        int sec1 = random.nextInt(101);
        int sec2 = random.nextInt(1000);
        int sec3 = random.nextInt(10000);

//        String formattedNumber = PhoneNumberFormatter.formatE164("+211", request.getAgentPhoneNumber());

        Optional<Agents> optionalAgents = agentsRepository.findByPhone(request.getAgentPhoneNumber());

        if (!optionalAgents.isPresent()){
            map.put("status", "01");
            map.put("message", "Agent not fount");
            return map;
        }

        Agents agents = optionalAgents.get();

        List<OutletWallets> outletWalletsList = outletWalletsRepository.findByOutletNo(agents.getId());

        Optional<CurrencyTypeWallets> currType = currencyTypeWallets.findByName( request.getCurrencyType() );

        BigDecimal agentBalance = BigDecimal.valueOf(0.00);


        for(OutletWallets outletWallets: outletWalletsList) {


            if (outletWallets.getWalletsLink().getCurrencyTypeWalletsLink().getName().equals(currType.get().getName())) {

                agentBalance =outletWallets.getWalletsLink().getAmount().subtract( request.getAmount());

                Wallets agentWallets = outletWallets.getWalletsLink();

                agentWallets.setAmount(agentBalance);
                walletsRepository.save(agentWallets);

            }
        }

        map.put("agentBalance", agentBalance);
        map.put("customerBalance", request.getAmount() );

        String uniqueCode = String.valueOf(sec1)+ String.valueOf(sec3)+String.valueOf(sec2);


        FlexWallets flexWallets= new FlexWallets();

        flexWallets
                .setFlag( FlexWallets.AVAILABLE )
                .setAmount( request.getAmount() )
                .setSecretCode( uniqueCode )
                .setReceiverNo( request.getReceiverPhoneNumber() )
                .setReceiverName( request.getReceiverName() )
                .setSenderNo( request.getSenderPhoneNumber() )
                .setReceiverName( request.getReceiverName() )
                .setCurrencyType( request.getCurrencyType() );
        flexWalletRepository.save( flexWallets );

        TransactionTypes transactionTypes = transactionTypesRepository.findByName(TransactionTypes.INCOUNTRY).get();
//        Long transactionTypeNo = Long.valueOf(13);

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction.setStatus("0");
        transaction
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypes.getId() )
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId()).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo(receiptNo);
        transaction.setAgentNames(agents.getName());
        transaction.setNames(request.getSenderNames());
        transactionsRepository.save( transaction );

        TransactionInternal transactionInternal = new TransactionInternal();
        transactionInternal.setReceivername(request.getReceiverName());
        transactionInternal.setReceiverPhoneNumber(request.getReceiverPhoneNumber());
        transactionInternal.setTransactionNo(transaction.getId());

        transactionInternalRepository.save(transactionInternal);

        String message = "Dear "+request.getReceiverName()+", "+ request.getSenderNames()+
                " just sent "+request.getCurrencyType()+" "+request.getAmount()+" walk with this code "+ uniqueCode +" to your nearest flex agent to redeem it. ";
        boolean send = smsService.sendSMS(new SmsOptions()
                .setMessage(message)
                .setMobileNo(request.getReceiverPhoneNumber())
        );


        CommmisionTransaction commmisionTransaction = new CommmisionTransaction();


//        Optional<Fees> optionalFees = feeRepository.findByTransactionTypeNoAndFlag(transactionTypes.getId(), AppConstants.STATUS_ACTIVERECORD);
        Optional<Fees> optionalFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(transactionTypes.getId(), request.getAmount());

        Fees fees = optionalFees.get();

        BigDecimal agentAmount = fees.getAgentAmount();
        BigDecimal flexAmount = fees.getFlexAmount();
        BigDecimal binaryAmount = fees.getBinaryAmount();

        commmisionTransaction.setAgentAmount(agentAmount);
        commmisionTransaction.setTariffAmount(request.getAmount());
        commmisionTransaction.setTransactionNo(transaction.getId());
        commmisionTransaction.setFlexAmount(flexAmount);
        commmisionTransaction.setBinaryAmount(binaryAmount);
        commmisionTransaction.setFeeNo(fees.getId());

        commissionTransactionRepository.save(commmisionTransaction);


        flexWallets.setFlag(FlexWallets.CLEARED);
        flexWalletRepository.save( flexWallets );

        map.put("status", "00");
        map.put("message", "successful");

        return map;
    }

    @Override
    public Map<String, Object>  codeAgentWithdrawal( CodeWithdrawalRequest request) throws Exception {

        Map<String, Object> map = new HashMap<>();

        Optional<FlexWallets> oFlexWallet = flexWalletRepository.findByReceiverNoAndSecretCodeAndFlagIsNot(request.getPhoneNumber(),request.getUniqueCode(),"0" );

        if (!oFlexWallet.isPresent()){
            map.put("status", "01");
            map.put("message", "Unique code not found");
            return map;
        }

        Optional<Agents> oAgents = agentsRepository.findByAgentCode( request.getAgentCode() );
        if (!oAgents.isPresent()){
            map.put("status", "01");
            map.put("message", "Invalid agent number");
            return map;
        }


        Agents agents = oAgents.get();

        FlexWallets flexWallets= oFlexWallet.get();

        List<OutletWallets> outletWalletsList = outletWalletsRepository.findByOutletNo(agents.getId());

        Optional<CurrencyTypeWallets> currType = currencyTypeWallets.findByName( request.getCurrencyType() );

        BigDecimal agentBalance = BigDecimal.valueOf(0.00);


        for(OutletWallets outletWallets: outletWalletsList) {


            if (outletWallets.getWalletsLink().getCurrencyTypeWalletsLink().getName().equals(currType.get().getName())) {


                agentBalance =outletWallets.getWalletsLink().getAmount().add( request.getAmount());

                Wallets agentWallets = outletWallets.getWalletsLink();

                agentWallets.setAmount(agentBalance);
                walletsRepository.save(agentWallets);

            }
        }



        BigDecimal custmerBalance = flexWallets.getAmount().subtract( request.getAmount());
        if(custmerBalance.equals(0)){

            flexWallets.setFlag( FlexWallets.CLEARED);
        }
        flexWallets.setFlag( FlexWallets.ONGOING );
        flexWallets.setAmount(custmerBalance);
        flexWalletRepository.save(flexWallets);


        String message = "You have successfully withdrawn "+ request.getCurrencyType() +" "+request.getAmount()+" " +
                "from agent "+ request.getAgentCode() +". New account balance is "+ custmerBalance +". ";
        boolean send = smsService.sendSMS(new SmsOptions()
                .setMessage(message)
                .setMobileNo( request.getPhoneNumber()  )
        );


        map.put("agentBalance", agentBalance);
        map.put("customerBalance", custmerBalance);


        TransactionTypes transactionTypes= transactionTypesRepository.findByName( TransactionTypes.WITHDRAWAL ).get();

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setCurrencyType( request.getCurrencyType() )
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypes.getId() )
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames(oAgents.get().getName() );
        transaction.setNames(flexWallets.getReceiverName());
        transactionsRepository.save( transaction );


        CommmisionTransaction commmisionTransaction = new CommmisionTransaction();


//        Optional<Fees> optionalFees = feeRepository.findByTransactionTypeNoAndFlag(transactionTypes.getId(), AppConstants.STATUS_ACTIVERECORD);
        Optional<Fees> optionalFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(transactionTypes.getId(), request.getAmount() );

        Fees fees = optionalFees.get();

        BigDecimal agentAmount = fees.getAgentAmount();
        BigDecimal flexAmount = fees.getFlexAmount();
        BigDecimal binaryAmount = fees.getBinaryAmount();


        commmisionTransaction.setAgentAmount(agentAmount);
        commmisionTransaction.setAgentNo(agents.getId());
        commmisionTransaction.setTariffAmount(request.getAmount());
        commmisionTransaction.setTransactionNo(transaction.getId());
        commmisionTransaction.setFlexAmount(flexAmount);
        commmisionTransaction.setBinaryAmount(binaryAmount);
        commmisionTransaction.setFeeNo( fees.getId() );

        commissionTransactionRepository.save(commmisionTransaction);




        map.put("status", "00");
        map.put("message", " Withdrawal successful");

        return map;
    }


    @Override
    public Map<String, Object>  receiveInternalTransaction(ReceiveInternalRequestVm request) throws Exception {

        Map<String, Object> map = new HashMap<>();

        Optional<Transactions> optionalTransactions = transactionsRepository.findByReceiptNo(request.getUniqueCode());

        if (!optionalTransactions.isPresent()){
            map.put("status", "01");
            map.put("message", "Unique code not found");
            return map;
        }

        Transactions transactions = optionalTransactions.get();

        String status = transactions.getStatus();

        Optional<TransactionInternal> optionalTransactionInternal = transactionInternalRepository.findByTransactionNo(transactions.getId());

        TransactionInternal transactionInternal = optionalTransactionInternal.get();

        if (!transactionInternal.getReceivername().equals(request.getNames())){
            map.put("status", "01");
            map.put("message", "Names did not match");
            return map;
        }

        if (!transactionInternal.getReceiverPhoneNumber().equals(request.getPhoneNumber())){

            map.put("status", "01");
            map.put("message", "Phone did not match");
            return map;
        }

        if (status.equals("1")){

            map.put("status", "01");
            map.put("message", "Code is already used");
            return map;
        }



        BigDecimal amount = transactions.getAmount();

        if (amount.equals(request.getAmount())) {


            transactions.setStatus("1");
        }else {

            map.put("status", "01");
            map.put("message", "Amount Didn't match");

            return map;
        }

        transactionsRepository.save(transactions);


        Optional<Agents> optionalAgents = agentsRepository.findByAgentCode(request.getAgentCode());

        if (optionalAgents.isPresent()){

            Agents agents = optionalAgents.get();

           int  res = agents.getTransactionAmount().compareTo(request.getAmount());

           if (res == 1) {

               BigDecimal transactionAmount = agents.getTransactionAmount();

               BigDecimal balanceAmount = transactionAmount.subtract(request.getAmount());
               agents.setTransactionAmount(balanceAmount);

               agentsRepository.save(agents);
           }else {

               map.put("status", "01");
               map.put("message", "Insufficient Float Amount");

               return map;
           }

        }


        map.put("status", "00");
        map.put("message", "successful");

        return map;
    }



    @Override
    public ResponseModel fetchKplcPrepaidMeterDetails(KplcPrepaidRequest request) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        String uri = env.getProperty("mfm.uris.kplc-prepaid.fetch-details");

        Map<String, Object> apiRequest = new HashMap<>();
        apiRequest.put("amount", request.getAmount());
        apiRequest.put("customer", request.getCustomerName());
        apiRequest.put("phone", request.getCustomerPhoneNo());
        apiRequest.put("meter_no", request.getMeterNo());

        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                uri,
                apiRequest
        );

        JsonNode jsonNode = objectMapper.readTree(responseEntity.getBody());
        ResponseModel response = new ResponseModel(
                jsonNode.get("status").asText(),
                jsonNode.get("message").asText(),
                jsonNode.get("data")
        );
        return response;
    }

    @Override
    public ResponseModel aritimePurcahse(AirtimePurchaseRequest request) throws Exception {

        ObjectMapper mapper = new ObjectMapper();

        Optional<Customers> oCustomers = customersRepository.findByPhone(request.getLoggedinPhone());


        if (!oCustomers.isPresent()){

            return new ResponseModel(
                    "01",
                    "User Not Found"

            );
        }

        Customers customers = oCustomers.get();

        String uri = env.getProperty("mfm.uris.airtime");
        Map<String, Object> mfmRequest = new HashMap<>();
        mfmRequest.put("amount", request.getAmount());
        mfmRequest.put("phone", request.getRecipientPhone());
        mfmRequest.put("provider", request.getServiceProvider());
        ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                uri,
                mfmRequest
        );

        JsonNode jsonNode = mapper.readValue(responseEntity.getBody(), JsonNode.class);

        String status = jsonNode.get("status").asText();
        String message = jsonNode.get("message").asText();

        if (status.equals("01")){

            return new ResponseModel(
                    "01",
                    message

            );
        }

        Long transactionTypeNo = Long.valueOf(5);

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        System.err.println("receiptNo"+transaction.getId());

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        System.err.println("receiptNo"+receiptNo);

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames("N/A");
        transaction.setNames(customers.getFirstName()+ " "+ customers.getSurname());
        transactionsRepository.save( transaction );


        return new ResponseModel(
                "00",
                "Transaction Successful"

        );
    }

    private FlexApiCommonRequestVm flexApiCommonRequestVm(){

        FlexApiCommonRequestVm requestVm = new FlexApiCommonRequestVm();

        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());

        requestVm.setPassword(PASSWORD);
        requestVm.setSysCode(SYSTEMCODE);
        requestVm.setTimestamp(TIMESTAMP);
        requestVm.setUserCode(USERCODE);

        return requestVm;

    }

    public ResponseModel callBackStk(JsonNode response) {
        System.err.println("callback Request" + response.toString());

        String status = response.get("status").asText();
        String message = response.get("message").asText();
        String transactionId = response.get("transactionId").asText();
        String mpesaRefNo = response.get("mpesaRefNo").asText();

        if (status != "00"){

            Optional<Transactions> optionalTransactions = transactionsRepository.findByMpesaTransactionNo(transactionId);

            if (optionalTransactions.isPresent()) {

                Transactions transactions = optionalTransactions.get();


                if (transactions.getNames() == "N/A"){

                    String messages = "Dear customer something happened we are working on it. Sorry for the inconvenience";
                    boolean send = smsService.sendSMS(new SmsOptions()
                            .setMessage(messages)
                            .setMobileNo(transactions.getSenderTelephoneNo())
                    );
                }else {

                    String messages = "Dear "+transactions.getNames()+" something happened we are working on it. Sorry for the inconvenience";
                    boolean send = smsService.sendSMS(new SmsOptions()
                            .setMessage(messages)
                            .setMobileNo(transactions.getSenderTelephoneNo())
                    );
                }

                }
        }

        return new ResponseModel(
                "00",
                "Successfull"
        );
    }
}
