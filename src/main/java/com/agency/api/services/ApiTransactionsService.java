package com.agency.api.services;

import com.agency.api.ApiTransactionsServiceInterface;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;
import com.agency.core.sms.SmsOptions;
import com.agency.core.sms.SmsService;
import com.agency.web.agency.entities.Fees;
import com.agency.web.agency.repository.FeeGroupRepository;
import com.agency.web.agency.repository.FeeRepository;
import com.agency.web.terminals.entities.TerminalOfficers;
import com.agency.web.terminals.entities.Terminals;
import com.agency.web.terminals.repository.TerminalRepository;
import com.agency.web.transactions.entities.MerchantTransactions;
import com.agency.web.transactions.entities.TransactionTypes;
import com.agency.web.transactions.entities.Transactions;
import com.agency.web.transactions.repository.MerchantTransactionRepository;
import com.agency.web.transactions.repository.TransactionTypesRepository;
import com.agency.web.transactions.repository.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
@Transactional
public class ApiTransactionsService implements ApiTransactionsServiceInterface {

    @Autowired private TerminalRepository terminalRepository;
    @Autowired private TransactionsRepository transactionsRepository;
    @Autowired private TransactionTypesRepository transactionTypesRepository;
    @Autowired private MerchantTransactionRepository merchantTransactionRepository;
    @Autowired private FeeRepository feeRepository;


    @Autowired private SmsService smsService;

    /**
     * Cardless deposit
     *
     * @return ResponseModel
     */
    @Override
    public ResponseModel deposit(DepositRequest request, AccountActions action){
        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );
        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        //When to sent token
        String tAction = action.toString();
        if( AccountActions.initiate.toString().equals( tAction ) ){
            String authToken = HelperUtil.generatePhoneToken( 4 );
            String message = "Your authorization code is : " + authToken ;
            //Send SMS
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo("")
            );

            return new ResponseModel(
                    "00",
                    "Request processed successfully"
            );
        }

        Long transactionTypeNo = null ;
        Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
        if( olTransactionType.isPresent() ){
            transactionTypeNo = olTransactionType.get().getId();
        }

        BigDecimal amount = request.getAmount();

        Optional<Fees> oFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(Fees.DEPOSIT, amount);

        if (!oFees.isPresent()){
            return new ResponseModel<>("01", "No fee group please configure");
        }

        Fees fees = oFees.get();

        String paymentMode = fees.getPaymentMode();

        BigDecimal saccoAmount;
        BigDecimal outletAmount;

        if (paymentMode.equals(Fees.FIXED)){

            outletAmount = fees.getAgentAmount();
            saccoAmount = fees.getFlexAmount();

        }else {
            BigDecimal agentRation = fees.getAgentAmount();
            BigDecimal saccoRatio = fees.getFlexAmount();
            BigDecimal transactingAmount = fees.getTransactionAmount();

            outletAmount = transactingAmount.multiply(agentRation.divide(BigDecimal.valueOf(100)));
            saccoAmount = transactingAmount.multiply(saccoRatio.divide(BigDecimal.valueOf(100)));

        }

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setTerminalNo( terminalNo )
                .setAccountNo( request.getAccountNo() )
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setOfficerId( request.getUserId() )
                .setAgentFee(outletAmount)
                .setBinaryFee(saccoAmount)
                .setAmount( request.getAmount() );

        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transactionsRepository.save( transaction );

        Map<String, Object> map = new HashMap<>();
        map.put("receiptNo", receiptNo);
        map.put("charges", BigDecimal.ZERO );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }

    /**
     * Card deposit
     *
     * @return ResponseModel
     */
    @Override
    public ResponseModel depositWithCard(){

        return new ResponseModel(
                "00",
                "Success"
        );
    }

    /**
     * Cardless withdrawal
     *
     * @return ResponseModel
     */
    @Override
    public ResponseModel withdraw( WithdrawRequest request, AccountActions action ){
        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );

        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        //When to sent token
        String tAction = action.toString();
        if( AccountActions.initiate.toString().equals( tAction ) ){
            String authToken = HelperUtil.generatePhoneToken( 4 );
            String message = "Your authorization code is : " + authToken ;
            //Send SMS
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo("")
            );

            return new ResponseModel(
                    "00",
                    "Request processed successfully"
            );
        }

        Long transactionTypeNo = null ;
        Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
        if( olTransactionType.isPresent() ){
            transactionTypeNo = olTransactionType.get().getId();
        }

        BigDecimal amount = request.getAmount();



        Optional<Fees> oFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(Long.valueOf("2"), amount);

        if (!oFees.isPresent()){
            return new ResponseModel<>("01", "No fee group please configure");
        }

        Fees fees = oFees.get();

        String paymentMode = fees.getPaymentMode();

        BigDecimal saccoAmount;
        BigDecimal outletAmount;

        if (paymentMode.equals(Fees.FIXED)){

            outletAmount = fees.getAgentAmount();
            saccoAmount = fees.getFlexAmount();

        }else {
            BigDecimal agentRation = fees.getAgentAmount();
            BigDecimal saccoRatio = fees.getFlexAmount();
            BigDecimal transactingAmount = fees.getTransactionAmount();

            outletAmount = transactingAmount.multiply(agentRation.divide(BigDecimal.valueOf(100)));
            saccoAmount = transactingAmount.multiply(saccoRatio.divide(BigDecimal.valueOf(100)));

        }

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setTerminalNo( terminalNo )
                .setReceiptNo( null )
                .setAccountNo( request.getAccountNo() )
                .setTransactionTypeNo( transactionTypeNo )
                .setOfficerId( request.getUserId() )
                .setBinaryFee(saccoAmount)
                .setAgentFee(outletAmount)
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transactionsRepository.save( transaction );

        Map<String, Object> map = new HashMap<>();
        map.put("receiptNo", receiptNo);
        map.put("charges", BigDecimal.ZERO );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }

    @Override
    public ResponseModel merchantTranasaction(MerchantTransactionsVm request) {

        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );

        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        MerchantTransactions merchantTransactions = new MerchantTransactions();
        merchantTransactions.setAccountNo(request.getAccountNo());
        merchantTransactions.setFlag("Success");
        merchantTransactions.setAmount(request.getAmount());
        merchantTransactions.setTerminalNo(terminalNo);
        merchantTransactions.setOfficerId(request.getOfficerNo());
        merchantTransactions.setTransactionReference(request.getTransactionNo());
        merchantTransactions.setTransactionTypeNo(request.getTransactionTypeNo());

        merchantTransactionRepository.save(merchantTransactions);
        return new ResponseModel(
                "00",
                "Success"
        );
    }

    /**
     * Cardless balance inquiry
     *
     * @return ResponseModel
     */
    @Override
    public ResponseModel balanceInquiry(BalanceInquiryRequest request, AccountActions action ){
        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );
        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        //When to sent token
        String tAction = action.toString();
        if( AccountActions.initiate.toString().equals( tAction ) ){
            String authToken = HelperUtil.generatePhoneToken( 4 );
            String message = "Your authorization code is : " + authToken ;
            //Send SMS
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo("")
            );

            return new ResponseModel(
                    "00",
                    "Request processed successfully"
            );
        }

        Long transactionTypeNo = null ;
        Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
        if( olTransactionType.isPresent() ){
            transactionTypeNo = olTransactionType.get().getId();
        }

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setTerminalNo( terminalNo )
                .setAccountNo( request.getAccountNo() )
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setOfficerId( request.getUserId() )
                .setAmount( BigDecimal.ZERO );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transactionsRepository.save( transaction );

        Map<String, Object> map = new HashMap<>();
        map.put("receiptNo", receiptNo);
        map.put("balance", new BigDecimal( 10000 ) );
        map.put("charges", BigDecimal.ZERO );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }

    /**
     * Cardless mini-statement
     *
     * @return ResponseModel
     */
    @Override
    public ResponseModel miniStatement(BalanceInquiryRequest request, AccountActions action ){
        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );
        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        //When to sent token
        String tAction = action.toString();
        if( AccountActions.initiate.toString().equals( tAction ) ){
            String authToken = HelperUtil.generatePhoneToken( 4 );
            String message = "Your authorization code is : " + authToken ;
            //Send SMS
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo("")
            );

            return new ResponseModel(
                    "00",
                    "Request processed successfully"
            );
        }

        Long transactionTypeNo = null ;
        Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
        if( olTransactionType.isPresent() ){
            transactionTypeNo = olTransactionType.get().getId();
        }

        BigDecimal amount = BigDecimal.ZERO;

        Optional<Fees> oFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(Fees.MINI_STATEMENT, amount);

        if (!oFees.isPresent()){
            return new ResponseModel<>("01", "No fee group please configure");
        }

        Fees fees = oFees.get();

        String paymentMode = fees.getPaymentMode();

        BigDecimal saccoAmount;
        BigDecimal outletAmount;

        if (paymentMode.equals(Fees.FIXED)){

            outletAmount = fees.getAgentAmount();
            saccoAmount = fees.getFlexAmount();

        }else {
            BigDecimal agentRation = fees.getAgentAmount();
            BigDecimal saccoRatio = fees.getFlexAmount();
            BigDecimal transactingAmount = fees.getTransactionAmount();

            outletAmount = transactingAmount.multiply(agentRation.divide(BigDecimal.valueOf(100)));
            saccoAmount = transactingAmount.multiply(saccoRatio.divide(BigDecimal.valueOf(100)));

        }

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setTerminalNo( terminalNo )
                .setReceiptNo( null )
                .setAccountNo( request.getAccountNo() )
                .setTransactionTypeNo( transactionTypeNo )
                .setOfficerId( request.getUserId() )
                .setBinaryFee(saccoAmount)
                .setAgentFee(outletAmount)
                .setAmount( BigDecimal.ZERO);
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transactionsRepository.save( transaction );

        //Mini-statement data
        List<Transactions> transactions = transactionsRepository.findAllByAccountNo( request.getAccountNo() );
        List<Object> data = new ArrayList<>();
        for( Transactions record : transactions ){
            Map<String, Object> node = new HashMap<>();
            Date createdOn  = record.getCreatedOn();


            SimpleDateFormat sdf = new SimpleDateFormat("MM/dd HH:mm");

            String formattedDate =  sdf.format(createdOn);


            node.put("receiptNo", record.getReceiptNo() );
            node.put("amount", record.getAmount() );
            node.put("transactionType", record.getTransactionTypeLink().getShortCode() );
            node.put("dateTime",  formattedDate);
            data.add( node );
        }

        Map<String, Object> map = new HashMap<>();
        map.put("receiptNo", receiptNo);
        map.put("transactions", data );
        map.put("charges", BigDecimal.ZERO );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }

    /**
     * Cardless funds transfer
     *
     * @param request
     * @param action
     * @return
     */
    @Override
    public ResponseModel fundsTransfer(FundsTransferRequest request, AccountActions action ){
        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalNo() );
        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        //When to sent token
        String tAction = action.toString();
        if( AccountActions.initiate.toString().equals( tAction ) ){
            String authToken = HelperUtil.generatePhoneToken( 4 );
            String message = "Your authorization code is : " + authToken ;
            //Send SMS
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo("")
            );

            return new ResponseModel(
                    "00",
                    "Request processed successfully"
            );
        }

        Long transactionTypeNo = null ;
        Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
        if( olTransactionType.isPresent() ){
            transactionTypeNo = olTransactionType.get().getId();
        }

        BigDecimal amount = request.getAmount();

        Optional<Fees> oFees = feeRepository.fetchFeeByTransactionTypeNoAndAmount(Fees.INTERNAL_FUNDS, amount);

        if (!oFees.isPresent()){
            return new ResponseModel<>("01", "No fee group please configure");
        }

        Fees fees = oFees.get();

        String paymentMode = fees.getPaymentMode();

        BigDecimal saccoAmount;
        BigDecimal outletAmount;

        if (paymentMode.equals(Fees.FIXED)){

            outletAmount = fees.getAgentAmount();
            saccoAmount = fees.getFlexAmount();

        }else {
            BigDecimal agentRation = fees.getAgentAmount();
            BigDecimal saccoRatio = fees.getFlexAmount();
            BigDecimal transactingAmount = fees.getTransactionAmount();

            outletAmount = transactingAmount.multiply(agentRation.divide(BigDecimal.valueOf(100)));
            saccoAmount = transactingAmount.multiply(saccoRatio.divide(BigDecimal.valueOf(100)));

        }

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setTerminalNo( terminalNo )
                .setReceiptNo( null )
                .setAccountNo( request.getRecipientAccNo() )
                .setTransactionTypeNo( transactionTypeNo )
                .setOfficerId( request.getUserId() )
                .setBinaryFee(saccoAmount)
                .setAgentFee(outletAmount)
                .setAmount( request.getAmount() );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transactionsRepository.save( transaction );

        Map<String, Object> map = new HashMap<>();
        map.put("receiptNo", receiptNo);
        map.put("charges", BigDecimal.ZERO );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }

    /**
     * Account opening
     *
     * @param request
     * @param action
     * @return
     */
    @Override
    public ResponseModel accountOpening(AccountOpeningRequest request, AccountActions action ){
        Map<String, Object> map = new HashMap<>();

        Optional<Terminals> oTerminal = terminalRepository.findBySerialNo( request.getTerminalId() );
        if( !oTerminal.isPresent() ){
            return new ResponseModel(
                    "01",
                    "Unknown terminal"
            );
        }

        Terminals terminal = oTerminal.get();
        Long terminalNo = terminal.getId();

        if( !terminal.getFlag().equals( "1") ){
            return new ResponseModel(
                    "01",
                    "Terminal is inactive"
            );
        }

        //When to sent token
        String tAction = action.toString();
        if( AccountActions.initiate.toString().equals( tAction ) ){
            String authToken = HelperUtil.generatePhoneToken( 4 );
            String message = "Your authorization code is : " + authToken ;
            //Send SMS
            boolean send = smsService.sendSMS(new SmsOptions()
                    .setMessage(message)
                    .setMobileNo("")
            );


            map.put("token", authToken );

            return new ResponseModel(
                    "00",
                    "Request processed successfully",
                    map
            );
        }

        Long transactionTypeNo = Long.valueOf(7);
        Optional<TransactionTypes> olTransactionType = transactionTypesRepository.findByCode( TransactionTypes.DEPOSIT );
        if( olTransactionType.isPresent() ){
            transactionTypeNo = olTransactionType.get().getId();
        }

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setTerminalNo( terminalNo )
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setOfficerId( request.getTerminalUserId() )
                .setAccountNo(HelperUtil.generatePhoneToken( 4))
                .setAmount( BigDecimal.ZERO );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transactionsRepository.save( transaction );

        map.put("receiptNo",  HelperUtil.generatePhoneToken( 10 ));
        map.put("charges", BigDecimal.ZERO );

        return new ResponseModel(
                "00",
                "Success",
                map
        );
    }
}
