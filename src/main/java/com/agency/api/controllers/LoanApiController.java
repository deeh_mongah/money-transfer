//package com.agency.api.controllers;
//
//import com.agency.api.vm.LoanApplicationRequest;
//import com.agency.api.vm.LoanRepaymentRequest;
//import com.agency.web.loans.LoansServiceInterface;
//import com.agency.web.loans.services.LoansTransactionsService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//import javax.validation.Valid;
//
///**
// * Created by Administrator on 3/20/2019.
// */
//
//@RestController
//@RequestMapping("/api/v1/loans")
//public class LoanApiController {
//
//    @Autowired
//    private LoansServiceInterface loanApiService;
//    @Autowired
//    private LoansTransactionsService loansTransactionsService;
//
//    @PostMapping("/submit")
//    public ResponseEntity<?> loan(@RequestBody @Valid LoanApplicationRequest request ){
//        return ResponseEntity.ok( loansTransactionsService.loanApplication( request ) );
//    }
//
//    @PostMapping("/repayment")
//    public ResponseEntity<?> loan(@RequestBody @Valid LoanRepaymentRequest request ){
//        return ResponseEntity.ok( loansTransactionsService.loanRepayment( request ) );
//    }
//
//    @GetMapping("/list")
//    public ResponseEntity<?> loansList() throws Exception {
//        Object response =loanApiService.fetchRecords( );
//        return ResponseEntity.ok().body(response);
//    }
//
//}
