package com.agency.api.controllers;

import com.agency.api.ApiAuthServiceInterface;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.OptValidateRequest;
import com.agency.api.vm.SetPinRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by User on 5/20/2020.
 */
@RestController
@RequestMapping( "/api/agent/v1")
public class ApiAgentController {


    @Autowired
    private ApiAuthServiceInterface apiAuthService;


    @GetMapping("/password-rest/{phone}")
    public ResponseEntity<?> agentPasswordReset(@PathVariable("phone")String phone){
        ResponseModel response = apiAuthService.agentPasswordReset( phone );
        return ResponseEntity.ok( response );
    }



    @PostMapping("/otp-validate")
    public ResponseEntity<?> agentOtpValidateRequest(@RequestBody OptValidateRequest request){
        ResponseModel response = apiAuthService.agentOtpValidateRequest( request );
        return ResponseEntity.ok( response );
    }

    @PostMapping("/agent-pin-reset")
    public ResponseEntity<?> pinReset(@RequestBody SetPinRequest request){
        ResponseModel response = apiAuthService.pinReset( request );
        return ResponseEntity.ok( response );
    }


}
