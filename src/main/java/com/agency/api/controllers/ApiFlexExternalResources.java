package com.agency.api.controllers;

import com.agency.api.FlexServiceInterface;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.http.MfmHttpService;
import com.agency.api.services.FlexApiService;
import com.agency.api.vm.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;

/**
 * Created by ben on 07/11/2019.
 */

@RestController
@RequestMapping( "/api/flex/v1")
public class ApiFlexExternalResources {

    @Autowired private FlexServiceInterface flexApiService;

    @GetMapping("/countries")
    public ResponseEntity<?> getCountries() throws Exception {
        return ResponseEntity.ok( flexApiService.getCountries());
    }

    @GetMapping("/currency")
    public ResponseEntity<?> getCurrency() throws Exception {
        return ResponseEntity.ok( flexApiService.getCurrency());
    }

    @GetMapping("/agents")
    public ResponseEntity<?> getAgent() throws Exception {
        return ResponseEntity.ok( flexApiService.getAgents());
    }

    @GetMapping("/forexRate/{currPair}")
    public ResponseEntity<?> getForexRate(@PathVariable("currPair") String currPair) throws Exception {
        return ResponseEntity.ok( flexApiService.getForexRate(currPair));
    }

    @PostMapping("/newTransaction")
    public ResponseEntity<?> newTransaction(@RequestBody FlexApiMobileRequest request) throws Exception {
        return ResponseEntity.ok( flexApiService.newTransaction(request));
    }

    @PostMapping("/tarrif")
    public ResponseEntity<?> tarrif(@RequestBody TarrrifRateRequestVm request) throws Exception {
        return ResponseEntity.ok( flexApiService.tarrif(request));
    }

    @PostMapping("/deposit-from-agent")
    public ResponseEntity<?> depositAgent(@RequestBody DepositAgentRequestVm request) throws Exception {
        return ResponseEntity.ok( flexApiService.depositTransaction(request));
    }


    @PostMapping("/withdraw-from-agent")
    public ResponseEntity<?> withdrawalAgent(@RequestBody WithdrawalRequest request) throws Exception {
        return ResponseEntity.ok( flexApiService.withdrawTransaction(request));
    }

    @PostMapping("/customer-to-customer")
    public ResponseEntity<?> C2CDeposit(@RequestBody C2CRequest request) throws Exception {
        return ResponseEntity.ok( flexApiService.C2CDeposit(request));
    }

    @PostMapping("/agent-balance")
    public ResponseEntity<?> agentBalance(@RequestBody AgentBalanceRequest request) throws Exception {
        return ResponseEntity.ok( flexApiService.agentBalance(request));
    }

    @PostMapping("/airtime-purchase")
    public ResponseEntity<?> airTimePurchase(@RequestBody AirtimePurchaseRequest request) throws Exception {
        return ResponseEntity.ok( flexApiService.aritimePurcahse(request));
    }
    @ApiOperation(value = "Purchase prepaid electricity tokens")
    @PostMapping("/kplc-prepaid/purchase")
    public ResponseEntity<?> kplcPrepaidPurchase(@RequestBody @Valid KplcPrepaidRequest request) throws Exception {
        Map<String, Object> response = flexApiService.kplcPrepaidHandler(request);
        return ResponseEntity.ok().body( response );
    }
    @GetMapping("/transaction-history/{phoneNumber}")
    public ResponseEntity<?> transactionHistory(@PathVariable("phoneNumber") String phoneNumber) throws Exception {
        return ResponseEntity.ok( flexApiService.transactionHistory(phoneNumber));
    }


    @PostMapping("/internal-transfer/forecast-transaction")
    public ResponseEntity<?> foreCastTransaction(@RequestBody @Valid FetchAmountRequestVm request) throws Exception {
        Map<String, Object> response = flexApiService.foreCastTransaction(request);
        return ResponseEntity.ok().body( response );
    }

    @PostMapping("/internal-transfer/sent")
    public ResponseEntity<?> internalTransferSent(@RequestBody @Valid SentInternalAmountRequestVm request) throws Exception {
        Map<String, Object> response = flexApiService.sentInternalTransaction(request);
        return ResponseEntity.ok().body( response );
    }


    @PostMapping("/internal-transfer/receive")
    public ResponseEntity<?> internalTransferReceive(@RequestBody @Valid ReceiveInternalRequestVm request) throws Exception {
        Map<String, Object> response = flexApiService.receiveInternalTransaction(request);
        return ResponseEntity.ok().body( response );
    }

    @PostMapping("/internal-transfer/withdrawal")
    public ResponseEntity<?> codeAgentWithdrawal(@RequestBody @Valid CodeWithdrawalRequest request) throws Exception {
        Map<String, Object> response = flexApiService.codeAgentWithdrawal(request);
        return ResponseEntity.ok().body( response );
    }


}
