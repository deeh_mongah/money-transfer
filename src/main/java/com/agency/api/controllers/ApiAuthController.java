package com.agency.api.controllers;

import com.agency.api.ApiAuthServiceInterface;
import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping( "/api/users/v1")
public class ApiAuthController {

    @Autowired private ApiAuthServiceInterface apiAuthService;

    @PostMapping("/agent-login")
    public ResponseEntity<?> loginRequest(@RequestBody LoginRequest request) throws Exception {
        ResponseModel response = apiAuthService.login( request );
        return ResponseEntity.ok( response );
    }

    @GetMapping("/{passport}/details")
    public ResponseEntity<?> fetchDetails(@PathVariable("passport")String passport) throws Exception{
        IPRSResponse response = apiAuthService.fetchDetails( passport );
        return ResponseEntity.ok( response );
    }

    @PostMapping("/create")
    public ResponseEntity<?> createRequest(@RequestBody CreateCustomerRequest request){
        ResponseModel response = apiAuthService.createUser( request );
        return ResponseEntity.ok( response );
    }

    @PostMapping("/customer-login")
    public ResponseEntity<?> customerLoginRequest(@RequestBody CustomerLoginRequest request) throws Exception {
        ResponseModel response = apiAuthService.customerLogin( request );
        return ResponseEntity.ok( response );
    }

    @GetMapping("/{phone}")
    public ResponseEntity<?> validatePhoneNumber(@PathVariable("phone")String phone){
        ResponseModel response = apiAuthService.phoneValidate( phone );
        return ResponseEntity.ok( response );
    }

    @GetMapping("/password-rest/{phone}")
    public ResponseEntity<?> passwordReset(@PathVariable("phone")String phone){
        ResponseModel response = apiAuthService.passwordReset( phone );
        return ResponseEntity.ok( response );
    }



    @PostMapping("/otp-validate")
    public ResponseEntity<?> otpValidateRequest(@RequestBody OptValidateRequest request){
        ResponseModel response = apiAuthService.otpValidateRequest( request );
        return ResponseEntity.ok( response );
    }

    @PostMapping("/change-password")
    public ResponseEntity<?> changePasswordRequest(@RequestBody SetPinRequest request){
        ResponseModel response = apiAuthService.changePassword( request );
        return ResponseEntity.ok( response );
    }


    @PostMapping("/agent-pin-reset")
    public ResponseEntity<?> pinReset(@RequestBody SetPinRequest request){
        ResponseModel response = apiAuthService.pinReset( request );
        return ResponseEntity.ok( response );
    }

    @PostMapping("/test")
    public ResponseEntity<?> test(@RequestBody AuthServiceRequestVm request){
        AuthServiceResponseVm response = apiAuthService.test( request );
        return ResponseEntity.ok( response );
    }


}
