package com.agency.api.controllers;

import com.agency.api.FlexServiceInterface;
import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/payment")
public class PaymentController {

    @Autowired private FlexServiceInterface flexApiService;

    @PostMapping("/callback")
    public ResponseEntity<?> callBakC2B(@RequestBody JsonNode request ){
        System.err.println("testLoana stk");
        return ResponseEntity.ok( flexApiService.callBackStk( request ) );
    }

}
