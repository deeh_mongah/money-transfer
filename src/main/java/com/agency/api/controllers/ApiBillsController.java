//package com.agency.api.controllers;
//
//import com.agency.api.ApiBillsServiceInterface;
//import com.agency.api.rpm.ResponseModel;
//import com.agency.api.vm.AirtimePurchaseRequest;
//import com.agency.api.vm.NairobiWaterRequest;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//@RestController
//@RequestMapping( "/api/bills/v1")
//public class ApiBillsController {
//
//    @Autowired private ApiBillsServiceInterface billsService;
//
//    @PostMapping("/airtime")
//    public ResponseEntity<?> airtime(@RequestBody AirtimePurchaseRequest request) throws Exception{
//        ResponseModel response = billsService.airrtimePurchase( request );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/nairobi-water")
//    public ResponseEntity<?> nairobiWater(@RequestBody NairobiWaterRequest request) throws Exception{
//        ResponseModel response = billsService.nairobiWater( request );
//        return ResponseEntity.ok( response );
//    }
//}
