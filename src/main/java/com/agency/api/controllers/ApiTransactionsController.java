//package com.agency.api.controllers;
//
//import com.agency.api.ApiTransactionsServiceInterface;
//import com.agency.api.rpm.ResponseModel;
//import com.agency.api.vm.*;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.*;
//
//
//@RestController
//@RequestMapping( "/api/transactions/v1")
//public class ApiTransactionsController {
//
//    @Autowired private ApiTransactionsServiceInterface apiTransactionsService;
//
//    @PostMapping("/cardless-deposit")
//    public ResponseEntity<?> cardlessDeposit(@RequestParam("action")AccountActions action,
//                                             @RequestBody DepositRequest request){
//        ResponseModel response = apiTransactionsService.deposit( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/card-deposit")
//    public ResponseEntity<?> cardDeposit(@RequestParam("action")AccountActions action,
//                                         @RequestBody DepositRequest request){
//        ResponseModel response = apiTransactionsService.deposit( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/cardless-withdrawal")
//    public ResponseEntity<?> cardlessWithdrawal(@RequestParam("action")AccountActions action,
//                                                @RequestBody WithdrawRequest request){
//        ResponseModel response = apiTransactionsService.withdraw( request, action );
//        return ResponseEntity.ok( response );
//    }
//    @PostMapping("/card-withdrawal")
//    public ResponseEntity<?> cardlWithdrawal(@RequestParam("action")AccountActions action,
//                                             @RequestBody WithdrawRequest request){
//        ResponseModel response = apiTransactionsService.withdraw( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/merchant-transaction")
//    public ResponseEntity<?> merchantTransaction(@RequestBody MerchantTransactionsVm request){
//        ResponseModel response = apiTransactionsService.merchantTranasaction( request );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/cardless-balance-inquiry")
//    public ResponseEntity<?> cardlessBalanceInquiry(@RequestParam("action")AccountActions action,
//                                                @RequestBody BalanceInquiryRequest request){
//        ResponseModel response = apiTransactionsService.balanceInquiry( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/card-balance-inquiry")
//    public ResponseEntity<?> cardBalanceInquiry(@RequestParam("action")AccountActions action,
//                                                    @RequestBody BalanceInquiryRequest request){
//        ResponseModel response = apiTransactionsService.balanceInquiry( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/cardless-mini-statement")
//    public ResponseEntity<?> cardlessMiniStatement(@RequestParam("action")AccountActions action,
//                                                    @RequestBody BalanceInquiryRequest request){
//        ResponseModel response = apiTransactionsService.miniStatement( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/card-mini-statement")
//    public ResponseEntity<?> cardMiniStatement(@RequestParam("action")AccountActions action,
//                                                   @RequestBody BalanceInquiryRequest request){
//        ResponseModel response = apiTransactionsService.miniStatement( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/cardless-funds-tranfer")
//    public ResponseEntity<?> cardlessFundsTransfer(@RequestParam("action")AccountActions action,
//                                                   @RequestBody FundsTransferRequest request){
//        ResponseModel response = apiTransactionsService.fundsTransfer( request, action );
//        return ResponseEntity.ok( response );
//    }
//    @PostMapping("/card-funds-tranfer")
//    public ResponseEntity<?> cardFundsTransfer(@RequestParam("action")AccountActions action,
//                                                   @RequestBody FundsTransferRequest request){
//        ResponseModel response = apiTransactionsService.fundsTransfer( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//    @PostMapping("/account-opening")
//    public ResponseEntity<?> accountOpening(@RequestParam("action")AccountActions action,
//                                            @RequestBody AccountOpeningRequest request){
//        ResponseModel response = apiTransactionsService.accountOpening( request, action );
//        return ResponseEntity.ok( response );
//    }
//
//
//}
