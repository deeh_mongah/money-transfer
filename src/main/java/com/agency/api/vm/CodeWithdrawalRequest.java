package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CodeWithdrawalRequest {

    private String phoneNumber;
    private String uniqueCode;
    private String agentCode;
    private String currencyType;
    private BigDecimal amount;


}
