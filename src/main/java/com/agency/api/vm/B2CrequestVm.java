package com.agency.api.vm;

import java.math.BigDecimal;

/**
 * Created by Ben on 23-Jul-19.
 */
public class B2CrequestVm {


    /**
     * amount : 10
     * description : test
     * occasion : test
     * recipientPhoneNo : 254711158196
     * resultUrl : https://531010c8.ngrok.io/api/v1/loans/callback
     * source : 718762
     * transactionType : BusinessPayment
     */

    private BigDecimal amount;
    private String description;
    private String occasion;
    private String recipientPhoneNo;
    private String resultUrl;
    private String source;
    private String transactionType;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOccasion() {
        return occasion;
    }

    public void setOccasion(String occasion) {
        this.occasion = occasion;
    }

    public String getRecipientPhoneNo() {
        return recipientPhoneNo;
    }

    public void setRecipientPhoneNo(String recipientPhoneNo) {
        this.recipientPhoneNo = recipientPhoneNo;
    }

    public String getResultUrl() {
        return resultUrl;
    }

    public void setResultUrl(String resultUrl) {
        this.resultUrl = resultUrl;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }
}
