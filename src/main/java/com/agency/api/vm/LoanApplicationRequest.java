package com.agency.api.vm;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class LoanApplicationRequest {

    @NotNull
    private String loanTypeCode;

    @NotNull
    private String loanTypeName;

    @NotNull
    private String purpose;

    @NotNull
    private BigDecimal amount;

    @NotNull
    private CustomerData customer;

    @NotNull
    private BigDecimal repaymentPeriod;



    @NotNull
    private String loanUniqueCode;


    public BigDecimal getRepaymentPeriod() {
        return repaymentPeriod;
    }

    public void setRepaymentPeriod(BigDecimal repaymentPeriod) {
        this.repaymentPeriod = repaymentPeriod;
    }

    public String getLoanTypeCode() {
        return loanTypeCode;
    }

    public void setLoanTypeCode(String loanTypeCode) {
        this.loanTypeCode = loanTypeCode;
    }

    public String getLoanTypeName() {
        return loanTypeName;
    }

    public void setLoanTypeName(String loanTypeName) {
        this.loanTypeName = loanTypeName;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public CustomerData getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerData customer) {
        this.customer = customer;
    }

    public String getLoanUniqueCode() {
        return loanUniqueCode;
    }

    public void setLoanUniqueCode(String loanUniqueCode) {
        this.loanUniqueCode = loanUniqueCode;
    }

    public static class CustomerData{

        @NotNull
        private String idNumber;


        @NotNull
        private String mobileNo;

        public String getIdNumber() {
            return idNumber;
        }

        public void setIdNumber(String idNumber) {
            this.idNumber = idNumber;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }
    }
}