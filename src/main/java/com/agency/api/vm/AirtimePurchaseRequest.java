package com.agency.api.vm;

import java.math.BigDecimal;

public class AirtimePurchaseRequest {

    private BigDecimal amount;
    private String serviceProvider;
    private String recipientPhone;
    private String loggedinPhone;
    private String agentPhone;


    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getLoggedinPhone() {
        return loggedinPhone;
    }

    public void setLoggedinPhone(String loggedinPhone) {
        this.loggedinPhone = loggedinPhone;
    }

    public String getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(String serviceProvider) {
        this.serviceProvider = serviceProvider;
    }

    public String getRecipientPhone() {
        return recipientPhone;
    }

    public void setRecipientPhone(String recipientPhone) {
        this.recipientPhone = recipientPhone;
    }

    public String getAgentPhone() {
        return agentPhone;
    }

    public void setAgentPhone(String agentPhone) {
        this.agentPhone = agentPhone;
    }
}
