package com.agency.api.vm;

import java.math.BigDecimal;

/**
 * Created by ben on 07/11/2019.
 */
public class DepositAgentRequestVm {


    /**
     * userPhoneNumber : 0711158196
     * currencyType : 0711158196
     * amount : 100
     */

    private String userPhoneNumber;
    private String agentPhoneNumber;
    private String currencyType;
    private BigDecimal amount;

    public String getUserPhoneNumber() {
        return userPhoneNumber;
    }

    public void setUserPhoneNumber(String userPhoneNumber) {
        this.userPhoneNumber = userPhoneNumber;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getAgentPhoneNumber() {
        return agentPhoneNumber;
    }

    public void setAgentPhoneNumber(String agentPhoneNumber) {
        this.agentPhoneNumber = agentPhoneNumber;
    }
}
