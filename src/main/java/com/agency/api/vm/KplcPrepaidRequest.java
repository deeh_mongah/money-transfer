package com.agency.api.vm;


import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class KplcPrepaidRequest {

    @NotNull
    @NotBlank
    private String meterNo;

    @NotNull
    @NotBlank
    private String customerName;

    @NotNull
    @NotBlank
    private String customerPhoneNo;

    private String loggedInPhoneNo;

    @NotNull
    @Min( value = 50)
    private BigDecimal amount;

    @NotNull
    @NotBlank
    private String referenceNo;

    public String getMeterNo() {
        return meterNo;
    }

    public void setMeterNo(String meterNo) {
        this.meterNo = meterNo;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNo() {
        return customerPhoneNo;
    }

    public String getLoggedInPhoneNo() {
        return loggedInPhoneNo;
    }

    public void setLoggedInPhoneNo(String loggedInPhoneNo) {
        this.loggedInPhoneNo = loggedInPhoneNo;
    }

    public void setCustomerPhoneNo(String customerPhoneNo) {
        this.customerPhoneNo = customerPhoneNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }
}
