package com.agency.api.vm;

/**
 * Created by ben on 07/11/2019.
 */
public class CreateCustomerRequest {


    /**
     * email : ytg@tedt.com
     * firstName : test
     * mobileNo : 0711158222
     * password : 1234567
     * surName : test
     */

    private String email;
    private String firstName;
    private String mobileNo;
    private String password;
    private String surName;
    private String id;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }
}
