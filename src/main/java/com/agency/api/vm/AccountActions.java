package com.agency.api.vm;

import io.swagger.annotations.ApiModel;

@ApiModel(value = "Account actions options")
public enum AccountActions {
    initiate {
        @Override
        public String toString() {
            return "initiate";
        }
    },
    complete {
        @Override
        public String toString() {
            return "complete";
        }
    }
}
