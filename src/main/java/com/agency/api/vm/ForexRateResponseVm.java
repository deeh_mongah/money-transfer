package com.agency.api.vm;

import lombok.Data;

@Data
public class ForexRateResponseVm {


    /**
     * rate : 100
     * errorCode : 0
     * errorMsg : Success
     */

    private int rate;

    private String errorCode;

    private String errorMsg;


}
