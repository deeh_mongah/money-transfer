package com.agency.api.vm;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

public class LoanRepaymentRequest {

    @NotNull
    private BigDecimal amount;


    @NotNull
    private String loanUniqueCode;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }


    public String getLoanUniqueCode() {
        return loanUniqueCode;
    }

    public void setLoanUniqueCode(String loanUniqueCode) {
        this.loanUniqueCode = loanUniqueCode;
    }


}