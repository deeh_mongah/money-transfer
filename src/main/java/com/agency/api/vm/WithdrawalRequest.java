package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by User on 5/20/2020.
 */

@Data
public class WithdrawalRequest {


    private String agentNo;
    private String phoneNo;
    private BigDecimal amount;
    private String currencyType;
}
