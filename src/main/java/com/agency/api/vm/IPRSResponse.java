package com.agency.api.vm;

public class IPRSResponse {

    private String status;
    private String message;
    private Data data = new Data();

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data {

        private String firstName;
        private String surname;
        private String otherName;
        private String gender;
        private String passport;
        private String dob;
        private String homeLocation;
        private String passportSerial;
        private String nationality;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getOtherName() {
            return otherName;
        }

        public void setOtherName(String otherName) {
            this.otherName = otherName;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getPassport() {
            return passport;
        }

        public void setPassport(String passport) {
            this.passport = passport;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getHomeLocation() {
            return homeLocation;
        }

        public void setHomeLocation(String homeLocation) {
            this.homeLocation = homeLocation;
        }

        public String getPassportSerial() {
            return passportSerial;
        }

        public void setPassportSerial(String passportSerial) {
            this.passportSerial = passportSerial;
        }

        public String getNationality() {
            return nationality;
        }

        public void setNationality(String nationality) {
            this.nationality = nationality;
        }
    }
}
