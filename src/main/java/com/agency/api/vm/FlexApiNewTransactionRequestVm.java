package com.agency.api.vm;

/**
 * Created by ben on 07/11/2019.
 */
public class FlexApiNewTransactionRequestVm {


    /**
     * payoutAmount : 5000
     * sysCode : 3
     * senderNationality : KEN
     * senderDocType : Identity Card
     * receiverDocument : Identity Card
     * receiverSurname : JOHN
     * userCode : 290
     * senderOtherNames : DOE
     * password : 0e69049a8b3d908f89be34885efc5501
     * senderSurname : JANE
     * payingAmount : 70000
     * receiverContactNo : 254797611547
     * purposeforFunds : Personal Use
     * tariff : 0.0
     * receiverDocumentId : Dynamic
     * timestamp : 20190221194654
     * payoutCountry : KEN
     * currencyPair : USD - KES
     * senderDocNumber : Dynamic
     * transactionReference : TRN334856
     * payoutAgent : DXB
     * originCountry : KEN
     * receiverOthernames : DOE
     * transferType : inPerson
     * currencyRate : 100
     */

    private String payoutAmount;
    private String sysCode;
    private String senderNationality;
    private String senderDocType;
    private String receiverDocument;
    private String receiverSurname;
    private String userCode;
    private String senderOtherNames;
    private String password;
    private String senderSurname;
    private int payingAmount;
    private String receiverContactNo;
    private String purposeforFunds;
    private String tariff;
    private String receiverDocumentId;
    private String timestamp;
    private String payoutCountry;
    private String currencyPair;
    private String senderDocNumber;
    private String transactionReference;
    private String payoutAgent;
    private String originCountry;
    private String receiverOthernames;
    private String transferType;
    private int currencyRate;

    public String getPayoutAmount() {
        return payoutAmount;
    }

    public void setPayoutAmount(String payoutAmount) {
        this.payoutAmount = payoutAmount;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getSenderNationality() {
        return senderNationality;
    }

    public void setSenderNationality(String senderNationality) {
        this.senderNationality = senderNationality;
    }

    public String getSenderDocType() {
        return senderDocType;
    }

    public void setSenderDocType(String senderDocType) {
        this.senderDocType = senderDocType;
    }

    public String getReceiverDocument() {
        return receiverDocument;
    }

    public void setReceiverDocument(String receiverDocument) {
        this.receiverDocument = receiverDocument;
    }

    public String getReceiverSurname() {
        return receiverSurname;
    }

    public void setReceiverSurname(String receiverSurname) {
        this.receiverSurname = receiverSurname;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getSenderOtherNames() {
        return senderOtherNames;
    }

    public void setSenderOtherNames(String senderOtherNames) {
        this.senderOtherNames = senderOtherNames;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSenderSurname() {
        return senderSurname;
    }

    public void setSenderSurname(String senderSurname) {
        this.senderSurname = senderSurname;
    }

    public int getPayingAmount() {
        return payingAmount;
    }

    public void setPayingAmount(int payingAmount) {
        this.payingAmount = payingAmount;
    }

    public String getReceiverContactNo() {
        return receiverContactNo;
    }

    public void setReceiverContactNo(String receiverContactNo) {
        this.receiverContactNo = receiverContactNo;
    }

    public String getPurposeforFunds() {
        return purposeforFunds;
    }

    public void setPurposeforFunds(String purposeforFunds) {
        this.purposeforFunds = purposeforFunds;
    }

    public String getTariff() {
        return tariff;
    }

    public void setTariff(String tariff) {
        this.tariff = tariff;
    }

    public String getReceiverDocumentId() {
        return receiverDocumentId;
    }

    public void setReceiverDocumentId(String receiverDocumentId) {
        this.receiverDocumentId = receiverDocumentId;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getPayoutCountry() {
        return payoutCountry;
    }

    public void setPayoutCountry(String payoutCountry) {
        this.payoutCountry = payoutCountry;
    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getSenderDocNumber() {
        return senderDocNumber;
    }

    public void setSenderDocNumber(String senderDocNumber) {
        this.senderDocNumber = senderDocNumber;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public String getPayoutAgent() {
        return payoutAgent;
    }

    public void setPayoutAgent(String payoutAgent) {
        this.payoutAgent = payoutAgent;
    }

    public String getOriginCountry() {
        return originCountry;
    }

    public void setOriginCountry(String originCountry) {
        this.originCountry = originCountry;
    }

    public String getReceiverOthernames() {
        return receiverOthernames;
    }

    public void setReceiverOthernames(String receiverOthernames) {
        this.receiverOthernames = receiverOthernames;
    }

    public String getTransferType() {
        return transferType;
    }

    public void setTransferType(String transferType) {
        this.transferType = transferType;
    }

    public int getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(int currencyRate) {
        this.currencyRate = currencyRate;
    }
}
