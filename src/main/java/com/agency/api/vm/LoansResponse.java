package com.agency.api.vm;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LoansResponse {

    private String status;
    private String message;

    private List<LoansData> data = new ArrayList<>();

    //This object will be populated only on successful processing
    public LoansResponse() {
        this.status = "00";
        this.message = "Request processed successfully.";
    }

    public static class LoansData{

        private BigDecimal maxTenure;
        private BigDecimal maxAmount;
        private String code;
        private String name;
        private Long id;

        public BigDecimal getMaxTenure() {
            return maxTenure;
        }

        public void setMaxTenure(BigDecimal maxTenure) {
            this.maxTenure = maxTenure;
        }

        public BigDecimal getMaxAmount() {
            return maxAmount;
        }

        public void setMaxAmount(BigDecimal maxAmount) {
            this.maxAmount = maxAmount;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<LoansData> getData() {
        return data;
    }

    public void setData(List<LoansData> data) {
        this.data = data;
    }

}
