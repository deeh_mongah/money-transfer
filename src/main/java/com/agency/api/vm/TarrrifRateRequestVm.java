package com.agency.api.vm;

/**
 * Created by ben on 15/01/2020.
 */
public class TarrrifRateRequestVm {

    private String currencyPair;

    private String amount;

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
