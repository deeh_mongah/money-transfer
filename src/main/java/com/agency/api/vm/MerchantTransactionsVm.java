package com.agency.api.vm;

import java.math.BigDecimal;
import java.util.Date;

public class MerchantTransactionsVm {


    /**
     * accountNo : 161137/8/1
     * terminalNo : 161137/8/1
     * officerNo : 161137/8/1
     * amount : 161137/8/1
     * transactionNo : 161137/8/1
     * transactionDate : 161137/8/1
     * transactionTypeNo : 161137/8/1
     */

    private String accountNo;
    private String terminalNo;
    private long officerNo;
    private BigDecimal amount;
    private String transactionNo;
    private Date transactionDate;
    private long transactionTypeNo;


    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(String terminalNo) {
        this.terminalNo = terminalNo;
    }

    public long getOfficerNo() {
        return officerNo;
    }

    public void setOfficerNo(long officerNo) {
        this.officerNo = officerNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public long getTransactionTypeNo() {
        return transactionTypeNo;
    }

    public void setTransactionTypeNo(long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
    }
}
