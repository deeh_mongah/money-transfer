package com.agency.api.vm;

public class AuthServiceResponseVm {

    /**
     * status : 000
     * description : Request successful
     * data : {"pin_setup_url":"http://localhost:8081/api/v3/user/pin/setup/2cb4340c1abe98bfb81bc20aad37e03fa684b675eac3d29392902033aa56e50e43a53e11c8a7c8527765f29d314f380c349d6cb5a9a57d539293b9c8cfff99ed","session_identifier":"2cb4340c1abe98bfb81bc20aad37e03fa684b675eac3d29392902033aa56e50e43a53e11c8a7c8527765f29d314f380c349d6cb5a9a57d539293b9c8cfff99ed"}
     */

    private String status;
    private String description;
    private DataBean data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * pin_setup_url : http://localhost:8081/api/v3/user/pin/setup/2cb4340c1abe98bfb81bc20aad37e03fa684b675eac3d29392902033aa56e50e43a53e11c8a7c8527765f29d314f380c349d6cb5a9a57d539293b9c8cfff99ed
         * session_identifier : 2cb4340c1abe98bfb81bc20aad37e03fa684b675eac3d29392902033aa56e50e43a53e11c8a7c8527765f29d314f380c349d6cb5a9a57d539293b9c8cfff99ed
         */

        private String pin_setup_url;
        private String session_identifier;

        public String getPin_setup_url() {
            return pin_setup_url;
        }

        public void setPin_setup_url(String pin_setup_url) {
            this.pin_setup_url = pin_setup_url;
        }

        public String getSession_identifier() {
            return session_identifier;
        }

        public void setSession_identifier(String session_identifier) {
            this.session_identifier = session_identifier;
        }
    }
}
