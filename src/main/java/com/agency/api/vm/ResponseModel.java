package com.agency.api.vm;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class ResponseModel<T> {

    private String status;
    private String message;
    private T data;

    public ResponseModel() {
    }

    public ResponseModel(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResponseModel(String status, String message, T data) {
        this(status, message);
        this.data = data;
    }

    /**
     * Checks if the message returned is successful
     *
     * @return
     */
    @JsonIgnore
    public boolean isSuccess() {
        return "00".equals(status) || "0".equals(status);
    }

    //<editor-fold defaultstate="collapsed" desc="GETTERS AND SETTERS">
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
//</editor-fold>
}
