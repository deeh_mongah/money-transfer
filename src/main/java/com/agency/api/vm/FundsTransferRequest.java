package com.agency.api.vm;

import java.math.BigDecimal;

public class FundsTransferRequest {

    private BigDecimal amount;
    private String recipientAccNo;
    private String sourceAccNo;
    private String userId;

    private String terminalNo;
    private String token;

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getRecipientAccNo() {
        return recipientAccNo;
    }

    public void setRecipientAccNo(String recipientAccNo) {
        this.recipientAccNo = recipientAccNo;
    }

    public String getSourceAccNo() {
        return sourceAccNo;
    }

    public void setSourceAccNo(String sourceAccNo) {
        this.sourceAccNo = sourceAccNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(String terminalNo) {
        this.terminalNo = terminalNo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
