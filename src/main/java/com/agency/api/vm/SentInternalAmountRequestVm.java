package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class SentInternalAmountRequestVm {


    /**
     * senderPhoneNumber : 0711158196
     * senderNames : Ben Muuo
     * receiverPhoneNumber : 07111581916
     * receiverName : Ben Muuo
     * amount : 10
     * tarrif : 100
     */

    private String senderPhoneNumber;
    private String agentPhoneNumber;
    private String senderNames;
    private String receiverPhoneNumber;
    private String receiverName;
    private String currencyType;
    private BigDecimal amount;
    private BigDecimal tarrif;


}
