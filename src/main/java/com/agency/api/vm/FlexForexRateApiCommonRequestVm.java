package com.agency.api.vm;

/**
 * Created by ben on 07/11/2019.
 */
public class FlexForexRateApiCommonRequestVm {


    /**
     * password : 17d0609065b48c3164e49bf17ed54efe
     * userCode : 290
     * sysCode : 3
     * timestamp : 20190221200814
     */

    private String password;
    private String userCode;
    private String sysCode;
    private String timestamp;
    private String currPair;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getCurrPair() {
        return currPair;
    }

    public void setCurrPair(String currPair) {
        this.currPair = currPair;
    }
}
