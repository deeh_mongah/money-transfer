package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by ben on 08/11/2019.
 */
@Data
public class FeeRequestVm {


    /**
     * name : 00
     * minAmount : Login successful
     * maxAmount : adsa
     * paymentMode :
     * agentAmount :
     * flexAmount :
     * transactingAmount :
     */

    private String name;
    private long id;
    private BigDecimal minAmount;
    private BigDecimal maxAmount;
//    private String paymentMode;
//    private BigDecimal agentAmount;
//    private BigDecimal flexAmount;
    private String transactingAmount;
    private String transactionType;

}
