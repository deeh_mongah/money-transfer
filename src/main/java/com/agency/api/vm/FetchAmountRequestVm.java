package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class FetchAmountRequestVm {

    private BigDecimal amount;
}
