package com.agency.api.vm;

/**
 * Created by ben on 07/11/2019.
 */
public class FlexApiMobileRequest {


    /**
     * payoutAmount : 5000
     * senderNationality : KEN
     * senderDocType : Identity Card
     * receiverDocument : Identity Card
     * receiverSurname : JOHN
     * senderOtherNames : DOE
     * senderSurname : JANE
     * payingAmount : 70000
     * receiverContactNo : 254797611547
     * purposeforFunds : Personal Use
     * receiverDocumentId : Dynamic
     * payoutCountry : KEN
     * currencyPair : USD - KES
     * senderDocNumber : Dynamic
     * transactionReference : TRN334856
     * originCountry : KEN
     * receiverOthernames : DOE
     * transferType : inPerson
     * currencyRate : 100
     */

    private String agentNumber;
    private String senderTelephoneNo;
    private String payoutAmount;
//    private String senderNationality;
    private String senderDocType;
//    private String receiverDocument;
    private String receiverSurname;
    private String senderOtherNames;
    private String senderSurname;
    private int payingAmount;
    private String receiverContactNo;
    private String purposeforFunds;
    private long feeNo;
//    private String receiverDocumentId;
//    private String payoutCountry;
    private String currencyPair;
    private String senderDocNumber;
    private String transactionReference;
//    private String originCountry;
    private String receiverOthernames;
//    private String transferType;
//    private int currencyRate;


    public String getAgentNumber() {
        return agentNumber;
    }

    public void setAgentNumber(String agentNumber) {
        this.agentNumber = agentNumber;
    }

    public String getPayoutAmount() {
        return payoutAmount;
    }

    public void setPayoutAmount(String payoutAmount) {
        this.payoutAmount = payoutAmount;
    }

//    public String getSenderNationality() {
//        return senderNationality;
//    }
//
//    public void setSenderNationality(String senderNationality) {
//        this.senderNationality = senderNationality;
//    }


    public long getFeeNo() {
        return feeNo;
    }

    public void setFeeNo(long feeNo) {
        this.feeNo = feeNo;
    }

    public String getSenderDocType() {
        return senderDocType;
    }

    public void setSenderDocType(String senderDocType) {
        this.senderDocType = senderDocType;
    }

    public String getReceiverSurname() {
        return receiverSurname;
    }

    public void setReceiverSurname(String receiverSurname) {
        this.receiverSurname = receiverSurname;
    }

    public String getSenderOtherNames() {
        return senderOtherNames;
    }

    public void setSenderOtherNames(String senderOtherNames) {
        this.senderOtherNames = senderOtherNames;
    }

    public String getSenderSurname() {
        return senderSurname;
    }

    public void setSenderSurname(String senderSurname) {
        this.senderSurname = senderSurname;
    }

    public int getPayingAmount() {
        return payingAmount;
    }

    public void setPayingAmount(int payingAmount) {
        this.payingAmount = payingAmount;
    }

    public String getReceiverContactNo() {
        return receiverContactNo;
    }

    public void setReceiverContactNo(String receiverContactNo) {
        this.receiverContactNo = receiverContactNo;
    }

    public String getPurposeforFunds() {
        return purposeforFunds;
    }

    public void setPurposeforFunds(String purposeforFunds) {
        this.purposeforFunds = purposeforFunds;
    }

//    public String getPayoutCountry() {
//        return payoutCountry;
//    }
//
//    public void setPayoutCountry(String payoutCountry) {
//        this.payoutCountry = payoutCountry;
//    }

    public String getCurrencyPair() {
        return currencyPair;
    }

    public void setCurrencyPair(String currencyPair) {
        this.currencyPair = currencyPair;
    }

    public String getSenderDocNumber() {
        return senderDocNumber;
    }

    public void setSenderDocNumber(String senderDocNumber) {
        this.senderDocNumber = senderDocNumber;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public String getSenderTelephoneNo() {
        return senderTelephoneNo;
    }

    public void setSenderTelephoneNo(String senderTelephoneNo) {
        this.senderTelephoneNo = senderTelephoneNo;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }
//
//    public String getOriginCountry() {
//        return originCountry;
//    }
//
//    public void setOriginCountry(String originCountry) {
//        this.originCountry = originCountry;
//    }

    public String getReceiverOthernames() {
        return receiverOthernames;
    }

    public void setReceiverOthernames(String receiverOthernames) {
        this.receiverOthernames = receiverOthernames;
    }
}
