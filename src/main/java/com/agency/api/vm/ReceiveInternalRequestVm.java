package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class ReceiveInternalRequestVm {

    private String phoneNumber;
    private String uniqueCode;
    private String agentCode;
    private String names;
    private BigDecimal amount;


}
