package com.agency.api.vm;

import lombok.Data;

import java.math.BigDecimal;

/**
 * Created by User on 5/20/2020.
 */

@Data
public class AgentBalanceResponse {

    private String currency;
    private BigDecimal agentBalance;
}
