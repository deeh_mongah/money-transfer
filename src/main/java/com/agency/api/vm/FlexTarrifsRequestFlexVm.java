package com.agency.api.vm;

/**
 * Created by ben on 15/01/2020.
 */

public class FlexTarrifsRequestFlexVm {


    /**
     * password : 17d0609065b48c3164e49bf17ed54efe
     * userCode : 290
     * sysCode : 3
     * amount : 1000
     * currPair : USD - KES
     * timestamp : 20190221200814
     */

    private String password;
    private String userCode;
    private String sysCode;
    private String amount;
    private String currPair;
    private String timestamp;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getSysCode() {
        return sysCode;
    }

    public void setSysCode(String sysCode) {
        this.sysCode = sysCode;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCurrPair() {
        return currPair;
    }

    public void setCurrPair(String currPair) {
        this.currPair = currPair;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
