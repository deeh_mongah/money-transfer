package com.agency.api.http;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.BufferingClientHttpRequestFactory;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

import java.util.Collections;

@Configuration
public class HttpBeans {

    @Autowired private RestTemplateBuilder restTemplateBuilder;

    @Bean
    public RestTemplate restTemplate(){
        ClientHttpRequestFactory factory = new BufferingClientHttpRequestFactory(
                new SimpleClientHttpRequestFactory()
        );

        RestTemplate restTemplate = restTemplateBuilder
                .errorHandler( new RestResponseErrorHandler() )
                .build();
        restTemplate.setRequestFactory( factory );
        restTemplate.setInterceptors(Collections.singletonList(new RestLogInterceptor()));
        return  restTemplate;
    }

//    @Bean
    public CommonsRequestLoggingFilter loggingFilter(){
        CommonsRequestLoggingFilter filter = new CommonsRequestLoggingFilter();
        filter.setIncludeQueryString(true);
        filter.setIncludePayload(true);
        filter.setMaxPayloadLength(10000);
        filter.setIncludeHeaders( true );
//        filter.setAfterMessagePrefix("REQUEST DATA : ");
        return filter;
    }
}
