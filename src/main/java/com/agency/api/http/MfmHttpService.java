package com.agency.api.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;

import java.nio.charset.Charset;

import static org.springframework.web.util.UriComponentsBuilder.fromHttpUrl;

@Component
public class MfmHttpService {

    @Value("${mfm.base-url}")
    protected String baseUrl;

    @Value("${mfm.client-key}")
    private String clientKey;

    @Value("${mfm.client-secret}")
    private String clientSecret;

    @Autowired protected RestTemplate restTemplate;
    @Autowired protected ObjectMapper objectMapper;

    public HttpHeaders createHeaders() {
        return new HttpHeaders() {
            {
                //Authorization
                String auth = String.format("%s:%s", clientKey, clientSecret );
                byte[] encodedAuth = Base64.encodeBase64(  auth.getBytes(Charset.forName("US-ASCII")) );
                String authHeader = "Basic " + new String( encodedAuth );
                set( "Authorization", authHeader );

                set("Content-Type", MediaType.APPLICATION_JSON.toString());
                set("Accept", "application/json");
            }
        };
    }

    /**
     *
     * @param uri
     * @param request
     * @return ResponseEntity<String>
     * @throws Exception
     */
    public ResponseEntity<String> postRequest( String uri, Object request ) throws Exception {
        //Fetch Host
        /*Pack the URL for this request*/
        UriComponents uriComponents = fromHttpUrl( baseUrl ).path( uri ).build().encode();
        HttpHeaders httpHeaders = createHeaders();
        String jsonRequest = objectMapper.writeValueAsString(request);

        HttpEntity<?> httpEntity = new HttpEntity<>( jsonRequest, httpHeaders );
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                uriComponents.toString(),
                HttpMethod.POST,
                httpEntity,
                String.class
        );

        return responseEntity;
    }
}
