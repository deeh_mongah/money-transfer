package com.agency.api;


import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.Map;

/**
 * Created by ben on 07/11/2019.
 */
public interface FlexServiceInterface {

    public ResponseModel getCountries() throws Exception;

    public ResponseModel callBackStk(JsonNode response);

    public ResponseModel getCurrency() throws Exception;

    public ResponseModel getAgents() throws Exception;

    public ResponseModel getForexRate(String currPair) throws Exception;

    public ResponseModel transactionHistory(String phoneNumber) throws Exception;

    public ResponseModel newTransaction(FlexApiMobileRequest currPair) throws Exception;

    public ResponseModel tarrif(TarrrifRateRequestVm requestVm) throws Exception;

    public ResponseModel depositTransaction(DepositAgentRequestVm currPair) throws Exception;


    public ResponseModel withdrawTransaction(WithdrawalRequest withdrawalRequest) throws Exception;

    public ResponseModel C2CDeposit(C2CRequest request) throws Exception;

    public ResponseModel agentBalance(AgentBalanceRequest balance) throws Exception;

    public Map<String, Object> kplcPrepaidHandler( KplcPrepaidRequest request) throws Exception;

    public Map<String, Object> foreCastTransaction( FetchAmountRequestVm request) throws Exception;

    public Map<String, Object> sentInternalTransaction( SentInternalAmountRequestVm request) throws Exception;

    public Map<String, Object> receiveInternalTransaction( ReceiveInternalRequestVm request) throws Exception;


    public Map<String, Object> codeAgentWithdrawal( CodeWithdrawalRequest request) throws Exception;


    public ResponseModel aritimePurcahse(AirtimePurchaseRequest currPair) throws Exception;

    public ResponseModel fetchKplcPrepaidMeterDetails(KplcPrepaidRequest request ) throws Exception;

}
