package com.agency.api;

import com.agency.api.rpm.ResponseModel;
import com.agency.api.vm.*;

public interface ApiTransactionsServiceInterface {

    /**
     * Cardless deposit
     *
     * @return ResponseModel
     */
    public ResponseModel deposit( DepositRequest request, AccountActions action );

    /**
     * Card deposit
     *
     * @return ResponseModel
     */
    public ResponseModel depositWithCard();


    /**
     * Cardless withdrawal
     *
     * @return ResponseModel
     */
    public ResponseModel withdraw( WithdrawRequest request, AccountActions action );

    public ResponseModel merchantTranasaction( MerchantTransactionsVm request );

    /**
     * Cardless balance inquiry
     *
     * @return ResponseModel
     */
    public ResponseModel balanceInquiry(BalanceInquiryRequest request, AccountActions action );

    /**
     * Cardless mini-statement
     *
     * @return ResponseModel
     */
    public ResponseModel miniStatement(BalanceInquiryRequest request, AccountActions action );

    /**
     * Cardless funds transfer
     *
     * @param request
     * @param action
     * @return
     */
    public ResponseModel fundsTransfer(FundsTransferRequest request, AccountActions action );

    /**
     * Account opening
     *
     * @param request
     * @param action
     * @return
     */
    public ResponseModel accountOpening(AccountOpeningRequest request, AccountActions action );

}
