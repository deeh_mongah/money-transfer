/*
 * The MIT License
 *
 * Copyright 2016 Riverbank Solutions Limited.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.agency.core.config;

import com.agency.web.usermanager.auth.AppAuthenticationProvider;
import com.agency.web.usermanager.auth.handlers.AppLogoutHandler;
import com.agency.web.usermanager.auth.handlers.LoginSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.LogoutHandler;

/**
 * This application uses this class to wrap up all security properties to be
 * used by this application.
 * <code>
 * It makes use of a custom authentication provider to achieve the following aspects according to PCI DSS:
 * a). Locks account after no more than 5 failed attempts
 * b). Sets the lockout duration to a minimum of 30 minutes( or as the system requirements detects) or
 *    until the administrator enables this account
 * c). Notifying the user during the executions of a). and b) above.
 * </code>
 *
 *
 * @author  Anthony Mwawughanga
 * @date    14-05-2016
 *
 * Notes:
 * 1. Spring boot auto configured security policy has been dropped
 * 2. Spring boot method-level security has been turned on
 * 3. These properties should be updated every now and then to ensure security of this application
 */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private AppAuthenticationProvider appAuthenticationProvider;

    private static final String[] SWAGGER_WHITELIST = {
            // -- swagger ui
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/configuration/**",
            "/v2/api-docs",
            "/webjars/**"
    };
//
    /**
     * Customized security policy for this application
     * Notes:
     * 1. The paths listed in antMatchers() are granted unconditional access
     * 2. All other paths other than 1 will require authentication
     * 3. Spring security is configured to use form-based authentication
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .authorizeRequests()
                    .antMatchers( SWAGGER_WHITELIST ).permitAll()
                    .antMatchers("/api/**", "/password-reset/**", "/setup-account/*").permitAll()
                    .antMatchers("/resetpassword/*", "/change-password").permitAll()
                    .antMatchers("/plugins/**").permitAll()
                    .anyRequest().authenticated()
                    .and()
                .formLogin()
                    .loginPage("/login")
                    .successHandler(successHandler())
                    .failureUrl("/login?error")
                    .usernameParameter("email").passwordParameter("password").permitAll()
                    .and()
                .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/login?logout")
                    .invalidateHttpSession( true )
                    .deleteCookies("JSESSIONID")
                    .addLogoutHandler( customLogoutHandler() )
                    .permitAll()
                    .and()
                .csrf().and()
                .sessionManagement()
                    .invalidSessionUrl("/login").enableSessionUrlRewriting(false)
                    /*Concurrent session control*/
                    .maximumSessions(1)
                    /*Session timeout management*/
                    .expiredUrl("/login?expired");

    }

    /**
     * Serve static resources-css, js, images- without authentication
     *
     * @param websecurity
     */
    @Override
    public void configure(WebSecurity websecurity) {
//        Register application requests to be ignored by Spring Security
        websecurity.ignoring().antMatchers("/css/**", "/plugins/**", "/layout/**", "/img/**", "/js/**", "/fonts/**", "/api/**")
                .antMatchers("/theme/**");
    }

    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(appAuthenticationProvider);
    }

    /**
     * Override default role prefix-ing(ROLE_somerole)
     *
     * @return DefaultRolesPrefixPostProcessor
     */
    @Bean
    public static DefaultRolesPrefixPostProcessor defaultRolesPrefixPostProcessor() {
        return new DefaultRolesPrefixPostProcessor();
    }

    /**
     * Custom handler to be called when a user has been successfully authenticated
     *
     * @return AuthenticationSuccessHandler
     */
    @Bean
    public AuthenticationSuccessHandler successHandler() {
        return new LoginSuccessHandler();
    }

    /**
     *  Bean to enable auditing of user logouts events
     *
     * @return LogoutHandler
     */
    @Bean
    public LogoutHandler customLogoutHandler(){
        return new AppLogoutHandler();
    }

}
