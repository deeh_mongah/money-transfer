/*
 * The MIT License
 *
 * Copyright 2016 Ken Gichia.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.agency.core.template.datatables;


/**
 * Datatables row format component.
 *
 * This interface defines the methods a row format class should implement when
 * defining a class that will be used to format row content
 *
 * @category  DB/DataTable
 * @package   Teke
 * @version   1.0.0
 * @since     2016-01-25
 * @author    Ken Gichia
 */
public interface RowFormatInterface {
    /**
     * All formatters will implement this method that will be called by the 
     * datatable class to format the content
     * 
     * @param   row
     * @return  Object[]
     */
    public Object[] formatRow(Object[] row);
}
