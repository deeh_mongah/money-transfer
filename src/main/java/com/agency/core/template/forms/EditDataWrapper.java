package com.agency.core.template.forms;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation marks a column that contains an instance of a bean 
 * that has been serialized into a string
 * <p>
 * This implementation follows that a modified bean is not persisted as a different entity
 * but to a column.
 * 
 * 
 * @category    Forms
 * @package     Quickpay
 * @version     1.0.0
 * @author      Anthony Mwawughanga
 * @since       08-08-2016
 */
@Target(value = {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface EditDataWrapper {}
