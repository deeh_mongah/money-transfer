package com.agency.core.template.forms;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation identifies all entity fields that changes can be tracked
 * <p>
 * Apply this annotation for entities with maker-checker requirement
 * 
 * @category    Forms
 * @package     Quickpay
 * @version     1.0.0
 * @author      Anthony Mwawughanga
 * @since       07-08-2016
 */
@Target(value = {ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface MutableField {

    String name() default "";
    String entity() default "";
    String reference() default "";
    boolean optional() default false;
}
