package com.agency.core.template;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.PhoneNumberUtil.PhoneNumberFormat;
import com.google.i18n.phonenumbers.Phonenumber;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;

import org.whispersystems.libsignal.logging.Log;

import java.util.Locale;

public class PhoneNumberFormatter {

    private static final String TAG = PhoneNumberFormatter.class.getSimpleName();

    public static boolean isValidNumber(String number) {
        return number.matches("^\\+[0-9]{10,}")  ||
                number.matches("^\\+685[0-9]{5}") ||
                number.matches("^\\+376[0-9]{6}") ||
                number.matches("^\\+299[0-9]{6}") ||
                number.matches("^\\+597[0-9]{6}") ||
                number.matches("^\\+298[0-9]{6}") ||
                number.matches("^\\+240[0-9]{6}") ||
                number.matches("^\\+687[0-9]{6}") ||
                number.matches("^\\+689[0-9]{6}");
    }



    public static String formatNumberInternational(String number) {
        try {
            PhoneNumberUtil util     = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber parsedNumber = util.parse(number, null);
            return util.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException e) {
            Log.w(TAG, e);
            return number;
        }
    }



    public static String getRegionDisplayName(String regionCode) {
        return (regionCode == null || regionCode.equals("ZZ") || regionCode.equals(PhoneNumberUtil.REGION_CODE_FOR_NON_GEO_ENTITY))
                ? "Unknown country" : new Locale("", regionCode).getDisplayCountry(Locale.getDefault());
    }

    public static String formatE164(String countryCode, String number) {
        try {
            PhoneNumberUtil util     = PhoneNumberUtil.getInstance();
            int parsedCountryCode    = Integer.parseInt(countryCode);
            PhoneNumber parsedNumber = util.parse(number,
                    util.getRegionCodeForCountryCode(parsedCountryCode));

            return util.format(parsedNumber, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (NumberParseException | NumberFormatException npe) {
            Log.w(TAG, npe);
        }

        return "+"                                                     +
                countryCode.replaceAll("[^0-9]", "").replaceAll("^0*", "") +
                number.replaceAll("[^0-9]", "");
    }

    public static String getInternationalFormatFromE164(String e164number) {
        try {
            PhoneNumberUtil util     = PhoneNumberUtil.getInstance();
            PhoneNumber parsedNumber = util.parse(e164number, null);
            return util.format(parsedNumber, PhoneNumberFormat.INTERNATIONAL);
        } catch (NumberParseException e) {
            Log.w(TAG, e);
            return e164number;
        }
    }
}
