package com.agency.core.template;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

/**
 *
 * This class overwrites Jackson JSON NullSerializer to output an empty String
 * for Bean fields with null values
 *
 * @author Mghanga
 * @version 1.0.0
 * @project quicklink
 * @since 23/05/2017 11:23
 */
public class NullSerializer extends JsonSerializer<Object> {

    // Generate the preferred JSON value
    @Override
    public void serialize(Object t, JsonGenerator jg, SerializerProvider sp) throws IOException {
        jg.writeString("");
    }
}
