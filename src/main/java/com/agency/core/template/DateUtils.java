package com.agency.core.template;

import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;

@Component
public class DateUtils {

    public static int daysUntilToday(Date date) {
        return daysBetween(date, new Date());
    }

    public static String cssClass(Date date) {
        int days = daysBetween(date, new Date());
        if( days >3 && days <= 5) return "badge-warning";
        else if( days <= 3) return "badge-danger";
        return "";
    }

    public static int daysBetween(Date before, Date after) {
        Calendar c1 = createCalendarWithoutTime(before);
        Calendar c2 = createCalendarWithoutTime(after);
        int days = 0;

        if ( c1.before(c2) ) {
            for (; c1.before(c2); days++) {
                c1.add(Calendar.DATE, 1);
            }
        }
        else {
            for (; c2.before(c1); days++) {
                c2.add(Calendar.DATE, 1);
            }
        }

        return days;
    }

    private static Calendar createCalendarWithoutTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        if ( null == date ) date = calendar.getTime();

        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }
}
