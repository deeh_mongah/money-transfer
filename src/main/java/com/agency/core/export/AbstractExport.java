package com.agency.core.export;

import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public abstract class AbstractExport implements ExportInterface {
    
    @PersistenceContext
    private EntityManager entityManager;
    
    private String _query;
    private boolean _nativeSql = false;
    protected String[] _columns;
    protected String _fileName = "export";
    protected HttpServletResponse _response;
    
    private Map<String, Object> _bag = new HashMap<String, Object>();
    private Map<String, Collection> _listParams = new HashMap<String, Collection>();
    
    /**
     * Set the filename to use
     * 
     * @param   response
     * @return  ExportInterface
     */
    @Override
    public ExportInterface setResponse(HttpServletResponse response) {
        _response = ( null != response ) ? response: null;
        return this;
    }
    
    /**
     * Set the filename to use
     * 
     * @param   fileName
     * @return  ExportInterface
     */
    @Override
    public ExportInterface setFileName(String fileName) {
        _fileName = ( null==fileName || fileName.isEmpty() ) ? "csv-export": fileName;
        return this;
    }
    
    /**
     * Set the columns to use in the export file title
     * 
     * @param   columns
     * @return  ExportInterface
     */
    @Override
    public ExportInterface setColumns(String[] columns) {
        _columns = columns;
        return this;
    }
    
    /**
     * Set the query string to use to generate the export document
     * 
     * @param   query
     * @return  ExportInterface
     */
    @Override
    public ExportInterface setQuery(String query) {
        _query = query;
        return this;
    }
    
    /**
     * Indicate that the query string supplied is a hibernate query or native 
     * sql
     * 
     * @param   flag
     * @return  ExportInterface
     */
    @Override
    public ExportInterface nativeSQL(boolean flag) {
        _nativeSql = flag;
        return this;
    }
    
    /**
     * Set auxiliary parameters that will be used by the export utility
     * 
     * @param   key
     * @param   value
     * @return  ExportInterface
     */
    @Override
    public ExportInterface setParameter(String key, Object value) {
        _bag.put(key, value);
        return this;
    }
    
    /**
     * Set auxiliary parameters that will be used by the export utility
     * 
     * @param   key
     * @param   value
     * @return  ExportInterface
     */
    @Override
    public ExportInterface setParameterList(String key, Collection value) {
        _listParams.put(key, value);
        return this;
    }
    
    /**
     * Called to build the query per the parameters defined if any
     * 
     * @return  Query
     */
    protected Query resultSet() {
        Session session = entityManager.unwrap( Session.class );
        Query q = _nativeSql ? session.createNativeQuery(_query): session.createQuery(_query);
        
        // Set the parameters needed
        if ( !_bag.isEmpty() ) {
            for (Map.Entry<String, Object> p : _bag.entrySet()) {
                try {
                    q.setParameter(p.getKey(), p.getValue());
                } 
                catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        
        if ( !_listParams.isEmpty() ) {
            for (Map.Entry<String, Collection> p : _listParams.entrySet()) {
                try {
                    q.setParameterList(p.getKey(), p.getValue());
                } 
                catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
        
        // The query object
        return q;
    }
    
    /**
     * Check whether all the required parameters have been defined
     * 
     * @param   setting
     * @throws  IOException
     */
    protected void checkParams(String setting) throws IOException {
        // The response object is not valid
        if ( null == _response ) 
            throw new IOException("Use the setResponse method to define a valid response object.");
        
        // Check if the hql is valid
        if ( "hql".equals(setting) && (null == _query || _query.isEmpty()) )
            throw new IOException("Use the setQuery method to define a valid HQL/SQL statement.");
    }
}
