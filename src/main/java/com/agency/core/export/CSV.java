package com.agency.core.export;

import com.opencsv.CSVWriter;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author Anthony
 */
@Transactional(readOnly = true)
@Service("csvGridExportService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class CSV extends AbstractExport {

     /**
     * Generate a csv file from an array of strings passed to the service
     * 
     * @param   data
     * @throws  IOException
     */
    @Override
    public void generateDoc(List<String[]> data) throws IOException {
        // Check if all the required params have been defined
        checkParams(null);
        
        // Set the appropriate headers and initialise the writer
        CSVWriter writer = setHeaders(_fileName);
        
        // Set the columns if defined
        if ( null != _columns && _columns.length > 0 ) writer.writeNext(_columns);
        
        // Write the data
        writer.writeAll(data);
        
        // Close the writer and send the response
        writer.close();
    }
    
    /**
     * Generate a csv file from the HQL passed to the object
     * 
     * @throws  IOException
     */
    @Override
    public void generateDoc() throws IOException {
        // If the response or the hql has not been defined, end here
        checkParams("hql");
        
        // Set the appropriate headers and initialise the writer
        CSVWriter writer = setHeaders(_fileName);
        List<String> row = new ArrayList<String>();
        int i = 0;
        
        // Set the columns if defined
        if ( null != _columns && _columns.length > 0 ) writer.writeNext(_columns);
        
        // Get the query
        Iterator<Object[]> it = resultSet()
            .setMaxResults( 100 )
            .setFirstResult(i)
            .list().iterator();
        
        // loop through the records
        while ( it.hasNext() ) {
            
            // Generate the list
            while ( it.hasNext() ) {
                row.clear();
                i = 0;
                
                for ( Object col: it.next() ) {
                    if ( i < _columns.length ) 
                        row.add((null != col) ? col.toString(): null); 
                    i++;
                }
                
                writer.writeNext(row.toArray(new String[0]));
            }
            
            // Get the next set of results
            i += 100;
            it = resultSet()
                .setMaxResults( 100 )
                .setFirstResult(i)
                .list().iterator();
        }
        
        // Close the writer and send the response
        writer.close();
    }
    
    /**
     * Generate the headers as required
     * 
     * @param   fileName
     * @return  CSVWriter
     * @throws  IOException
     */
    protected CSVWriter setHeaders(String fileName) throws IOException {
        _response.setContentType("text/csv;charset=utf-8"); 
        _response.setHeader("Content-Disposition","attachment; filename=\""+fileName+".csv\"");
        
        return new CSVWriter(new OutputStreamWriter(new BufferedOutputStream(_response.getOutputStream())));
    }
}
