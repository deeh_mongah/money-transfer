package com.agency.core.export;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface PDFInterface {
    /**
     * Set the html used to generate the desired document
     * 
     * @param   template
     * @return  PDFInterface
     */
    public PDFInterface setHTML(String template);
    
    /**
     * Set the html used to generate the desired document
     * 
     * @param   template
     * @param   map
     * @return  PDFInterface
     */
    public PDFInterface setHTML(String template, Map<String, Object> map);
    
    /**
     * Generate the pdf document for export
     * 
     * @param   response
     * @throws ServletException
     */
    public void generateDoc(HttpServletResponse response) throws ServletException;
}
