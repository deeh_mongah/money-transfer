package com.agency.core.export;

import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Cell;
import be.quodlibet.boxable.Row;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.awt.*;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@Transactional(readOnly = true)
@Service("pdfGridExportService")
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PDFGrid extends AbstractExport {

    /**
     * Generate a pdf file from an array of strings passed to the service
     * 
     * @param   data
     * @throws  IOException
     */
    @Override
    public void generateDoc(List<String[]> data) throws IOException {
        // Check if all the required params have been defined
        checkParams(null);
        
        // Set the appropriate headers
        setHeaders(_fileName);
        
        // Create a document and add a page to it
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage( page );
        
        // The base table object
        BaseTable table = getBaseTable(document, page);
        Row<PDPage> row; Cell<PDPage> cell;
        int i = 0; int length = 0;
        
        // Set the columns if defined
        if ( null != _columns && _columns.length > 0 ) {
            row = table.createRow(15f);
            length = _columns.length;
            
            for ( String ccel: _columns ) {
                cell = row.createCell((100f / length), ccel);
                cell.setFont(PDType1Font.TIMES_ROMAN );
                cell.setFillColor(Color.BLACK);
                cell.setTextColor(Color.WHITE);
            }
            
            table.addHeaderRow(row);
        }
        
        // Write the data
        for ( String[] rrow: data ) {
            if ( length == 0 ) length = rrow.length;
            
            row = table.createRow(10f);

            for ( String ccel: rrow ) {
                cell = row.createCell((100f / length), ccel);
                cell.setTextColor(Color.BLACK);
            }
        }
        
        // Draw the table
        table.draw();

        // Save the document in the output stream
        BufferedOutputStream ostream = new BufferedOutputStream(_response.getOutputStream());
        document.save( ostream );

        // Close the document
        document.close();
    }
    
    /**
     * Generate a csv file from the HQL passed to the object
     * 
     * @throws  IOException
     */
    @Override
    public void generateDoc() throws IOException {
        // If the response or the hql has not been defined, end here
        checkParams("hql");
        
        // Set the appropriate headers
        setHeaders(_fileName);
        
        // Create a document and add a page to it
        PDDocument document = new PDDocument();
        PDPage page = new PDPage();
        document.addPage( page );
        
        // The base table object
        BaseTable table = getBaseTable(document, page);
        Row<PDPage> row; Cell<PDPage> cell;
        int i = 0; int length = 0;
        Object[] rrow;
        
        // Set the columns if defined
        if ( null != _columns && _columns.length > 0 ) {
            row = table.createRow(15f);
            length = _columns.length;
            
            for ( String ccel: _columns ) {
                cell = row.createCell((100f / length), ccel);
                cell.setFont(PDType1Font.HELVETICA_BOLD);
                cell.setFillColor(Color.BLACK);
                cell.setTextColor(Color.WHITE);
            }
            
            table.addHeaderRow(row);
        }
        
        // Get the query
        Iterator<Object[]> it = resultSet()
//            .setMaxResults(MAX_RECORDS)
//            .setFirstResult(i)
            .list().iterator();
        
        // loop through the records
//        while ( it.hasNext() ) {
            
            // Generate the list
            while ( it.hasNext() ) {
                row = table.createRow(10f);
                rrow = it.next();
                
                if ( length == 0 ) length = _columns.length;
                i = 0;

                for ( Object col: rrow ) {
                    if ( i < length ) {
                        cell = row.createCell((100f / length), ((null != col) ? col.toString(): null));
                        cell.setTextColor(Color.BLACK);
                    } i++;
                }
            }
            
            // Get the next set of results
//            i += MAX_RECORDS;
//            it = resultSet()
//                .setMaxResults(MAX_RECORDS)
//                .setFirstResult(i)
//                .list().iterator();
//        }
        
        // Draw the table
        table.draw();

        // Save the document in the output stream
        BufferedOutputStream ostream = new BufferedOutputStream(_response.getOutputStream());
        document.save(ostream);

        // Close the document
        document.close();
    }
    
    /**
     * Generate the headers as required
     * 
     * @param   fileName
     */
    protected void setHeaders(String fileName) {
        _response.setContentType("application/pdf;charset=utf-8"); 
        _response.setHeader("Content-Disposition","attachment; filename=\""+fileName+".pdf\"");
    }
    
    /**
     * Get the basetable item used to generate the pdf document
     * 
     * @param   page
     * @return  BaseTable
     */
    private BaseTable getBaseTable(PDDocument document, PDPage page) throws IOException {
        // The page margins
        float ymargin = 20;
        float xmargin = 30;
        
        // Return the object
        return new BaseTable (
            page.getMediaBox().getHeight() - (2 * ymargin), // yStart
            page.getMediaBox().getHeight() - (2 * ymargin), // yStartNewPage
            70,                                             // bottomMargin
            page.getMediaBox().getWidth() - (2 * xmargin),  // tableWidth
            xmargin,                                        // page margin
            document, 
            page, 
            true, 
            true                                            // drawContent
        );
    }
}
