package com.agency.core.export;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public interface ExportInterface {
    /**
     * Set the filename to use
     * 
     * @param   response
     * @return  ExportInterface
     */
    public ExportInterface setResponse(HttpServletResponse response);
    
    /**
     * Set the filename to use
     * 
     * @param   fileName
     * @return  ExportInterface
     */
    public ExportInterface setFileName(String fileName);
    
    /**
     * Set the columns to use in the export file title
     * 
     * @param   columns
     * @return  ExportInterface
     */
    public ExportInterface setColumns(String[] columns);
    
    /**
     * Set the query string to use to generate the export document
     * 
     * @param   query
     * @return  ExportInterface
     */
    public ExportInterface setQuery(String query);
    
    /**
     * Indicate that the query string supplied is a hibernate query or native 
     * sql
     * 
     * @param   flag
     * @return  ExportInterface
     */
    public ExportInterface nativeSQL(boolean flag);
    
    /**
     * Set auxiliary parameters that will be used by the export utility
     * 
     * @param   key
     * @param   value
     * @return  ExportInterface
     */
    public ExportInterface setParameter(String key, Object value);
    
    /**
     * Set auxiliary parameters that will be used by the export utility
     * 
     * @param   key
     * @param   value
     * @return  ExportInterface
     */
    public ExportInterface setParameterList(String key, Collection value);
    
    /**
     * Generate the export document using the list of 
     * 
     * @param   data
     * @throws  IOException
     */
    public void generateDoc(List<String[]> data) throws IOException;
    
    /**
     * Generate a export document from the query passed to the object
     * 
     * @throws  IOException
     */
    public void generateDoc() throws IOException;
}
