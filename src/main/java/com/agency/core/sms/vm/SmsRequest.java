package com.agency.core.sms.vm;


import com.agency.core.sms.SmsOptions;

public class SmsRequest {

    private String recipients;

    private String text;

    public SmsRequest(){}

    public SmsRequest(SmsOptions smsOptions){
        this.recipients = smsOptions.getMobileNo();
        this.text = smsOptions.getMessage();
    }

    public String getRecipients() {
        return recipients;
    }

    public void setRecipients(String recipients) {
        this.recipients = recipients;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
