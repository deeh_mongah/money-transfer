package com.agency.core.sms;

/**
 * The parameters used to send SMS to a client
 *
 * @author Anthony Mwawughanga
 * @version 1.0.0
 * @category SMS
 * @package Quickpay
 * @since Jan 14, 2017
 */
public class SmsOptions {

    /*Receipient's mobile number*/
    private String mobileNo;

    /*Text message*/
    private String message;

    public String getMobileNo() {
        return mobileNo;
    }

    public SmsOptions setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public SmsOptions setMessage(String message) {
        this.message = message;
        return this;
    }
}
