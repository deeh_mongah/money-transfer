package com.agency.core.sms;

import com.agency.api.http.MfmHttpService;
import com.agency.core.sms.vm.SmsRequest;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Service("smsService")
public class SmsService implements SmsServiceInterface {

    @Autowired private MfmHttpService mfmHttpService;

    @Value("${mfm.uris.sms}")
    private String smsUri;

    /**
     * Get the object that will be used to send SMS via the Teke SMS Gateway API
     *
     * @return SMS
     */
    @Override
    public SmsOptions smsInit() {
        return new SmsOptions();
    }

    /**
     * Send SMS to a client
     *
     * @param smsOptions
     * @return True: if successful, else false
     */
    @Override
    public boolean sendSMS(SmsOptions smsOptions){
        boolean result = false;
        try
        {
            SmsRequest smsRequest = new SmsRequest( smsOptions  );
            //TODO: For testing purposes
            String recipients = smsRequest.getRecipients() ;
            smsRequest.setRecipients( recipients );
            ResponseEntity<String> responseEntity = mfmHttpService.postRequest(
                    smsUri,
                    smsRequest
            );

            result = responseEntity.getStatusCode().is2xxSuccessful();
        }
        catch(Exception e){
            e.printStackTrace();
        }

        return result;
    }

    /**
     * Validate a client phone number
     *
     * @param mobileNumber
     * @param country
     * @return True- if its valid, else false
     */
    @Override
    public boolean validatePhoneNumber(String mobileNumber, String country) {
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber numberProto = null;
            //when the country is defined
            if (null != country || "".equals(country)) {
                Map<String, String> countries = new HashMap<>();
                for (String isoCountry : Locale.getISOCountries()) {
                    Locale l = new Locale("", isoCountry);
                    countries.put(l.getDisplayCountry(), isoCountry);
                }
                String countryCode = countries.get(country);

                numberProto = phoneUtil.parse(mobileNumber, countryCode);
            }

            //when country is unknown
            else {
                numberProto = phoneUtil.parse(mobileNumber, "");
            }

            return phoneUtil.isValidNumber(numberProto);
        } catch (Exception e) {
//            appAuditLogRepo.mailError(e.getLocalizedMessage());
//            Console.printStackTrace(e);
            return false;
        }
    }

    /**
     * Generate an international phone number
     *
     * @param mobileNumber
     * @param country
     * @return Mobile Phone Number in E64 format
     */
    @Override
    public String getInternationalPhoneNumber(String mobileNumber, String country) {
        try {

            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber numberProto = null;
            //when the country is defined
            if (null != country || "".equals(country)) {
                Map<String, String> countries = new HashMap<>();
                for (String isoCountry : Locale.getISOCountries()) {
                    Locale l = new Locale("", isoCountry);
                    countries.put(l.getDisplayCountry(), isoCountry);
                }
                String countryCode = countries.get(country);
                numberProto = phoneUtil.parse(mobileNumber, countryCode);
            }

            //when country is unknown
            else {
                numberProto = phoneUtil.parse(mobileNumber, "");
            }

            return phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.E164);
        } catch (NumberParseException e) {
            e.printStackTrace();
            return mobileNumber;
        } catch (Exception e) {
            e.printStackTrace();
            return mobileNumber;
        }
    }
}
