package com.agency.web.floatmanager.controller;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.OutletsServiceInterface;
import com.agency.web.wallets.WalletsTransactionsServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class OutletFloatManagerV1Controller {

    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private OutletsServiceInterface outletsServiceInterface;
    @Autowired
    private WalletsTransactionsServiceInterface walletsTransactionsServiceInterface;

    @RequestMapping(value = "/outlet-float-manager-v1")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("float-manager/outlet-float-allocation-v1");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if (null != action)
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view    Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view) {
        Map<String, Object> map = new HashMap<>();
        try {
            String action = request.getParameter("action");

            //When creating a record
            if("fetch-record".equals( action ) )
                map = this.outletsServiceInterface.fetchRecord( request );

            else if("push-float".equals( action ))
                map = this.walletsTransactionsServiceInterface.pushOutletFloat( request );
//
            else if("pull-float".equals( action ))
                map = this.walletsTransactionsServiceInterface.pullOutletFloat( request );




        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON(map);
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");

        dataTable
                .select("a.outletsLink.name,a.outletsLink.agentLink.name,a.outletsLink.email,a.walletsLink.amount,DATE_FORMAT(a.outletsLink.createdOn; '%Y-%m-%d %T' ), a.outletsLink.id")
                .from("OutletWallets a");


        return view.sendJSON( dataTable.showTable() );
    }
}
