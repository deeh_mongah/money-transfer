package com.agency.web.floatmanager.controller;

import com.agency.web.wallets.entity.CurrencyTypeWallets;
import com.agency.web.wallets.entity.OutletWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.OutletWalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class CurrencyController {


    @Autowired
    private OutletWalletsRepository outletWalletsRepository;

    @GetMapping("/currency/{id}")
    public  List<String> transactionHistory(@PathVariable("id") String id) throws Exception {

        Optional<OutletWallets> optionalOutletWallets =outletWalletsRepository.findById(Long.valueOf(id));

        if (!optionalOutletWallets.isPresent()){

        }

        OutletWallets outletWallets = optionalOutletWallets.get();

        Wallets wallets = outletWallets.getWalletsLink();

        String amount = String.valueOf(wallets.getAmount());

        CurrencyTypeWallets currencyTypeWallets = wallets.getCurrencyTypeWalletsLink();

        String currency = currencyTypeWallets.getName();

        List<String> currencyWalletList = new ArrayList<>();

        currencyWalletList.add(0, currency);
        currencyWalletList.add(1, amount);

        System.err.println("<><><>id<><><><>"+id);




        return currencyWalletList;
    }


}
