package com.agency.web.floatmanager.controller;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.AgentsServiceInterface;
import com.agency.web.agency.OutletsServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.wallets.OutletWalletsServiceInterface;
import com.agency.web.wallets.WalletsTransactionsServiceInterface;
import com.agency.web.wallets.repository.CurrencyTypeWalletsRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class OutletFloatManagerController {

    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private AgentsServiceInterface agentsServiceInterface;
    @Autowired
    private WalletsTransactionsServiceInterface walletsTransactionsServiceInterface;
    @Autowired
    private CurrencyTypeWalletsRepository currencyTypeWalletsRepository;
    @Autowired
    private OutletWalletsServiceInterface outletWalletsServiceInterface;

    @RequestMapping(value = "/outlet-float-manager")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("float-manager/outlet-float");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if (null != action)
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);
        }

        return view
                .addAttribute("banks", currencyTypeWalletsRepository.findAll(  ) )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view    Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view) {
        Map<String, Object> map = new HashMap<>();
        try {
            String action = request.getParameter("action");

            //When creating a record
            if("fetch-record".equals( action ) )
                map = this.outletWalletsServiceInterface.fetchRecord( request );

            else if("push-float".equals( action ))
                map = this.walletsTransactionsServiceInterface.pushOutletFloat( request );
//
            else if("pull-float".equals( action ))
                map = this.walletsTransactionsServiceInterface.pullOutletFloat( request );


        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON(map);
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String outletId = request.getParameter("data");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( StringUtils.isEmpty( outletId ) ) {
            dataTable
//                    .nativeSQL(true)
                    .select("a.name, a.email, a.phone , a.agentCode, a.id")
                    .from("Agents a ");
//                    .groupBy("a.outletsLink.id");
//            if( UserTypes.BANK.equals( parentType ) ){
//                dataTable
//                        .where("b.bank_no = :parentNo")
//                        .setParameter("parentNo", parentNo);
//            }
        }
        else{
            dataTable
                .select("a.outletsLink.name,  a.outletsLink.email, a.walletsLink.currencyTypeWalletsLink.name,a.walletsLink.amount,DATE_FORMAT(a.outletsLink.createdOn; '%Y-%m-%d %T' ), a.id, a.walletsLink.amount ")
                .from("OutletWallets a")
                .where("a.outletsLink.id=:agentNo")
                .setParameter("agentNo",Long.valueOf( outletId ) );
//            dataTable
////                    .nativeSQL( true)
//                    .select("a.serial_no, 'N/A', '-',  DATE_FORMAT(a.assigned_date; '%Y-%m-%d %T' ), a.id")
//                    .from("terminals a")
//                    .where("a.outlet_no = :outletNo")
//                    .setParameter("outletNo", Long.valueOf( outletId ) );
        }

//        dataTable
//                .select("a.outletsLink.name,a.outletsLink.agentLink.name,a.outletsLink.email,a.walletsLink.amount,DATE_FORMAT(a.outletsLink.createdOn; '%Y-%m-%d %T' ), a.outletsLink.id")
//                .from("OutletWallets a");


        return view.sendJSON( dataTable.showTable() );
    }
}
