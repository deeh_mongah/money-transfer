package com.agency.web.floatmanager.controller;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.OutletsServiceInterface;
import com.agency.web.terminals.TerminalServiceInterface;
import com.agency.web.wallets.WalletsTransactionsServiceInterface;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TerminalFloatManagerController {

    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private TerminalServiceInterface terminalServiceInterface;

    @Autowired
    private WalletsTransactionsServiceInterface walletsTransactionsServiceInterface;

    @RequestMapping(value = "/terminal-float-manager")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("float-manager/terminal-float");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if (null != action)
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view    Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view) {
        Map<String, Object> map = new HashMap<>();
        try {
            String action = request.getParameter("action");

            //When creating a record
            if("fetch-record".equals( action ) )
                map = this.terminalServiceInterface.fetchRecord( request );

            else if("push-float".equals( action ))
                map = this.walletsTransactionsServiceInterface.pushTerminalFloat( request );
//
            else if("pull-float".equals( action ))
                map = this.walletsTransactionsServiceInterface.pullTerminalFloat( request );




        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON(map);
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){

        String outletId = request.getParameter("data");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( StringUtils.isEmpty( outletId ) ) {
            dataTable
//                    .nativeSQL(true)
                    .select("a.outletsLink.agentLink.name, a.outletsLink.amount, a.outletsLink.agentLink.id")
                    .from("OutletWallets a ")
                    .groupBy("a.outletsLink.agentLink.name");
//            if( UserTypes.BANK.equals( parentType ) ){
//                dataTable
//                        .where("b.bank_no = :parentNo")
//                        .setParameter("parentNo", parentNo);
//            }
        }
        else{
//            dataTable
//                    .select("a.outletsLink.name, a.outletsLink.agentLink.name,a.outletsLink.email,a.walletsLink.amount,DATE_FORMAT(a.outletsLink.createdOn; '%Y-%m-%d %T' ), a.outletsLink.id, a.outletsLink.amount ")
//                    .from("OutletWallets a")
//                    .where("a.outletsLink.agentNo=:agentNo")
//                    .setParameter("agentNo",Long.valueOf( outletId ) );
            dataTable
                    .nativeSQL(true)
                    .select("a.SERIAL_NO,o.name, o.email, w.amount,DATE_FORMAT(a.created_on; '%Y-%m-%d %T'),a.id, a.amount")
                    .from("terminals a")
                    .from("left join terminal_allocation ta ON ta.id = a.id")
                    .from("left join outlets o ON o.id = ta.outlet_no")
                    .from("left join terminal_wallets tw ON tw.terminal_no = a.id")
                    .from("left join wallets w ON w.id= tw.wallet_no")
                    .where("o.id = :outletNo")
                    .setParameter("outletNo",Long.valueOf( outletId ));
//            dataTable
//                .select("a.terminalsLink.serialNo, 'N/A','N/A',a.walletsLink.amount,DATE_FORMAT(a.terminalsLink.createdOn; '%Y-%m-%d %T' ), a.terminalsLink.id,a.terminalsLink.amount")
//                .from("TerminalWallets a")
//                .where("a.terminalsLink.haveWallets = :flag ")
//                .setParameter("flag", "1");
//            dataTable
////                    .nativeSQL( true)
//                    .select("a.serial_no, 'N/A', '-',  DATE_FORMAT(a.assigned_date; '%Y-%m-%d %T' ), a.id")
//                    .from("terminals a")
//                    .where("a.outlet_no = :outletNo")
//                    .setParameter("outletNo", Long.valueOf( outletId ) );
        }

//        dataTable
//                .select("a.terminalsLink.serialNo, 'N/A','N/A',a.walletsLink.amount,DATE_FORMAT(a.terminalsLink.createdOn; '%Y-%m-%d %T' ), a.terminalsLink.id,a.terminalsLink.amount")
//                .from("TerminalWallets a")
//                .where("a.terminalsLink.haveWallets = :flag ")
//                .setParameter("flag", "1");

        ;


        return view.sendJSON( dataTable.showTable() );
    }
}
