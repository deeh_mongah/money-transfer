package com.agency.web.transactions;

import com.agency.web.transactions.entities.TransactionTypes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface TransactionTypesServiceInterface {

    /**
     * Fetch all persisted records
     *
     * @return List<TransactionTypes>
     */
    public List<TransactionTypes> fetchRecords(HttpServletRequest request);
}
