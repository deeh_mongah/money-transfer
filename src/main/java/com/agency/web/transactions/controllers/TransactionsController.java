package com.agency.web.transactions.controllers;

import com.agency.core.export.ExportService;
import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.reports.controllers.ReportAbstractController;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class TransactionsController extends ReportAbstractController {

    @RequestMapping( value = "/transactions")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("transactions/transactions");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    @RequestMapping(value = "/transactions/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "transactions", format
        );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        String[] columns = new String[]{
                "Transaction ID",
                "Date/Time",
                "Amount",
                "Transaction Type",
                "Customer",
                "Agent",
        };

        dataTable
                .nativeSQL( true )
                .select("a.receipt_no, DATE_FORMAT(a.created_on; '%Y-%m-%d %H:%i'), a.amount, b.name, a.flag, a.names, a.agent_name")
                .from("transactions a ")
                .from("LEFT JOIN transaction_types b ON b.id = a.transaction_type_no ")
                .from("LEFT JOIN terminal_allocation tc ON tc.id = a.terminal_no ")
                .from("LEFT JOIN outlets d ON d.id = tc.outlet_no ")
                ;

        if( UserTypes.BANK.equals( parentType ) ){
           dataTable
                   .where("c.bank_no = :parentNo ")
                   .setParameter("parentNo", parentNo);

        }

        setDateFilters( request, "a" );
        setTransactionStatus( request, "a" );
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }
}
