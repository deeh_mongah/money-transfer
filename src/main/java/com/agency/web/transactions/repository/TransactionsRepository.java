package com.agency.web.transactions.repository;

import com.agency.web.agency.entities.Fees;
import com.agency.web.transactions.entities.Transactions;
import org.omg.CORBA.TRANSACTION_MODE;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface TransactionsRepository extends CrudRepository<Transactions, Long> {

    List<Transactions> findAllByAccountNo(String accountNo);

    List<Transactions> findAllByNames(String names);

    @Query(value = "SELECT *  FROM fees a where a.transaction_type_no =?1  and ?2 BETWEEN  a.min_amount AND a.max_amount ", nativeQuery = true)
    public Optional<Fees> fetchFeeByTransactionTypeNoAndAmount(Long transactionTypeNo, BigDecimal amount);

    @Query(value = "SELECT *  FROM fees a where a.transaction_type_no =?1  and ?2 BETWEEN  a.min_amount AND a.max_amount ", nativeQuery = true)
    public List<Fees> fetchFeeByTransactionTypeNoAndAmountV1(Long transactionTypeNo, BigDecimal amount);


    List<Transactions> findByTransactionTypeNo(long transactionType);


    Optional<Transactions> findByReceiptNo(String receiptNo);


    Optional<Transactions> findByMpesaTransactionNo(String transactionNo);
}
