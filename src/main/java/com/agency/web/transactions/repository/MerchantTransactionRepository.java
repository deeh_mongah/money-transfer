package com.agency.web.transactions.repository;

import com.agency.web.transactions.entities.MerchantTransactions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MerchantTransactionRepository extends CrudRepository<MerchantTransactions, Long> {



}
