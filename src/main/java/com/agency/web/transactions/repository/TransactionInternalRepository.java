package com.agency.web.transactions.repository;

import com.agency.web.transactions.entities.TransactionInternal;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface TransactionInternalRepository extends CrudRepository<TransactionInternal, Long>{

    Optional<TransactionInternal> findByTransactionNo(long transactionNo);
}
