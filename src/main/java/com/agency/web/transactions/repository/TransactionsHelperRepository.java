package com.agency.web.transactions.repository;

import com.agency.web.transactions.entities.TransactionTypes;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class TransactionsHelperRepository {

    @PersistenceContext
    private EntityManager entityManager;

    protected final Session getCurrentSession() {
        return entityManager.unwrap(Session.class);
    }

    @SuppressWarnings("unchecked")
    public List<Object[]> fetchCalendarData(String parentType, Long parentNo, String startDate, String endDate){
        Session session = getCurrentSession();
        StringBuilder query = new StringBuilder();
        Query hql;

        query
                .append("SELECT a.created_on, ")
                .append("SUM( CASE WHEN b.code = :deposit THEN a.amount ELSE 0 END ), ")
                .append("SUM( CASE WHEN b.code = :withdrawal THEN a.amount ELSE 0 END ), ")
                .append("COUNT( CASE WHEN b.code = :balance THEN 1 ELSE 0 END ), ")
                .append("COUNT( CASE WHEN b.code = :statement THEN 1 ELSE 0 END ) ")
                .append("FROM transactions a ")
                .append("LEFT JOIN transaction_types b ON b.id = a.transaction_type_no ")
                .append("LEFT JOIN bank_terminals c ON c.id = a.terminal_no  ")
                .append("WHERE c.bank_no = :parentNo ")
                .append("AND DATE_FORMAT(a.created_on, '%Y-%m-%d %H:%i') BETWEEN DATE_FORMAT(:startDate, '%Y-%m-%d %H:%i') AND DATE_FORMAT(:endDate, '%Y-%m-%d %H:%i') ")
                .append("GROUP BY a.created_on ")

        ;
        hql = session.createNativeQuery( query.toString() );

        return hql
                .setParameter("deposit", TransactionTypes.DEPOSIT )
                .setParameter("withdrawal", TransactionTypes.DEPOSIT )
                .setParameter("balance", TransactionTypes.DEPOSIT )
                .setParameter("statement", TransactionTypes.DEPOSIT )
                .setParameter("parentNo", parentNo )
                .setParameter("startDate", startDate )
                .setParameter("endDate", endDate ).list();
    }

}
