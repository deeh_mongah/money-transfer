package com.agency.web.transactions.repository;

import com.agency.web.transactions.entities.CommmisionTransaction;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommissionTransactionRepository extends CrudRepository<CommmisionTransaction, Long> {




}
