package com.agency.web.transactions.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "transaction_types")
public class TransactionTypes implements Serializable {

    public static final String DEPOSIT = "Deposit";
    public static final String TRANSFER = "Transfer";
    public static final String PUSH_FLOAT = "Push Float";
    public static final String PULL_FLOAT = "Pull Float";
    public static final String WITHDRAWAL = "Withdrawal";
    public static final String INCOUNTRY = "Send Money In-country";
    public static final String OUTCOUNTRY = "Send Money Out-country";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "code", length = 100)
    private String code;

    @Size(max = 20)
    @Column(name = "short_code", length = 20)
    private String shortCode;

    @Size(max = 100)
    @Column(name = "name", length = 100)
    private String name;


    public TransactionTypes() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getShortCode() {
        return shortCode;
    }

    public void setShortCode(String shortCode) {
        this.shortCode = shortCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionTypes)) {
            return false;
        }
        TransactionTypes other = (TransactionTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TransactionTypes[ id=" + id + " ]";
    }
    
}
