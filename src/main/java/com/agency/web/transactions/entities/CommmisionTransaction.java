package com.agency.web.transactions.entities;

import com.agency.web.agency.entities.Agents;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "commission_transaction")
public class CommmisionTransaction implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "transaction_no", length = 100)
    private long transactionNo;

    @Column(name = "agent_no", length = 100)
    private Long agentNo;

    @Column(name = "fee_no", length = 100)
    private Long feeNo;

    @Column(name = "agent_amount", precision = 18, scale = 2)
    private BigDecimal agentAmount;

    @Column(name = "flex_amount", precision = 18, scale = 2)
    private BigDecimal flexAmount;

    @Column(name = "binary_amount", precision = 18, scale = 2)
    private BigDecimal binaryAmount;


    @Column(name = "tariff_amount", precision = 18, scale = 2)
    private BigDecimal tariffAmount;



    @JoinColumn(name = "transaction_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Transactions transactionLink;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );


    @JoinColumn(name = "agent_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Agents agentsLink;

    public CommmisionTransaction() { }


    public Long getFeeNo() {
        return feeNo;
    }

    public void setFeeNo(Long feeNo) {
        this.feeNo = feeNo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(long transactionNo) {
        this.transactionNo = transactionNo;
    }

    public Long getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(Long agentNo) {
        this.agentNo = agentNo;
    }

    public BigDecimal getAgentAmount() {
        return agentAmount;
    }

    public void setAgentAmount(BigDecimal agentAmount) {
        this.agentAmount = agentAmount;
    }

    public BigDecimal getFlexAmount() {
        return flexAmount;
    }

    public void setFlexAmount(BigDecimal flexAmount) {
        this.flexAmount = flexAmount;
    }

    public BigDecimal getBinaryAmount() {
        return binaryAmount;
    }

    public void setBinaryAmount(BigDecimal binaryAmount) {
        this.binaryAmount = binaryAmount;
    }

    public BigDecimal getTariffAmount() {
        return tariffAmount;
    }

    public void setTariffAmount(BigDecimal tariffAmount) {
        this.tariffAmount = tariffAmount;
    }

    @JsonIgnore
    public Transactions getTransactionLink() {
        return transactionLink;
    }

    public void setTransactionLink(Transactions transactionLink) {
        this.transactionLink = transactionLink;
    }


    @JsonIgnore
    public Agents getAgentsLink() {
        return agentsLink;
    }

    public void setAgentsLink(Agents agentsLink) {
        this.agentsLink = agentsLink;
    }
}
