package com.agency.web.transactions.entities;

import com.agency.web.terminals.entities.Terminals;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "transactions")
public class Transactions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "receipt_no", length = 100)
    private String receiptNo;

    @Size(max = 100)
    @Column(name = "status", length = 100)
    private String status;

    @Column(name = "amount", precision = 18, scale = 2)
    private BigDecimal amount;

    @Size(max = 200)
    @Column(name = "location", length = 200)
    private String location;

    @Size(max = 200)
    @Column(name = "names", length = 200)
    private String names;

    @Size(max = 200)
    @Column(name = "agent_name", length = 200)
    private String agentNames;

    @Size(max = 100)
    @Column(name = "lat", length = 100)
    private String lat;

    @Size(max = 100)
    @Column(name = "lng", length = 100)
    private String lng;

    @Column(name = "agent_present")
    private String agentPresent;

    @Column(name = "mpesa_transaction_no")
    private String mpesaTransactionNo;

    @Column(name = "mpesa_no")
    private String mpesaNo;

    @Column(name = "flag", length = 100)
    private String flag = "Success";

    @Column(name = "account_no", length = 100)
    private String accountNo;

    @Column(name = "bank_fee", precision = 18, scale = 2 )
    private BigDecimal bankFee;

    @Column(name = "binary_fee", precision = 18, scale = 2 )
    private BigDecimal binaryFee;

    @Column(name = "agent_fee", precision = 18, scale = 2 )
    private BigDecimal agentFee;

    @Column(name = "total_fee", precision = 18, scale = 2 )
    private BigDecimal totalFee;

    @Column(name = "officer_id" )
    private String officerId;

    @Column(name = "receiver_contact_no" )
    private String receiverContactNo;

    @Column(name = "sender_telephone_no" )
    private String senderTelephoneNo;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "terminal_no")
    private Long terminalNo;

    @Column(name = "transaction_type_no")
    private Long transactionTypeNo;

    @Column(name = "currency_type" )
    private String currencyType;

    @JoinColumn(name = "terminal_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Terminals terminalLink;

    @JoinColumn(name = "transaction_type_no", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionTypes transactionTypeLink;

    public Transactions() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getReceiptNo() {
        return receiptNo;
    }

    public Transactions setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
        return this;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public Transactions setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
        return this;
    }

    public String getAgentNames() {
        return agentNames;
    }

    public void setAgentNames(String agentNames) {
        this.agentNames = agentNames;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAgentPresent() {
        return agentPresent;
    }

    public void setAgentPresent(String agentPresent) {
        this.agentPresent = agentPresent;
    }

    public String getMpesaTransactionNo() {
        return mpesaTransactionNo;
    }

    public Transactions setMpesaTransactionNo(String mpesaTransactionNo) {
        this.mpesaTransactionNo = mpesaTransactionNo;
        return this;
    }

    public String getMpesaNo() {
        return mpesaNo;
    }

    public void setMpesaNo(String mpesaNo) {
        this.mpesaNo = mpesaNo;
    }

    public String getAccountNo() {  return accountNo; }
    public Transactions setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

    public BigDecimal getBankFee() {
        return bankFee;
    }

    public void setBankFee(BigDecimal bankFee) {
        this.bankFee = bankFee;
    }

    public BigDecimal getBinaryFee() {
        return binaryFee;
    }

    public Transactions setBinaryFee(BigDecimal binaryFee) {
        this.binaryFee = binaryFee;
        return this;
    }

    public BigDecimal getAgentFee() {
        return agentFee;
    }

    public Transactions setAgentFee(BigDecimal agentFee) {
        this.agentFee = agentFee;
        return this;
    }

    public BigDecimal getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(BigDecimal totalFee) {
        this.totalFee = totalFee;
    }

    public String getOfficerId() {   return officerId;  }
    public Transactions setOfficerId(String officerId) {
        this.officerId = officerId;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getTerminalNo() {  return terminalNo;  }
    public Transactions setTerminalNo(Long terminalNo) {
        this.terminalNo = terminalNo;
        return this;
    }

    public String getReceiverContactNo() {
        return receiverContactNo;
    }

    public void setReceiverContactNo(String receiverContactNo) {
        this.receiverContactNo = receiverContactNo;
    }

    public String getSenderTelephoneNo() {
        return senderTelephoneNo;
    }

    public void setSenderTelephoneNo(String senderTelephoneNo) {
        this.senderTelephoneNo = senderTelephoneNo;
    }

    public Long getTransactionTypeNo() {  return transactionTypeNo; }
    public Transactions setTransactionTypeNo(Long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
        return this;
    }

    @JsonIgnore public Terminals getTerminalLink() {  return terminalLink; }
    @JsonIgnore public TransactionTypes getTransactionTypeLink() {   return transactionTypeLink; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) {
            return false;
        }
        Transactions other = (Transactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Transactions[ id=" + id + " ]";
    }
    
}
