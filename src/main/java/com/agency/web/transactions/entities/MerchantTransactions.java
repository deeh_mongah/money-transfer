package com.agency.web.transactions.entities;

import com.agency.web.terminals.entities.Terminals;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "merchant_transactions")
public class MerchantTransactions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Long id;

    @Size(max = 100)
    @Column(name = "transaction_reference", length = 100)
    private String transactionReference;

    @Column(name = "amount", precision = 18, scale = 2)
    private BigDecimal amount;


    @Column(name = "transaction_type")
    private Long transactionTypeNo;


    @Column(name = "flag", length = 100)
    private String flag = "Success";

    @Column(name = "account_no", length = 100)
    private String accountNo;

//    @Column(name = "bank_fee", precision = 18, scale = 2 )
//    private BigDecimal bankFee;
//
//    @Column(name = "binary_fee", precision = 18, scale = 2 )
//    private BigDecimal binaryFee;
//
//    @Column(name = "agent_fee", precision = 18, scale = 2 )
//    private BigDecimal agentFee;
//
//    @Column(name = "total_fee", precision = 18, scale = 2 )
//    private BigDecimal totalFee;

    @Column(name = "officer_id" )
    private long officerId;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "terminal_no")
    private Long terminalNo;

    @JoinColumn(name = "terminal_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Terminals terminalLink;

    @JoinColumn(name = "transaction_type_no", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionTypes transactionTypeLink;

    public MerchantTransactions() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTransactionReference() {
        return transactionReference;
    }

    public void setTransactionReference(String transactionReference) {
        this.transactionReference = transactionReference;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public void setTerminalLink(Terminals terminalLink) {
        this.terminalLink = terminalLink;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getAccountNo() {  return accountNo; }
    public MerchantTransactions setAccountNo(String accountNo) {
        this.accountNo = accountNo;
        return this;
    }

//    public BigDecimal getBankFee() {
//        return bankFee;
//    }
//
//    public void setBankFee(BigDecimal bankFee) {
//        this.bankFee = bankFee;
//    }
//
//    public BigDecimal getBinaryFee() {
//        return binaryFee;
//    }
//
//    public void setBinaryFee(BigDecimal binaryFee) {
//        this.binaryFee = binaryFee;
//    }
//
//    public BigDecimal getAgentFee() {
//        return agentFee;
//    }
//
//    public void setAgentFee(BigDecimal agentFee) {
//        this.agentFee = agentFee;
//    }
//
//    public BigDecimal getTotalFee() {
//        return totalFee;
//    }
//
//    public void setTotalFee(BigDecimal totalFee) {
//        this.totalFee = totalFee;
//    }

    public long getOfficerId() {   return officerId;  }
    public MerchantTransactions setOfficerId(long officerId) {
        this.officerId = officerId;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getTerminalNo() {  return terminalNo;  }
    public MerchantTransactions setTerminalNo(Long terminalNo) {
        this.terminalNo = terminalNo;
        return this;
    }

    public Long getTransactionTypeNo() {  return transactionTypeNo; }
    public MerchantTransactions setTransactionTypeNo(Long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
        return this;
    }

    @JsonIgnore public Terminals getTerminalLink() {  return terminalLink; }
    @JsonIgnore public TransactionTypes getTransactionTypeLink() {   return transactionTypeLink; }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MerchantTransactions)) {
            return false;
        }
        MerchantTransactions other = (MerchantTransactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Transactions[ id=" + id + " ]";
    }
    
}
