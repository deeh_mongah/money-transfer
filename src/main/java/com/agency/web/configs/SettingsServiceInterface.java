package com.agency.web.configs;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface SettingsServiceInterface {

    /**
     * Fetch a record information
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchRecord(HttpServletRequest request);

    /**
     * Update a record with new values
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> editRecord(HttpServletRequest request);
}
