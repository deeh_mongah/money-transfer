package com.agency.web.configs.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.configs.entities.AppSettings;
import org.springframework.stereotype.Component;

@Component
public class AppSettingsForm extends Form<AppSettings> {

    //This class constructor initiliazes the mapping managed by the Form class
    public AppSettingsForm() {
        setMapping( AppSettings.class );
    }
}
