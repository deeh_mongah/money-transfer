package com.agency.web.configs.repository;

import com.agency.web.configs.entities.AuditTrail;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuditTrailRepository extends CrudRepository<AuditTrail, Long> {
}
