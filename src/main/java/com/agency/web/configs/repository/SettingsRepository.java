package com.agency.web.configs.repository;

import com.agency.web.configs.entities.AppSettings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SettingsRepository extends CrudRepository<AppSettings, Long> {

    /**
     *
     * @param code
     * @return Optional<AppSettings>
     */
    public Optional<AppSettings> findByCode(String code);
}
