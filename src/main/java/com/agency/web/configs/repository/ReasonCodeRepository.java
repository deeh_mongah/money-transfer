package com.agency.web.configs.repository;

import com.agency.web.configs.entities.ReasonCodes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReasonCodeRepository extends CrudRepository<ReasonCodes, Long> {

    /**
     * Fetch all records by flag
     *
     * @param flag
     * @return List<ReasonCodes>
     */
    List<ReasonCodes> findAllByFlag(String flag );
}
