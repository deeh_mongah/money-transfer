package com.agency.web.configs.repository;

import com.agency.web.configs.entities.MakerChecker;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MakerCheckerRepository extends CrudRepository<MakerChecker, Long> {

    public Optional<MakerChecker> findByModule(String module);

    public Optional<MakerChecker> findByCode(String code);
}
