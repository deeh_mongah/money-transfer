package com.agency.web.configs.repository;

import com.agency.web.configs.entities.AppSettings;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AppSettingsRepository extends CrudRepository<AppSettings, Long> {
}
