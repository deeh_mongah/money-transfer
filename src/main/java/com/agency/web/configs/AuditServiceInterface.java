package com.agency.web.configs;

import com.agency.web.configs.entities.AuditTrail;

public interface AuditServiceInterface {

    /**
     *  Save record given the current user's principal name
     *
     * @param trail
     * @param email
     */
    public void saveLog(AuditTrail trail, String email);

}
