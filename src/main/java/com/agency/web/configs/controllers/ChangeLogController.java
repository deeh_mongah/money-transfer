package com.agency.web.configs.controllers;

import com.agency.core.template.View;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ChangeLogController {

    @RequestMapping("/change-log")
    public ModelAndView index(){
        View view = new View("configs/change-log");
        return view.getView();
    }

}
