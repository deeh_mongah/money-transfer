package com.agency.web.configs.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AuditLogController {

    @Autowired
    private DatatablesInterface datatable;

    @RequestMapping("/audit-logs")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("configs/audit-logs");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        // Fetch the table data
        if ( AjaxUtils.isAjaxRequest( request ) ) {

            //Set-up data
            datatable
                    .select("str(a.createdOn; 'YYYY-MM-DD HH24:MI'), CONCAT(b.firstName; ' '; b.surname ), a.activity, a.status, a.oldValues, a.newValues ")
                    .from("AuditTrail a LEFT JOIN a.userLink b");

            if( UserTypes.BANK.equals( parentType ) ){
                datatable
                        .from("LEFT JOIN b.bankUser c")
                        .where("c.bankNo = :parentNo ")
                        .setParameter("parentNo", parentNo);
            }

            return view.sendJSON( datatable.showTable() );
        }

        return view.getView();
    }
}
