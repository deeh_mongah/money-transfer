package com.agency.web.configs;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface MakerCheckerServiceInterface {


    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    public Map<String, Object> editRecord(HttpServletRequest request);

    /**
     * Fetch a record information
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchRecord(HttpServletRequest request);
}
