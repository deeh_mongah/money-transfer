package com.agency.web.configs.services;

import com.agency.web.configs.MakerCheckerServiceInterface;
import com.agency.web.configs.entities.MakerChecker;
import com.agency.web.configs.forms.MakerCheckerForm;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.configs.repository.MakerCheckerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class MakerCheckerService implements MakerCheckerServiceInterface {

    @Autowired private MakerCheckerRepository entityRepo;
    @Autowired private AuditTrailRepository auditRepository;
    @Autowired private MakerCheckerForm entityForm;

    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        String index = request.getParameter("id");
        Boolean enabled = Boolean.valueOf(request.getParameter("enabled"));

//        String enabledField ;
//        if (enabled){
//
//            enabledField = "1";
//
//        }else {
//            enabledField = "0";
//        }
        MakerChecker entity = this.entityRepo.findById( Long.valueOf( index ) ).get();
        System.err.println("booleanValue"+enabled);
        entity.setEnabled( enabled );
        this.entityRepo.save( entity );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    /**
     * Fetch a record information
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return this.entityForm.transformEntity(index);
    }
}
