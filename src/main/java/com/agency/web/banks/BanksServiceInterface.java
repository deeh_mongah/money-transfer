package com.agency.web.banks;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.banks.entities.Banks;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BanksServiceInterface extends BaseServiceInterface {

    /**
     * Fetch all persisted records
     *
     * @return List<Banks>
     */
    public List<Banks> fetchRecords(HttpServletRequest request);
}
