package com.agency.web.banks.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.banks.entities.Branches;
import org.springframework.stereotype.Component;

@Component
public class BranchesForm extends Form<Branches> {

    public BranchesForm(){
        setMapping( Branches.class );
        setRole("ROLE_BRANCHES", "Branches", "superadmin");
    }
}
