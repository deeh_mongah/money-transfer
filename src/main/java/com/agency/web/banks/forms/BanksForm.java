package com.agency.web.banks.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.banks.entities.Banks;
import org.springframework.stereotype.Component;

@Component
public class BanksForm extends Form<Banks> {

    public BanksForm(){
        setMapping( Banks.class );
        setRole("ROLE_BANKS", "Banks", "superadmin");
    }
}
