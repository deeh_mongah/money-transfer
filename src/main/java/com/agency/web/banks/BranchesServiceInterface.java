package com.agency.web.banks;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.banks.entities.Branches;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface BranchesServiceInterface extends BaseServiceInterface{

    /**
     * Fetch all persisted records
     *
     * @return List<Branches>
     */
    public List<Branches> fetchRecords(HttpServletRequest request);
}
