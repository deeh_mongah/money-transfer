package com.agency.web.banks.repositories;

import com.agency.web.banks.entities.Banks;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BanksRepository extends CrudRepository<Banks, Long> {

    /**
     * Fetch a record given their name
     *
     * @param name
     * @return Optional<Banks>
     */
    public Optional<Banks> findByName(String name);


    /**
     * Fetch a record by bank code
     *
     * @param code
     * @return Optional<Banks>
     */
    public List<Banks> findAllByBankCode(String code);

    /**
     * Fetch all records by status
     *
     * @param status
     * @return List<Banks>
     */
    public List<Banks> findAllByFlag(String status );
}
