package com.agency.web.banks.repositories;

import com.agency.web.banks.entities.Branches;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BranchesRepository extends CrudRepository<Branches, Long> {

    /**
     * Fetch a record given their name
     *
     * @param name
     * @return Optional<Branches>
     */
    public Optional<Branches> findByName(String name);


    /**
     * Fetch a record by branch code
     *
     * @param code
     * @return Optional<Branches>
     */
    public List<Branches> findAllByBranchCode(String code);

    /**
     * Fetch all records by status
     *
     * @param status
     * @return List<Branches>
     */
    public List<Branches> findAllByFlag( String status );

}
