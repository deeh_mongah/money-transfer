package com.agency.web.terminals.repository;

import com.agency.web.terminals.entities.TerminalOfficers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TerminalOfficersRepository extends CrudRepository<TerminalOfficers, Long> {

    /**
     * Fetch record by passport
     *
     * @param passport
     * @return Optional<TerminalOfficers>
     */
    public Optional<TerminalOfficers> findByPassport(String passport);

    /**
     * Fetch record by userCode
     *
     * @param userCode
     * @return Optional<TerminalOfficers>
     */
    public Optional<TerminalOfficers> findByUserCode(String userCode);

    /**
     * Fetch record by userCode
     *
     * @param userCode
     * @return Optional<TerminalOfficers>
     */
    public Optional<TerminalOfficers> findByUserCodeAndTerminalNo(String userCode, Long terminalNo);

//    public Optional<TerminalOfficers> findByUserCode(String userCode);
}
