package com.agency.web.terminals.repository;

import com.agency.web.terminals.entities.TerminalAllocation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TerminalAllocationRepository extends CrudRepository<TerminalAllocation, Long> {


    public Optional<TerminalAllocation> findByIdAndAllocate(long id, String allocate);

}
