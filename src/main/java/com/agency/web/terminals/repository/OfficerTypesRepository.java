package com.agency.web.terminals.repository;

import com.agency.web.terminals.entities.OfficerTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficerTypesRepository extends CrudRepository<OfficerTypes, Long> {
}
