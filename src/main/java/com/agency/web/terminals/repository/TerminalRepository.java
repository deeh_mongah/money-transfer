package com.agency.web.terminals.repository;

import com.agency.web.terminals.entities.Terminals;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TerminalRepository extends CrudRepository<Terminals, Long> {

    /**
     * Fetch record by serialNo
     *
     * @param serialNo
     * @return Optional<Terminals>
     */
    public Optional<Terminals> findBySerialNo(String serialNo);


    @Query("SELECT a FROM Terminals a WHERE a.outletNo IS NULL ")
    public List<Terminals> fetchUnallocatedRecords();


    public List<Terminals> findAllByFlag(String flag);

    public List<Terminals> findAllByFlagAndBankNo(String flag, Long bankNo);


    public List<Terminals> findAllByFlagAndOutletLinkIsNull(String flag);

    public List<Terminals> findAllByFlagAndOutletLinkIsNullAndBankNo(String flag, Long bankNo);
}
