package com.agency.web.terminals;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface TerminalAllocationServiceInterface {

    public Map<String, Object> saveRecord(HttpServletRequest request);

    public Map<String, Object> dealocate(HttpServletRequest request);
    public Map<String, Object> allocation(HttpServletRequest request);
}
