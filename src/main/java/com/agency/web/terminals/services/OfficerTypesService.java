package com.agency.web.terminals.services;

import com.agency.web.terminals.OfficerTypesServiceInterface;
import com.agency.web.terminals.entities.OfficerTypes;
import com.agency.web.terminals.repository.OfficerTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Transactional
public class OfficerTypesService implements OfficerTypesServiceInterface {

    @Autowired
    private OfficerTypesRepository officerTypesRepository;

    /**
     * Fetch all persisted records
     *
     * @return List<OfficerTypes>
     */
    @Override
    public List<OfficerTypes> fetchRecords(HttpServletRequest request){
        return (List)officerTypesRepository.findAll();
    }
}
