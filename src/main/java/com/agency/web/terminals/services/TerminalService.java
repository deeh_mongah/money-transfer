package com.agency.web.terminals.services;

import com.agency.core.template.AppConstants;
import com.agency.core.template.forms.AuditData;
import com.agency.web.agency.entities.Outlets;
import com.agency.web.agency.repository.OutletsRepository;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.terminals.TerminalServiceInterface;
import com.agency.web.terminals.entities.Terminals;
import com.agency.web.terminals.forms.TerminalForm;
import com.agency.web.terminals.repository.TerminalRepository;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.wallets.entity.AgentsAccount;
import com.agency.web.wallets.entity.TerminalWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.AgentsAccountRepository;
import com.agency.web.wallets.repository.TerminalWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional
public class TerminalService implements TerminalServiceInterface{

    @Autowired private TerminalForm entityForm;
    @Autowired private TerminalRepository entityRepository;
    @Autowired private AuditTrailRepository auditTrailRepository;
    @Autowired private OutletsRepository outletsRepository;
    @Autowired private WalletsRepository walletsRepository;
    @Autowired private TerminalWalletsRepository terminalWalletsRepository;

    @Autowired private ObjectMapper objectMapper;
    @Autowired private AgentsAccountRepository agentsAccountRepository;

    /**
     * Fetch all persisted records
     *
     * @param request
     * @return List<Terminals>
     */
    @Override
    public List<Terminals> fetchRecords(HttpServletRequest request){
        List<Terminals> data = new ArrayList<>();
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if(UserTypes.BANK.equals( parentType ) ){
            data = entityRepository.findAllByFlagAndBankNo(
                    AppConstants.STATUS_ACTIVERECORD, parentNo );
        }
        else{
            data = entityRepository.findAllByFlag( AppConstants.STATUS_ACTIVERECORD );
        }

        return data;
    }

    /**
     * Fetch all unallocated records
     *
     * @param request
     * @return List<Terminals>
     */
    @Override
    public List<Terminals> fetchUnallocatedRecords(HttpServletRequest request){
        List<Terminals> data = new ArrayList<>();
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if(UserTypes.BANK.equals( parentType ) ){
            data = entityRepository.findAllByFlagAndOutletLinkIsNullAndBankNo(
                    AppConstants.STATUS_ACTIVERECORD, parentNo );
        }
        else{
            data = entityRepository.findAllByFlagAndOutletLinkIsNull( AppConstants.STATUS_ACTIVERECORD );
        }

        return data;
    }

    /**
     * Allocate terminals to an outlet
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> allocateTerminals(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");

        String outletNoStr = request.getParameter("outletNo");
        String[] indices = request.getParameterValues("terminals");

        Long outletNo = Long.valueOf( outletNoStr );
        List<Terminals> allocatedTerminals = new ArrayList<>();
        StringBuilder terminalInfo = new StringBuilder();
        Outlets outlet = outletsRepository.findById( outletNo ).get();

        for (String index : indices) {
            Terminals terminal = entityRepository.findById( Long.valueOf( index ) ).get();
            if (null != terminal ) {

                terminal
                        .setOutletNo( outletNo )
                        .setAssigned( true )
                        .setAssignedDate( new Date( System.currentTimeMillis() ))
                        .updatedOn( userId )
                ;

                allocatedTerminals.add( terminal );

                if( terminalInfo.length() > 0 ) terminalInfo.append(", ");
                terminalInfo.append( terminal.getSerialNo() );
            }
        }

        //Update records
        entityRepository.saveAll( allocatedTerminals );

        //Generate log
        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Allocated new terminals to "+ outlet.getName() +" : " + terminalInfo.toString() )
                .setStatus("Success")
                .setUserNo( userId );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully.");
        return map;
    }

    /**
     *Allocates a single terminal to an outlet
     *
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    @Override
    public Map<String, Object> allocateOneTerminal(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Long outletNo = Long.valueOf( request.getParameter("outletNo") );
        Long terminalId = Long.valueOf( request.getParameter("terminalId") );

        Outlets outlet = outletsRepository.findById( outletNo ).get();
        Terminals entity = entityRepository.findById( terminalId ).get();

        entity
                .setOutletNo( outletNo )
                .setAssigned( true )
                .setAssignedDate( new Date( System.currentTimeMillis() ))
                .updatedOn( userId )
        ;

        //Generate log
        AuditTrail log = new AuditTrail();
        String activity = String.format("Allocated terminal %s to outlet - %s", entity.getSerialNo(), outlet.getName() );
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity( activity )
                .setStatus("Success")
                .setUserNo( userId );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully.");
        return map;
    }

    /**
     * De-allocate one terminal
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> deAllocateOneTerminal(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Long terminalId = Long.valueOf( request.getParameter("index") );

        Terminals entity = entityRepository.findById( terminalId ).get();
        entity
                .setOutletNo( null )
                .setAssigned( false )
                .setAssignedDate( null )
                .updatedOn( userId )
        ;
        entityRepository.save( entity );

        //Generate log
        AuditTrail log = new AuditTrail();
        String activity = String.format("De-allocated terminal %s from outlet - %s", entity.getSerialNo(), entity.getOutletLink().getName() );
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity( activity )
                .setStatus("Success")
                .setUserNo( userId );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully.");
        return map;
    }


    /**
     * Upload a list of terminals
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> uploadData(HttpServletRequest request) throws Exception{
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Long bankNo = Long.valueOf( request.getParameter("bankNo") );
        String data = request.getParameter("data");

        List<Terminals> terminalList = new ArrayList<>();
        JsonNode jsonNode = objectMapper.readTree( data );
        if( jsonNode.isArray() ){

            for( JsonNode jsonEl: jsonNode){
                String terminalNo = jsonEl.get("SerialNo").asText();

                if ( entityRepository.findBySerialNo( terminalNo ).isPresent() ) {
                    continue;
                }

                // Save record
                Terminals terminal = new Terminals();
                terminal
                        .setSerialNo( terminalNo )
                        .setBankNo( bankNo )
                        .createdOn( userId )
                        .setFlag( AppConstants.STATUS_NEWRECORD )
                ;

                terminalList.add( terminal );
            }
        }

        //Save the records
        entityRepository.saveAll( terminalList );

        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Uploaded a list of terminals successfully.")
                .setNewValues("N/A")
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        String enabled = request.getParameter("floatAccount");
        System.err.println("<><>enabled<><>"+enabled);
        Terminals entity = entityForm.handleRequests( request );

        String haveTerminal;

        if ( entityRepository.findBySerialNo( entity.getSerialNo() ).isPresent() ) {
            map.put("status", "01");
            map.put("message",  "Name already exists.");
            return map;
        }

        if (enabled.equals("null")){
            haveTerminal = "0";
        }else {
            haveTerminal = "1";
        }

        System.err.println("<><>haveTerminal<><>"+haveTerminal);

        Optional<AgentsAccount> oAgentsAccount = agentsAccountRepository.findByAccountNo("1234567");

        AgentsAccount agentsAccount = oAgentsAccount.get();

        entity
                .createdOn( userId )
                .setAmount(agentsAccount.getBalance())
                .setHaveWallets(haveTerminal)
                .setFlag( AppConstants.STATUS_NEWRECORD );

        /*Save record*/
        entityRepository.save( entity );
        
        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new terminal : " + entity.getSerialNo() )
                .setNewValues( entity.getSerialNo() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }


    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        boolean isModified = entityForm.handleEditRequest(request);
        Terminals record = entityForm.getEntity();

        //If record has changes
        if( isModified ){

            record
                    .setFlag( AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn( userNo );
            //Persist record
            entityRepository.save( record );

            //Generate log
            AuditData auditData = entityForm.auditData();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Edited terminal - %s : %s", record.getSerialNo(), auditData.getDescription() ) )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
                    .setUserNo( userNo );

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            //Generate log
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Attempt to update terminal - %s : no changes made", record.getSerialNo())  )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed")
                    .setUserNo( userNo );

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save( log );
        return map;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");

        Terminals record = entityRepository.findById( index ).get();
        boolean proceed = entityForm.applyMakerChecker(record, action);

        map.put("message", entityForm.getResponse());
        if ( proceed ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        }
        else  map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getSerialNo() );
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.transformEntity(index);
    }

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.fetchChanges( index );
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords( HttpServletRequest request ){
        Map<String, Object> map = new HashMap<>();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Terminals record = entityRepository.findById( index ).get();

        boolean success = entityForm.applyMakerChecker(record, action);
        map.put("message", entityForm.getResponse());

        if ( success ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");


        }
        else map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getSerialNo());
        log.setActivity( logActivity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Terminals record = entityForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        entityRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated a terminal successfully. Reference %s", record.getSerialNo() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        Terminals record = entityRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedByLink().getFullNames() );

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
}
