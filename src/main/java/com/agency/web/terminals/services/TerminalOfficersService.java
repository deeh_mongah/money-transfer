package com.agency.web.terminals.services;

import com.agency.core.template.AppConstants;
import com.agency.core.template.forms.AuditData;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.terminals.TerminalOfficersServiceInterface;
import com.agency.web.terminals.entities.TerminalOfficers;
import com.agency.web.terminals.forms.TerminalOfficersForm;
import com.agency.web.terminals.repository.TerminalOfficersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional
public class TerminalOfficersService implements TerminalOfficersServiceInterface {

    @Autowired private TerminalOfficersForm entityForm;
    @Autowired private TerminalOfficersRepository entityRepository;
    @Autowired private AuditTrailRepository auditTrailRepository;
    @Autowired private TerminalOfficerHelper terminalOfficerHelper;

    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        TerminalOfficers entity = entityForm.handleRequests( request );

        if ( entityRepository.findByPassport( entity.getPassport() ).isPresent() ) {
            map.put("status", "01");
            map.put("message",  "Passport already exists.");
            return map;
        }

        String userCode = terminalOfficerHelper.getNewUserId();
        String pin = TerminalOfficerHelper.randomNumber( 4 );

        entity
                .setUserCode( userCode )
                .setPin( pin )
                .createdOn( userId )
                .setFlag( AppConstants.STATUS_NEWRECORD );

        /*Save record*/
        entityRepository.save( entity );

        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new terminal officer: " + entity.getFullNames() )
                .setNewValues( entity.getFullNames() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }


    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        boolean isModified = entityForm.handleEditRequest(request);
        TerminalOfficers record = entityForm.getEntity();

        //If record has changes
        if( isModified ){

            record
                    .setFlag( AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn( userNo );
            //Persist record
            entityRepository.save( record );

            //Generate log
            AuditData auditData = entityForm.auditData();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Edited terminal officer - %s : %s", record.getFullNames(), auditData.getDescription() ) )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
                    .setUserNo( userNo );

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            //Generate log
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Attempt to update terminal officer - %s : no changes made", record.getFullNames())  )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed")
                    .setUserNo( userNo );

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save( log );
        return map;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");

        TerminalOfficers record = entityRepository.findById( index ).get();
        boolean proceed = entityForm.applyMakerChecker(record, action);

        map.put("message", entityForm.getResponse());
        if ( proceed ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        }
        else  map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getFullNames() );
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.transformEntity(index);
    }

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.fetchChanges( index );
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords( HttpServletRequest request ){
        Map<String, Object> map = new HashMap<>();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        TerminalOfficers record = entityRepository.findById( index ).get();

        boolean success = entityForm.applyMakerChecker(record, action);
        map.put("message", entityForm.getResponse());

        if ( success ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        }
        else map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getFullNames() );
        log.setActivity( logActivity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        TerminalOfficers record = entityForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        entityRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated a terminal officer successfully. Reference %s", record.getFullNames() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        TerminalOfficers record = entityRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedByLink().getFullNames() );

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
}
