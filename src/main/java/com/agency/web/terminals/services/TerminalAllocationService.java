package com.agency.web.terminals.services;

import com.agency.web.terminals.TerminalAllocationServiceInterface;
import com.agency.web.terminals.entities.TerminalAllocation;
import com.agency.web.terminals.entities.Terminals;
import com.agency.web.terminals.forms.TerminalAllocationForm;
import com.agency.web.terminals.repository.TerminalAllocationRepository;
import com.agency.web.terminals.repository.TerminalRepository;
import com.agency.web.wallets.entity.TerminalWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.TerminalWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class TerminalAllocationService implements TerminalAllocationServiceInterface{
    @Autowired
    private TerminalAllocationForm terminalAllocationForm;
    @Autowired
    private TerminalRepository terminalRepository;

    @Autowired
    private TerminalAllocationRepository terminalAllocationRepository;

    @Autowired
    private  TerminalWalletsRepository terminalWalletsRepository;
    @Autowired
    private WalletsRepository walletsRepository;

    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");

        Long terminalNo = Long.valueOf(request.getParameter("terminalNo"));
        Long agentNo = Long.valueOf(request.getParameter("agentNo"));

        TerminalAllocation entity = this.terminalAllocationForm.handleRequests( request );






        entity  .setId(terminalNo)
                .setFlag("1")
                .setAllocate("1")
                .setOutletNo(agentNo)
                .createdOn(userId);

        terminalAllocationRepository.save( entity );

        Optional<Terminals> oTerminals = terminalRepository.findById(terminalNo);

        if (!oTerminals.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Terminal");
        }

        Terminals terminals = oTerminals.get();

        terminals.setAssigned(true);

        terminalRepository.save(terminals);

        if (terminals.getHaveWallets().equals("1")){
            Optional<TerminalWallets> oTerminalWallets = terminalWalletsRepository.findByTerminalNo(terminalNo);

            if (!oTerminalWallets.isPresent()) {

                Wallets wallets = new Wallets();
                wallets.setAmount(BigDecimal.ZERO);

                walletsRepository.save(wallets);

                TerminalWallets terminalWallets = new TerminalWallets();
                terminalWallets.setTerminalNo(terminals.getId());
                terminalWallets.setWalletNo(wallets.getId());

                terminalWalletsRepository.save(terminalWallets);
            }
        }

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    @Override
    public Map<String, Object> dealocate(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf(request.getParameter("index"));

        Optional<TerminalAllocation> oTerminalAllocation = terminalAllocationRepository.findById(index);

        if (!oTerminalAllocation.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Terminal");
            return map;
        }

        TerminalAllocation terminalAllocation = oTerminalAllocation.get();

        terminalAllocation.setFlag("1");
        terminalAllocation.setAllocate("0");

        terminalAllocationRepository.save(terminalAllocation);

        Optional<Terminals> oTerminals = terminalRepository.findById(index);

        if (!oTerminals.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Terminal");
        }

        Terminals terminals = oTerminals.get();

        terminals.setAssigned(false);

        terminalRepository.save(terminals);

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    @Override
    public Map<String, Object> allocation(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf(request.getParameter("index"));

        Long agentNo = Long.valueOf(request.getParameter("agentNo"));

        TerminalAllocation entity = this.terminalAllocationForm.handleRequests( request );

        entity
                .setFlag("1")
                .setOutletNo(agentNo)
                .createdOn(userId);

        terminalAllocationRepository.save( entity );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }
}
