package com.agency.web.terminals.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.terminals.entities.TerminalAllocation;
import org.springframework.stereotype.Component;

@Component
public class TerminalAllocationForm extends Form<TerminalAllocation> {

    public TerminalAllocationForm(){
        setMapping( TerminalAllocation.class );
        setRole("ROLE_TERMINALS", "Terminals", "superadmin");
    }
}
