package com.agency.web.terminals.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.terminals.entities.TerminalOfficers;
import org.springframework.stereotype.Component;

@Component
public class TerminalOfficersForm extends Form<TerminalOfficers> {

    public TerminalOfficersForm(){
        setMapping( TerminalOfficers.class );
        setRole("ROLE_TERMINAL_OFFICERS", "Terminal Officers", "school");
    }
}
