package com.agency.web.terminals.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.terminals.entities.Terminals;
import org.springframework.stereotype.Component;

@Component
public class TerminalForm  extends Form<Terminals> {

    public TerminalForm(){
        setMapping( Terminals.class );
        setRole("ROLE_TERMINALS", "Terminals", "superadmin");
    }
}
