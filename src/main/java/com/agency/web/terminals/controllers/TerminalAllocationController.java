package com.agency.web.terminals.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.OutletsServiceInterface;
import com.agency.web.terminals.TerminalAllocationServiceInterface;
import com.agency.web.terminals.TerminalServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class TerminalAllocationController {

    @Autowired private TerminalServiceInterface entityService;
    @Autowired private OutletsServiceInterface outletsServiceInterface;
    @Autowired private TerminalAllocationServiceInterface terminalAllocationServiceInterface;
    @Autowired private DatatablesInterface dataTable;

    @RequestMapping(value = "/allocations")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("terminal-manager/terminal-allocation");
//        View view = new View("terminal-manager/allocation");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if (null != action)
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);
        }

        return view
                .addAttribute("terminals", entityService.fetchUnallocatedRecords( request ) )
                .addAttribute("outlet", outletsServiceInterface.fetchRecords(request) )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view    Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view) {
        Map<String, Object> map = new HashMap<>();
        try {
            String action = request.getParameter("action");

//            //When creating a record
            if("new".equals( action ) )
                map = this.terminalAllocationServiceInterface.saveRecord( request );

            else if("fetch-record".equals( action ))
                map = this.outletsServiceInterface.fetchRecord( request );

            else if("deallocate".equals( action ))
                map = this.terminalAllocationServiceInterface.dealocate( request );

            else if("allocate".equals( action ))
                map = this.terminalAllocationServiceInterface.allocation( request );




        } catch (Exception e) {
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON(map);
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");

        dataTable
//                    .nativeSQL( true)
                .select("a.terminalLink.serialNo, 'N/A', a.terminalLink.dailyTransLimit, " +
                        "case when a.flag ='1' then a.outletsLink.name  else '-' end," +
                        "DATE_FORMAT(a.createdOn; '%Y-%m-%d %T' ), a.id, a.flag")
                .from("TerminalAllocation a")
                .where("a.allocate = :allocate")
                .setParameter("allocate", state)
        ;

//        String outletId = request.getParameter("data");
//        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
//        String parentType = (String)request.getSession().getAttribute("_userParentType");

//        if( StringUtils.isEmpty( outletId ) ) {
//            dataTable
//                    .nativeSQL(true)
//                    .select("a.name, COUNT(b.id), a.id ")
//                    .from("outlets a ")
//                    .from("LEFT JOIN terminals b on b.outlet_no = a.id ")
//                    .groupBy(" a.name, a.id ");
//            if( UserTypes.BANK.equals( parentType ) ){
//                dataTable
//                        .where("b.bank_no = :parentNo")
//                        .setParameter("parentNo", parentNo);
//            }
//        }
//        else{
//            dataTable
//                    .nativeSQL( true)
//                    .select("a.serial_no, 'N/A', '-',  DATE_FORMAT(a.assigned_date; '%Y-%m-%d %T' ), a.id")
//                    .from("terminals a");
////                    .where("a.outlet_no = :outletNo")
////                    .setParameter("outletNo", Long.valueOf( outletId ) );
//        }

        return view.sendJSON( dataTable.showTable() );
    }
}
