package com.agency.web.terminals;

import com.agency.web.terminals.entities.OfficerTypes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface OfficerTypesServiceInterface {

    /**
     * Fetch all persisted records
     *
     * @return List<OfficerTypes>
     */
    public List<OfficerTypes> fetchRecords(HttpServletRequest request);
}
