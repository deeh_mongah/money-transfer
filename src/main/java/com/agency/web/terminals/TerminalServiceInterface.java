package com.agency.web.terminals;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.terminals.entities.Terminals;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface TerminalServiceInterface extends BaseServiceInterface{

    /**
     * Fetch all persisted records
     *
     * @return List<Terminals>
     */
    public List<Terminals> fetchRecords(HttpServletRequest request);

    /**
     * Fetch all unallocated records
     *
     * @param request
     * @return List<Terminals>
     */
    public List<Terminals> fetchUnallocatedRecords(HttpServletRequest request);

    /**
     * Allocate a batch of terminals to an outlet
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> allocateTerminals(HttpServletRequest request);

    /**
     *Allocates a single terminal to an outlet
     *
     * @param request
     * @return Map<String, Object>
     * @throws Exception
     */
    public Map<String, Object> allocateOneTerminal(HttpServletRequest request);


    /**
     * De-allocate one terminal
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> deAllocateOneTerminal(HttpServletRequest request);

    /**
     * Upload a list of terminals
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> uploadData(HttpServletRequest request)  throws Exception;


}
