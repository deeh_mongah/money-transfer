package com.agency.web.terminals.entities;

import com.agency.core.template.forms.EditDataWrapper;
import com.agency.core.template.forms.MutableField;
import com.agency.web.agency.entities.Outlets;
import com.agency.web.banks.entities.Banks;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "terminals")
public class Terminals implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @MutableField(name = "Serial No")
    @Column(name = "SERIAL_NO", length = 100)
    private String serialNo;

    @Size(max = 100)
    @Column(name = "VERSION_NO", length = 100)
    private String versionNo = "N/A";

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "have_wallets")
    private String haveWallets;

    @Size(max = 100)
    @MutableField(name = "Daily Deposit Limit", optional = true)
    @Column(name = "DAILY_DEPOSIT_LIMIT", length = 100)
    private String dailyDepositLimit;

    @Size(max = 100)
    @MutableField(name = "Daily Transaction Limit", optional = true)
    @Column(name = "DAILY_TRANS_LIMIT", length = 100)
    private String dailyTransLimit;

    @Size(max = 100)
    @MutableField(name = "Daily Withdraw Limit", optional = true)
    @Column(name = "DAILY_WITHDRAW_LIMIT", length = 100)
    private String dailyWithdrawLimit;

    @Column(name = "ASSIGNED")
    private boolean assigned = false;

    @Column(name = "ASSIGNED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date assignedDate;



    @Size(max = 200)
    @Column(name = "location", length = 200)
    private String location;

    @Size(max = 100)
    @Column(name = "lat", length = 100)
    private String lat;

    @Size(max = 100)
    @Column(name = "lng", length = 100)
    private String lng;

    @Column(name = "fee_group_no")
    private Long feeGroupNo;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Column(name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Lob
    @EditDataWrapper
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Column(name = "OUTLET_NO")
    private Long outletNo;

    @Column(name = "bank_no")
    private Long bankNo;

    @JoinColumn(name = "OUTLET_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Outlets outletLink;

    @JoinColumn(name = "bank_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Banks bankLink;

    @OneToMany(mappedBy = "terminalNo", fetch = FetchType.LAZY)
    private Set<TerminalOfficers> terminalOfficers;

    @JoinColumn(name = "REASON_CODE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonCodeLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;

    public Terminals() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSerialNo() { return serialNo; }
    public Terminals setSerialNo(String serialNo) {
        this.serialNo = serialNo;
        return this;
    }

    public String getHaveWallets() {
        return haveWallets;
    }

    public Terminals setHaveWallets(String haveWallets) {
        this.haveWallets = haveWallets;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Terminals setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public String getVersionNo() {
        return versionNo;
    }

    public void setVersionNo(String versionNo) {
        this.versionNo = versionNo;
    }

    public String getDailyDepositLimit() {
        return dailyDepositLimit;
    }

    public void setDailyDepositLimit(String dailyDepositLimit) {
        this.dailyDepositLimit = dailyDepositLimit;
    }

    public String getDailyTransLimit() {
        return dailyTransLimit;
    }

    public void setDailyTransLimit(String dailyTransLimit) {
        this.dailyTransLimit = dailyTransLimit;
    }

    public String getDailyWithdrawLimit() {
        return dailyWithdrawLimit;
    }

    public void setDailyWithdrawLimit(String dailyWithdrawLimit) {
        this.dailyWithdrawLimit = dailyWithdrawLimit;
    }

    public boolean isAssigned() {   return assigned; }
    public Terminals setAssigned(boolean assigned) {
        this.assigned = assigned;
        return this;
    }

    public Date getAssignedDate() {   return assignedDate; }
    public Terminals setAssignedDate(Date assignedDate) {
        this.assignedDate = assignedDate;
        return this;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public Long getFeeGroupNo() {
        return feeGroupNo;
    }

    public void setFeeGroupNo(Long feeGroupNo) {
        this.feeGroupNo = feeGroupNo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFlag() {  return flag;  }
    public Terminals setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getOutletNo() {   return outletNo; }
    public Terminals setOutletNo(Long outletNo) {
        this.outletNo = outletNo;
        return this;
    }

    public Long getBankNo() {   return bankNo; }
    public Terminals setBankNo(Long bankNo) {
        this.bankNo = bankNo;
        return this;
    }

    @JsonIgnore public Banks getBankLink() {  return bankLink;  }
    @JsonIgnore public Outlets getOutletLink() {  return outletLink;  }
    @JsonIgnore public Set<TerminalOfficers> getTerminalOfficers() {   return terminalOfficers;  }
    @JsonIgnore public ReasonCodes getReasonCodeLink() {  return reasonCodeLink; }
    @JsonIgnore public Users getCreatedByLink() {  return createdByLink; }
    @JsonIgnore public Users getUpdatedByLink() {  return updatedByLink; }

    public Terminals createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Terminals updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Terminals)) {
            return false;
        }
        Terminals other = (Terminals) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Terminals[ id=" + id + " ]";
    }
    
}
