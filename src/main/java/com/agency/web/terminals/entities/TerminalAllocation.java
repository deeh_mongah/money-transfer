package com.agency.web.terminals.entities;

import com.agency.web.agency.entities.Outlets;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "terminal_allocation")
public class TerminalAllocation implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "outlet_no")
    private Long outletNo;


    @Column(name = "flag")
    private String flag;

    @Column(name = "allocate")
    private String allocate;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "deleted_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "updated_by")
    private Long updatedBy;

    @JoinColumn(name = "id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Terminals terminalLink;

    @JoinColumn(name = "outlet_no", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Outlets outletsLink;

    public TerminalAllocation() { }

    public Long getId() {  return id; }
    public TerminalAllocation setId(Long id) {
        this.id = id;
        return this;
    }

    public TerminalAllocation createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }



    public String getFlag() {
        return flag;
    }

    public TerminalAllocation setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getAllocate() {
        return allocate;
    }

    public TerminalAllocation setAllocate(String allocate) {
        this.allocate = allocate;
        return this;
    }

    public Long getOutletNo() {
        return outletNo;
    }

    public TerminalAllocation setOutletNo(Long outletNo) {
        this.outletNo = outletNo;
        return this;
    }

    @JsonIgnore
    public Outlets getOutletsLink() {
        return outletsLink;
    }

    public void setOutletsLink(Outlets outletsLink) {
        this.outletsLink = outletsLink;
    }

    @JsonIgnore
    public Terminals getTerminalLink() {
        return terminalLink;
    }


    public void setTerminalLink(Terminals terminalLink) {
        this.terminalLink = terminalLink;
    }



    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TerminalAllocation)) {
            return false;
        }
        TerminalAllocation other = (TerminalAllocation) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BranchGroups[ id=" + id + " ]";
    }
}
