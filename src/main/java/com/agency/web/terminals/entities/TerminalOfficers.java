/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.web.terminals.entities;

import com.agency.core.template.forms.EditDataWrapper;
import com.agency.core.template.forms.MutableField;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "terminal_officers")
public class TerminalOfficers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @MutableField( name = "First Name")
    @Column(name = "FIRST_NAME", length = 100)
    private String firstName;

    @Size(max = 100)
    @MutableField( name = "Last Name")
    @Column(name = "LAST_NAME", length = 100)
    private String lastName;

    @Size(max = 100)
    @MutableField( name = "Passport")
    @Column(name = "PASSPORT", length = 100)
    private String passport;

    @Size(max = 50)
    @Column(name = "PIN", length = 50)
    private String pin;

    @Size(max = 100)
    @Column(name = "USER_CODE", length = 100)
    private String userCode;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Column(name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Lob
    @EditDataWrapper
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @MutableField( name = "Terminal", entity = "Terminals", reference = "serialNo")
    @Column(name = "TERMINAL_NO")
    private Long terminalNo;

    @MutableField( name = "Officer Type", entity = "OfficerTypes", reference = "name")
    @Column(name = "OFFICER_TYPE_NO")
    private Long officerTypeNo;

    @JoinColumn(name = "TERMINAL_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Terminals terminalLink;

    @JoinColumn(name = "OFFICER_TYPE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private OfficerTypes officerTypeLink;

    @JoinColumn(name = "REASON_CODE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonCodeLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;

    @Transient
    private String fullNames;

    public TerminalOfficers() { }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getPin() {   return pin;  }
    public TerminalOfficers setPin(String pin) {
        this.pin = pin;
        return this;
    }

    public String getUserCode() {  return userCode;  }
    public TerminalOfficers setUserCode(String userCode) {
        this.userCode = userCode;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFlag() {  return flag; }
    public TerminalOfficers setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(Long terminalNo) {
        this.terminalNo = terminalNo;
    }

    public Long getOfficerTypeNo() {
        return officerTypeNo;
    }

    public void setOfficerTypeNo(Long officerTypeNo) {
        this.officerTypeNo = officerTypeNo;
    }

    public String getFullNames() {
        this.fullNames = String.format("%s %s", this.firstName, this.lastName );
        return fullNames;
    }

    @JsonIgnore
    public Terminals getTerminalLink() {  return terminalLink;  }
    @JsonIgnore
    public OfficerTypes getOfficerTypeLink() {   return officerTypeLink;  }
    @JsonIgnore
    public ReasonCodes getReasonCodeLink() {  return reasonCodeLink; }
    @JsonIgnore
    public Users getCreatedByLink() {  return createdByLink; }
    @JsonIgnore
    public Users getUpdatedByLink() {  return updatedByLink; }

    public TerminalOfficers createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public TerminalOfficers updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TerminalOfficers)) {
            return false;
        }
        TerminalOfficers other = (TerminalOfficers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TerminalOfficers[ id=" + id + " ]";
    }
    
}
