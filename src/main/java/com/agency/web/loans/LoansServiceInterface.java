package com.agency.web.loans;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 *
 * @category    Merchants
 * @package     Remittance
 * @author      Anthony Mwawughanga
 * @version     1.0.0
 * @since       Dec 8, 2016
 *
 */
public interface LoansServiceInterface {

    /**
     * Fetch all active records
     *
     * @return List<Merchants>
     */
    public Object fetchRecords();

    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    public Map<String, Object> saveRecord(HttpServletRequest request);

    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    public Map<String, Object> editRecord(HttpServletRequest request);

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    public Map<String, Object> approveEditChanges(HttpServletRequest request);

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchRecord(HttpServletRequest request);

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request);

    /**
     * Update record status
     *
     * @param request
     * @param status Object status e.g active, inactive, etc
     * @param checker True - makerchecker; False - editorchecker
     * @param action
     * @return Map<String, Object>
     */
    public Map<String, Object> flagRecords(HttpServletRequest request, String status, boolean checker, String action);

    /**
     * Removes a record from permanent storage
     *
     * @param request
     * @param   action
     * @return Map<String, Object>
     */
    public Map<String, Object> deleteRecord(HttpServletRequest request);

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> deactivateRecord(HttpServletRequest request);

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request);

    String fetchName(String index);
}
