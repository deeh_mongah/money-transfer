package com.agency.web.loans.form;

import com.agency.core.template.forms.Form;
import com.agency.web.loans.entities.Loans;
import com.agency.web.loans.entities.LoansTransaction;
import org.springframework.stereotype.Component;

@Component
public class LoanTransactionsForm extends Form<LoansTransaction> {

    public LoanTransactionsForm(){
        setMapping( LoansTransaction.class );
        setRole("ROLE_USERS", "Users", "superadmin");
    }
}
