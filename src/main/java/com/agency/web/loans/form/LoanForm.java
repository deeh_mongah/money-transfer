package com.agency.web.loans.form;

import com.agency.core.template.forms.Form;
import com.agency.web.loans.entities.Loans;
import com.agency.web.usermanager.entities.Users;
import org.springframework.stereotype.Component;

@Component
public class LoanForm extends Form<Loans> {

    public LoanForm(){
        setMapping( Loans.class );
        setRole("ROLE_USERS", "Users", "superadmin");
    }
}
