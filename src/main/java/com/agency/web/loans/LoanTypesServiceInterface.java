package com.agency.web.loans;




import com.agency.web.loans.entities.LoanTypes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Administrator on 3/19/2019.
 */
public interface LoanTypesServiceInterface {
    /**
     * Fetch all records
     *
     * @param request
     * @return List<loanTypes>
     */
    public List<LoanTypes> fetchRecords(HttpServletRequest request);
}
