package com.agency.web.loans.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.AppConstants;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.configs.ReasonCodeServiceInterface;
import com.agency.web.loans.LoansServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @category    Merchants
 * @package     Remittance
 * @author      Anthony Mwawughanga
 * @version     1.0.0
 * @since       Dec 8, 2016
 *
 */
@Controller
public class LoansController {

    @Autowired
    private LoansServiceInterface loansServiceInterface;
    @Autowired
    private ReasonCodeServiceInterface reasonCodesService;
    @Autowired
    private DatatablesInterface dataTable;
    
    @RequestMapping(value = "/loans")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("loans/loans");
        
        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");
            
            if( null != action )
                return handleRequests(request, view);
            
            //When fetching table data
            else 
                return fetchTableInfo(request, view);
            
        }
        
        return view
                .addAttribute("reasoncodes", reasonCodesService.fetchRecords())
                .getView();
    }
    
    /**
     * Handle various client requests
     * 
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");
            
            //When creating a record
            if("new".equals( action ))
                map = this.loansServiceInterface.saveRecord( request );
            
            else if("edit".equals( action ))
                map = this.loansServiceInterface.editRecord( request );
            
            //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.loansServiceInterface.fetchRecord( request );
                
            //View record changes
            else if("vedit".equals( action ) )
                map = this.loansServiceInterface.fetchRecordChanges( request );
            
            //When approving changes
            else if("approveedit".equals( action ) || "declineedit".equals( action) )
                map = this.loansServiceInterface.approveEditChanges( request );
            
            //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.loansServiceInterface.fetchDeactivationInfo( request );
            
            //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.loansServiceInterface.deactivateRecord( request );
            
            //When declining a deactivation request
            else if("declinedeactivation".equals( action ) )
                map = this.loansServiceInterface.flagRecords(request, AppConstants.STATUS_ACTIVERECORD, true, action);
            
            //When approving a deactivation request
            else if("approvedeactivation".equals( action ) )
                map = this.loansServiceInterface.flagRecords(request, AppConstants.STATUS_INACTIVERECORD, true, action);
            
            //When activating a record
            else if("activate".equals( action ) || "approve-new".equals( action ) )
                map = this.loansServiceInterface.flagRecords(request, AppConstants.STATUS_ACTIVERECORD, true, action);
            
            //When deleting a record
            else if("delete".equals( action ) || "declinenew".equals( action ) )
                map = this.loansServiceInterface.deleteRecord( request );
            
        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }
        
        return view.sendJSON(map);
    }
    
    /**
     * Fetch table information
     * 
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");
        
        dataTable
                .select("v.name, v.code,v.maxTenure, v.maxAmount, str(v.updatedOn;'YYYY-MM-DD HH24:MI:SS'), v.createdBy, v.id")
                .from("Loans v ")
                .where("v.flag =:flag")
                .setParameter("flag", state);

        return view.sendJSON(dataTable.showTable());
    }

}
