package com.agency.web.loans.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.configs.ReasonCodeServiceInterface;
import com.agency.web.loans.LoanTransactionsServiceInterface;
import com.agency.web.loans.LoanTypesServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Administrator on 3/19/2019.
 */

@Controller
public class LoansTransactionController {
    @Autowired
    private LoanTransactionsServiceInterface entityService;
    @Autowired
    private DatatablesInterface dataTable;
    @Autowired
    private ReasonCodeServiceInterface reasonCodeService;


    @Autowired
    private LoanTypesServiceInterface loanTypesServiceInterface;


    @RequestMapping( value = "/loan-applications")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("loans/loan-application");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");
            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view
//                .addAttribute("loanTypes", loanTypesServiceInterface.fetchRecords(request) )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String transactionAlias = "";

            String[]  columns = new String[]{
                    "Name",
                    "Mobile",
                    "NRC",
                    "Customer Ref",
                    "Loan Type",
                    "Amount",
                    "Repayment Period",
                    "Status",
                    "Applied On"
            };
            dataTable
//                    .nativeSQL( true )

                    .select(" a.transactionCode, a.customerId,a.mobile,b.name,a.amount,a.repaymentPeriod, a.flag, str(a.createdOn;'YYYY-MM-DD HH24:MI:SS')")
                    .from ("LoansTransaction a LEFT JOIN a.loanTypesLink b")


            ;

            transactionAlias = "a";

//        //Set-up filters
//        setDateFilters( request, transactionAlias );
//        setTransactionStatus( request, transactionAlias );
//        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }


//    @RequestMapping(value = "/loan-applications/{format}", method = RequestMethod.GET)
//    public void export (
//            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
//    ) throws IOException {
//        generateDoc (
//                request, response, "loan applications", format
//        );
//    }
}
