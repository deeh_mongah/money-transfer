package com.agency.web.loans.repository;

import com.agency.web.loans.entities.LoanTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by Administrator on 3/19/2019.
 */


@Repository
public interface LoanTypesRepository extends CrudRepository<LoanTypes, Long> {

    public Optional<LoanTypes> findByCode(String code);
}
