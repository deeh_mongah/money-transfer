package com.agency.web.loans.repository;

import com.agency.web.loans.entities.Loans;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LoansRepository extends CrudRepository<Loans, Long> {

    List<Loans> findByFlag(String flag);
}
