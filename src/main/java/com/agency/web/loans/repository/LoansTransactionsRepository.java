package com.agency.web.loans.repository;

import com.agency.web.loans.entities.LoansTransaction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Administrator on 3/19/2019.
 */
public interface LoansTransactionsRepository extends CrudRepository<LoansTransaction, Long> {
   public List<LoansTransaction> findById(List<String> id);

   public Optional<LoansTransaction> findByCustomerRefAndLoanTypeNo(String customerRef, Long loanTypeNo);

   public Optional<LoansTransaction> findByTransactionCode(String transactionCode);

}
