package com.agency.web.loans;



import com.agency.api.vm.LoanApplicationRequest;
import com.agency.api.vm.LoanRepaymentRequest;
import com.agency.api.vm.ResponseModel;
import com.agency.core.template.forms.BaseServiceInterface;

import java.util.Map;

/**
 * Created by Administrator on 3/19/2019.
 */
public interface LoanTransactionsServiceInterface extends BaseServiceInterface {

    public ResponseModel loanApplication(LoanApplicationRequest request);

    public Map<String, Object> loanRepayment(LoanRepaymentRequest request);

//    /**
//     * Fetch all records
//     *
//     * @param request
//     * @return List<Loans>
//     */
//    public List<LoansTransaction> fetchRecords(HttpServletRequest request);

//    Map<String,Object> saveRecord(HttpServletRequest request);
}
