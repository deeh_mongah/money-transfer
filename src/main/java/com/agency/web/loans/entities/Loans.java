/*
 * The MIT License
 *
 * Copyright 2016 Binary Limited.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package com.agency.web.loans.entities;

import com.agency.core.template.forms.EditDataWrapper;
import com.agency.core.template.forms.MutableField;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.usermanager.auth.SecurityUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @category    Merchants
 * @package     Remittance
 * @author      Anthony Mwawughanga
 * @version     1.0.0
 * @since       Dec 8, 2016
 *
 */
@Entity
@Table(name = "Loans")
public class Loans implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @MutableField( name = "Name")
    @Column(name = "name", length = 100)
    private String name;

    @Size(max = 100)
    @MutableField( name = "code")
    @Column(name = "code", length = 100)
    private String code;

    @Column(name = "max_tenure", precision = 18, scale = 2)
    private BigDecimal maxTenure;

    @Column(name = "max_amount", precision = 18, scale = 2)
    private BigDecimal maxAmount;



    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Size(max = 150)
    @Column(name = "CREATED_BY", length = 150)
    private String createdBy;

    @Size(max = 150)
    @Column(name = "UPDATED_BY", length = 150)
    private String updatedBy;

    @Column(name = "FLAG")
    private String flag;

    @Lob
    @EditDataWrapper
    @Column(name = "EDIT_DATA")
    private String editData;

    @Column(name = "REASON_NO")
    private Long reasonNo;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;



    @JoinColumn(name = "REASON_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonLink;

    public Loans() {}
    public Long getId() { return id; }
    public void setId(Long id) {
        this.id = id;
    }

    public String getName() { return name; }
    public void setName(String name) {
        this.name = name;
    }



    public Date getCreatedOn() { return createdOn; }
    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() { return updatedOn; }
    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getCreatedBy() { return createdBy; }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() { return updatedBy; }
    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFlag() { return flag; }
    public Loans setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public BigDecimal getMaxTenure() {
        return maxTenure;
    }

    public void setMaxTenure(BigDecimal maxTenure) {
        this.maxTenure = maxTenure;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }



    public String getEditData() { return editData; }
    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getReasonNo() { return reasonNo; }
    public void setReasonNo(Long reasonNo) {
        this.reasonNo = reasonNo;
    }

    public String getReasonDescription() { return reasonDescription; }
    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    @JsonIgnore
    public ReasonCodes getReasonLink() {
        return reasonLink;
    }

    public void setReasonLink(ReasonCodes reasonLink) {
        this.reasonLink = reasonLink;
    }

    public Loans createdOn(){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = SecurityUtils.getCurrentUserLogin();
        this.createdBy = SecurityUtils.getCurrentUserLogin();
        return this;
    }
    
    public Loans updatedOn(){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = SecurityUtils.getCurrentUserLogin();
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Loans)) {
            return false;
        }
        Loans other = (Loans) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Loans[ id=" + id + " ]";
    }

}
