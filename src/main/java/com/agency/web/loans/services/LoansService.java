package com.agency.web.loans.services;

import com.agency.api.vm.LoansResponse;
import com.agency.core.template.AppConstants;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.loans.LoansServiceInterface;
import com.agency.web.loans.entities.Loans;
import com.agency.web.loans.form.LoanForm;
import com.agency.web.loans.repository.LoansRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

//import com.bi.core.configs.database.MakerCheckerUtilService;

/*
 * The MIT License
 *
 * Copyright 2016 Binary Limited.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 *
 * @category    Merchants
 * @package     Remittance
 * @author      Anthony Mwawughanga
 * @version     1.0.0
 * @since       Dec 8, 2016
 *
 */
@Service
@Transactional
public class LoansService implements LoansServiceInterface {

    @Autowired
    private LoansRepository loansRepository;
    @Autowired
    private LoanForm loanForm;

    @Autowired private AuditTrailRepository auditTrailRepository;
    
//    public BankService(){
//        setMapping( Banks.class );
//    }
    
    /**
     * Fetch all active records
     * 
     * @return List<Merchants>
     */
    @Override
    public Object fetchRecords(){
        Map<String, Object> response = new HashMap<>();


        List<Loans> loansList = loansRepository.findByFlag(AppConstants.STATUS_ACTIVERECORD);

        //Package response
        LoansResponse responses = new LoansResponse();

        List<LoansResponse.LoansData> dataList = new ArrayList<>();

        for (Loans node : loansList){
            LoansResponse.LoansData data = new LoansResponse.LoansData();

            data.setCode(node.getCode());
            data.setId(node.getId());
            data.setMaxAmount(node.getMaxAmount());
            data.setMaxTenure(node.getMaxTenure());
            data.setName(node.getName());

            dataList.add(data);

        }

        responses.setData(dataList);


        return responses;
    }
    
    /**
     * Persist a new record 
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        Loans record = this.loanForm.handleRequests( request );
        
        Map<String, Object> filter = new HashMap<>();
        filter.put("name", record.getName() );

            
            record
                .setFlag( AppConstants.STATUS_NEWRECORD )
                .createdOn();

            /*Save object*/
            loansRepository.save(record);
            
            map.put("status", "00");
            map.put("message", "Request processed successfully");

        return map;
    }
    
    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return this.loanForm.transformEntity(index);
    }
    
    /**
     * Edit a record 
     * 
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<String, Object>();
        
        boolean isModified = this.loanForm.handleEditRequest(request);
        Loans record = this.loanForm.getEntity();
        
        //If record has changes
        if( isModified ){
            record
                    .setFlag(AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn();
            //Persist record
            this.loansRepository.save(record );
            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }
        return map;
    }
    
    /**
     * Approve edit changes
     * 
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){
        Map<String, Object> map = new HashMap<String, Object>();
        String index = request.getParameter("index");
        String action  = request.getParameter("action");
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        Loans record = loansRepository.findById(Long.valueOf(index)).get();
        boolean proceed = loanForm.applyMakerChecker(record, action);

        map.put("message", loanForm.getResponse());
        if ( proceed ) {
            record = loanForm.getEntity();
            loansRepository.save( record );
            map.put("status", "00");
        }
        else  map.put("status", "01");

        //Insert logs
        AuditTrail log = loanForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getName() );
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }


    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request ){
        String index = request.getParameter("index");
        return this.loanForm.fetchChanges(index);
    }
    
    /**
     * Update record status
     *
     * @param request
     * @param status Object status e.g active, inactive, etc
     * @param checker True - makerchecker; False - editorchecker
     * @param action
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords(HttpServletRequest request, String status, boolean checker, String action
    ) {
        Map<String, Object> map = new HashMap<String, Object>();
        String index = request.getParameter("index");

        Loans record = loansRepository.findById(Long.valueOf(index)).get();

        StringBuilder activity = new StringBuilder();
        
//        if (applyMakerChecker(record, action)) {
            record
                  .setFlag(status).updatedOn();


            switch (action) {
                case AppConstants.ACTION_APRROVE_NEW:
                    activity.append("Record approved successfully");
                    break;
                case AppConstants.ACTION_ACTIVATE:
                    activity.append("Record activated successfully");
                    break;
                case AppConstants.ACTION_APPROVE_DEACTIVATION:
                    activity.append("Record deactivated successfully");
                    break;
                case AppConstants.ACTION_DECLINE_DEACTIVATION:
                    activity.append("Record activated successfully");
                    break;
                case AppConstants.ACTION_SOFT_DELETE:
                    activity.append("Request processed successfully");
                    break;
            }
            this.loansRepository.save(record );
            map.put("status", "00");

        map.put("message", activity.toString());
        return map;
    }
    
    /**
     * Deactivate a record
     * 
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord( HttpServletRequest request ){
       Map<String, Object> map = new HashMap<String, Object>();
       Loans record = this.loanForm.deactivateRequest( request );
       record
               .setFlag(AppConstants.STATUS_DEACTIVATED )
               .updatedOn();
       
        this.loansRepository.save(record );
            map.put("status", "00");
            map.put("message", "Request processed successfully");
       
       return map;
    }
    
    /**
     * Fetch deactivation details 
     * 
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo( HttpServletRequest request ){
       Map<String, Object> map = new HashMap<String, Object>();
       String index = request.getParameter("index");

        Loans record = loansRepository.findById(Long.valueOf(index)).get();

        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedBy() );

        if( null != record.getReasonLink() ){
            ReasonCodes reasonCode = record.getReasonLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
    
    /**
     * Removes a record from permanent storage
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deleteRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<String, Object>();
        String index = request.getParameter("index");
        String action = request.getParameter("action");
        Loans record = loansRepository.findById(Long.valueOf(index)).get();

//        if (applyMakerChecker(record, action)) {

            record.setFlag(AppConstants.STATUS_SOFTDELETED).updatedOn();

            this.loansRepository.save(record );
            map.put("status", "00");
            map.put("message", "Request processed successfully");
//        } else {
//            map.put("status", "01");
//            switch (action) {
//                case AppConstants.ACTION_DECLINE_NEW:
//                    map.put("message", "You can not delete a record you created");
//                    break;
//                case AppConstants.ACTION_DELETE:
//                    map.put("message", "You can not delete a record you deactivated");
//                    break;
//            }
//        }
        return map;
    }

    @Override
    public String fetchName(String index) {
        return loansRepository.findById(Long.valueOf(index)).get().getName();
    }
}
