package com.agency.web.loans.services;

import com.agency.web.loans.LoanTypesServiceInterface;
import com.agency.web.loans.entities.LoanTypes;
import com.agency.web.loans.repository.LoanTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by Deeh on 3/19/2019.
 */

@Service
@Transactional
public class LoanTypesService implements LoanTypesServiceInterface {

    @Autowired
    private LoanTypesRepository loanTypesRepository;

    @Override
    public List<LoanTypes> fetchRecords(HttpServletRequest request) {
        return null;
    }

//    @Override
//    public List<LoanTypes> fetchRecords(HttpServletRequest request){
//        List<LoanTypes> data = new ArrayList<>();
//        List<String> codes = new ArrayList<>();
//        data = loanTypesRepository.findByCode(codes);
//        return data;
//    }


}
