package com.agency.web.loans.services;

import com.agency.api.vm.LoanApplicationRequest;
import com.agency.api.vm.LoanRepaymentRequest;
import com.agency.api.vm.ResponseModel;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.loans.LoanTransactionsServiceInterface;
import com.agency.web.loans.entities.LoanTypes;
import com.agency.web.loans.entities.LoansTransaction;
import com.agency.web.loans.form.LoanTransactionsForm;
import com.agency.web.loans.repository.LoanTypesRepository;
import com.agency.web.loans.repository.LoansTransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * Created by Deeh on 3/19/2019.
 */

@Service
@Transactional
public class LoansTransactionsService implements LoanTransactionsServiceInterface {

    @Autowired
    private AuditTrailRepository auditTrailRepository;
    @Autowired
    private LoanTransactionsForm entityForm;
    @Autowired
    private LoansTransactionsRepository loansRepository;
    @Autowired
    private LoanTypesRepository loanTypesRepository;


    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        LoansTransaction entity = entityForm.handleRequests( request );
        Long loanTypeNo = entity.getLoanTypeNo();
        String customerRef= entity.getCustomerRef();
        if ( loansRepository.findByCustomerRefAndLoanTypeNo( customerRef,loanTypeNo ).isPresent() ) {
            map.put("status", "01");
            map.put("message",  "Transaction already exists.");
            return map;
        }

        entity
                .createdOn( userId )
                .setFlag( "Success" );

        /*Save record*/
        loansRepository.save( entity );

        AuditTrail log = new AuditTrail();
        log
                .setActivity("Created a new Loan : " + entity.getLoanTypeNo() )
                .setStatus("Success");

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    @Override
    public Map<String, Object> editRecord(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request) {
        return null;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.transformEntity(index);
    }

    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> flagRecords(HttpServletRequest request) {
        return null;
    }


    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request) {
        return null;
    }

    @Override
    public ResponseModel loanApplication(LoanApplicationRequest request) {
        // Fetch the loan type
        LoanTypes loanType;
        Optional<LoanTypes> oLoanTypes = loanTypesRepository.findByCode( request.getLoanTypeCode() );
        if(!oLoanTypes.isPresent()){
            loanType = new LoanTypes();
            loanType.setCode( request.getLoanTypeCode() );
            loanType.setName( request.getLoanTypeName() );

            loanTypesRepository.save( loanType );
        }
        else loanType = oLoanTypes.get();

        LoanApplicationRequest.CustomerData customer = request.getCustomer();




        LoansTransaction loansTransaction= new LoansTransaction();
        loansTransaction.setCustomerId(customer.getIdNumber());
        loansTransaction.setRepaymentPeriod(request.getRepaymentPeriod());
        loansTransaction.setAmount(request.getAmount());
        loansTransaction.setLoanTypeNo(loanType.getId());
        loansTransaction.setTransactionCode(request.getLoanUniqueCode());
        loansTransaction.setMobile(customer.getMobileNo());

        loansRepository.save(loansTransaction);
        return new ResponseModel("00", "Loan Application Successful");  }

    @Override
    public Map<String, Object> loanRepayment(LoanRepaymentRequest request) {
        Map<String, Object> map = new HashMap<>();

        //getting resources
        String loanUniqueCode = request.getLoanUniqueCode();

        Optional<LoansTransaction> oLoansTransaction = loansRepository.findByTransactionCode(loanUniqueCode);

        if (!oLoansTransaction.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Loan Code.");
            return map;
        }

        LoansTransaction loansTransaction = oLoansTransaction.get();

        String loanType = loansTransaction.getLoanTypesLink().getName();

        BigDecimal loanAmount = loansTransaction.getAmount();

        BigDecimal loanBalance = loanAmount.subtract(request.getAmount());



        loansTransaction.setAmount(loanBalance);

        loansRepository.save(loansTransaction);




        map.put("status", "00");
        map.put("loanType",loanType );
        map.put("message", "Loan repayment Successful.");
        return map;
    }
}

