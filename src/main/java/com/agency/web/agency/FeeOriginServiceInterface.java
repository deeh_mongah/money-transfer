package com.agency.web.agency;

import com.agency.web.agency.entities.FeeOrigin;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FeeOriginServiceInterface {

    /**
     * Fetch all persisted records
     *
     * @return List<FeeOrigin>
     */
    public List<FeeOrigin> fetchRecords(HttpServletRequest request);
}
