package com.agency.web.agency.repository;

import com.agency.web.agency.entities.Fees;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Repository
public interface FeeRepository extends CrudRepository<Fees, Long> {

    /**
     * Fetch a record given their name
     *
     * @param name
     * @return Optional<Fees>
     */
    public Optional<Fees> findByName(String name);

    @Query(value = "SELECT *  FROM fees a where a.transaction_type_no =?1  and ?2 BETWEEN  a.min_amount AND a.max_amount ", nativeQuery = true)
    public Optional<Fees> fetchFeeByTransactionTypeNoAndAmount(Long transactionTypeNo, BigDecimal amount);

    List<Fees> findByFlag(String flag);

    Optional<Fees> findByTransactionTypeNoAndFlag(long transactionTypeNo, String flag);

    @Query("SELECT a FROM Fees a WHERE ((?1 BETWEEN a.minAmount AND a.maxAmount) " +
            "OR (?2 BETWEEN a.minAmount AND a.maxAmount)) " +
            "AND a.transactionTypeNo = ?3 ")
    public List<Fees> fetchDuplicates(BigDecimal minAmount, BigDecimal maxAmount, Long transactionTypeNo );



}
