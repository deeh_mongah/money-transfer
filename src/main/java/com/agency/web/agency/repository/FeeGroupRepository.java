package com.agency.web.agency.repository;

import com.agency.web.agency.entities.FeeGroups;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface FeeGroupRepository extends CrudRepository<FeeGroups, Long> {

    /**
     * Fetch a record given their name
     *
     * @param name
     * @return Optional<FeeGroups>
     */
    public Optional<FeeGroups> findByName(String name);

    /**
     * Fetch records by flag
     *
     * @param flag
     * @return List<FeeGroups>
     */
    public List<FeeGroups> findAllByFlag( String flag );

}
