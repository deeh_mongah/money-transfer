package com.agency.web.agency.repository;

import com.agency.web.agency.entities.Channels;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ChannelsRepository extends CrudRepository<Channels, Long> {
}
