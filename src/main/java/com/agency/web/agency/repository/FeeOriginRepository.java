package com.agency.web.agency.repository;

import com.agency.web.agency.entities.FeeOrigin;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FeeOriginRepository extends CrudRepository<FeeOrigin, Long> {
}
