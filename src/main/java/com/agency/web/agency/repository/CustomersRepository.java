package com.agency.web.agency.repository;

import com.agency.web.agency.entities.Customers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ben on 06/11/2019.
 */
@Repository
public interface CustomersRepository extends CrudRepository<Customers, Long> {

    Optional<Customers> findByPhone(String phone);

}
