package com.agency.web.agency.repository;

import com.agency.web.agency.entities.Outlets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OutletsRepository extends CrudRepository<Outlets, Long> {

    /**
     * Fetch a record given their name
     *
     * @param name
     * @return Optional<Outlets>
     */
    public Optional<Outlets> findByName(String name);

    /**
     * Fetch bank records
     *
     * @param flag
     * @return List<Outlets>
     */
    public List<Outlets> findAllByFlag(String flag);

    /**
     * Fetch bank records
     *
     * @param flag
     * @param bankNo
     * @return List<Outlets>
     */
    public List<Outlets> findAllByFlagAndAgentLink_BankNo(String flag, Long bankNo);
}
