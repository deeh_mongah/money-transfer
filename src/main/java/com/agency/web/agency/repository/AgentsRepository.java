package com.agency.web.agency.repository;

import com.agency.web.agency.entities.Agents;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AgentsRepository extends CrudRepository<Agents, Long> {

    public Optional<Agents> findByName(String name);

    public List<Agents> findAllByFlag(String flag);

    public Optional<Agents> findByAgentCode(String agentCode);

    public Optional<Agents> findByAgentCodeAndFlag(String agentCode, String flag);

    public Optional<Agents> findByPhone(String phone);

}
