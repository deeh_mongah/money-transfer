package com.agency.web.agency;

import com.agency.web.agency.entities.Channels;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface ChannelsServiceInterface {

    /**
     * Fetch all persisted records
     *
     * @return List<Channels>
     */
    public List<Channels> fetchRecords(HttpServletRequest request);
}
