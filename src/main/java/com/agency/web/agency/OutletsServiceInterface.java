package com.agency.web.agency;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.agency.entities.Outlets;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

public interface OutletsServiceInterface extends BaseServiceInterface {

    /**
     * Fetch all records
     *
     * @param request
     * @return List<Outlets>
     */
    public List<Outlets> fetchRecords(HttpServletRequest request);

    public ArrayList<String> fetchAccounts(String agentNo);

    public ArrayList<String> fetchDemos(String agentNo);

}
