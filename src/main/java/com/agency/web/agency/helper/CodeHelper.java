package com.agency.web.agency.helper;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Component
public class CodeHelper {

    /**
     * Generate random digits for general purposes
     *
     * @param length
     * @return String
     */
    public static String randomNumber(int length) {
        Random random = new Random();
        char[] digits = new char[length];
        digits[0] = (char) (random.nextInt(9) + '1');
        for (int i = 1; i < length; i++) {
            digits[i] = (char) (random.nextInt(10) + '0');
        }
        return new String(digits);
    }

    /**
     * Generate the unique number code using for the specified column
     * Cab be used as a verification token
     *
     * @return  String
     */
//    public String getNewUserId() {
//        String code = randomNumber(4 );
//        boolean loop = true;
//
//        while (loop) {
//
//            Optional<County> oTerminalOfficer = entityRepository.findByUserCode( code );
//
//            // If the record has not been found, return the code
//            if( oTerminalOfficer.isPresent() ) code = randomNumber( 4 );
//            else loop = false;
//        }
//
//        // Return the code
//        return code;
//    }
}
