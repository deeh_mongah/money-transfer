package com.agency.web.agency.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.ChannelsServiceInterface;
import com.agency.web.agency.FeeGroupServiceInterface;
import com.agency.web.agency.FeeOriginServiceInterface;
import com.agency.web.agency.FeesServiceInterface;
import com.agency.web.configs.ReasonCodeServiceInterface;
import com.agency.web.transactions.TransactionTypesServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class FeesController {

    @Autowired private FeesServiceInterface entityService;
    @Autowired private DatatablesInterface dataTable;
    @Autowired private ReasonCodeServiceInterface reasonCodeService;

    @Autowired private FeeGroupServiceInterface feeGroupService;
    @Autowired private FeeOriginServiceInterface feeOriginService;
    @Autowired private TransactionTypesServiceInterface transactionTypesService;
    @Autowired private ChannelsServiceInterface channelsService;

    @RequestMapping( value = "/fees")
    @PreAuthorize("hasRole('ROLE_FEES')")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("agency-manager/fees");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

        }

        return view
                .addAttribute("transactionTypes", transactionTypesService.fetchRecords( request ) )
                .addAttribute("feeOrigin", feeOriginService.fetchRecords( request ) )
                .addAttribute("feeGroups", feeGroupService.fetchRecords( request ) )
                .addAttribute("reasoncodes", reasonCodeService.fetchRecords() )
                .addAttribute("channels", channelsService.fetchRecords( request ) )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

            else if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.entityService.fetchDeactivationInfo( request );

                //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.entityService.deactivateRecord( request );

                //When toggling entity status
            else if("decline-deactivation".equals( action ) || "approve-deactivation".equals( action )
                    || "activate".equals( action ) || "approve-new".equals( action )
                    || "delete".equals( action ) || "decline-new".equals( action ) )
                map = this.entityService.flagRecords( request );

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        dataTable
                .select("a.name, a.minAmount, a.maxAmount, b.name, str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'), str(a.updatedOn; 'YYYY-MM-DD HH24:MI:SS'), a.id")
                .from("Fees a LEFT JOIN a.feeGroupLink b")
                .where("a.flag = :flag ")
                .setParameter("flag", state );

        if( UserTypes.BANK.equals( parentType ) ){
            dataTable
                    .where("b.bankNo = :parentNo")
                    .setParameter("parentNo", parentNo);
        }

        return view.sendJSON( dataTable.showTable() );
    }
}
