package com.agency.web.agency.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.FeeGroupServiceInterface;
import com.agency.web.agency.OutletsServiceInterface;
import com.agency.web.banks.BanksServiceInterface;
import com.agency.web.configs.ReasonCodeServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class FeeGroupsController {

    @Autowired private FeeGroupServiceInterface entityService;
    @Autowired private BanksServiceInterface banksService;

    @Autowired private DatatablesInterface dataTable;
    @Autowired private ReasonCodeServiceInterface reasonCodeService;
    @Autowired private OutletsServiceInterface outletsServiceInterface;

    @RequestMapping( value = "/fee-groups")
    @PreAuthorize("hasRole('ROLE_FEE_GROUPS')")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("agency-manager/fee-groups");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( UserTypes.BANK.equals( parentType ) ){
            view = new View("agency-manager/fee-groups-bank-view");
        }

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

        }

        return view
                .addAttribute("outlets", outletsServiceInterface.fetchRecords( request ) )
                .addAttribute("reasoncodes", reasonCodeService.fetchRecords() )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

            else if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.entityService.fetchDeactivationInfo( request );

                //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.entityService.deactivateRecord( request );

                //When toggling entity status
            else if("decline-deactivation".equals( action ) || "approve-deactivation".equals( action )
                    || "activate".equals( action ) || "approve-new".equals( action )
                    || "delete".equals( action ) || "decline-new".equals( action ) )
                map = this.entityService.flagRecords( request );

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( null == parentType ) {
            dataTable
                    .select("a.name,  str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'), str(a.updatedOn; 'YYYY-MM-DD HH24:MI:SS'), a.id")
                    .from("FeeGroups a");
        }
        else if( UserTypes.BANK.equals( parentType ) ){
            dataTable
                    .select("a.name, str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'), str(a.updatedOn; 'YYYY-MM-DD HH24:MI:SS'), a.id")
                    .from("FeeGroups a ")
                    .where("a.bankNo = :parentNo")
                    .setParameter("parentNo", parentNo)
            ;
        }

        dataTable
                .where("a.flag = :flag ")
                .setParameter("flag", state);

        return view.sendJSON( dataTable.showTable() );
    }
}
