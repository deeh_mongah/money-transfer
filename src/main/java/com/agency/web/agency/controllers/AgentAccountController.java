package com.agency.web.agency.controllers;

import com.agency.web.agency.OutletsServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by Ben on 20-Aug-19.
 */
@RestController
@RequestMapping( "/agent")
public class AgentAccountController {

    @Autowired
    private OutletsServiceInterface outletsServiceInterface;

    @GetMapping("/commission/{agentNo}")
    public List<String> chart(@PathVariable("agentNo")String agentNo) {

//        int test = transactionServicesRepo.fetchTransactionServices().size();
//        System.err.println("testThree"+test);

        return outletsServiceInterface.fetchAccounts(agentNo);
    }

    @GetMapping("/amounts/{accountNo}")
    public List<String> amounts(@PathVariable("accountNo")String agentNo) {

//        int test = transactionServicesRepo.fetchTransactionServices().size();
//        System.err.println("testThree"+test);

        return outletsServiceInterface.fetchDemos(agentNo);
    }
}
