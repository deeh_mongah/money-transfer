package com.agency.web.agency.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.AgentsServiceInterface;
import com.agency.web.banks.BanksServiceInterface;
import com.agency.web.banks.BranchesServiceInterface;
import com.agency.web.configs.ReasonCodeServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

@Controller
public class AgentsController implements Serializable {

    @Autowired private BanksServiceInterface banksService;
    @Autowired private AgentsServiceInterface entityService;
    @Autowired private DatatablesInterface dataTable;
    @Autowired private ReasonCodeServiceInterface reasonCodeService;
    @Autowired private BranchesServiceInterface branchesServiceInterface;

    @RequestMapping( value = "/agents")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("agency-manager/agents");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( UserTypes.BANK.equals( parentType ) ){
            view = new View("agency-manager/agents-bank-view");
        }

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

        }

        return view
                .addAttribute("banks", banksService.fetchRecords( request ) )
                .addAttribute("branches", branchesServiceInterface.fetchRecords( request ) )
                .addAttribute("reasoncodes", reasonCodeService.fetchRecords() )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

            else if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.entityService.fetchDeactivationInfo( request );

                //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.entityService.deactivateRecord( request );

                //When toggling entity status
            else if("decline-deactivation".equals( action ) || "approve-deactivation".equals( action )
                    || "activate".equals( action ) || "approve-new".equals( action )
                    || "delete".equals( action ) || "decline-new".equals( action ) )
                map = this.entityService.flagRecords( request );

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( null == parentType ) {
            dataTable
                    .select("a.name, a.email, a.phone,a.agentCode, a.location ,str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'),a.id")
                    .from("Agents a");
        }

        else if( UserTypes.BANK.equals( parentType ) ){
            dataTable
                    .select("a.name, a.email, a.phone,a.location,a.agentCode,a.location, str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'), a.id")
                    .from("Agents a LEFT JOIN a.bankLink b ")
                    .where("b.id = :parentNo")
                    .setParameter("parentNo", parentNo);
        }

        dataTable
                .where("a.flag = :flag ")
                .setParameter("flag", state);

        return view.sendJSON( dataTable.showTable() );
    }
}
