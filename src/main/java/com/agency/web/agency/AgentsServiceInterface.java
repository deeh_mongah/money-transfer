package com.agency.web.agency;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.agency.entities.Agents;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface AgentsServiceInterface extends BaseServiceInterface {

    /**
     * Fetch all persisted records
     *
     * @return List<Agents>
     */
    public List<Agents> fetchRecords(HttpServletRequest request);

}
