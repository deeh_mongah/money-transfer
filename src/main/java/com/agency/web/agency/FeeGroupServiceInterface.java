package com.agency.web.agency;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.agency.entities.FeeGroups;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface FeeGroupServiceInterface extends BaseServiceInterface{


    /**
     * Fetch all persisted records
     *
     * @return List<FeeGroups>
     */
    public List<FeeGroups> fetchRecords(HttpServletRequest request);
}
