package com.agency.web.agency.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.agency.entities.Outlets;
import org.springframework.stereotype.Component;

@Component
public class OutletsForm extends Form<Outlets> {

    public OutletsForm(){
        setMapping( Outlets.class );
        setRole("ROLE_OUTLETS", "Outlets", "superadmin");
    }
}
