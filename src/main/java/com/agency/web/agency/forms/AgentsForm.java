package com.agency.web.agency.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.agency.entities.Agents;
import org.springframework.stereotype.Component;

@Component
public class AgentsForm extends Form<Agents> {

    public AgentsForm(){
        setMapping( Agents.class );
        setRole("ROLE_AGENTS", "Agents", "superadmin");
    }
}
