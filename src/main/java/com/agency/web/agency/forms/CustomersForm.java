package com.agency.web.agency.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.agency.entities.Customers;
import com.agency.web.agency.entities.Outlets;
import org.springframework.stereotype.Component;

@Component
public class CustomersForm extends Form<Customers> {

    public CustomersForm(){
        setMapping( Customers.class );
        setRole("ROLE_OUTLETS", "Customers", "superadmin");
    }
}
