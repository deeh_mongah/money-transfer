package com.agency.web.agency.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.agency.entities.FeeGroups;
import org.springframework.stereotype.Component;

@Component
public class FeeGroupForm extends Form<FeeGroups> {

    public FeeGroupForm(){
        setMapping( FeeGroups.class );
        setRole("ROLE_FEE_GROUPS", "FeeGroups", "superadmin");
    }
}
