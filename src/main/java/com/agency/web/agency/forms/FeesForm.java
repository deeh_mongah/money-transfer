package com.agency.web.agency.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.agency.entities.Fees;
import org.springframework.stereotype.Component;

@Component
public class FeesForm extends Form<Fees> {

    public FeesForm(){
        setMapping( Fees.class );
        setRole("ROLE_FEES", "Fees", "superadmin");
    }
}
