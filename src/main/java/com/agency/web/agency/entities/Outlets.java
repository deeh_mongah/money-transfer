/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.web.agency.entities;

import com.agency.core.template.forms.EditDataWrapper;
import com.agency.core.template.forms.MutableField;
import com.agency.web.banks.entities.Branches;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "outlets")
public class Outlets implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @MutableField( name = "Name ")
    @Column(name = "NAME", length = 100)
    private String name;

    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 200)
    @MutableField( name = "Email ")
    @Column(name = "EMAIL", length = 200)
    private String email;

    @Size(max = 200)
    @MutableField( name = "Location ")
    @Column(name = "LOCATION", length = 200)
    private String location;

    @Size(max = 20)
    @MutableField( name = "Mobile No ")
    @Column(name = "MOBILE_NO", length = 20)
    private String mobileNo;

    @Size(max = 100)
    @MutableField( name = "Float Account")
    @Column(name = "float_account", length = 100)
    private String floatAccount;

    @Size(max = 100)
    @MutableField( name = "Commission Account")
    @Column(name = "commission_account", length = 100)
    private String commissionAccount;

    @MutableField( name = "Fee Group", entity = "FeeGroups", reference = "name")
    @Column(name = "fee_group_no")
    private Long feeGroupNo;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Lob
    @EditDataWrapper
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Column(name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @MutableField( name = "Bank Branch", entity = "Branches", reference = "name")
    @Column(name = "BRANCH_NO")
    private Long branchNo;

    @MutableField( name = "Agent", entity = "Agents", reference = "name")
    @Column(name = "agent_no")
    private Long agentNo;

    @Column(name = "amount")
    private BigDecimal amount;

    @JoinColumn(name = "agent_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Agents agentLink;

    @JoinColumn(name = "REASON_CODE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonCodeLink;

    @JoinColumn(name = "BRANCH_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Branches branchLink;


    @JoinColumn(name = "fee_group_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private FeeGroups feeGroupsLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;

    public Outlets() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Outlets setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }

    public Long getFeeGroupNo() {
        return feeGroupNo;
    }

    public void setFeeGroupNo(Long feeGroupNo) {
        this.feeGroupNo = feeGroupNo;
    }

    @JsonIgnore
    public FeeGroups getFeeGroupsLink() {
        return feeGroupsLink;
    }

    public void setFeeGroupsLink(FeeGroups feeGroupsLink) {
        this.feeGroupsLink = feeGroupsLink;
    }

    public String getFloatAccount() {
        return floatAccount;
    }

    public void setFloatAccount(String floatAccount) {
        this.floatAccount = floatAccount;
    }

    public String getCommissionAccount() {
        return commissionAccount;
    }

    public void setCommissionAccount(String commissionAccount) {
        this.commissionAccount = commissionAccount;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFlag() {   return flag;  }
    public Outlets setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public Long getBranchNo() {
        return branchNo;
    }

    public void setBranchNo(Long branchNo) {
        this.branchNo = branchNo;
    }

    public Long getAgentNo() {
        return agentNo;
    }

    public void setAgentNo(Long agentNo) {
        this.agentNo = agentNo;
    }

    @JsonIgnore public Agents getAgentLink() { return agentLink; }
    @JsonIgnore public ReasonCodes getReasonCodeLink() { return reasonCodeLink; }
    @JsonIgnore public Branches getBranchLink() { return branchLink; }
    @JsonIgnore public Users getCreatedByLink() {  return createdByLink; }
    @JsonIgnore public Users getUpdatedByLink() {  return updatedByLink; }

    public Outlets createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Outlets updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Outlets)) {
            return false;
        }
        Outlets other = (Outlets) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Outlets[ id=" + id + " ]";
    }
    
}
