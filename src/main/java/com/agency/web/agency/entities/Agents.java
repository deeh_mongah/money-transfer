package com.agency.web.agency.entities;

import com.agency.core.template.forms.EditDataWrapper;
import com.agency.core.template.forms.MutableField;
import com.agency.web.banks.entities.Banks;
import com.agency.web.banks.entities.Branches;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.security.core.userdetails.User;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table( name = "agents")
public class Agents implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @MutableField( name = "Name")
    @Column( name = "name", length = 100)
    private String name;

    @MutableField( name = "Business ID")
    @Column( name = "business_id", length = 100)
    private String businessId;

    @MutableField( name = "Email")
    @Column( name = "email", length = 100)
    private String email;

    @MutableField( name = "Mobile No")
    @Column( name = "phone", length = 100)
    private String phone;

    @MutableField( name = "Location")
    @Column( name = "location", length = 100)
    private String location;

//    @MutableField( name = "Agent Code")
    @Column( name = "agent_code", length = 100)
    private String agentCode;

    @Column( name = "float_account", length = 100)
    private String floatAccount;

    @Column( name = "commission_account", length = 100)
    private String commissionAccount;

    @Column( name = "bank_no")
    private Long bankNo;

    @Column( name = "branches_no")
    private Long branchesNo;

    @Column( name = "pin", length = 100)
    private String pin;

    @Size(max = 250)
    @MutableField( name = "Phone Code")
    @Column(name = "phone_code", length = 250)
    private String phoneCode;


    @Column(name = "flag")
    private String flag = "0";

    @Column(name = "created_by")
    private Long createdBy;

    @Column(name = "transaction_amount")
    private BigDecimal transactionAmount;

    @Column(name = "updated_by")
    private Long updatedBy;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "updated_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "deleted_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Lob
    @EditDataWrapper
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Column( name = "reason_code_no")
    private Long reasonCodeNo;

    @Size(max = 200)
    @Column(name = "reason_description", length = 200)
    private String reasonDescription;

    @JoinColumn(name = "bank_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Banks bankLink;

    @JoinColumn(name = "branches_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Branches branchesLink;

    @JoinColumn(name = "reason_code_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonCodeLink;

    @JoinColumn(name = "created_by", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    @JoinColumn(name = "updated_by", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;



    public Agents(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBusinessId() {
        return businessId;
    }

    public void setBusinessId(String businessId) {
        this.businessId = businessId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public Agents setPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public Agents setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
        return this;
    }

    public String getFloatAccount() {
        return floatAccount;
    }

    public void setFloatAccount(String floatAccount) {
        this.floatAccount = floatAccount;
    }

    public String getCommissionAccount() {
        return commissionAccount;
    }

    public void setCommissionAccount(String commissionAccount) {
        this.commissionAccount = commissionAccount;
    }

    public Long getBankNo() {
        return bankNo;
    }

    public void setBankNo(Long bankNo) {
        this.bankNo = bankNo;
    }

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    public String getFlag() {
        return flag;
    }

    public Agents setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getBranchesNo() {
        return branchesNo;
    }

    public void setBranchesNo(Long branchesNo) {
        this.branchesNo = branchesNo;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }



    public void setBranchesLink(Branches branchesLink) {
        this.branchesLink = branchesLink;
    }

    @JsonIgnore public Branches getBranchesLink() {
        return branchesLink;
    }
    @JsonIgnore public Banks getBankLink() { return bankLink;  }
    @JsonIgnore public ReasonCodes getReasonCodeLink() { return reasonCodeLink;  }
    @JsonIgnore public Users getCreatedByLink() {  return createdByLink; }
    @JsonIgnore public Users getUpdatedByLink() {  return updatedByLink; }

    public Agents createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Agents updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }

    @Override
    public String toString() {
        return "Agents{" + "id=" + id + '}';
    }
}
