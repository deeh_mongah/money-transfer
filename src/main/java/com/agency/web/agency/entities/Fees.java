package com.agency.web.agency.entities;

import com.agency.core.template.forms.EditDataWrapper;
import com.agency.core.template.forms.MutableField;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.transactions.entities.TransactionTypes;
import com.agency.web.usermanager.entities.Users;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "fees")
public class Fees implements Serializable {

    public static final Long DEPOSIT = Long.valueOf(1);
    public static final Long WITHDRAW = Long.valueOf(2);
    public static final Long BALANCE_INQUERY = Long.valueOf(3);
    public static final Long MINI_STATEMENT = Long.valueOf(4);
    public static final Long INTERNAL_FUNDS = Long.valueOf(5);
    public static final Long FIXED = Long.valueOf(1);
    public static final String PERCENTAGE_MODE = "2";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 100)
    @MutableField( name = "Name")
    @Column(name = "NAME", length = 100)
    private String name;

    @MutableField( name = "Min Amount")
    @Column(name = "MIN_AMOUNT", precision = 18, scale = 2)
    private BigDecimal minAmount;

    @MutableField( name = "Max Amount")
    @Column(name = "MAX_AMOUNT", precision = 18, scale = 2)
    private BigDecimal maxAmount;

    @MutableField( name = "Amount")
    @Column(name = "transactional_amount", precision = 18, scale = 2)
    private BigDecimal transactionAmount;

    @MutableField( name = "Fee Group", entity = "FeeGroups", reference = "name")
    @Column(name = "FEE_GROUP_NO")
    private Long feeGroupNo;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;

    @Column(name = "DELETED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date deletedOn;

    @Column(name = "CREATED_BY")
    private Long createdBy;

    @Column(name = "UPDATED_BY")
    private Long updatedBy;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Size(max = 200)
    @Column(name = "REASON_DESCRIPTION", length = 200)
    private String reasonDescription;

    @Lob
    @EditDataWrapper
    @Size(max = 2147483647)
    @Column(name = "EDIT_DATA", length = 2147483647)
    private String editData;

    @Column( name = "REASON_CODE_NO")
    private Long reasonCodeNo;

    @MutableField( name = "Transaction Type", entity = "TransactionTypes", reference = "name")
    @Column(name = "TRANSACTION_TYPE_NO")
    private Long transactionTypeNo;

    @MutableField( name = "Fee Origin", entity = "FeeOrigin", reference = "name")
    @Column(name = "FEE_ORIGIN_NO")
    private Long feeOriginNo;

    @MutableField( name = "Agent Amount")
    @Column(name = "AGENT_AMOUNT", precision = 18, scale = 2)
    private BigDecimal agentAmount;

    @MutableField( name = "Flex Amount")
    @Column(name = "FLEX_AMOUNT", precision = 18, scale = 2)
    private BigDecimal flexAmount;

    @MutableField( name = "Binary Amount")
    @Column(name = "BINARY_AMOUNT", precision = 18, scale = 2)
    private BigDecimal binaryAmount;
//
//    @MutableField( name = "Outlet Ratio")
//    @Column(name = "OUTLET_RATIO", precision = 18, scale = 2)
//    private BigDecimal outletRatio;
//
//    @MutableField( name = "Bank Ratio")
//    @Column(name = "SACCO_RATIO", precision = 18, scale = 2)
//    private BigDecimal saccoRatio;
//
////    @MutableField( name = "Binary Ratio")
//    @Column(name = "binary_ratio", precision = 18, scale = 2)
//    private BigDecimal binaryRatio;

    @Size(max = 100)
    @MutableField( name = "Payment Mode")
    @Column(name = "PAYMENT_MODE", length = 100)
    private String paymentMode;

    @JoinColumn(name = "REASON_CODE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private ReasonCodes reasonCodeLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "CREATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users createdByLink;

    //DISCLAIMER: RELATION NOT ENFORCED IN THE DB
    @JoinColumn(name = "UPDATED_BY", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Users updatedByLink;

    @JoinColumn(name = "TRANSACTION_TYPE_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private TransactionTypes transactionTypeLink;

    @JoinColumn(name = "FEE_ORIGIN_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private FeeOrigin feeOriginLink;

    @JoinColumn(name = "FEE_GROUP_NO", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private FeeGroups feeGroupLink;

    public Fees() { }


    public BigDecimal getAgentAmount() {
        return agentAmount;
    }

    public void setAgentAmount(BigDecimal agentAmount) {
        this.agentAmount = agentAmount;
    }

    public BigDecimal getFlexAmount() {
        return flexAmount;
    }

    public void setFlexAmount(BigDecimal flexAmount) {
        this.flexAmount = flexAmount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(BigDecimal minAmount) {
        this.minAmount = minAmount;
    }

    public BigDecimal getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Long getFeeGroupNo() {
        return feeGroupNo;
    }

    public void setFeeGroupNo(Long feeGroupNo) {
        this.feeGroupNo = feeGroupNo;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public BigDecimal getTransactionAmount() {
        return transactionAmount;
    }

    public void setTransactionAmount(BigDecimal transactionAmount) {
        this.transactionAmount = transactionAmount;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public Date getDeletedOn() {
        return deletedOn;
    }

    public void setDeletedOn(Date deletedOn) {
        this.deletedOn = deletedOn;
    }

    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getFlag() {  return flag;  }
    public Fees setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getReasonDescription() {
        return reasonDescription;
    }

    public void setReasonDescription(String reasonDescription) {
        this.reasonDescription = reasonDescription;
    }

    public String getEditData() {
        return editData;
    }

    public void setEditData(String editData) {
        this.editData = editData;
    }

    public Long getReasonCodeNo() {
        return reasonCodeNo;
    }

    public void setReasonCodeNo(Long reasonCodeNo) {
        this.reasonCodeNo = reasonCodeNo;
    }

    public Long getTransactionTypeNo() {
        return transactionTypeNo;
    }

    public void setTransactionTypeNo(Long transactionTypeNo) {
        this.transactionTypeNo = transactionTypeNo;
    }

    public Long getFeeOriginNo() {
        return feeOriginNo;
    }

    public void setFeeOriginNo(Long feeOriginNo) {
        this.feeOriginNo = feeOriginNo;
    }


    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public void setReasonCodeLink(ReasonCodes reasonCodeLink) {
        this.reasonCodeLink = reasonCodeLink;
    }

    public void setCreatedByLink(Users createdByLink) {
        this.createdByLink = createdByLink;
    }

    public void setUpdatedByLink(Users updatedByLink) {
        this.updatedByLink = updatedByLink;
    }

    public void setTransactionTypeLink(TransactionTypes transactionTypeLink) {
        this.transactionTypeLink = transactionTypeLink;
    }

    public void setFeeOriginLink(FeeOrigin feeOriginLink) {
        this.feeOriginLink = feeOriginLink;
    }

    public void setFeeGroupLink(FeeGroups feeGroupLink) {
        this.feeGroupLink = feeGroupLink;
    }

    public BigDecimal getBinaryAmount() {
        return binaryAmount;
    }

    public void setBinaryAmount(BigDecimal binaryAmount) {
        this.binaryAmount = binaryAmount;
    }

    @JsonIgnore public FeeGroups getFeeGroupLink() {   return feeGroupLink; }
    @JsonIgnore public TransactionTypes getTransactionTypeLink() {  return transactionTypeLink;  }
    @JsonIgnore public FeeOrigin getFeeOriginLink() {  return feeOriginLink; }
    @JsonIgnore public ReasonCodes getReasonCodeLink() {  return reasonCodeLink; }
    @JsonIgnore public Users getCreatedByLink() {  return createdByLink; }
    @JsonIgnore public Users getUpdatedByLink() {  return updatedByLink; }

    public Fees createdOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.createdOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        this.createdBy = userId;
        return this;
    }

    public Fees updatedOn(Long userId){
        this.updatedOn = new Date( System.currentTimeMillis() );
        this.updatedBy = userId;
        return this;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Fees)) {
            return false;
        }
        Fees other = (Fees) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Fees[ id=" + id + " ]";
    }
    
}
