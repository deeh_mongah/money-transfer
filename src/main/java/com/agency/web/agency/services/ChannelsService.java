package com.agency.web.agency.services;

import com.agency.web.agency.ChannelsServiceInterface;
import com.agency.web.agency.entities.Channels;
import com.agency.web.agency.repository.ChannelsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Repository
@Transactional
public class ChannelsService implements ChannelsServiceInterface {

    @Autowired private ChannelsRepository entityRepository;

    /**
     * Fetch all persisted records
     *
     * @return List<Channels>
     */
    @Override
    public List<Channels> fetchRecords(HttpServletRequest request){
        return (List)entityRepository.findAll();
    }
}
