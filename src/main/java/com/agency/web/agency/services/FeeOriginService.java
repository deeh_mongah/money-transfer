package com.agency.web.agency.services;

import com.agency.web.agency.FeeOriginServiceInterface;
import com.agency.web.agency.entities.FeeOrigin;
import com.agency.web.agency.repository.FeeOriginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
@Transactional
public class FeeOriginService implements FeeOriginServiceInterface {

    @Autowired
    private FeeOriginRepository entityRepository;
    /**
     * Fetch all persisted records
     *
     * @return List<FeeGroups>
     */
    @Override
    public List<FeeOrigin> fetchRecords(HttpServletRequest request){
        return (List)entityRepository.findAll();
    }
}
