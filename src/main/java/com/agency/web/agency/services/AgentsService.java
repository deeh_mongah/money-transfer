package com.agency.web.agency.services;

import com.agency.core.sms.SmsOptions;
import com.agency.core.sms.SmsService;
import com.agency.core.template.AppConstants;
import com.agency.core.template.PhoneNumberFormatter;
import com.agency.core.template.forms.AuditData;
import com.agency.web.agency.AgentsServiceInterface;
import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.forms.AgentsForm;
import com.agency.web.agency.repository.AgentsRepository;
import com.agency.web.agency.repository.OutletsRepository;
import com.agency.web.banks.entities.Branches;
import com.agency.web.banks.repositories.BranchesRepository;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.terminals.services.TerminalOfficerHelper;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.usermanager.services.UserService;
import com.agency.web.wallets.entity.CurrencyTypeWallets;
import com.agency.web.wallets.entity.OutletWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.entity.WalletsTransactions;
import com.agency.web.wallets.repository.CurrencyTypeWalletsRepository;
import com.agency.web.wallets.repository.OutletWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.agency.web.agency.helper.CodeHelper.randomNumber;

@Service
@Transactional
public class AgentsService implements AgentsServiceInterface {

    @Autowired private AgentsForm entityForm;
    @Autowired private AgentsRepository entityRepository;
    @Autowired private AuditTrailRepository auditTrailRepository;
    @Autowired private WalletsRepository walletsRepository;
    @Autowired private OutletWalletsRepository outletWalletsRepository;
    @Autowired private UserService userService;
    @Autowired private CurrencyTypeWalletsRepository currencyTypeWalletsRepository;
    @Autowired private SmsService smsService;

    /**
     * Fetch all persisted records
     *
     * @return List<Agents>
     */
    @Override
    public List<Agents> fetchRecords(HttpServletRequest request){
        return entityRepository.findAllByFlag( AppConstants.STATUS_ACTIVERECORD );
    }

    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");



        Agents entity = entityForm.handleRequests( request );



        String pin = TerminalOfficerHelper.randomNumber( 4 );


        String phoneCode = request.getParameter("phoneCode");
        String phone = request.getParameter("phone");



        boolean valid = userService.validatePhoneNumber(phone,phoneCode);


        if (!valid){

            map.put("status", "01");
            map.put("message", "Invalid Phone Number");
            return map;
        }

        String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phone);

        Optional<Agents> oAgents = entityRepository.findByPhone(formattedNumber);

        if (oAgents.isPresent()){

            map.put("status", "01");
            map.put("message", "Enter Unique Phone Number");
            return map;
        }



        entity
                .createdOn( userId )
                .setPhone(formattedNumber)
                .setPhoneCode(phoneCode)

                .setFlag( AppConstants.STATUS_NEWRECORD );





       entity.setAgentCode(getNewUserId());
       entity.setPin(pin);

        /*Save record*/
        entityRepository.save( entity );



        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new agent: " + entity.getName() )
                .setNewValues( entity.getName() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }




    private String getNewUserId() {
        String code = randomNumber(4 );
        boolean loop = true;

        while (loop) {

            Optional<Agents> oAgents = entityRepository.findByAgentCode( code );

            // If the record has not been found, return the code
            if( oAgents.isPresent() ) code = randomNumber( 4 );
            else loop = false;
        }

        // Return the code
        return code;
    }

    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        boolean isModified = entityForm.handleEditRequest(request);
        Agents record = entityForm.getEntity();

        //If record has changes
        if( isModified ){

            String phoneCode = request.getParameter("phoneCode");
            String phone = request.getParameter("phone");



            boolean valid = userService.validatePhoneNumber(phone,phoneCode);


            if (!valid){

                map.put("status", "01");
                map.put("message", "Invalid Phone Number");
                return map;
            }

            String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phone);

            record
                    .setPhoneCode(phoneCode)
                    .setPhone(formattedNumber)


                    .setFlag( AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn( userNo );
            //Persist record
            entityRepository.save( record );

            //Generate log
            AuditData auditData = entityForm.auditData();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Edited agent - %s : %s", record.getName(), auditData.getDescription() ) )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
                    .setUserNo( userNo );

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            //Generate log
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Attempt to update agent - %s : no changes made", record.getName())  )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed")
                    .setUserNo( userNo );

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save( log );
        return map;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");

        Agents record = entityRepository.findById( index ).get();
        boolean proceed = entityForm.applyMakerChecker(record, action);

        map.put("message", entityForm.getResponse());
        if ( proceed ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        }
        else  map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getName() );
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.transformEntity(index);
    }

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.fetchChanges( index );
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords( HttpServletRequest request ){
        Map<String, Object> map = new HashMap<>();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        Agents record = entityRepository.findById( index ).get();

        boolean success = entityForm.applyMakerChecker(record, action);
        map.put("message", entityForm.getResponse());

        if ( success ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");

            if (action.equals("approve-new")){

                List<CurrencyTypeWallets> currencyTypeWalletsList = (List<CurrencyTypeWallets>) currencyTypeWalletsRepository.findAll();


                for (CurrencyTypeWallets node: currencyTypeWalletsList) {

                    Wallets wallets = new Wallets();
                    wallets.setAmount(BigDecimal.ZERO);
                    wallets.setCurrencyNo(node.getId());

                    walletsRepository.save(wallets);


                    OutletWallets outletWallets = new OutletWallets();
                    outletWallets.setWalletNo(wallets.getId());
                    outletWallets.setOutletNo(index);

                    outletWalletsRepository.save(outletWallets);


                }

                String message = "You have successfully been registered with Flex Money. Your new agent code is "+record.getAgentCode()+" and PIN is "+record.getPin()+".";
                smsService.sendSMS(new SmsOptions()
                        .setMessage(message)
                        .setMobileNo( record.getPhone()  )
                );


            }
        }
        else map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getName());
        log.setActivity( logActivity );
        auditTrailRepository.save(log);



        return map;
    }

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Agents record = entityForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        entityRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated an agent successfully. Reference %s", record.getName() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        Agents record = entityRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedByLink().getFullNames() );

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
}
