package com.agency.web.agency.services;

import com.agency.core.template.AppConstants;
import com.agency.core.template.forms.AuditData;
import com.agency.web.agency.OutletsServiceInterface;
import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.entities.Outlets;
import com.agency.web.agency.forms.OutletsForm;
import com.agency.web.agency.repository.AgentsRepository;
import com.agency.web.agency.repository.OutletsRepository;
import com.agency.web.banks.entities.Branches;
import com.agency.web.banks.repositories.BranchesRepository;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.wallets.entity.AgentsAccount;
import com.agency.web.wallets.entity.OutletWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.AgentsAccountRepository;
import com.agency.web.wallets.repository.OutletWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.*;

@Service
@Transactional
public class OutletsService implements OutletsServiceInterface{

    @Autowired private OutletsForm entityForm;
    @Autowired private OutletsRepository entityRepository;
    @Autowired private AuditTrailRepository auditTrailRepository;
    @Autowired private AgentsRepository agentsRepository;
    @Autowired private WalletsRepository walletsRepository;
    @Autowired private OutletWalletsRepository outletWalletsRepository;
    @Autowired private AgentsAccountRepository agentsAccountRepository;
    @Autowired private BranchesRepository branchesRepository;


    /**
     * Fetch all records
     *
     * @param request
     * @return List<Outlets>
     */
    @Override
    public List<Outlets> fetchRecords(HttpServletRequest request){
        List<Outlets> data = new ArrayList<>();
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if(UserTypes.BANK.equals( parentType ) ){
            data = entityRepository.findAllByFlagAndAgentLink_BankNo( AppConstants.STATUS_ACTIVERECORD, parentNo);
        }
        else data = entityRepository.findAllByFlag( AppConstants.STATUS_ACTIVERECORD );

        return data;
    }

    @Override
    public ArrayList<String> fetchAccounts(String agentNo) {

        System.err.println("agentNo"+agentNo);

        ArrayList<String> accounts = new ArrayList<>();

        Optional<Agents> oAgents = agentsRepository.findById(Long.valueOf(agentNo));

        Agents agents = oAgents.get();

        Branches branches = agents.getBranchesLink();



        accounts.add(0,agents.getFloatAccount());
        accounts.add(1, agents.getCommissionAccount());
        accounts.add(2, branches.getName());

        System.err.println("accountNumber"+accounts);

        return accounts;
    }

    @Override
    public ArrayList<String> fetchDemos(String agentNo) {

        Optional<AgentsAccount> oAgentsAccount = agentsAccountRepository.findByAccountNo(agentNo);

        AgentsAccount agentsAccount = oAgentsAccount.get();

        Optional<Branches> oBranches = branchesRepository.findById(Long.valueOf(agentsAccount.getBranchNo()));

        Branches branches = oBranches.get();


        ArrayList<String> accounts = new ArrayList<>();

        accounts.add(0, agentsAccount.getEmail());
        accounts.add(1, agentsAccount.getName());
        accounts.add(2, agentsAccount.getPhone());
        accounts.add(3, agentsAccount.getCommissionAccount());
        accounts.add(4, agentsAccount.getBussinessId());
        accounts.add(5, branches.getName());


        return accounts;
    }

    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        Outlets entity = entityForm.handleRequests( request );

        if ( entityRepository.findByName( entity.getName() ).isPresent() ) {
            map.put("status", "01");
            map.put("message",  "Name already exists.");
            return map;
        }

        Optional<AgentsAccount> oAgentsAccount = agentsAccountRepository.findByAccountNo("1234567");

        AgentsAccount agentsAccount = oAgentsAccount.get();

        entity
                .createdOn( userId )
                .setAmount(agentsAccount.getBalance())
                .setFlag( AppConstants.STATUS_NEWRECORD );

        /*Save record*/
        entityRepository.save( entity );

        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new outlet : " + entity.getName() )
                .setNewValues( entity.getName() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }


    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        boolean isModified = entityForm.handleEditRequest(request);
        Outlets record = entityForm.getEntity();

        //If record has changes
        if( isModified ){

            record
                    .setFlag( AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn( userNo );
            //Persist record
            entityRepository.save( record );

            //Generate log
            AuditData auditData = entityForm.auditData();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Edited outlet - %s : %s", record.getName(), auditData.getDescription() ) )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
                    .setUserNo( userNo );

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            //Generate log
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Attempt to update outlet - %s : no changes made", record.getName())  )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed")
                    .setUserNo( userNo );

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save( log );
        return map;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");

        Outlets record = entityRepository.findById( index ).get();
        boolean proceed = entityForm.applyMakerChecker(record, action);

        map.put("message", entityForm.getResponse());
        if ( proceed ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        }
        else  map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getName() );
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        System.err.println("<><><>index<><><>"+index);
        return entityForm.transformEntity(index);
    }

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request){
        String index = request.getParameter("index");
        return entityForm.fetchChanges( index );
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords( HttpServletRequest request ){
        Map<String, Object> map = new HashMap<>();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Outlets record = entityRepository.findById( index ).get();

        boolean success = entityForm.applyMakerChecker(record, action);
        map.put("message", entityForm.getResponse());

        if ( success ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");

            if (action.equals("approve-new")){
                Wallets wallets = new Wallets();
                wallets.setAmount(BigDecimal.ZERO);
                walletsRepository.save(wallets);


                OutletWallets outletWallets = new OutletWallets();
                outletWallets.setOutletNo(record.getId());
                outletWallets.setWalletNo(wallets.getId());
                outletWalletsRepository.save(outletWallets);
            }
        }
        else map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getName());
        log.setActivity( logActivity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Outlets record = entityForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        entityRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated a outlet successfully. Reference %s", record.getName() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        Outlets record = entityRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedByLink().getFullNames() );

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
    
}
