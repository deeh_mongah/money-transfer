package com.agency.web.agency.services;

import com.agency.core.sms.SmsOptions;
import com.agency.core.sms.SmsService;
import com.agency.core.template.AppConstants;
import com.agency.core.template.PhoneNumberFormatter;
import com.agency.core.template.forms.AuditData;
import com.agency.web.agency.CustomerServiceInterface;
import com.agency.web.agency.entities.Customers;
import com.agency.web.agency.forms.CustomersForm;
import com.agency.web.agency.repository.CustomersRepository;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.usermanager.services.UserService;
import com.agency.web.wallets.entity.CustomersWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.CustomerWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by ben on 06/11/2019.
 */
@Transactional
@Service
public class CustomerService implements CustomerServiceInterface {


    @Autowired private CustomersForm entityForm;

    @Autowired private CustomersRepository entityRepository;
    @Autowired private AuditTrailRepository auditTrailRepository;

    @Autowired private WalletsRepository walletsRepository;
    @Autowired private CustomerWalletsRepository customerWalletsRepository;
    @Autowired private UserService userService;
    @Autowired private SmsService smsService;


    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");

        Customers entity = entityForm.handleRequests( request );

        String phoneCode = request.getParameter("phoneCode");
        String phone = request.getParameter("phone");




        String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phone);

        Optional<Customers> oCustomers1 = entityRepository.findByPhone(formattedNumber);

        if (oCustomers1.isPresent()){

            map.put("status", "01");
            map.put("message", "Enter Unique Phone Number");
            return map;
        }



        entity.setPhone(formattedNumber);
        entity.setPhoneCode(phoneCode);

        entity .createdOn(userId)
                .setFlag( AppConstants.STATUS_NEWRECORD );

        entityRepository.save(entity);

        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new customer: " + entity.getFirstName()+ " "+ entity.getSurname() )
                .setNewValues( entity.getFirstName()+ " "+ entity.getSurname() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

        auditTrailRepository.save( log );


        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }

    @Override
    public Map<String, Object> editRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        boolean isModified = entityForm.handleEditRequest(request);
        Customers record = entityForm.getEntity();

        //If record has changes
        if( isModified ){

            String phoneCode = request.getParameter("phoneCode");
            String phone = request.getParameter("phone");



            boolean valid = userService.validatePhoneNumber(phone,phoneCode);


            if (!valid){

                map.put("status", "01");
                map.put("message", "Invalid Phone Number");
                return map;
            }

            String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phone);

            record
                    .setPhoneCode(phoneCode)
                    .setPhone(formattedNumber)
                    .setFlag( AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn( userNo );
            //Persist record
            entityRepository.save( record );

            //Generate log
            AuditData auditData = entityForm.auditData();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Edited customer - %s : %s", record.getFirstName() +" " + record.getSurname(), auditData.getDescription() ) )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
                    .setUserNo( userNo );

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            //Generate log
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Attempt to update customer - %s : no changes made",  record.getFirstName() +" " + record.getSurname())  )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed")
                    .setUserNo( userNo );

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

        auditTrailRepository.save( log );
        return map;
    }

    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");

        Customers record = entityRepository.findById( index ).get();
        boolean proceed = entityForm.applyMakerChecker(record, action);

        map.put("message", entityForm.getResponse());
        if ( proceed ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");
        }
        else  map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getFirstName() +" " + record.getSurname() );
        log.setActivity( activity );
        auditTrailRepository.save(log);
        return map;
    }

    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request) {
        String index = request.getParameter("index");
        return entityForm.transformEntity(index);
    }

    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request) {
        String index = request.getParameter("index");
        return entityForm.fetchChanges( index );
    }

    @Override
    public Map<String, Object> flagRecords(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        Customers record = entityRepository.findById( index ).get();

        boolean success = entityForm.applyMakerChecker(record, action);
        map.put("message", entityForm.getResponse());

        if ( success ) {
            record = entityForm.getEntity();
            entityRepository.save( record );
            map.put("status", "00");

            if (action.equals("approve-new")){

                Wallets wallets = new Wallets();
                wallets.setAmount(BigDecimal.ZERO);

                walletsRepository.save(wallets);

                CustomersWallets customersWallets = new CustomersWallets();
                customersWallets.setWalletNo(wallets.getId());
                customersWallets.setCustomerNo(index);

                customerWalletsRepository.save(customersWallets);

                String message = "Dear "+record.getFirstName()+" you have been created successfully, please log in using flex mobile app to setup your account thank you" ;

                boolean send = smsService.sendSMS(new SmsOptions()
                        .setMessage(message)
                        .setMobileNo(record.getPhone())
                );

            }
        }
        else map.put("status", "01");

        //Insert logs
        AuditTrail log = entityForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getFirstName() +" " + record.getSurname());
        log.setActivity( logActivity );
        auditTrailRepository.save(log);
        return map;
    }

    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Customers record = entityForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        entityRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated Customer successfully. Reference %s", record.getFirstName() +" " + record.getSurname() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        Customers record = entityRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedBy() );

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }
}
