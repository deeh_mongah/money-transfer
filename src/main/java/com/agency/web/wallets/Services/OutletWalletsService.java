package com.agency.web.wallets.Services;

import com.agency.web.agency.forms.OutletsForm;
import com.agency.web.wallets.OutletWalletsServiceInterface;
import com.agency.web.wallets.form.OutletWalletsForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Service
public class OutletWalletsService implements OutletWalletsServiceInterface {


    @Autowired private OutletWalletsForm entityForm;


    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> editRecord(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request) {
        String index = request.getParameter("index");

        System.err.println("<><><>index<><><><>"+index);
        return entityForm.transformEntity(index);
    }

    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> flagRecords(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request) {
        return null;
    }

    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request) {
        return null;
    }
}
