package com.agency.web.wallets.Services;

import com.agency.api.services.HelperUtil;
import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.entities.Outlets;
import com.agency.web.agency.repository.AgentsRepository;
import com.agency.web.agency.repository.OutletsRepository;
import com.agency.web.terminals.entities.TerminalAllocation;
import com.agency.web.terminals.entities.Terminals;
import com.agency.web.terminals.repository.TerminalAllocationRepository;
import com.agency.web.terminals.repository.TerminalRepository;
import com.agency.web.transactions.entities.Transactions;
import com.agency.web.transactions.repository.TransactionTypesRepository;
import com.agency.web.transactions.repository.TransactionsRepository;
import com.agency.web.wallets.WalletsTransactionsServiceInterface;
import com.agency.web.wallets.entity.OutletWallets;
import com.agency.web.wallets.entity.TerminalWallets;
import com.agency.web.wallets.entity.Wallets;
import com.agency.web.wallets.repository.OutletWalletsRepository;
import com.agency.web.wallets.repository.TerminalWalletsRepository;
import com.agency.web.wallets.repository.WalletsRepository;
import com.agency.web.wallets.repository.WalletsTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@Transactional
public class WalletsTransactionService implements WalletsTransactionsServiceInterface {

    @Autowired
    private WalletsTransactionRepository walletsTransactionRepository;

    @Autowired
    private TerminalWalletsRepository terminalWalletsRepository;

    @Autowired
    private OutletWalletsRepository outletWalletsRepository;

    @Autowired
    private TerminalAllocationRepository terminalAllocationRepository;

    @Autowired
    private WalletsRepository walletsRepository;

    @Autowired
    private OutletsRepository outletsRepository;

    @Autowired
    private AgentsRepository agentsRepository;

    @Autowired
    private TerminalRepository terminalRepository;

    @Autowired
    private TransactionsRepository transactionsRepository;

    @Autowired
    private TransactionTypesRepository transactionTypesRepository;




    @Override
    public Map<String, Object> pushOutletFloat(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();

        Long index = Long.valueOf(request.getParameter("id"));

        System.err.println("index"+index);

        Optional<OutletWallets> oOutletWallets = outletWalletsRepository.findById(index);

        if (!oOutletWallets.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Outlet");
            return map;
        }

        OutletWallets outletWallets = oOutletWallets.get();

        Wallets wallets = outletWallets.getWalletsLink();

        String amount  = request.getParameter("amount");

        BigDecimal updatedAmount = wallets.getAmount().add(new BigDecimal(amount));

        wallets.setAmount(updatedAmount);

        walletsRepository.save(wallets);

        Long transactionTypeNo = Long.valueOf(3);

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setAmount( new BigDecimal(amount) );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames(outletWallets.getOutletsLink().getName());
        transaction.setNames("N/A");
        transactionsRepository.save( transaction );



        map.put("status", "00");
        map.put("message", "Request processed successfully\n Your Wallet Balance is"+updatedAmount);
        return map;
    }

    @Override
    public Map<String, Object> pullOutletFloat(HttpServletRequest request) {

        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf(request.getParameter("id"));

        System.err.println("index"+index);

        //        OutletWallets outletWallets = outletWalletsRepository.findByOutletNo(index).get();
        Optional<OutletWallets> outletWalletsList = outletWalletsRepository.findById(index);
        OutletWallets outletWallets = outletWalletsList.get();

//        if (!oOutletWallets.isPresent()){
//            map.put("status", "01");
//            map.put("message", "Unknown Outlet");
//            return map;
//        }
//
//        OutletWallets outletWallets = oOutletWallets.get();

        Wallets wallets = outletWallets.getWalletsLink();

        String amount  = request.getParameter("amount");

        int compareAmount = new BigDecimal(amount).compareTo(wallets.getAmount());

        if (compareAmount == 1 ){
            map.put("status", "01");
            map.put("message", "Amount Too Big than float available");
            return map;
        }


        BigDecimal updatedAmount = wallets.getAmount().subtract(new BigDecimal(amount));

        wallets.setAmount(updatedAmount);

        walletsRepository.save(wallets);

        Long transactionTypeNo = Long.valueOf(4);

        //Populate transaction
        Transactions transaction = new Transactions();
        transaction
                .setReceiptNo( null )
                .setTransactionTypeNo( transactionTypeNo )
                .setAmount( new BigDecimal(amount) );
        transactionsRepository.save( transaction );

        String receiptNo = String.format("%1$" + 10 + "s", transaction.getId() ).replace(' ', '0');

        //Update transaction
        transaction.setReceiptNo( receiptNo );
        transaction.setAgentNames(outletWallets.getOutletsLink().getName());
        transaction.setNames("N/A");
        transactionsRepository.save( transaction );




        map.put("status", "00");
        map.put("message", "Request processed successfully\n Your Wallet Balance is"+updatedAmount);
        return map;
    }

    @Override
    public Map<String, Object> pushTerminalFloat(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf(request.getParameter("id"));

        System.err.println("index"+index);

        Optional<TerminalWallets> oTerminalWallets = terminalWalletsRepository.findByTerminalNo(index);

        String amount  = request.getParameter("amount");

        if (!oTerminalWallets.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Terminal");
            return map;
        }

        TerminalWallets terminalWallets = oTerminalWallets.get();


        Optional<TerminalAllocation> oTerminalAllocation = terminalAllocationRepository.findById(terminalWallets.getTerminalNo());

        if (!oTerminalAllocation.isPresent()){
            map.put("status", "01");
            map.put("message", "Please Allocate Terminal");
            return map;
        }

        TerminalAllocation terminalAllocation = oTerminalAllocation.get();

//        Optional<OutletWallets> oOutletWallets = outletWalletsRepository.findByOutletNo(terminalAllocation.getOutletNo());



       BigDecimal terminalAmount = terminalWallets.getWalletsLink().getAmount().add(new BigDecimal(amount));

        Wallets wallets1 = terminalWallets.getWalletsLink();

        wallets1.setAmount(terminalAmount);

        walletsRepository.save(wallets1);


//        OutletWallets outletWallets = oOutletWallets.get();

//        Wallets wallets = outletWallets.getWalletsLink();



//        BigDecimal updatedAmount = wallets.getAmount().subtract(new BigDecimal(amount));
//
//        wallets.setAmount(updatedAmount);
//
//        walletsRepository.save(wallets);
//
//        Optional<Terminals> oTerminals = terminalRepository.findById(index);
//
//        Terminals terminal = oTerminals.get();
//        BigDecimal amountTerminal = terminal.getAmount();
//
//        terminal.setAmount(amountTerminal.subtract(new BigDecimal(amount)));
//        terminalRepository.save(terminal);


        map.put("status", "00");
        map.put("message", "Request processed successfully\n Your Wallet Balance is"+terminalAmount);
        return map;
    }

    @Override
    public Map<String, Object> pullTerminalFloat(HttpServletRequest request) {
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf(request.getParameter("id"));

        System.err.println("index"+index);

        Optional<TerminalWallets> oTerminalWallets = terminalWalletsRepository.findByTerminalNo(index);

        String amount  = request.getParameter("amount");

        if (!oTerminalWallets.isPresent()){
            map.put("status", "01");
            map.put("message", "Unknown Terminal");
            return map;
        }

        TerminalWallets terminalWallets = oTerminalWallets.get();


        Optional<TerminalAllocation> oTerminalAllocation = terminalAllocationRepository.findById(terminalWallets.getTerminalNo());

        if (!oTerminalAllocation.isPresent()){
            map.put("status", "01");
            map.put("message", "Please Allocate Terminal");
            return map;
        }

        TerminalAllocation terminalAllocation = oTerminalAllocation.get();

//        Optional<OutletWallets> oOutletWallets = outletWalletsRepository.findByOutletNo(terminalAllocation.getOutletNo());
//
//
//
//        BigDecimal terminalAmount = terminalWallets.getWalletsLink().getAmount().subtract(new BigDecimal(amount));
//
//        Wallets wallets1 = terminalWallets.getWalletsLink();
//
//        wallets1.setAmount(terminalAmount);
//
//        walletsRepository.save(wallets1);
//
//
//        OutletWallets outletWallets = oOutletWallets.get();
//
//        Wallets wallets = outletWallets.getWalletsLink();
//
//
//
//        BigDecimal updatedAmount = wallets.getAmount().add(new BigDecimal(amount));
//
//        wallets.setAmount(updatedAmount);
//
//        walletsRepository.save(wallets);
//
//        Optional<Terminals> oTerminals = terminalRepository.findById(index);
//
//        Terminals terminal = oTerminals.get();
//        BigDecimal amountTerminal = terminal.getAmount();
//
//        terminal.setAmount(amountTerminal.add(new BigDecimal(amount)));
//        terminalRepository.save(terminal);


        map.put("status", "00");
//        map.put("message", "Request processed successfully\n Your Wallet Balance is"+terminalAmount);
        map.put("message", "Request processed successfully\n Your Wallet Balance is");
        return map;
    }
}
