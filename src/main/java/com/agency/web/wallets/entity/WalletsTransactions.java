package com.agency.web.wallets.entity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "wallets_transactions")
public class WalletsTransactions {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "wallet_no")
    private Long walletNo;

    @Column(name = "entry_type")
    private Long entryType;

    @JoinColumn(name = "wallet_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Wallets walletsLink;

    @JoinColumn(name = "entry_type", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private JournalEntryTypes journalEntryTypesLink;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(Long walletNo) {
        this.walletNo = walletNo;
    }

    public Long getEntryType() {
        return entryType;
    }

    public void setEntryType(Long entryType) {
        this.entryType = entryType;
    }

    public Wallets getWalletsLink() {
        return walletsLink;
    }

    public void setWalletsLink(Wallets walletsLink) {
        this.walletsLink = walletsLink;
    }

    public JournalEntryTypes getJournalEntryTypesLink() {
        return journalEntryTypesLink;
    }

    public void setJournalEntryTypesLink(JournalEntryTypes journalEntryTypesLink) {
        this.journalEntryTypesLink = journalEntryTypesLink;
    }
}
