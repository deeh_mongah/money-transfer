package com.agency.web.wallets.entity;

import com.agency.web.agency.entities.Outlets;
import com.agency.web.terminals.entities.Terminals;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ben on 16-Jul-19.
 */
@Entity
@Table(name = "terminal_wallets")
public class TerminalWallets implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "terminal_no")
    private Long terminalNo;

    @Column(name = "wallet_no")
    private Long walletNo;

    @JoinColumn(name = "terminal_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Terminals terminalsLink;

    @JoinColumn(name = "wallet_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Wallets walletsLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(Long walletNo) {
        this.walletNo = walletNo;
    }

    public Long getTerminalNo() {
        return terminalNo;
    }

    public void setTerminalNo(Long terminalNo) {
        this.terminalNo = terminalNo;
    }

    public Terminals getTerminalsLink() {
        return terminalsLink;
    }

    public void setTerminalsLink(Terminals terminalsLink) {
        this.terminalsLink = terminalsLink;
    }

    public Wallets getWalletsLink() {
        return walletsLink;
    }

    public void setWalletsLink(Wallets walletsLink) {
        this.walletsLink = walletsLink;
    }
}
