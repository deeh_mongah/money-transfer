package com.agency.web.wallets.entity;

import com.agency.web.terminals.entities.Terminals;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Ben on 16-Jul-19.
 */
@Entity
@Table(name = "wallets")
public class Wallets  implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "amount")
    private BigDecimal amount = BigDecimal.ZERO;


    @Column(name = "currency_no")
    private long currencyNo;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }



    @JoinColumn(name = "currency_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private CurrencyTypeWallets currencyTypeWalletsLink;


    public long getCurrencyNo() {
        return currencyNo;
    }

    public void setCurrencyNo(long currencyNo) {
        this.currencyNo = currencyNo;
    }

    @JsonIgnore
    public CurrencyTypeWallets getCurrencyTypeWalletsLink() {
        return currencyTypeWalletsLink;
    }

    public void setCurrencyTypeWalletsLink(CurrencyTypeWallets currencyTypeWalletsLink) {
        this.currencyTypeWalletsLink = currencyTypeWalletsLink;
    }
}
