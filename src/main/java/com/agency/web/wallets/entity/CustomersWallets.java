package com.agency.web.wallets.entity;

import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.entities.Customers;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ben on 16-Jul-19.
 */
@Entity
@Table(name = "customer_wallets")
public class CustomersWallets implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "customer_no")
    private Long customerNo;

    @Column(name = "wallet_no")
    private Long walletNo;

    @JoinColumn(name = "customer_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Customers customersLink;

    @JoinColumn(name = "wallet_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Wallets walletsLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(Long walletNo) {
        this.walletNo = walletNo;
    }

    public Long getCustomerNo() {
        return customerNo;
    }

    public void setCustomerNo(Long customerNo) {
        this.customerNo = customerNo;
    }

    public Customers getCustomersLink() {
        return customersLink;
    }

    public void setCustomersLink(Customers customersLink) {
        this.customersLink = customersLink;
    }

    public Wallets getWalletsLink() {
        return walletsLink;
    }

    public void setWalletsLink(Wallets walletsLink) {
        this.walletsLink = walletsLink;
    }
}
