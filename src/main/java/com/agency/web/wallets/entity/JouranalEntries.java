package com.agency.web.wallets.entity;


import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Ben on 16-Jul-19.
 */
@Entity
@Table(name = "journal_entries")
public class JouranalEntries {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;


    @Column(name = "entry_type")
    private Long entryType;

    @Column(name = "wallet_no")
    private Long walletNo;


    @Column(name = "transaction_no")
    private Long transactionNo;


    @Column(name = "running_balance")
    private BigDecimal runningBalance = BigDecimal.ZERO;

    @Column(name = "created_on")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn = new Date( System.currentTimeMillis() );

    @JoinColumn(name = "wallet_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Wallets walletsLink;

//    @JoinColumn(name = "transaction_no", referencedColumnName = "ID", insertable = false, updatable = false)
//    @ManyToOne(fetch = FetchType.LAZY)
//    private Transactions transactionsLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEntryType() {
        return entryType;
    }

    public void setEntryType(Long entryType) {
        this.entryType = entryType;
    }

    public Long getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(Long walletNo) {
        this.walletNo = walletNo;
    }

    public Long getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(Long transactionNo) {
        this.transactionNo = transactionNo;
    }

    public BigDecimal getRunningBalance() {
        return runningBalance;
    }

    public void setRunningBalance(BigDecimal runningBalance) {
        this.runningBalance = runningBalance;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Wallets getWalletsLink() {
        return walletsLink;
    }

    public void setWalletsLink(Wallets walletsLink) {
        this.walletsLink = walletsLink;
    }

//    public Transactions getTransactionsLink() {
//        return transactionsLink;
//    }
//
//    public void setTransactionsLink(Transactions transactionsLink) {
//        this.transactionsLink = transactionsLink;
//    }
}
