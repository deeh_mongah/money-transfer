package com.agency.web.wallets.entity;

import com.agency.core.template.forms.MutableField;
import com.agency.web.agency.entities.Customers;
import com.agency.web.wallets.repository.FlexWalletRepository;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by Ben on 16-Jul-19.
 */
@Entity
@Table(name = "flex_wallets")
public class FlexWallets implements Serializable {

    public static final String CLEARED = "1";
    public static final String AVAILABLE = "0";
    public static final String ONGOING = "2";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max=100)
    @Column(name = "sender_no", length = 100)
    private String senderNo;

    @Size(max=100)
    @Column(name = "receiver_no", length = 100)
    private String receiverNo;

    @Size(max=100)
    @Column(name = "receiver_name", length = 100)
    private String receiverName;

    @Size(max = 20)
    @Column(name = "FLAG", length = 20)
    private String flag;

    @Size(max = 20)
    @Column(name = "currency_type", length = 20)
    private String currencyType;

    @Size(max = 20)
    @Column(name = "secret_code", length = 20)
    private String secretCode;

    @MutableField( name = "Amount")
    @Column(name = "amount", precision = 18, scale = 2)
    private BigDecimal amount;

    @Column(name = "CREATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createdOn;

    @Column(name = "UPDATED_ON")
    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedOn;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSenderNo() {
        return senderNo;
    }

    public FlexWallets setSenderNo(String senderNo) {
        this.senderNo = senderNo;
        return this;
    }

    public String getReceiverNo() {
        return receiverNo;
    }

    public FlexWallets setReceiverNo(String receiverNo) {
        this.receiverNo = receiverNo;
        return this;
    }

    public String getFlag() {
        return flag;
    }

    public FlexWallets setFlag(String flag) {
        this.flag = flag;
        return this;
    }

    public String getCurrencyType() {
        return currencyType;
    }

    public FlexWallets setCurrencyType(String currencyType) {
        this.currencyType = currencyType;
        return this;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public FlexWallets setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
        return this;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public FlexWallets setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
        return this;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public FlexWallets setReceiverName(String receiverName) {
        this.receiverName = receiverName;
        return this;
    }

    public String getSecretCode() {
        return secretCode;
    }

    public FlexWallets setSecretCode(String secretCode) {
        this.secretCode = secretCode;
        return this;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public FlexWallets setAmount(BigDecimal amount) {
        this.amount = amount;
        return this;
    }
}
