package com.agency.web.wallets.entity;

import com.agency.web.agency.entities.Agents;
import com.agency.web.agency.entities.Outlets;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Ben on 16-Jul-19.
 */
@Entity
@Table(name = "outlets_wallets")
public class OutletWallets implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Column(name = "outlet_no")
    private Long outletNo;

    @Column(name = "wallet_no")
    private Long walletNo;

    @JoinColumn(name = "outlet_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Agents outletsLink;

    @JoinColumn(name = "wallet_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Wallets walletsLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getWalletNo() {
        return walletNo;
    }

    public void setWalletNo(Long walletNo) {
        this.walletNo = walletNo;
    }

    public Long getOutletNo() {
        return outletNo;
    }

    public void setOutletNo(Long outletNo) {
        this.outletNo = outletNo;
    }

    @JsonIgnore
    public Agents getOutletsLink() {
        return outletsLink;
    }

    public void setOutletsLink(Agents outletsLink) {
        this.outletsLink = outletsLink;
    }

    @JsonIgnore
    public Wallets getWalletsLink() {
        return walletsLink;
    }

    public void setWalletsLink(Wallets walletsLink) {
        this.walletsLink = walletsLink;
    }
}
