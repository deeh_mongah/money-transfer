package com.agency.web.wallets.form;

import com.agency.core.template.forms.Form;
import com.agency.web.usermanager.entities.Users;
import com.agency.web.wallets.entity.OutletWallets;
import org.springframework.stereotype.Component;

@Component
public class OutletWalletsForm extends Form<OutletWallets> {

    public OutletWalletsForm(){
        setMapping( OutletWallets.class );
        setRole("ROLE_USERS", "Users", "superadmin");
    }
}
