package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.CustomersWallets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by ben on 08/11/2019.
 */
@Repository
public interface CustomerWalletsRepository extends CrudRepository<CustomersWallets, Long> {

    Optional<CustomersWallets> findByCustomerNo(long customerNo );


    Optional<CustomersWallets> findByWalletNo(long customerNo );

}
