package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.TerminalWallets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TerminalWalletsRepository extends CrudRepository<TerminalWallets, Long> {


    Optional<TerminalWallets> findByTerminalNo(long terminalNo);


}
