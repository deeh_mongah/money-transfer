package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.JournalEntryTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by Ben on 16-Jul-19.
 */
@Repository
public interface JournalEntryTypesRepository extends CrudRepository<JournalEntryTypes, Long> {

    Optional<JournalEntryTypes> findByAppCode(String appCode);
}
