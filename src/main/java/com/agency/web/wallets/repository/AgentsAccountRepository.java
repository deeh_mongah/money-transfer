package com.agency.web.wallets.repository;


import com.agency.web.wallets.entity.AgentsAccount;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AgentsAccountRepository  extends CrudRepository<AgentsAccount, Long> {

    Optional<AgentsAccount> findByAccountNo(String accountNo);
}
