package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.OutletWallets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OutletWalletsRepository  extends CrudRepository<OutletWallets, Long> {

//    Optional<OutletWallets> findByOutletNo(long outletNo);

    List<OutletWallets> findByOutletNo(long outletNo);
}
