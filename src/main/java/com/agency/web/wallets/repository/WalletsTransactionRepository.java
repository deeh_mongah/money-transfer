package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.WalletsTransactions;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WalletsTransactionRepository extends CrudRepository<WalletsTransactions, Long> {
}
