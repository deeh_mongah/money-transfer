package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.CurrencyTypeWallets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CurrencyTypeWalletsRepository extends CrudRepository<CurrencyTypeWallets, Long> {


    Optional<CurrencyTypeWallets> findByName( String name );


}
