package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.Wallets;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Created by Ben on 16-Jul-19.
 */
@Repository
public interface WalletsRepository extends CrudRepository<Wallets, Long> {

    Optional<Wallets> findByCurrencyNo( Long id );


}
