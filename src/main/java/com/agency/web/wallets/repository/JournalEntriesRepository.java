package com.agency.web.wallets.repository;

import com.agency.web.wallets.entity.JouranalEntries;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Ben on 16-Jul-19.
 */
@Repository
public interface JournalEntriesRepository  extends CrudRepository<JouranalEntries, Long> {

    List<JouranalEntries> findByWalletNo(long walletNo);



    Optional<JouranalEntries> findByTransactionNo(long transactionNo);


}
