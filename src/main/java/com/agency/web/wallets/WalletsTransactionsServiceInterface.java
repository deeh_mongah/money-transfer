package com.agency.web.wallets;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

public interface WalletsTransactionsServiceInterface {

    public Map<String, Object> pushOutletFloat(HttpServletRequest request);
    public Map<String, Object> pullOutletFloat(HttpServletRequest request);
    public Map<String, Object> pushTerminalFloat(HttpServletRequest request);
    public Map<String, Object> pullTerminalFloat(HttpServletRequest request);
}
