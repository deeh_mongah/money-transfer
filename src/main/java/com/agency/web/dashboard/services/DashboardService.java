package com.agency.web.dashboard.services;

import com.agency.web.dashboard.DashboardServiceInterface;
import com.agency.web.dashboard.vm.DashboardVm;
import com.agency.web.transactions.entities.Transactions;
import com.agency.web.transactions.repository.TransactionsHelperRepository;
import com.agency.web.transactions.repository.TransactionsRepository;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.usermanager.repository.UserRepository;
import org.hibernate.Session;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class DashboardService implements DashboardServiceInterface {

    @Autowired private UserRepository userRepository;
    @Autowired  private TransactionsHelperRepository transactionsHelperRepository;
    @Autowired private TransactionsRepository transactionsRepository;

    @PersistenceContext
    private EntityManager entityManager;

    /**
     * Fetch dashboard statistics
     *
     * @return  DashboardVm
     */
    @Override
    public DashboardVm fetchStatistics(HttpServletRequest request){
        DashboardVm model = new DashboardVm();
        Session session = entityManager.unwrap( Session.class );

        BigInteger users = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM users").uniqueResult();
        BigInteger devices = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM terminals").uniqueResult();
        BigInteger transactions = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM transactions").uniqueResult();
        BigInteger outlets = (BigInteger)session.createNativeQuery("SELECT COUNT(*) FROM outlets").uniqueResult();

        List<Transactions> transactionsList = transactionsRepository.findByTransactionTypeNo(Long.parseLong("2"));
        List<Transactions> depositTransaction = transactionsRepository.findByTransactionTypeNo(Long.parseLong("1"));

        BigDecimal WithdrawalAmount = BigDecimal.ZERO;

        BigDecimal depositAmount = BigDecimal.ZERO;

        for (Transactions node: transactionsList){

            BigDecimal amount = node.getAmount();

            WithdrawalAmount = WithdrawalAmount.add(amount);

            System.err.println("<><>finalAmount<><>"+WithdrawalAmount);

        }

        for (Transactions nodez:depositTransaction){

            depositAmount = depositAmount.add(nodez.getAmount());
        }
//        System.err.println("<><>amount<><>"+amount.getAmount());

        model.setUsers( users.longValue() );
        model.setDevices( devices.longValue() );
        model.setTransactions( transactions.longValue() );
        model.setOutlets( outlets.longValue() );
        model.setWithdrawalAmount(WithdrawalAmount);
        model.setDepositAmount(depositAmount);

        return model;
    }

    /**
     * Fetch calendar data
     *
     * @param request
     * @return List<Object>
     */
    @Override
    public List<Object> fetchCalendarData(final HttpServletRequest request){
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");
        String startDate = request.getParameter("start");
        String endDate = request.getParameter("end");
        List<Object> response = new ArrayList<>();


        List<Object[]> data =  transactionsHelperRepository.fetchCalendarData( parentType, parentNo, startDate, endDate );
        DateTime dtStartTime = new DateTime(startDate);
        DateTime dtEndTime = new DateTime(endDate);

        BigDecimal deposit = BigDecimal.ZERO;
        BigDecimal withdrawal = BigDecimal.ZERO;
        long balanceInquiry = 0;
        long miniStatement = 0;

        for (DateTime date = dtStartTime; date.isBefore(dtEndTime.plusDays(1)); date = date.plusDays(1)) {


            for (Object[] obj : data) {
                String _date = (new SimpleDateFormat("yyyy-MM-dd")).format( (java.util.Date) obj[0] );

                if ( _date.equals( date.toString("yyyy-MM-dd") )) {
                    deposit = (BigDecimal) obj[1];
                    withdrawal = (BigDecimal) obj[2];
                    balanceInquiry = (Long)obj[3];
                    miniStatement = (Long)obj[4];

                    Map<String, Object> node = new HashMap<>();
                    node.put("start", _date);
                    node.put("title", String.format("Deposit: %,.2f", deposit.setScale(2, RoundingMode.HALF_UP)));
                    response.add( node );

                    Map<String, Object> node2 = new HashMap<>();
                    node2.put("start", _date);
                    node2.put("title", String.format("Withdrawal: %,.2f", withdrawal.setScale(2, RoundingMode.HALF_UP)));
                    response.add( node2 );

                    Map<String, Object> node3 = new HashMap<>();
                    node3.put("start", date.toString("yyyy-MM-dd"));
                    node3.put("title", String.format("B. Inquiry: %ul", balanceInquiry ));
                    node3.put("backgroundColor", "#F3565D");
                    response.add ( node3 );

                    Map<String, Object> node4 = new HashMap<>();
                    node4.put("start", date.toString("yyyy-MM-dd"));
                    node3.put("title", String.format("M. Statement: %ul", miniStatement ));
                    node4.put("backgroundColor", "#F3565D");
                    response.add ( node4 );
                    break;
                }
            }
        }

        return response;
    }
}
