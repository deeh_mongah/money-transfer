package com.agency.web.dashboard.vm;

import java.math.BigDecimal;

public class DashboardVm {

    private long users;
    private long devices;
    private long outlets;
    private long transactions;
    private BigDecimal withdrawalAmount;
    private BigDecimal depositAmount;

    public long getUsers() {
        return users;
    }

    public void setUsers(long users) {
        this.users = users;
    }

    public long getDevices() {
        return devices;
    }

    public void setDevices(long devices) {
        this.devices = devices;
    }

    public long getOutlets() {
        return outlets;
    }

    public void setOutlets(long outlets) {
        this.outlets = outlets;
    }

    public long getTransactions() {
        return transactions;
    }

    public void setTransactions(long transactions) {
        this.transactions = transactions;
    }

    public BigDecimal getWithdrawalAmount() {
        return withdrawalAmount;
    }

    public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
        this.withdrawalAmount = withdrawalAmount;
    }

    public BigDecimal getDepositAmount() {
        return depositAmount;
    }

    public void setDepositAmount(BigDecimal depositAmount) {
        this.depositAmount = depositAmount;
    }
}
