package com.agency.web.dashboard;

import com.agency.web.dashboard.vm.DashboardVm;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

public interface DashboardServiceInterface {

    /**
     * Fetch dashboard statistics
     *
     * @param request
     * @return  DashboardVm
     */
    public DashboardVm fetchStatistics(HttpServletRequest request);

    /**
     * Fetch calendar data
     *
     * @param request
     * @return List<Object>
     */
    public List<Object> fetchCalendarData(final HttpServletRequest request);
}
