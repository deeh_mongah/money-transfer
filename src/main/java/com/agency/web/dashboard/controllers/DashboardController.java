package com.agency.web.dashboard.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.dashboard.DashboardServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class DashboardController {

    @Autowired private DatatablesInterface dataTable;
    @Autowired private DashboardServiceInterface dashboardService;

    @RequestMapping("/")
    public ModelAndView index(HttpServletRequest request) {
        View view = new View("dashboard/default-view");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if (AjaxUtils.isAjaxRequest(request)) {
            String calendarStart = request.getParameter("start");

            if( null != calendarStart )
                return view.sendJSON( dashboardService.fetchCalendarData( request ) );

            //When fetching table data
            else
                return fetchTableInfo(request, view);
        }

        return view
                .addAttribute("stats", dashboardService.fetchStatistics( request ) )
                .getView();
    }

    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        dataTable
                .nativeSQL( true )
                .select(" a.name, COUNT(b.id), CONCAT('KES '; FORMAT( COALESCE( SUM(c.amount); 0) ; 2) )")
                .from("counties a")
                .from("LEFT JOIN beneficiaries b ON b.county_id = a.id ")
                .from("LEFT JOIN payment_details c ON c.beneficiary_no = b.id ")
                .groupBy("a.name ")
                ;

        return view.sendJSON( dataTable.showTable() );
    }
}
