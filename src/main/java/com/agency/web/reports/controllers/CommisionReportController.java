package com.agency.web.reports.controllers;

import com.agency.core.export.ExportService;
import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class CommisionReportController extends ReportAbstractController{

    @RequestMapping( value = "/commission-report")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("reports/commission-report");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String transactionAlias = "";
        String strTransactionTypeNo = request.getParameter("data");

        String[] columns;

        if(StringUtils.isEmpty( strTransactionTypeNo ) ) {

            columns = new String[]{
                    "Transaction Type",
                    "No of Transactions",
                    "Total Amount"
            };

            dataTable
                    .nativeSQL(true)
                    .select("a.name, COUNT(b.id),FORMAT(SUM(b.amount); 2) as 'Total Cost', a.id")
                    .from("transaction_types a ")
                    .from("LEFT JOIN transactions b ON b.transaction_type_no = a.id ")
                    .groupBy("a.id")
            ;

            transactionAlias = "b";
        }
        else{
            columns = new String[]{
                    "Date/Time",
                    "Reference No",
                    "Amount",
                    "Transaction Type",
                    "Agent",
                    "Agent Amount",
                    "Flex Amount",
                    "Binary Amount"
            };

            Long transactionTypeNo = Long.valueOf( strTransactionTypeNo );
            dataTable
                    .nativeSQL( true )
                    .select("DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), c.receipt_no, a.tariff_amount, b.name, d.name, a.agent_amount, a.flex_amount, a.binary_amount ")
                    .from("commission_transaction a ")
                    .from("LEFT JOIN transactions c ON c.id = a.transaction_no ")
                    .from("LEFT JOIN transaction_types b ON b.id = c.transaction_type_no ")
                    .from("LEFT JOIN agents d ON d.id = a.agent_no ")
                    .where("b.id = :transactionTypeNo ")
                    .setParameter("transactionTypeNo", transactionTypeNo)
                    ;

            transactionAlias = "a";
        }

        //Set-up filters
        setDateFilters( request, transactionAlias );
        setTransactionStatus( request, transactionAlias );
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }


    @RequestMapping(value = "/commission-report/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "/commission-report", format
        );
    }
}
