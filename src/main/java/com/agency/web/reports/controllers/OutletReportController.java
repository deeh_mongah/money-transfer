package com.agency.web.reports.controllers;

import com.agency.core.export.ExportService;
import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class OutletReportController extends ReportAbstractController{

    @RequestMapping( value = "/outlet-report")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("reports/outlet-report");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view) {
        String transactionAlias = "";
        String strTransactionTypeNo = request.getParameter("data");

        String[] columns;

        if(StringUtils.isEmpty( strTransactionTypeNo ) ) {

            columns = new String[]{
                    "Terminal",
                    "No of Transactions",
                    "Total Amount"
            };

            dataTable
                    .nativeSQL(true)
                    .select("a.name,COUNT(d.id),CONCAT('KES '; FORMAT(SUM(d.amount); 2)) as 'Total Cost', a.id ")
                    .from("outlets a")
                    .from("left join terminal_allocation b ON b.outlet_no = a.id")
                    .from("left join terminals c ON c.id = b.id ")
                    .from("left join transactions d ON d.terminal_no = c.id")
                    .groupBy("a.id");

//            dataTable
//                    .nativeSQL(true)/allocations
//                    .select("a.name, COUNT(b.id), CONCAT('KES '; FORMAT(SUM(b.amount); 2)) as 'Total Cost', a.id")
//                    .from("outlets a ")
//                    .from("LEFT JOIN transactions b ON b.terminal_no = a.id ")
//                    .groupBy("a.id")
//            ;

            transactionAlias = "b";
        }
        else{
            columns = new String[]{
                    "Date/Time",
                    "Reference No",
                    "Amount",
                    "Transaction Type",
                    "Status",
                    "Officer ID",
                    "Terminal"
            };

            Long transactionTypeNo = Long.valueOf( strTransactionTypeNo );
//            dataTable
//                    .nativeSQL( true )
//                    .select("DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), a.receipt_no, a.amount, b.name, a.flag, a.officer_id, c.serial_no ")
//                    .from("transactions a ")
//                    .from("LEFT JOIN transaction_types b ON b.id = a.transaction_type_no ")
//                    .from("LEFT JOIN terminals c ON c.id = a.terminal_no ")
//                    .where("b.id = :transactionTypeNo ")
//                    .setParameter("transactionTypeNo", transactionTypeNo);
            dataTable
                    .nativeSQL( true )
                    .select("DATE_FORMAT(a.created_on; '%Y-%m-%d %T'), a.receipt_no, a.amount, b.name, a.flag, a.officer_id, c.serial_no ")
                    .from("transactions a ")
                    .from("LEFT JOIN transaction_types b ON b.id = a.transaction_type_no ")
                    .from("LEFT JOIN terminals c ON c.id = a.terminal_no ")
                    .from("LEFT JOIN terminal_allocation d ON d.id = a.terminal_no ")
                    .from("LEFT JOIN outlets o ON o.id = d.outlet_no")
                    .where("o.id = :transactionTypeNo ")
                    .setParameter("transactionTypeNo", transactionTypeNo);

            transactionAlias = "a";
        }

        //Set-up filters
        setDateFilters( request, transactionAlias );
        setTransactionStatus( request, transactionAlias );
        ExportService.init(dataTable, request, columns, true);

        return view.sendJSON( dataTable.showTable() );
    }


    @RequestMapping(value = "/outlet-report/{format}", method = RequestMethod.GET)
    public void export (
            HttpServletRequest request, HttpServletResponse response, @PathVariable("format") String format
    ) throws IOException {
        generateDoc (
                request, response, "outlet-report", format
        );
    }
}
