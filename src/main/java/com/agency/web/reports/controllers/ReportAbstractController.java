package com.agency.web.reports.controllers;

import com.agency.core.export.ExportInterface;
import com.agency.core.template.datatables.DatatablesInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Map;

public class ReportAbstractController {

    @Autowired
    @Qualifier("csvGridExportService")
    protected ExportInterface csvGridExportService;

    @Autowired
    @Qualifier("excelGridExportService")
    protected ExportInterface excelGridExportService;

    @Autowired
    @Qualifier("pdfGridExportService")
    protected ExportInterface pdfGridExportService;

    @Autowired
    protected DatatablesInterface dataTable;

    /**
     * Set Date Range data filters
     *
     * @param request
     * @param transactionAlias
     */
    public void setDateFilters(HttpServletRequest request, String transactionAlias ){
        String startDate = request.getParameter("startDate");
        String endDate = request.getParameter("endDate");
        String query = "";

        // If the dates have not been defined, end here
        if ( null == startDate ) return;

        // When no date has been set
        if ( StringUtils.isEmpty( startDate ) ) startDate = Calendar.getInstance().get(Calendar.YEAR) + "-01-01";
        if ( StringUtils.isEmpty( endDate ) ) endDate = Calendar.getInstance().get(Calendar.YEAR) + "-12-31";

        // When the start date and end date are the same
        if ( startDate.equals( endDate ) ) {
            query = String.format("DATE(%s.created_on) = :startDate ", transactionAlias);
            dataTable
                    .where( query )
                    .setParameter("startDate", startDate);
        }

        // when the date range is defined
        else {
            startDate = startDate + " 00:00:00";
            endDate = endDate+ " 23:59:00";
            query = String.format("DATE(%s.created_on) BETWEEN :startDate AND :endDate ", transactionAlias);
            dataTable
                    .where( query )
                    .setParameter("startDate", startDate)
                    .setParameter("endDate", endDate);
        }
    }

    /**
     * Set status filter
     *
     * @param request
     * @param transactionAlias
     */
    protected void setTransactionStatus(HttpServletRequest request, String transactionAlias ){
        String status = request.getParameter("status");

        // If the payment mode have not been defined, end here
        if ( StringUtils.isEmpty( status ) ) return;

        dataTable
                .where( String.format("%s.flag = :flag", transactionAlias ) )
                .setParameter("flag", status);
    }


    /**
     * Using the request parameter provided, generate the expected response
     * document
     *
     * @param   request
     * @param   response
     * @param   filename
     * @param   format
     * @throws IOException
     */
    protected void generateDoc (
            HttpServletRequest request, HttpServletResponse response,
            String filename, String format
    ) throws IOException {
        HttpSession session = request.getSession();
        ExportInterface exportService;

        // The service to use
        switch (format) {
            case "pdf":
                exportService = pdfGridExportService;
                break;
            case "excel":
                exportService = excelGridExportService;
                break;
            default:
                exportService = csvGridExportService;
                break;
        }

        // The columns in the result set
        String[] columns = (String[])session.getAttribute("columns");
        if ( null == columns ) throw new IOException("The result column set has not been defined!");
        exportService.setColumns( columns );

        // Set the query
        String query = (String)session.getAttribute("requestSQL");
        if ( null == query ) throw new IOException("The request sql has not been defined!");

        //The kind of a query to process
        boolean isNativeSql = (boolean)session.getAttribute("isNativeSql");

        exportService
                .setQuery( query )
                .nativeSQL( isNativeSql );

        // Set the parameters that have been defined
        if ( null != session.getAttribute("requestParam") ) {
            Map<String, Object> map = (Map<String, Object>)session.getAttribute("requestParam");
            for (Map.Entry<String, Object> p : map.entrySet())
                exportService.setParameter(p.getKey(), p.getValue());
        }
        if ( null != session.getAttribute("requestParamList") ) {
            Map<String, Collection> map = (Map<String, Collection>)session.getAttribute("requestParamList");
            for (Map.Entry<String, Collection> p : map.entrySet())
                exportService.setParameterList(p.getKey(), p.getValue());
        }

        // The parameters that will be passed to the service
        exportService
                .setResponse(response)
                .setFileName(filename)
                .generateDoc();
    }

}
