package com.agency.web.usermanager;

import com.agency.web.usermanager.entities.UserTypes;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserTypeServiceInterface {

    /**
     * Fetch a list of user types
     *
     * @return List<UserType>
     */
    public List<UserTypes> fetchRecords(HttpServletRequest request);
}
