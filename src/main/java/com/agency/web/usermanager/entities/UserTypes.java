/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.agency.web.usermanager.entities;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 *
 * @author mghanga
 */
@Entity
@Table(name = "user_types")
public class UserTypes implements Serializable {

    public static final String SYSTEM_ADMIN = "system-admin";
    public static final String BANK_ADMIN = "bank-admin";
    public static final String AGENT = "agent-admin";
    public static final String BANK = "bank";
    public static final String MOBILE_CUSTOMER = "mobile-customer";

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "ID", nullable = false)
    private Long id;

    @Size(max = 30)
    @Column(name = "NAME", length = 30)
    private String name;

    @Size(max = 100)
    @Column(name = "CODE", length = 100)
    private String code;

    public UserTypes() {
    }

    public UserTypes(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTypes)) {
            return false;
        }
        UserTypes other = (UserTypes) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "UserType[ id=" + id + " ]";
    }
    
}
