package com.agency.web.usermanager.entities;

import com.agency.core.template.forms.MutableField;
import com.agency.web.banks.entities.Banks;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table( name = "bank_users")
public class BankUsers implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "bank_no")
    private Long bankNo;

    @JoinColumn(name = "bank_no", referencedColumnName = "ID", insertable = false, updatable = false)
    @ManyToOne(fetch = FetchType.LAZY)
    private Banks bankLink;

    @JoinColumn(name = "id", referencedColumnName = "ID", nullable = false, insertable = false, updatable = false)
    @OneToOne(optional = false, fetch = FetchType.LAZY)
    private Users userLink;

    public BankUsers() {}

    public Long getId() {  return id; }
    public BankUsers setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getBankNo() {  return bankNo;  }
    public BankUsers setBankNo(Long bankNo) {
        this.bankNo = bankNo;
        return this;
    }

    @JsonIgnore public Banks getBankLink() { return bankLink; }
    @JsonIgnore public Users getUserLink() {  return userLink; }
}
