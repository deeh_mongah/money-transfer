package com.agency.web.usermanager.controllers;

import com.agency.core.mail.MailerService;
import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.agency.AgentsServiceInterface;
import com.agency.web.banks.BanksServiceInterface;
import com.agency.web.banks.BranchesServiceInterface;
import com.agency.web.configs.ReasonCodeServiceInterface;
import com.agency.web.usermanager.UserGroupServiceInterface;
import com.agency.web.usermanager.UserServiceInterface;
import com.agency.web.usermanager.UserTypeServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UsersController {

    @Autowired private UserServiceInterface entityService;
    @Autowired private UserGroupServiceInterface userGroupService;
    @Autowired private UserTypeServiceInterface userTypeService;
    @Autowired private BanksServiceInterface banksService;
    @Autowired private AgentsServiceInterface agentsServiceInterface;

    @Autowired private DatatablesInterface dataTable;
    @Autowired private ReasonCodeServiceInterface reasonCodeService;
    @Autowired private MailerService mailService;

    @Value("${app.endpoint}")
    private String baseURL;

    @RequestMapping( value = "/users")
    @PreAuthorize("hasRole('ROLE_USERS')")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("user-manager/users");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        if( UserTypes.BANK.equals( parentType ) ){
            view = new View("user-manager/users-bank-view");
        }

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            if( null != action )
                return handleRequests(request, view);

                //When fetching table data
            else
                return fetchTableInfo(request, view);

        }

        return view
                .addAttribute("banks", banksService.fetchRecords( request ) )
                .addAttribute("agent", agentsServiceInterface.fetchRecords( request ) )
                .addAttribute("userTypes", userTypeService.fetchRecords( request ) )
                .addAttribute("usergroups", userGroupService.fetchAllRecords( request ))
                .addAttribute("reasoncodes", reasonCodeService.fetchRecords() )
                .getView();
    }

    /**
     * Handle various client requests
     *
     * @param request Current Request
     * @param view Current view
     * @return ModelAndView
     */
    private ModelAndView handleRequests(HttpServletRequest request, View view){
        Map<String, Object> map = new HashMap<String, Object>();
        try
        {
            String action = request.getParameter("action");

            //When creating a record
            if("new".equals( action ) )
                map = this.entityService.saveRecord( request );

            else if("edit".equals( action ))
                map = this.entityService.editRecord( request );

                //When fetching a record
            else if("fetch-record".equals( action ) )
                map = this.entityService.fetchRecord( request );

                //View record changes
            else if("vedit".equals( action ) )
                map = this.entityService.fetchRecordChanges( request );

                //When approving changes
            else if("approve-edit".equals( action ) || "decline-edit".equals( action) )
                map = this.entityService.approveEditChanges( request );

                //View deactivation reasons
            else if("vdeactivation".equals( action ) )
                map = this.entityService.fetchDeactivationInfo( request );

                //When deactivating an object
            else if("deactivate".equals( action ))
                map = this.entityService.deactivateRecord( request );

                //When toggling entity status
            else if("decline-deactivation".equals( action ) || "approve-deactivation".equals( action )
                    || "activate".equals( action ) || "approve-new".equals( action )
                    || "delete".equals( action ) || "decline-new".equals( action ) ) {

                map = this.entityService.flagRecords(request);

                System.err.println("sendMail : " + map.get("sendMail") );

                //When to send an email
                if( !ObjectUtils.isEmpty( map.get("sendMail") ) && "approve-new".equals( action ) ){
                    String email = String.valueOf( map.get("email") );
                    String token = String.valueOf( map.get("token") );
                    String names = String.valueOf( map.get("names") );
                    String firstName = String.valueOf( map.get("firstName") );

                    boolean sent = mailService.sendMail( mailService.sendGridConfig()
                            .setTo( email, names )
                            .setTemplateId("b5ea3ee2-b955-440c-8374-db685ccc2005")
                            .setSubject("Account Setup")
                            .addAttribute("_lastname", firstName )
                            .addAttribute("_baseUrl", baseURL + "/setup-account/" + token )
                    );
                }
            }

                //When unlocking a user
            else if ( "unlock".equals( action ))
                map = this.entityService.unlockUser( request );

        }
        catch(Exception e){
            e.printStackTrace();
            map.put("status", "01");
            map.put("message", "Internal server error: contact admin");
        }

        return view.sendJSON( map );
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){
        String state = request.getParameter("fetch-table");
        Long userNo = (Long)request.getSession().getAttribute("_userNo");
        Long parentNo = (Long)request.getSession().getAttribute("_userParentNo");
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        dataTable
                .select("CONCAT(a.firstName; ' '; a.surname), a.email, b.name, c.name, str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS'), str(a.updatedOn; 'YYYY-MM-DD HH24:MI:SS'), a.id")
                .from("Users a LEFT JOIN a.userGroupLink b")
                .from("LEFT JOIN a.userTypeLink c")
                .where("a.flag = :flag AND a.id <> :currentUserNo  ")
                .setParameter("currentUserNo", userNo)
                .setParameter("flag", state)
        ;

        if( UserTypes.BANK.equals( parentType ) ){
            dataTable
                    .from("LEFT JOIN a.bankUser d")
                    .where("d.bankNo = :parentNo ")
                    .setParameter("parentNo", parentNo);
        }

        return view.sendJSON( dataTable.showTable() );
    }
}
