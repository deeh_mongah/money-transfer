package com.agency.web.usermanager.controllers;

import com.agency.core.template.AjaxUtils;
import com.agency.core.template.View;
import com.agency.core.template.datatables.DatatablesInterface;
import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MobileUsersController {

    @Autowired private DatatablesInterface dataTable;

    @RequestMapping( value = "/mobile-users")
    public ModelAndView index(HttpServletRequest request){
        View view = new View("user-manager/mobile-users");

        if (AjaxUtils.isAjaxRequest(request)) {
            String action = request.getParameter("action");

            //When fetching table data
            return fetchTableInfo(request, view);
        }

        return view.getView();
    }

    /**
     * Fetch table information
     *
     * @param request Current Request
     * @param view Current View
     * @return JsonView - json structure that DataTable can consume
     */
    private ModelAndView fetchTableInfo(HttpServletRequest request, View view){

        dataTable
                .select("CONCAT(a.firstName; ' '; a.middleName; ' '; a.surname), a.memberNo, a.email, a.phone, str(a.createdOn; 'YYYY-MM-DD HH24:MI:SS')")
                .from("Users a LEFT JOIN a.userTypeLink b")
                .where("b.code = :userType ")
                .setParameter("userType", UserTypes.MOBILE_CUSTOMER )
        ;

        return view.sendJSON( dataTable.showTable() );
    }
}
