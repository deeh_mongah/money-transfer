package com.agency.web.usermanager.controllers;

import com.agency.web.usermanager.PermissionsServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping( value = "/permissions")
public class PermissionsController {

    @Autowired private PermissionsServiceInterface resourceService;

    /**
     * Fetch module permissions
     *
     * @param module
     * @param request
     * @return Object
     */
    @PostMapping( value = "/{module}")
    public Object fetchModulePermissions(@PathVariable String module, HttpServletRequest request){
        return resourceService.fetchModulePermissions( module, request );
    }
}
