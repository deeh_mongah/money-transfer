package com.agency.web.usermanager.controllers;

import com.agency.core.template.View;
import com.agency.web.usermanager.UserProfileServiceInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@Controller
public class UserProfileController {

    private static final Logger logger = LoggerFactory.getLogger( UserProfileController.class );

    @Autowired
    private UserProfileServiceInterface userProfileService;

    @RequestMapping(value = "/profile")
    public ModelAndView index(HttpServletRequest request, RedirectAttributes redirectAttributes) {
        // Get the view
        View view = new View("user-manager/profile");
        Long userId = (Long)request.getSession().getAttribute("_userNo");

        // If we have posted the data
        if (request.getMethod().equals("POST")) {
            return handleRequests(request, redirectAttributes, view);
        }

        // Show the profile page
        return view
                .addAttribute("userDetails", userProfileService.fetchUserDetails(userId))
                .getView();
    }

    private ModelAndView handleRequests(HttpServletRequest request, RedirectAttributes redirectAttributes, View view){
        String action = request.getParameter("action");
        Map<String, Object> map = new HashMap<>();

        try {
            switch (action) {
                //Update profile
                case "profile":
                    map = userProfileService.updateProfile(request);
                    break;
                //Change password
                case "password":
                    map = userProfileService.changePassword(request);
                    break;

                //Update profile image
                default:
                    map = userProfileService.updatePhoto(request);
                    break;
            }
        }
        catch(Exception e){
            e.printStackTrace();
            redirectAttributes.addFlashAttribute("error", "Internal server error: try again later");
            return view.redirect("profile");
        }

        redirectAttributes.addFlashAttribute((String)map.get("status"), map.get("message"));
        return view.redirect("profile");
    }
}
