/*
 * Copyright 2016 Anthony.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.agency.web.usermanager.auth;

import com.agency.web.configs.AuditServiceInterface;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.usermanager.auth.exceptions.InactiveAccountException;
import com.agency.web.usermanager.auth.services.UserAttemptsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

/**
 *
 * @author Anthony
 */
@Component
public class AppAuthenticationProvider extends DaoAuthenticationProvider {

    @Autowired private UserAttemptsService userAttempts;
    @Autowired private AuditServiceInterface auditService;

    @Autowired
    @Qualifier("appUserDetailsService")
    @Override
    public void setUserDetailsService(UserDetailsService userDetailsService) {
        super.setUserDetailsService( userDetailsService );

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        super.setPasswordEncoder( passwordEncoder );
    }

    @Override
    public Authentication authenticate(Authentication authentication)
            throws AuthenticationException {

        StringBuilder error = new StringBuilder();
        String email = authentication.getName();
        AuditTrail log = new AuditTrail();
        try {
            //Login has been successful.
            Authentication auth = super.authenticate(authentication);
            
            //Reset user attempts
            userAttempts.resetFailedAttempts(authentication.getName());
            
            //Generate audit trail
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity("Logged into the System")
                    .setNewValues( "Log in" )
                    .setOldValues("Log out")
                    .setStatus("Success");
            auditService.saveLog(log, email);

            return auth;
        } catch (BadCredentialsException e) {
            //invalid login, update user_attempts
            long remainingRetries = userAttempts.updateFailedAttempts(authentication.getName());

            if (remainingRetries > 0) {
                error.append("Invalid email/password");
                if (remainingRetries == 1) {
                    error.append(": 1 attempt remaining");
                } else {
                    error.append(": ").append(remainingRetries).append(" attempts remaining");
                }
            } else {
                error.append(userAttempts.processLockedAccount(authentication.getName()));
            }

            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity("Attempt to log into the system - " +error.toString() )
                    .setNewValues( "Log out" )
                    .setOldValues("Log out")
                    .setStatus("Failed");
            auditService.saveLog(log, email);

            /*Return a custom message*/
            throw new BadCredentialsException(error.toString());
        } catch (LockedException e) {
            /*User account is locked*/
            error.append(userAttempts.processLockedAccount(authentication.getName()));

            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity("Attempt to log into the system - " +error.toString() )
                    .setNewValues( "Log out" )
                    .setOldValues("Log out")
                    .setStatus("Failed");
            auditService.saveLog(log, email);

            throw new LockedException(error.toString());
        } catch (InactiveAccountException en) {
            error.append("Sorry! Your account is inactive. Go to your email to activate your account ");

            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity("Attempt to log into the system - " +error.toString() )
                    .setNewValues( "Log out" )
                    .setOldValues("Log out")
                    .setStatus("Failed");
            auditService.saveLog(log, email);

            throw new InactiveAccountException(error.toString());
        }

        catch( CredentialsExpiredException e){
            //Pass the email address for later processing
            error.append( authentication.getName() );
            System.err.println("error >>" + authentication.getName() );

            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity("Attempt to log into the system: account has expired" )
                    .setNewValues( "Log out" )
                    .setOldValues("Log out")
                    .setStatus("Failed");
            auditService.saveLog(log, email);
            throw new CredentialsExpiredException( error.toString() );
        }
    }
}
