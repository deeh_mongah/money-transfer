package com.agency.web.usermanager.auth.services;

import com.agency.web.usermanager.entities.UserAttempts;

import java.util.List;

/**
 * @author Mghanga
 * @version 1.0.0
 * @project quicklink
 * @since 25/05/2017 10:52
 */
public interface UserAttemptsServiceInterface {

    public long updateFailedAttempts(String email);

    /**
     * Sets number of login attempts of a user to zero
     *
     * @param email Email(or username) of user accessing the system
     */
    public void resetFailedAttempts(String email);

    /**
     * Fetch the number of retries exhausted by a user
     *
     * @param email Email(or username) of user accessing the system
     * @return Login attempts
     */
    public List<UserAttempts> fetchUserAttempts(String email);

    public String processLockedAccount(String email);
}
