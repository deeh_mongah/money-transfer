package com.agency.web.usermanager.auth.services;

import com.agency.core.template.AppConstants;
import com.agency.web.usermanager.auth.exceptions.InactiveAccountException;
import com.agency.web.usermanager.auth.exceptions.UserAccountStatusException;
import com.agency.web.usermanager.entities.Permissions;
import com.agency.web.usermanager.entities.Users;
import com.agency.web.usermanager.repository.UserGroupRepository;
import com.agency.web.usermanager.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * This class creates an instance of a custom spring security UserDetails
 *
 * @author Anthony Mwawughanga Date April 6, 2016
 */
@Service("appUserDetailsService")
@Transactional
public class AppUserDetailsService implements UserDetailsService {

    @Autowired private UserRepository userRepository;
    @Autowired private UserGroupRepository userGroupRepository;
    @Autowired private UserAttemptsService userAttemptsService;
    
    private static final Logger logger = LoggerFactory.getLogger(AppUserDetailsService.class);

    /**
     * Custom UserDetails
     *
     * @param email
     * @return
     * @throws UsernameNotFoundException
     * @throws UserAccountStatusException
     */
    @Override
    public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException, UserAccountStatusException {
        Optional<Users> oUser = userRepository.findByEmail( email );

        // If the user was not found, throw an exception
        if( !oUser.isPresent() ) throw new UsernameNotFoundException("The email '" + email + "' was not found");
        
        return buildUserForAuthentication( oUser.get() );
    }

    private User buildUserForAuthentication(Users user) {
        List<GrantedAuthority> roles = new ArrayList<>();
        List<String> userRoles = new ArrayList<>();

        //Get permissions
        String field = "";
//        Set<Permissions> permissions = user.getUserGroupLink().getPermissions();
        Set<Permissions> permissions = userGroupRepository.findById( user.getUserGroupNo() ).get().getPermissions();
        for (Permissions row : permissions) {
            //Current role for this given permission
            field = row.getRoleLink().getAppCode();

            //Add the role if it has been added
            if ( !userRoles.contains( field ) ) {
                roles.add(new SimpleGrantedAuthority( field ) );
                userRoles.add( field );
            }

            //Add the permission
            roles.add(new SimpleGrantedAuthority( row.getName()));
        }

        roles.forEach( System.err::println );

        // Check if the user is active
        String groupStatus = user.getUserGroupLink().getFlag();
        boolean isUserActive = user.getEnabled() && (user.getFlag().compareTo( AppConstants.STATUS_ACTIVERECORD) == 0)
                && (groupStatus.compareTo(AppConstants.STATUS_ACTIVERECORD) == 0);

        // Check if email has been verified: by default, user account is inactive
        if (!user.isEmailVerified() || StringUtils.isEmpty( user.getPassword() )) {
            throw new InactiveAccountException("Sorry! Your account is inactive. Go to your email to activate your account");
        }

         /*Check if user account is locked; if yes, check if elapsed time has been exhausted*/
        boolean isAccountNonLocked = userAttemptsService.isAccountNonLocked( user.getEmail() );

        /*Return Spring Security User object*/
        return new User(
                user.getEmail(),
                user.getPassword(),
                isUserActive,
                true,
                !user.isExpired(),
                isAccountNonLocked,
                roles
        );
    }
    
    

}
