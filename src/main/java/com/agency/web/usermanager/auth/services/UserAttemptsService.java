package com.agency.web.usermanager.auth.services;

import com.agency.web.configs.entities.AppSettings;
import com.agency.web.configs.repository.SettingsRepository;
import com.agency.web.usermanager.entities.UserAttempts;
import com.agency.web.usermanager.entities.Users;
import com.agency.web.usermanager.repository.UserAttemptsRepository;
import com.agency.web.usermanager.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * @author Mghanga
 * @version 1.0.0
 * @project quicklink
 * @since 25/05/2017 10:51
 */
@Service
@Transactional
public class UserAttemptsService implements UserAttemptsServiceInterface{

    private static final String INTERVAL_SECONDS = "seconds";
    private static final String INTERVAL_MINUTES = "minutes";
    private static final String INTERVAL_HOURS = "hours";
    private static final String INTERVAL_DAYS = "days";

    @Autowired private UserAttemptsRepository userAttemptsRepository;
    @Autowired private SettingsRepository settingsRepository;
    @Autowired private UserRepository userRepository;

    @Override
    public long updateFailedAttempts(String email) {

        long remainingRetries = 0;
        List<UserAttempts> attempts = fetchUserAttempts(email);
        Optional<Users> oUser = userRepository.findByEmail( email );
        boolean isUserExists = oUser.isPresent();

        /*Fetch allowed number of login retries*/
        Optional<AppSettings> oConfig = settingsRepository.findByCode( "LOGIN_TRIALS");

        //When the config is not found(worst scenario), return a default value
        long systemAllowedTrials = 0;
        if( !oConfig.isPresent() ) systemAllowedTrials =  5;
        else{
            AppSettings config = oConfig.get();
            systemAllowedTrials = Long.valueOf( config.getValue() );
        }

        if (attempts.size() < 1) {
            if ( isUserExists ) {
                /*User has no record; insert a new one*/
                UserAttempts userAttempt = new UserAttempts();
                userAttempt.setAttempts(1L);
                userAttempt.setEmail(email);
                userAttempt.setLastmodified(new Date(System.currentTimeMillis()));
                userAttemptsRepository.save( userAttempt );

                remainingRetries = systemAllowedTrials - 1;
            }
        } else {
            /*User has a failed attempt record*/
            UserAttempts userAttempt = attempts.get(0);
            long user_attempts = userAttempt.getAttempts();
            /*Increment attempts by 1*/
            user_attempts++;

            /*If number of retries exceed maximum retries, lock user account */
            if (user_attempts > systemAllowedTrials ) {
                /*Lock user account - AccountNonLocked value is this case should be "false"*/
                Users currentUser = oUser.get();
                currentUser.setNonlocked( false );
                userRepository.save( currentUser );

            } else {
                remainingRetries = systemAllowedTrials - user_attempts;
                /*Update number of retries*/
                userAttempt.setAttempts( user_attempts );
                userAttempt.setLastmodified(new Date(System.currentTimeMillis()));
                userAttemptsRepository.save( userAttempt );
            }
        }
        return remainingRetries;
    }

    /**
     * Sets number of login attempts of a user to zero
     *
     * @param email Email(or username) of user accessing the system
     */
    @Override
    public void resetFailedAttempts(String email) {
        List<UserAttempts> attempts = fetchUserAttempts(email);
        if (attempts.size() > 0) {
            /*Update number of retries*/
            UserAttempts userAttempt = attempts.get(0);
            userAttempt.setAttempts(0L);
            userAttempt.setLastmodified(null);
            userAttemptsRepository.save( userAttempt );

            /*Always activate account*/
            Optional<Users> oUser = userRepository.findByEmail( email );

            if( oUser.isPresent() ){
                Users currentUser = oUser.get();
                currentUser.setNonlocked( true );
                userRepository.save( currentUser );
            }
        }
    }

    /**
     * Fetch the number of retries exhausted by a user
     *
     * @param email Email(or username) of user accessing the system
     * @return Login attempts
     */
    @Override
    public List<UserAttempts> fetchUserAttempts(String email) {
        return userAttemptsRepository.findByEmail( email );
    }

    @Override
    public String processLockedAccount(String email) {
        List<UserAttempts> attempts = fetchUserAttempts(email);
        StringBuilder error = new StringBuilder();

        if (attempts.size() > 0) {
            /*Get elapsed time*/
            UserAttempts UserAttempts = attempts.get(0);
            Date lastAttempts = UserAttempts.getLastmodified();
            Date timestamp = new Date(System.currentTimeMillis());

            /*Fetch account lock out duration*/
            int lockOutPeriod = 0;
            Optional<AppSettings> oConfig = settingsRepository.findByCode( "LOCK_OUT_PERIOD");

            //When the config is not found(worst scenario), use default value of 30 minutes
            if( !oConfig.isPresent() ) {
                lockOutPeriod = 30;
            }
            else{
                AppSettings config = oConfig.get();
                lockOutPeriod = Integer.valueOf( config.getValue() );
            }

            int elapsedMinutes = timeIntevalInCustomPeriod(lastAttempts, timestamp, INTERVAL_MINUTES);
            int nextRetry = lockOutPeriod - elapsedMinutes;

            if (nextRetry == 0) {
                /*Difference is in seconds*/
                /*Update the last attempt with the config value*/
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(lastAttempts);
                Date updatedLastAttempt = new Date(calendar.getTimeInMillis() + (lockOutPeriod * 60000));
                nextRetry = timeIntevalInCustomPeriod( updatedLastAttempt, timestamp, INTERVAL_SECONDS);
                error.append("Sorry! You account has been locked. Please try again after ")
                        .append( nextRetry )
                        .append(" seconds");
            } else {
                /*Difference is in minutes*/
                error.append("Sorry! You account has been locked. Please try again after ")
                        .append(nextRetry)
                        .append(" minutes");
            }

        } else {
            /*Check if this account exists*/
            Optional<Users> oUser = userRepository.findByEmail( email );
            boolean userExists = oUser.isPresent();
            if ( userExists ) {
                error.append(" Sorry! Your account has been locked!");
            } else {
                error.append("Sorry! You've entered an invalid email or password");
            }
        }
        return error.toString();
    }

    public boolean isAccountNonLocked(String email) {
        System.err.println(" email ->" + email);
        //Fetch login attempts
        List<UserAttempts> attempts = fetchUserAttempts(email);
        System.err.println(" attempts =>" + attempts.size() );

        boolean isAccountNonLocked = false;
        /*Get elapsed time*/
        if (attempts.size() > 0) {
            UserAttempts userAttempts = attempts.get(0);
            if ( userAttempts.getLastmodified() != null && userAttempts.getAttempts() > 0) {

                Optional<Users> oUser = userRepository.findByEmail( email );
                if( oUser.isPresent() ){

                    Users currentUser = oUser.get();

                    if ( !currentUser.getNonlocked() ) {
                        Date lastAttempts = userAttempts.getLastmodified();
                        Date timestamp = new Date(System.currentTimeMillis());

                        /*Fetch account lock out duration*/
                        int lockOutPeriod = 0;
                        Optional<AppSettings> oConfig = settingsRepository.findByCode( "LOCK_OUT_PERIOD");

                        //When the config is not found(worst scenario), use default value of 30 minutes
                        if( !oConfig.isPresent() ) {
                            lockOutPeriod = 30;
                        }
                        else{
                            AppSettings config = oConfig.get();
                            lockOutPeriod = Integer.valueOf( config.getValue() );
                        }

                        int elapsedDays = timeIntevalInCustomPeriod(lastAttempts, timestamp, INTERVAL_DAYS);
                        int elapsedHours = timeIntevalInCustomPeriod(lastAttempts, timestamp, INTERVAL_HOURS);
                        int elapsedMinutes = timeIntevalInCustomPeriod(lastAttempts, timestamp, INTERVAL_MINUTES);
                        //                    int elapseSeconds = QuickpayFunctions.timeIntevalInCustomPeriod(lastAttempts, timestamp, AppConstants.INTERVAL_SECONDS);

                        if (elapsedDays > 0 || elapsedHours > 0 || elapsedMinutes >= lockOutPeriod) {
                            /*Activate account: AccountNonLocked value is this case should be "true"*/
                            currentUser.setNonlocked(  true );
                            /*Update user object*/
                            userRepository.save( currentUser );

                            /*Set attempt status to default*/
                            userAttempts.setAttempts( 0L );
                            userAttempts.setLastmodified(null);
                            userAttemptsRepository.save( userAttempts );

                            isAccountNonLocked = true;
                        }
                    } else {
                        isAccountNonLocked = true;
                    }
                }

            } else {
                isAccountNonLocked = true;
            }
        } else {
            isAccountNonLocked = true;
        }

        return isAccountNonLocked;
    }

    private static int timeIntevalInCustomPeriod(Date startDate, Date endDate, String timeperiod) {
        Instant start = Instant.ofEpochMilli( startDate.getTime() );
        Instant end = Instant.ofEpochMilli( endDate.getTime() );

        long timeInterval = 0;
        switch (timeperiod) {
            case INTERVAL_MINUTES:
                timeInterval = ChronoUnit.MINUTES.between( start, end );
                break;
            case INTERVAL_SECONDS:
                timeInterval = ChronoUnit.SECONDS.between( start, end );
                break;
            case INTERVAL_HOURS:
                timeInterval = ChronoUnit.HOURS.between( start, end );
                break;
            case INTERVAL_DAYS:
                timeInterval = ChronoUnit.DAYS.between( start, end );
                break;
        }

        return Math.toIntExact( timeInterval );
    }

}
