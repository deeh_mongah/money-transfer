package com.agency.web.usermanager.services;

import com.agency.web.usermanager.UserTypeServiceInterface;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.usermanager.repository.UserTypesRepository;
import org.hibernate.usertype.UserType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author      Mghanga
 * @version     1.0.0
 * @project     Quickgas
 * @since       05/05/2017 10:44
 */
@Service
@Transactional
public class UserTypeService implements UserTypeServiceInterface{

    @Autowired
    private UserTypesRepository userTypesRepository;

    /**
     * Fetch a list of user types
     *
     * @return List<UserType>
     */
    @Override
    public List<UserTypes> fetchRecords(HttpServletRequest request){
        List<UserTypes> userTypes = new ArrayList<>();
        String parentType = (String)request.getSession().getAttribute("_userParentType");

        //When serving superadmin
        if(ObjectUtils.isEmpty( parentType )){
            List<String> codes = Arrays.asList(UserTypes.SYSTEM_ADMIN, UserTypes.BANK_ADMIN, UserTypes.AGENT);
            userTypes = userTypesRepository.findByCodeIn( codes );
        }
//        //When server non-superadmins
//        else {
//            List<String> codes = new ArrayList<>();
//            //When serving bank admin
//             if (parentType.equals(UserTypes.BANK_PARENT)) {
//                codes.add( UserTypes.BANK_ADMIN );
//            }
//
//            userTypes = userTypesRepository.findByCodeIn( codes );
//        }

        //Return result
        return userTypes;
    }
}
