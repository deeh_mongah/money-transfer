package com.agency.web.usermanager.services;

import com.agency.web.configs.entities.MakerChecker;
import com.agency.web.configs.repository.MakerCheckerRepository;
import com.agency.web.usermanager.PermissionsServiceInterface;
import com.agency.web.usermanager.entities.AppRoles;
import com.agency.web.usermanager.entities.Permissions;
import com.agency.web.usermanager.repository.AppRolesRepository;
import com.agency.web.usermanager.vm.ModulePermissionsVm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;
import java.util.Set;

@Service
public class PermissionsService implements PermissionsServiceInterface {

    @Autowired private AppRolesRepository appRolesRepository;
    @Autowired private MakerCheckerRepository makerCheckerRepository;

    /**
     * Fetch module permissions
     *
     * @param module
     * @param request
     * @return ModulePermissionsVm
     */
    @Override
    public ModulePermissionsVm fetchModulePermissions(String module, HttpServletRequest request){
        ModulePermissionsVm data = new ModulePermissionsVm();
        //Long userNo = (Long)request.getSession().getAttribute("_userNo");
        Optional<AppRoles> oAppRole = appRolesRepository.findByName( module );

        if( oAppRole.isPresent() ){
            AppRoles appRole = oAppRole.get();
            Set<Permissions> permissionsSet = appRole.getPermissions();
            //List<ModulePermissionsVm.ModulePermission> modulePermissions = new ArrayList<>();

            //If maker-checker is enabled
            Optional<MakerChecker> oMakerChecker = makerCheckerRepository.findByCode( module );
            if( oMakerChecker.isPresent() ){
                MakerChecker makerChecker = oMakerChecker.get();
                data.setMakerChecker( makerChecker.isEnabled() );
            }

            for( Permissions permission : permissionsSet ){

                String action = permission.getAppCode();

                //Check within the session object if user has the current permission( cause we;re in a loop)
                boolean hasRole = request.isUserInRole( permission.getName() );
                ModulePermissionsVm.ModulePermission node = new ModulePermissionsVm.ModulePermission(
                        action, hasRole
                );

                //How does this even work?
                data.getRoles().add( node );

            }

        }
        System.err.println("testModule"+data);

        return data;
    }
}
