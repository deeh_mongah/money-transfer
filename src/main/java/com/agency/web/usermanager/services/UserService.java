package com.agency.web.usermanager.services;

import com.agency.core.mail.MailerService;
import com.agency.core.template.AppConstants;
import com.agency.core.template.PhoneNumberFormatter;
import com.agency.core.template.forms.AuditData;
import com.agency.web.configs.entities.AppSettings;
import com.agency.web.configs.entities.AuditTrail;
import com.agency.web.configs.entities.ReasonCodes;
import com.agency.web.configs.repository.AuditTrailRepository;
import com.agency.web.configs.repository.SettingsRepository;
import com.agency.web.configs.services.AuditService;
import com.agency.web.usermanager.UserServiceInterface;
import com.agency.web.usermanager.auth.SecurityUtils;
import com.agency.web.usermanager.entities.BankUsers;
import com.agency.web.usermanager.entities.UserAttempts;
import com.agency.web.usermanager.entities.UserTypes;
import com.agency.web.usermanager.entities.Users;
import com.agency.web.usermanager.forms.UsersForm;
import com.agency.web.usermanager.repository.BankUsersRepository;
import com.agency.web.usermanager.repository.UserAttemptsRepository;
import com.agency.web.usermanager.repository.UserRepository;
import com.agency.web.usermanager.repository.UserTypesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Transactional
@Service
public class UserService implements UserServiceInterface{

    @Autowired private UserRepository userRepository;
    @Autowired private BankUsersRepository bankUsersRepository;
    @Autowired private AuditService auditLogService;
    @Autowired private AuditTrailRepository auditTrailRepository;
    @Autowired private UserTypesRepository userTypesRepository;
    @Autowired private UserAttemptsRepository userAttemptsRepository;
    @Autowired private SettingsRepository settingsRepository;

    @Autowired private UsersForm usersForm;
    @Autowired private MailerService mailService;

    @Value("${app.endpoint}")
    private String baseURL;

    /**
     * Validates email and sends a reset password link to the email in question
     *
     * @param email
     * @return
     */
    @Override
    public Map<String, Object> generateResetToken(String email){
        Map<String, Object> map = new HashMap<>();
        Optional<Users> oUser = userRepository.findByEmail( email );
        if( oUser.isPresent() ){

            //Generate a random unique token
            String token = UUID.randomUUID().toString();

            //Update user record
            Users user = oUser.get();
            user
                    .setEmailToken( token ).setResetReqDate( new Date( System.currentTimeMillis() ));
            userRepository.save( user );
            auditLogService.logActivity("Password reset request processed successfully.", email, "Success");

            map.put("status", "ok");
            map.put("names", String.format("%s", user.getSurname()) );
            map.put("token", token);
            map.put("email", email);

            AuditTrail log = new AuditTrail();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity("Generated password reset token")
                    .setStatus("Success").setUserNo( user.getId() );
            auditTrailRepository.save( log );
        }
        else {
            map.put( "status","not-found");
        }

        return map;
    }

    /**
     * Allows a user to reset their password
     *
     * @param names Client full names
     * @param email Client email address
     * @param token Password reset token
     * @return Boolean Results of processing
     */
    @Override
    public boolean sendPasswordToken(String names, String email, String token) {
        try {
            StringBuilder emailLink = new StringBuilder();
            emailLink.append( baseURL ).append("/password-reset/").append( token );

            mailService.sendMail( mailService.sendGridConfig()
                    .setTemplateId("c356b2ea-1e6a-4bd6-a68f-1f5f8d12d2fb")
                    .setTo(email, names )
                    .setSubject("Password Reset")
                    .addAttribute("_lastname", names )
                    .addAttribute( "_baseUrl", emailLink.toString() )
            );

        } catch (Exception e) {
            //logger.error("Error while sending mail for password reset :", e);
            return false;
        }
        return true;
    }
    
    /**
     * Validates the secure token send to the user
     *
     * @param token
     * @return
     */
    @Override
    public String validateResetToken(final String token) {
        String result = "";
        Optional<Users> oUser = userRepository.findByEmailToken( token );

        if ( oUser.isPresent() ) {
            Users user = oUser.get();
            /*Check token life span*/
//                Date current_stamp = new Date();
//            long timespan = (current_stamp.getTime() - user.getResetReqdate().getTime()) / (60 * 60 * 1000) % 24;
//            System.err.println("timespan:"+timespan);
//            result = (timespan > 0) ? "expired": user.getEmail();
            result = user.getEmail();
        } else {
            result = "invalid";
        }
        return result;

    }

    /**
     * Set up new account password | handle password change request
     *
     * @param token Random secure code send to the user email
     * @param password New password for this account
     * @return String
     */
    @Override
    public Map<String, Object> setupNewPassword(String token, String password) {
        Map<String, Object> map = new HashMap<>();
        String status = "error";
        Optional<Users> oUser = userRepository.findByEmailToken( token );
        if ( oUser.isPresent() ) {
            Users user = oUser.get();

            //Update password expiry date
            AppSettings appSettings = settingsRepository.findByCode( "PASSWORD_EXPIRY_WINDOW").get();
            String expiryWindow = appSettings.getValue();

            user
                    .setExpiryDate( expiryWindow )
                    .setEnabled( true )
                    .setEmailVerified( true )
                    .setPassword( SecurityUtils.hashPassword( password ) )
                    .setResetReqDate( null )
                    .setEmailToken( null );
            userRepository.save( user );

            //Audit this action
            AuditTrail trail = new AuditTrail()
                    .setActivity("Password reset processed successfully.")
                    .setUserNo( user.getId() )
                    .setStatus("Success");
            auditTrailRepository.save( trail );


            //Package email properties
            map.put("sendMail", true);
            map.put("email", user.getEmail());
            map.put("firstName", user.getFirstName() );
            map.put("names", user.getFullNames() );

            status = "ok";
        }
        else  status = "invalid";

        map.put("status", status);
        return map;
    }

    /**
     * Change password when credentials expire
     *
     * @param request
     * @return String
     */
    @Override
    public String changePasswordOnExpiry( HttpServletRequest request){
        String email = request.getParameter("email");
        String oldPassword = request.getParameter("oldPassword");
        String newPassword = request.getParameter("newPassword");

        Optional<Users> oUser = userRepository.findByEmail( email );
        if( !oUser.isPresent() ){
            return "invalid";
        }

        Users entity = oUser.get();
        if( !SecurityUtils.checkPasswords( oldPassword, entity.getPassword() ) ){
            return "mismatch";
        }

        //Update password expiry date
        AppSettings appSettings = settingsRepository.findByCode( "PASSWORD_EXPIRY_WINDOW").get();
        String expiryWindow = appSettings.getValue();

        //Update the entity
        entity
                .setExpiryDate( expiryWindow )
                .setEnabled( true )
                .setPassword( SecurityUtils.hashPassword( newPassword ) )
                .setUpdatedOn( new Date(System.currentTimeMillis() ))
                ;

        //Save record
        userRepository.save( entity );

        // All is well
        return "ok";
    }


    /**
     * Persist a new record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> saveRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userId = (Long)request.getSession().getAttribute("_userNo");
        String parentType = (String) request.getSession().getAttribute("_userParentType");
        Long parentNo = (Long) request.getSession().getAttribute("_userParentNo");

        Users entity = this.usersForm.handleRequests( request );
        List<Users> existingUsers = userRepository.findAllByEmail( entity.getEmail() );
        if ( existingUsers.size() > 0 ) {
            map.put("status", "01");
            map.put("message",  "Email is already being used by another user");
            return map;
        }

        String phoneCode = request.getParameter("phoneCode");
        String phone = request.getParameter("phone");



        boolean valid = validatePhoneNumber(phone,phoneCode);


        if (!valid){

            map.put("status", "01");
            map.put("message", "Invalid Phone Number");
            return map;
        }

        String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phone);



        String userTypeCode = "";
        String bankNo = request.getParameter("bankNo");
        String branchNo = request.getParameter("branchNo");

        if (null == parentType) {
            UserTypes userTypeNode = userTypesRepository.findById(entity.getUserTypeNo()).get();
            userTypeCode = userTypeNode.getCode();
        }
        else if( UserTypes.BANK.equals( parentType )){
            UserTypes node = userTypesRepository.findByCode( UserTypes.BANK_ADMIN ).get();
            userTypeCode = node.getCode();

            entity.setUserTypeNo( node.getId() );
        }

        //Update password expiry date
        AppSettings appSettings = settingsRepository.findByCode( "PASSWORD_EXPIRY_WINDOW").get();
        String expiryWindow = appSettings.getValue();

        entity
                .setPhone(formattedNumber)
                .setPhoneCode(phoneCode)
                .setExpiryDate( expiryWindow )
                .createdOn( userId )
                .setFlag( AppConstants.STATUS_NEWRECORD );

        /*Save record*/
        userRepository.save( entity );

        //When a super-admin is creating this record
        if (null == parentType) {

            if( UserTypes.BANK_ADMIN.equals( userTypeCode ) ){
                BankUsers child = new BankUsers();
                child.setId( entity.getId() ).setBankNo( Long.valueOf( bankNo ) );

                //Save child record
                bankUsersRepository.save( child );
            }else if (UserTypes.AGENT.equals(userTypeCode)){

            }
        }

        //When a bank admin is creating their own
        else if( UserTypes.BANK.equals( parentType )){
            BankUsers child = new BankUsers();
            child.setId( entity.getId() ).setBankNo( parentNo );

            //Save child record
            bankUsersRepository.save( child );
        }

        AuditTrail log = new AuditTrail();
        log
                .setLogType( AuditTrail.USER_GENERATED )
                .setActivity("Created a new user : " + entity.getFullNames() )
                .setNewValues( entity.getFullNames() )
                .setOldValues("N/A")
                .setStatus("Success")
                .setUserNo( userId );

       auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }


    /**
     * Edit a record
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> editRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        AuditTrail log = new AuditTrail();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        boolean isModified = this.usersForm.handleEditRequest(request);
        Users record = this.usersForm.getEntity();

        //If record has changes
        if( isModified ){

            String phoneCode = request.getParameter("phoneCode");
            String phone = request.getParameter("phone");



            boolean valid = validatePhoneNumber(phone,phoneCode);


            if (!valid){

                map.put("status", "01");
                map.put("message", "Invalid Phone Number");
                return map;
            }

            String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phone);

            record
                    .setPhoneCode(phoneCode)
                    .setPhone(formattedNumber)
                    .setFlag( AppConstants.STATUS_EDITEDRECORD )
                    .updatedOn( userNo );
            //Persist record
            userRepository.save( record );

            //Generate log
            AuditData auditData = usersForm.auditData();
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Edited user - %s : %s", record.getFullNames(), auditData.getDescription() ) )
                    .setNewValues( auditData.getNewValue() )
                    .setOldValues( auditData.getOldValue() )
                    .setStatus("Success")
                    .setUserNo( userNo );

            map.put("status", "00");
            map.put("message", "Request processed successfully");
        }
        //No changes were made
        else{
            //Generate log
            log
                    .setLogType( AuditTrail.USER_GENERATED )
                    .setActivity(String.format("Attempt to update user - %s : no changes made", record.getFullNames())  )
                    .setNewValues( "N/A" )
                    .setOldValues("N/A" )
                    .setStatus("Failed")
                    .setUserNo( userNo );

            map.put("status", "01");
            map.put("message", "No changes were made to this record");
        }

       auditTrailRepository.save( log );
        return map;
    }

    /**
     * Approve edit changes
     *
     * @param request
     * @return  Map<String, Object>
     */
    @Override
    public Map<String, Object> approveEditChanges(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Long index = Long.valueOf( request.getParameter("index") );
        String action = request.getParameter("action");

        Users record = this.userRepository.findById( index ).get();
        boolean proceed = usersForm.applyMakerChecker(record, action);

        map.put("message", usersForm.getResponse());
        if ( proceed ) {
            record = usersForm.getEntity();
            this.userRepository.save( record );
            map.put("status", "00");
        } else {
            map.put("status", "01");
        }

        //Insert logs
        AuditTrail log = usersForm.getLog().setUserNo(userNo);
        String activity = String.format( "%s Reference: %s", log.getActivity(), record.getFullNames() );
        log.setActivity( activity );
       auditTrailRepository.save(log);
        return map;
    }

    /**
     * Fetch a record information
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecord(HttpServletRequest request){
        String index = request.getParameter("index");
        return this.usersForm.transformEntity(index);
    }

    @Override
    public Boolean validatePhoneNumber(String phoneNumber, String phoneCode) {




        String formattedNumber = PhoneNumberFormatter.formatE164(phoneCode, phoneNumber);

        boolean validPhone = PhoneNumberFormatter.isValidNumber(formattedNumber);

        return validPhone;
    }

    /**
     * Fetch edit changes
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchRecordChanges(HttpServletRequest request){
        String index = request.getParameter("index");
        return this.usersForm.fetchChanges( index );
    }

    /**
     * Update record status
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> flagRecords( HttpServletRequest request ){
        Map<String, Object> map = new HashMap<>();
        StringBuilder activity = new StringBuilder();

        String action = request.getParameter("action");
        Long index = Long.valueOf( request.getParameter("index") );
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Users record = this.userRepository.findById( index ).get();

        boolean success = usersForm.applyMakerChecker(record, action);
        map.put("message", usersForm.getResponse());

        if ( success ) {
            record = usersForm.getEntity();

            if( action.equals( AppConstants.ACTION_APRROVE_NEW )){
                activity.append("Record approved successfully");
                    /*Set up the secure token*/
                String token = UUID.randomUUID().toString();
                record.setEmailToken( token );

                //Package email properties
                map.put("sendMail", true);
                map.put("token", token );
                map.put("email", record.getEmail());
                map.put("firstName", record.getFirstName() );
                map.put("names", record.getFullNames() );
            }

            userRepository.save( record );
            map.put("status", "00");
        } else {
            map.put("status", "01");
        }

        //Insert logs
        AuditTrail log = usersForm.getLog().setUserNo( userNo );
        String logActivity = String.format( "%s Reference: %s", log.getActivity(), record.getFullNames());
        log.setActivity( logActivity );
        auditTrailRepository.save(log);
        return map;
    }

    /**
     * Deactivate a record
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> deactivateRecord(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");
        Users record = this.usersForm.deactivateRequest( request );
        record
                .setFlag( AppConstants.STATUS_DEACTIVATED )
                .updatedOn( userNo );
        userRepository.save( record );

        AuditTrail log = new AuditTrail()
                .setActivity(String.format("Deactivated a user successfully. Reference %s", record.getFullNames() ) )
                .setStatus("Success")
                .setOldValues("Active").setNewValues("Deactivated")
                .setUserNo( userNo );
       auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");

        return map;
    }

    /**
     * Fetch deactivation details
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> fetchDeactivationInfo(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long index = Long.valueOf( request.getParameter("index") );

        Users record = this.userRepository.findById( index ).get();
        map.put("index", record.getId() );
        map.put("editor", record.getUpdatedByLink().getFullNames() );

        if( null != record.getReasonCodeLink() ){
            ReasonCodes reasonCode = record.getReasonCodeLink();
            map.put("reason", reasonCode.getName() );
            map.put("description", record.getReasonDescription() );
        }
        else{
            map.put("reason", "" );
            map.put("description", "");
        }
        map.put("status", "00");
        return map;
    }


    /**
     * Reset user attempts
     *
     * @param request
     * @return Map<String, Object>
     */
    @Override
    public Map<String, Object> unlockUser(HttpServletRequest request){
        Map<String, Object> map = new HashMap<>();
        Long userNo = (Long) request.getSession().getAttribute("_userNo");

        Long index = Long.valueOf( request.getParameter("index") );
        Users user = this.userRepository.findById( index ).get();

        AuditTrail log = new AuditTrail().setUserNo( userNo );
        List<UserAttempts> attempts = userAttemptsRepository.findByEmail( user.getEmail() );
        if (attempts.size() > 0) {
            /*Update number of retries*/
            UserAttempts userAttempt = attempts.get(0);
            userAttempt.setAttempts( 0L );
            userAttempt.setLastmodified( null );
            userAttemptsRepository.save( userAttempt );

            user.setNonlocked( true );
            userRepository.save( user );

            log
                    .setActivity(String.format("Unlocked user account successfully. Reference %s", user.getFullNames() ) )
                    .setStatus( "Success" )
                    .setOldValues( "Locked" ).setNewValues( "Active" );
        }
        else{
            log
                    .setActivity(String.format("Attempt to unlocked user account failed. Reference %s", user.getFullNames() ) )
                    .setStatus( "Failed" )
                    .setOldValues( "N/A" ).setNewValues( "N/A" );
        }

        auditTrailRepository.save( log );

        map.put("status", "00");
        map.put("message", "Request processed successfully");
        return map;
    }


}
