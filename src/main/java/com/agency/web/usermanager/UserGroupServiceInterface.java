package com.agency.web.usermanager;

import com.agency.core.template.forms.BaseServiceInterface;
import com.agency.web.usermanager.entities.Permissions;
import com.agency.web.usermanager.entities.UserGroups;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface UserGroupServiceInterface extends BaseServiceInterface{

    /**
     * Fetch a list of permissions
     *
     * @param request
     * @return List<Object>
     */
    public List<Object> fetchPermissions(HttpServletRequest request);

    /**
     * Fetch  a list of records given their status
     *
     * @return List<UserGroups>
     */
    public List<UserGroups> fetchAllRecords(HttpServletRequest request);
}
