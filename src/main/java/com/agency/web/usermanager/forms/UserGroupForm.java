package com.agency.web.usermanager.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.usermanager.entities.UserGroups;
import org.springframework.stereotype.Component;

@Component
public class UserGroupForm extends Form<UserGroups> {

    //This class constructor initialiazes the mapping managed by the Form class
    public UserGroupForm() {
        setMapping( UserGroups.class);
        setRole("ROLE_USERGROUP", "User Groups", "superadmin");
    }
}
