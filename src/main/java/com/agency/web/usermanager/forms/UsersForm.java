package com.agency.web.usermanager.forms;

import com.agency.core.template.forms.Form;
import com.agency.web.usermanager.entities.Users;
import org.springframework.stereotype.Component;

@Component
public class UsersForm extends Form<Users> {

    public UsersForm(){
        setMapping( Users.class );
        setRole("ROLE_USERS", "Users", "superadmin");
    }
}
