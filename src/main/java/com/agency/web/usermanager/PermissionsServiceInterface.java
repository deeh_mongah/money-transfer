package com.agency.web.usermanager;

import com.agency.web.usermanager.vm.ModulePermissionsVm;

import javax.servlet.http.HttpServletRequest;

public interface PermissionsServiceInterface {

    /**
     * Fetch module permissions
     *
     * @param module
     * @param request
     * @return ModulePermissionsVm
     */
    public ModulePermissionsVm fetchModulePermissions(String module, HttpServletRequest request);
}
