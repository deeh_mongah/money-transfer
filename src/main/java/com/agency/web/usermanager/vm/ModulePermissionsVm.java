package com.agency.web.usermanager.vm;

import java.util.ArrayList;
import java.util.List;

public class ModulePermissionsVm {

    private boolean makerChecker = false;
    private List<ModulePermission> roles = new ArrayList<>();

    public boolean isMakerChecker() { return makerChecker; }
    public void setMakerChecker(boolean makerChecker) {
        this.makerChecker = makerChecker;
    }

    public List<ModulePermission> getRoles() {  return roles;  }
    public void setRoles(List<ModulePermission> roles) {
        this.roles = roles;
    }

    public static class ModulePermission{
        private String action;
        private boolean hasRole;

        public ModulePermission(){}

        public ModulePermission(String action, boolean hasRole ){
            this.action = action;
            this.hasRole = hasRole;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public boolean isHasRole() {
            return hasRole;
        }

        public void setHasRole(boolean hasRole) {
            this.hasRole = hasRole;
        }
    }
}
