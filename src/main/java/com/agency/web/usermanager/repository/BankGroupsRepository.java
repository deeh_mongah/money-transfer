package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.BankGroups;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankGroupsRepository extends CrudRepository<BankGroups, Long> {
}
