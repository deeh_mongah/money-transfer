package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.UserAttempts;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserAttemptsRepository extends CrudRepository<UserAttempts, Long> {

    /**
     * Fetch a list of attempts of a specific user
     * @param email User principal object
     * @return List<UserAttempts>
     */
    public List<UserAttempts> findByEmail(String email);
}
