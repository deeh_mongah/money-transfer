package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.Permissions;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface PermissionsRepository extends CrudRepository<Permissions, Long> {

    /**
     * Fetch a set of permissions given their PKS
     *
     * @param pks
     * @return Set<Permissions>
     */
    @Query("SELECT a FROM Permissions a WHERE a.id IN( :pks ) ")
    public Set<Permissions> fetchByIds(@Param("pks") List<Long> pks);
}
