package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.UserTypes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserTypesRepository extends CrudRepository<UserTypes, Long> {

    /**
     * Fetch records given  a list of codes
     *
     * @param codes
     * @return  List<UserTypes>
     */

    public List<UserTypes> findByCodeIn(List codes);

    /**
     * Fetch record by code
     *
     * @param code
     * @return Optional<UserTypes>
     */
    public Optional<UserTypes> findByCode(String code );

}
