package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.BankUsers;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BankUsersRepository extends CrudRepository<BankUsers, Long> {
}
