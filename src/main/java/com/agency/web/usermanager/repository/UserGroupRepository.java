package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.UserGroups;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserGroupRepository extends CrudRepository<UserGroups, Long> {

    /**
     * Fetch a collection of records with the supplied name
     *
     * @param name
     * @return List<UserGroups>
     */
    public List<UserGroups> findByName(String name);

    /**
     * Fetch a list of records with the supplied flag
     *
     * @param flag
     * @return List<UserGroups>
     */
    public List<UserGroups> findAllByFlag( String flag );

    /**
     * Fetch bank records
     *
     * @param flag
     * @param bankNo
     * @return List<UserGroups>
     */
    public List<UserGroups> findAllByFlagAndBankGroup_BankNo( String flag, Long bankNo );
}
