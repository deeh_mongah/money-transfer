package com.agency.web.usermanager.repository;

import com.agency.web.usermanager.entities.AppRoles;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppRolesRepository extends CrudRepository<AppRoles, Long> {

    /**
     * Fetch record by name
     *
     * @param name
     * @return Optional<AppRoles>
     */
    public Optional<AppRoles> findByName(String name);
}
